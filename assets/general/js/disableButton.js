$(function () {
    //disable button while ajax
    $.nette.ext("disableButton", {
        start: function (xhr, settings) {
            if (!settings.nette || !settings.nette.el) {
                return;
            }

            settings.nette.el.attr('disabled', true);
            settings.nette.el.addClass('disabled');
        },
        complete: function (xhr, status, settings) {
            if (settings.nette && settings.nette.el) {
                settings.nette.el.removeAttr('disabled');
                settings.nette.el.removeClass('disabled');
            }
        },
        error: function (xhr, status, settings) {
            if (settings.nette && settings.nette.el) {
                settings.nette.el.removeAttr('disabled');
                settings.nette.el.removeClass('disabled');
            }
        }
    });
});