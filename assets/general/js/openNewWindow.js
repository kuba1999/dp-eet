$(function () {
    $("a.openNewWindow").click(function (e) {
        $width = $(window).width();

        window.open(
            $(this).attr("href"),
            '_blank',
            'location=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=' + $width
        );
        e.preventDefault();
    });
});