$(function () {
    $(document).on('keyup', 'input[data-autosubmit]', function (e) {
        code = e.which || e.keyCode || 0;

        if ((code != 13) && ((code >= 9 && code <= 40) || (code >= 112 && code <= 123))) {
            return;
        }

        clearTimeout(window.datagridTimer);

        $this= $(this);
        window.datagridTimer = setTimeout(function() {
            $this.closest('form').submit();
        }, 400);
    })
    .on('change', 'input[data-autosubmit]', function (e) {
        clearTimeout(window.datagridTimer);

        $this= $(this);
        window.datagridTimer = setTimeout(function() {
            $this.closest('form').submit();
        }, 400);
    })
    .on('change', 'input[data-autosubmit-change]', function (e) {
        $(this).closest('form').submit();
    })
    .on('change', 'select[data-autosubmit-change]', function (e) {
        $(this).closest('form').submit();
    });

    $.nette.ext({
        before: function (xhr, settings) {
            if (!settings.nette) {
                return;
            }

            var question = settings.nette.el.data('confirm');
            if (question) {
                return confirm(question);
            }
        }
    });
});