moment.locale('cs');

$(function () {
    //nette ajax
    $.nette.init();

    //venobox
    $('.venobox').venobox();

    //froms required - label star
    $("label").each(function () {
        var inputID = "#" + $(this).attr("for");

        if($(inputID).attr("required")) {
            $(this).addClass("required");
        }
    });
});