moment.locale('cs');

function addStarToRequiredControls() {
    $("label").each(function () {
        var inputID = "#" + $(this).attr("for");

        if($(inputID).attr("required")) {
            $(this).addClass("required");
        }
    });
}

$(function () {
    //disable JS form validation
    Nette = {
        initForm: function () {}
    };

    $.nette.ext('init', false);
    $.nette.ext('init', {
        load: function (rh) {
            rh = function (e) {
                $.nette.ajax({
                    off: $(this).is('.ajax-history') ? [] : ['history']
                }, this, e);
            };

            $(this.linkSelector).off('click.nette').on('click.nette', rh);
            $(this.formSelector).off('submit.nette').on('submit.nette', rh)
                .off('click.nette', ':image').on('click.nette', ':image', rh)
                .off('click.nette', ':submit').on('click.nette', ':submit', rh);
            $(this.buttonSelector).closest('form')
                .off('click.nette', this.buttonSelector).on('click.nette', this.buttonSelector, rh);
        }
    }, {
        linkSelector: 'a.ajax',
        formSelector: 'form.ajax',
        buttonSelector: 'input.ajax[type="submit"], button.ajax[type="submit"], input.ajax[type="image"]'
    });


    $.nette.init();

    //venobox
    $('.venobox').venobox();

    //froms required - label star
    addStarToRequiredControls();

    $.nette.ext('controls-required-star', {
        complete: function ()
        {
            addStarToRequiredControls();
        }
    });

    //tooltip
    $('[data-toggle="tooltip"]').tooltip();

    //cookies default
    $.cookie.defaults = { path: '/', expires: 365 };

    //remember sidebar state
    $("body").on('expanded.pushMenu', function (e) {
        $.cookie('sidebarExpanded', 1);
    }).on('collapsed.pushMenu', function (e) {
        $.cookie('sidebarExpanded', 0);
    });
});