(function($){

    $.barcodeScannerListener = function(context, options) {
        //default settings
        var defaults = {
            debug: false,
        };

        var $this = this;
        $this.options = $.extend( {}, defaults, options );
        $this.element = $(context);
        $this.code = '';
        $this.timeout = 0;

        //initialisation
        $this.init = function(){
            //listen if key is pressed
            $this.element.on('keypress', function(e){
                $target = $(e.target);
                if($target.is(":input") && $this.element.not(":input")) {
                    return;
                }

                $this.listen(e);
            });
        };

        //reinitialisation
        $this.reinit = function(){
            $this.code = '';
            $this.timeout = 0;
        };

        /**
         * Listen typed chars
         * @param e
         */
        $this.listen = function(e){
            var $key = e.which;
            var $char = $this.validateKey($key);

            //invalid char
            if($char === false) {
                $this.reinit();
            }
            else if($char === "\r") { //end of code - validate code
                $this.validate();
            }
            else {
                if($this.code === "") {
                    setTimeout($this.reinit(), 1000);
                }

                $this.appendChar($char);
            }
        };

        /**
         * Validate read char
         * @param key
         * @returns {*}
         */
        $this.validateKey = function(key){
            if((key >= "0".charCodeAt() && key <= "9".charCodeAt()) || key === "\r".charCodeAt()) {
                return String.fromCharCode(key);
            }

            return false;
        };

        /**
         * Validate code
         */
        $this.validate = function(){
            var $tmp = $this.code;

            var $interval = (new Date()).getTime() - $this.timeout;
            $this.reinit();

            if($interval <= 1000 && $this.validateCode($tmp)) {
                debug("Valid: " + $tmp);
                $this.element.trigger('barcode.valid', [$tmp]);
            } else {
                debug("Invalid: " + $tmp);
                $this.element.trigger('barcode.invalid', [$tmp]);
            }
        };

        /**
         * Append read char
         * @param $char
         */
        $this.appendChar = function($char){
            if($this.timeout === 0) {
                $this.timeout = (new Date()).getTime();
            }

            $this.code += $char;
        };

        /**
         * Validate code
         * @param $code
         * @returns {boolean}
         */
        $this.validateCode = function($code) {
            var $codeArray = $code.split("").reverse();
            var $oddCharsSum = 0;
            var $evenCharsSum = 0;

            $.each($codeArray, function (index, value) {
                if(index === 0) { //ignore first char, because it is for checking
                    return;
                }

                if((index+1) % 2 === 0) {
                    $evenCharsSum += parseInt(value); //even char
                } else {
                    $oddCharsSum += parseInt(value); //odd char
                }
            });

            var $sum = $evenCharsSum * 3 + $oddCharsSum; //multiple even sum by 3
            var $roundedSum = Math.ceil($sum / 10) * 10;

            return $roundedSum - $sum === parseInt($codeArray[0]);
        };

        function debug( value ) {
            if ( window.console && window.console.log && $this.options.debug) {
                window.console.log( value );
            }
        };


        $this.init();
    };


    $.fn.barcodeScannerListener = function(options) {
        return this.each(function(){
            new $.barcodeScannerListener(this, options || {});

        });
    }

})(jQuery);
