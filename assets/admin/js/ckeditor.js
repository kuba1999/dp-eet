/**
 * Created by jakub on 4.6.17.
 */
//no &nbsp;
CKEDITOR.config.fillEmptyBlocks = false;
CKEDITOR.config.tabSpaces = 0;


CKEDITOR.on('instanceReady', function (e) {
    e.editor.on('change', function() { e.editor.updateElement() });
});