function autofocus() {
    $("[autofocus]").on("focus", function () {
        if (this.setSelectionRange) {
            var len = this.value.length * 2;
            this.setSelectionRange(len, len);
        } else {
            this.value = this.value;
        }
        this.scrollTop = 999999;
    });
    $("[autofocus]").first().focus();
}


$(document).ready(function (e) {
    autofocus();
});

// Every time a modal is shown, if it has an autofocus element, focus on it.
$(document).on('shown.bs.modal', function (e) {
    autofocus();
});