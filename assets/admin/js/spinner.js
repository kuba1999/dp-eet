$(function () {
    $.nette.ext('spinner', {
        start: function(jqXHR, settings) {

            if(!settings.nette || !settings.nette.el.is(".spinner")) {
                return;
            }

            $element = settings.nette.el;

            if($element.find(".overlay").length > 0) {
                return;
            }

            if($element.data("spinner-target")) {
                $target = $($element.data("spinner-target"));
                if($target.not(".box")) {
                    $target.addClass("overlay-wrapper");
                }
            } else {
                $target = $element.closest(".box");
            }

            settings.spinnerTarget = $target;

            //show spinner
            $target.append($('<div class="overlay">' +
                '                             <i class="fas fa-spinner fa-spin fa-2x"></i>' +
                '                          </div>'));
        },
        success: function(payload, status, jqXHR, settings){
            if(!settings.spinnerTarget) {
                return;
            }

            $target = settings.spinnerTarget;

            //remove spinner
            $target.find(".overlay").fadeOut();
        }
    });
});