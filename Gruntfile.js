// AdminLTE Gruntfile
module.exports = function (grunt) {

    'use strict';

    grunt.initConfig({
        watch: {
            files: ["assets/admin/css/*.less", "assets/admin/js/*.js", "assets/portal/css/*.less"],
            tasks: ["less", "concat"]
        },
        less: {
            admin: {
                options: {
                    // Whether to compress or not
                    compress: false
                },
                files: {
                    "build/adminLTE.css": "assets/admin/css/admin.less",
                    "build/bootstrapAdmin.css": "bower_components/bootstrap/less/bootstrap.less",
                }
            },
            portal: {
                options: {
                    // Whether to compress or not
                    compress: false
                },
                files: {
                    "build/portal.css": "assets/portal/css/portal.less",
                }
            }
        },
        concat: {
            adminCSS: {
                src: [
                    'bower_components/select2/dist/css/select2.css',
                    'build/bootstrapAdmin.css',
                    'build/adminLTE.css',

                    'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
                    'bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.css',
                    'bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.css',
                    'bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                    'bower_components/venobox/venobox/venobox.css',
                    'bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css',
                    'bower_components/fontIconPicker/css/jquery.fonticonpicker.css',
                    //'node_modules/inputmask/css/inputmask.css',
                    'bower_components/font-awesome-animation/dist/font-awesome-animation.css',
                    'bower_components/EasyAutocomplete/dist/easy-autocomplete.css'
                ],
                dest: 'www/assets/css/admin.css',
            },
            adminJS: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/jquery.cookie/jquery.cookie.js',
                    'bower_components/AdminLTE/dist/js/adminlte.min.js',
                    'bower_components/nette.ajax.js/nette.ajax.js',
                    'bower_components/history.nette.ajax.js/client-side/history.ajax.js',
                    'vendor/nette/forms/src/assets/netteForms.js',
                    'bower_components/chart.js/dist/Chart.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',

                    'bower_components/moment/moment.js',
                    'bower_components/moment/min/locales.js',

                    'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                    'bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.cs.min.js',
                    'bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js',
                    'bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.js',
                    'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                    'bower_components/bootstrap-select/dist/js/i18n/defaults-cs_CZ.js',
                    'bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
                    'bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.js',
                    'bower_components/select2/dist/js/select2.full.js',
                    'bower_components/select2/dist/js/i18n/cs.js',

                    'bower_components/venobox/venobox/venobox.js',
                    'bower_components/sticky-kit/jquery.sticky-kit.js',

                    'bower_components/EasyAutocomplete/dist/jquery.easy-autocomplete.js',

                    'assets/admin/js/autofocus.js',
                    'assets/admin/js/spinner.js',
                    'assets/admin/js/barcodeScannerListener.js',
                    'assets/admin/js/ckeditorPath.js',
                    'bower_components/ckeditor/ckeditor.js',
                    'assets/admin/js/ckeditor.js',

                    'assets/general/js/datagrid.js',
                    'assets/general/js/openNewWindow.js',
                    'assets/general/js/ajaxModal.js',
                    'assets/general/js/disableButton.js',
                    'assets/admin/js/main.js',
                ],
                dest: 'www/assets/js/admin.js',
            },
            portalCSS: {
                src: [
                    'build/portal.css',

                    'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
                    'bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.css',
                    'bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.css',
                    'bower_components/bootstrap-select/dist/css/bootstrap-select.css',
                    'bower_components/jquery-loading/dist/jquery.loading.css',

                    'bower_components/venobox/venobox/venobox.css',
                    'bower_components/select2/dist/css/select2.css',
                ],
                dest: 'www/assets/css/portal.css',
            },
            portalJS: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/nette.ajax.js/nette.ajax.js',
                    'vendor/nette/forms/src/assets/netteForms.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',

                    'bower_components/moment/moment.js',
                    'bower_components/moment/min/locales.js',

                    'bower_components/venobox/venobox/venobox.js',

                    'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                    'bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.cs.min.js',
                    'bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js',
                    'bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.js',
                    'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
                    'bower_components/bootstrap-select/dist/js/i18n/defaults-cs_CZ.js',
                    'bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.js',

                    'assets/admin/js/ckeditorPath.js',
                    'bower_components/ckeditor/ckeditor.js',
                    'assets/admin/js/ckeditor.js',

                    'bower_components/select2/dist/js/select2.full.js',
                    'bower_components/select2/dist/js/i18n/cs.js',

                    'bower_components/jquery-loading/dist/jquery.loading.js',
                    'assets/portal/js/spinner.js',

                    'assets/general/js/datagrid.js',
                    'assets/general/js/openNewWindow.js',
                    'assets/general/js/ajaxModal.js',
                    'assets/general/js/disableButton.js',
                    'assets/portal/js/main.js',
                ],
                dest: 'www/assets/js/portal.js',
            }
        },
        // Uglify task info. Compress the js files.
        uglify: {
            options: {
                mangle: true,
                preserveComments: 'some'
            },
            my_target: {
                files: {
                    'www/assets/js/admin.js': ['www/assets/js/admin.js'],
                    'www/assets/js/portal.js': ['www/assets/js/portal.js'],
                }
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'www/assets/css/admin.css': ['www/assets/css/admin.css'],
                    'www/assets/css/portal.css': ['www/assets/css/portal.css'],
                }
            }
        },
        copy: {
            admin: {
                files: [
                    {expand: true, cwd: 'bower_components/fa5/web-fonts-with-css/webfonts', src: ['**'], dest: 'www/assets/fonts/'},
                    {expand: true, cwd: 'bower_components/bootstrap/fonts/', src: ['**'], dest: 'www/assets/fonts/'},
                    {expand: true, cwd: 'assets/admin/img', src: ['**'], dest: 'www/assets/img/admin'},
                    {expand: true, cwd: 'bower_components/ckeditor', src: ['**'], dest: 'www/libs/ckeditor'},
                    {expand: true, cwd: 'bower_components/bootstrap-colorpicker/dist/img/bootstrap-colorpicker', src: ['**'], dest: 'www/assets/img/admin/bootstrap-colorpicker/'},
                ],
            },
            portal: {
                files: [
                    {expand: true, cwd: 'bower_components/fa5/web-fonts-with-css/webfonts', src: ['**'], dest: 'www/assets/fonts/'},
                    {expand: true, cwd: 'assets/portal/img', src: ['**'], dest: 'www/assets/img/portal'},
                    {expand: true, cwd: 'assets/portal/fonts', src: ['**'], dest: 'www/assets/fonts/'},
                ],
            },
        },
        replace: {
            bootstrapColorPicker: {
                options: {
                    patterns: [
                        {
                            match: /url\(('|")?(.[^'"\)]+\/)?(.[^'"\)]+\.\w+)('|")?\)/g,
                            replacement: "url('/assets/img/admin/bootstrap-colorpicker/$3')"
                        }
                    ]
                },
                files: [
                    {
                        expand: false,
                        flatten: true,
                        src: ['bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css'],
                        dest: 'bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css'
                    }
                ]
            }
        }
    });

    // Load all grunt tasks

    // LESS Compiler
    grunt.loadNpmTasks('grunt-contrib-less');
    // Watch File Changes
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Concat files
    grunt.loadNpmTasks('grunt-contrib-concat');
    // Copy files
    grunt.loadNpmTasks('grunt-contrib-copy');
    // Compress JS Files
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // Compress JS Files
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // Replace
    grunt.loadNpmTasks('grunt-replace');


    // The default task (running "grunt" in console) is "watch"
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('production', [
        'replace',
        'less',
        'concat',
        'uglify',
        'cssmin',
        'copy'
    ]);
};
