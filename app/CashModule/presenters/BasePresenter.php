<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Presenters;


use App\CashModule\Components\ChangeView\ChangeViewModal;
use App\CashModule\Components\ChangeView\ChangeViewModalFactory;
use App\CashModule\Components\SelectCashModal\SelectCashModal;
use App\CashModule\Components\SelectCashModal\SelectCashModalFactory;
use App\Components\Breadcrumb\Breadcrumb;
use App\Components\Breadcrumb\BreadcrumbFactory;
use App\Components\DataGrid\DataGridFactory;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Cash;
use App\Model\Entities\SellerData;
use App\Model\Exceptions\CashierWithoutAnyCash;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\CashierCacheService\CashierCacheService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;

class BasePresenter extends \App\Presenters\BasePresenter
{
   /** @var BreadcrumbFactory @inject */
   public $breadcrumbFactory;

   /** @var Breadcrumb */
   public $breadcrumb;

   /** @var DataGridFactory @inject */
   public $dataGridFactory;

   /** @var ReceiptCacheService @inject */
   public $receiptCacheService;

   /** @var ChangeViewModalFactory @inject */
   public $changeViewModalFactory;

   /** @var SelectCashModalFactory @inject */
   public $selectCashModalFactory;

   /** @var CashierCacheService @inject */
   public $cashierCacheService;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb = $this->breadcrumbFactory->create();

      $this->breadcrumb->add("Domů", ":Cash:MakeSell:");

      $this['flashMessage']->redrawControl();
      $this['flashMessageNotify']->redrawControl();
   }

   /**
    * Component for breadcrumb navigation
    * @return Breadcrumb
    */
   public function createComponentBreadcrumb() : Breadcrumb
   {
      return $this->breadcrumb;
   }

   public function checkRequirements($element)
   {
      parent::checkRequirements($element);

      $this->checkCash();
   }

   /**
    * @return CachedReceipt|null
    */
   public function getActiveReceipt() : ?CachedReceipt
   {
      if(!is_null($cachedReceipt = $this->receiptCacheService->getActiveReceiptByCashier($this->getLoggedUser()))) {
         return $cachedReceipt;
      }

      return null;
   }

   public function getCash() : Cash
   {
      $cashId = $this->cashierCacheService->getCashierData()->getActiveCashId();
      /** @var Cash $cash */
      $cash = $this->_em->find(Cash::class, $cashId);

      return $cash;
   }

   /**
    * @return bool
    */
   protected function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->_em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   private function checkCash()
   {
      /** @var CashRepository $cashRepo */
      $cashRepo = $this->_em->getRepository(Cash::class);

      try {
         //check if cash exists
         $cashId = $this->cashierCacheService->getCashierData()->getActiveCashId();
      } catch (CashierWithoutAnyCash $e) {
         $this->flashMessage("Nemáte přiřazenou žádnou pokladnu", FlashMessage::TYPE_DANGER);
         $this->redirect(":Admin:Dashboard:");
      }

      /** @var Cash $cash */
      $cash = $this->_em->find(Cash::class, $cashId);

      if(is_null($cash) || $cash->isArchived()) {
         $this->flashMessage("Pokladna neexistuje", FlashMessage::TYPE_DANGER);
         if($cash) {
            $this->cashierCacheService->clearCashierCashSettings($this->getLoggedUser(), $cash);
         }
         $this->cashierCacheService->clearCashierData($this->getLoggedUser());
         $this->redirect(":Admin:Dashboard:");
      }

      //check privileges
      if(!$cashRepo->canUserAccessToCash($this->getLoggedUser(), $cash)) {
         $this->flashMessage("K pokladně nemáte přístup", FlashMessage::TYPE_DANGER);
         $this->cashierCacheService->clearCashierCashSettings($this->getLoggedUser(), $cash);
         $this->cashierCacheService->clearCashierData($this->getLoggedUser());
         $this->redirect(":Admin:Dashboard:");
      }
   }

   ////////////////////////////////////////////////////////////////////////// change view
   protected function createComponentChangeViewModal() : ChangeViewModal
   {
      $comp =  $this->changeViewModalFactory->create();

      $comp->onSelected[] = function () {
         $this->flashMessageNotify("Zobrazení změněno");

         $this['changeViewModal']->hide();
         $this->redirect("this");
      };

      return $comp;
   }

   public function handleChangeView()
   {
      $this['changeViewModal']->show();
   }

   ////////////////////////////////////////////////////////////////////////// select cash
   protected function createComponentSelectCashModal() : SelectCashModal
   {
      $comp =  $this->selectCashModalFactory->create();

      $comp->onSelected[] = function () {
         $this->flashMessageNotify("Pokladna změněna");

         $this['selectCashModal']->hide();
         $this->redirect("this");
      };

      return $comp;
   }

   public function handleSelectCash()
   {
      $this['selectCashModal']->show();
   }

   //////////////////////////////////////////////////////////////////////////// helpers
   public function getCashName() : string
   {
      return $this->getCash()->getName();
   }
}