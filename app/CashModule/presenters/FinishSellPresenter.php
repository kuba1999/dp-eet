<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Presenters;

use App\CashModule\Components\FinishReceipt\FinishReceipt;
use App\CashModule\Components\FinishReceipt\FinishReceiptFactory;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualization;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualizationFactory;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Model\Builders\ReceiptBuilder;
use App\Model\Entities\Receipt;
use App\Model\Entities\SettingsEet;
use App\Model\Enums\TypeOfDocument;
use App\Model\Helpers\ReceiptHelper;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Services\CashierCacheService\CashierCacheService;
use App\Model\Services\EETService;

class FinishSellPresenter extends BasePresenter
{
   /** @var ReceiptVisualizationFactory @inject */
   public $receiptVisualizationFactory;

   /** @var FinishReceiptFactory @inject */
   public $finishReceiptFactory;

   /** @var CashierCacheService @inject */
   public $cashierDataCacheService;

   /** @var ReceiptBuilder @inject */
   public $receiptBuilder;

   /** @var EETService @inject */
   public $eetService;

   protected function startup()
   {
      parent::startup();

      $this->cleanUpReceipt();
   }

   public function actionDefault()
   {
      $this->breadcrumb->add("Dokončit");

      if(!$this->getActiveReceipt()) {
         $this->flashMessageNotify("Není otevřen žádný účet", FlashMessageNotify::TYPE_DANGER);
         $this->redirect("MakeSell:");
      }
      elseif($this->getActiveReceipt()->countItems() === 0) {
         $this->flashMessageNotify("Účet bez položek", FlashMessageNotify::TYPE_DANGER);
         $this->redirect("MakeSell:");
      }
   }


   /****************************************************************************************************** components */
   /**
    * @return ReceiptVisualization
    */
   protected function createComponentReceiptVisualization() : ReceiptVisualization
   {
      $comp = $this->receiptVisualizationFactory->create($this->getActiveReceipt());

      return $comp;
   }

   /**
    * @return FinishReceipt
    */
   protected function createComponentFinishReceipt() : FinishReceipt
   {
      $comp = $this->finishReceiptFactory->create(
         $this->getActiveReceipt()
      );

      $comp->onFinished [] = function (TypeOfDocument $type) {
         $receipt = $this->receiptBuilder->createFromCachedReceipt($this->getActiveReceipt(), $type);

         /** @var ReceiptRepository $receiptRepo */
         $receiptRepo = $this->_em->getRepository(Receipt::class);
         $receiptRepo->createReceipt($receipt);

         $this->flashMessageNotify("Účet vytvořen", FlashMessageNotify::TYPE_SUCCESS);
         $this->receiptCacheService->removeReceipt($this->getActiveReceipt()->getId());

         /** @var SettingsRepository $settingsRepo */
         $settingsRepo = $this->_em->getRepository(SettingsEet::class);

         //send to EET
         if($settingsRepo->isEetEnabled() && ReceiptHelper::isPossibleSendToEET($receipt)) {
            if($this->eetService->send($receipt)) {
               $this->flashMessageNotify(
                  "Úspěšně zaevidováno",
                  FlashMessageNotify::TYPE_INFO,
                  "EET"
               );
            } else {
               $this->flashMessageNotify(
                  "Nastala chyba - bude proveden další pokus o zaevidování",
                  FlashMessageNotify::TYPE_WARNING,
                  "EET"
               );
            }
         }

         $this->redirect("Receipts:detail", $receipt->getId());
      };

      return $comp;
   }

   /**
    * Clean item in receipt
    */
   private function cleanUpReceipt()
   {
      if(!$this->getActiveReceipt()) {
         return;
      }

      foreach ($this->getActiveReceipt()->getItems() AS $item)
      {
         if($this->isSellerTaxablePerson() && is_null($item->getTaxRateId())) {
            $this->getActiveReceipt()->removeItem($item->getId());
         }
         elseif($item->getPrice() === 0.0) {
            $this->getActiveReceipt()->removeItem($item->getId());
         }
      }

      $this->receiptCacheService->save();
   }
}