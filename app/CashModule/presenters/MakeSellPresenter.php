<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Presenters;



use App\CashModule\Components\ProductSelect\ProductSelect;
use App\CashModule\Components\ProductSelect\ProductSelectFactory;
use App\CashModule\Components\ReceiptImmediateSale\ReceiptImmediateSale;
use App\CashModule\Components\ReceiptImmediateSale\ReceiptImmediateSaleFactory;
use App\CashModule\Components\ReceiptManualCreation\ReceiptManualCreation;
use App\CashModule\Components\ReceiptManualCreation\ReceiptManualCreationFactory;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualization;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualizationFactory;
use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Model\Entities\Product;
use App\Model\Enums\CashView;
use App\Model\Helpers\BarcodeHelper;
use App\Model\Repositories\ProductRepository;

class MakeSellPresenter extends BasePresenter
{
   /** @var ProductSelectFactory @inject */
   public $productSelectFactory;

   /** @var ReceiptVisualizationFactory @inject */
   public $receiptVisualizationFactory;

   /** @var ReceiptImmediateSaleFactory @inject */
   public $receiptImmediateSaleFactory;

   public function actionDefault()
   {
      $this->breadcrumb->hide();
   }

   /****************************************************************************************************** components */

   /**
    * @return ProductSelect|BaseControl
    */
   protected function createComponentProductSelect()
   {
      if($this->getActiveView()->equalsValue(CashView::TABLE)) {
         $comp = $this->productSelectFactory->createTableView();
      }
      elseif($this->getActiveView()->equalsValue(CashView::GRID)) {
         $comp = $this->productSelectFactory->createGridView();
      }
      else {
         return new BaseControl();
      }

      $comp->onProductSelected[] = [$this, "productSelected"];

      $comp->isProductInReceiptCallback = function (Product $product) {
         if(!$this->getActiveReceipt()) {
            return false;
         }

         return $this->getActiveReceipt()->getItemByProduct($product) !== null;
      };

      if($this->getActiveReceipt()) {
         //update suggested products
         $comp->suggestProducts($this->getActiveReceipt());
      }

      return $comp;
   }

   /**
    * @return ReceiptVisualization
    */
   protected function createComponentReceiptVisualization() : ReceiptVisualization
   {
      $comp = $this->receiptVisualizationFactory->create($this->getActiveReceipt());
      $comp->onProductRemoved[] = function (Product $product) {
         $this['productSelect']->productRemovedFromReceipt($product);

         //update suggested products
         if($this->getActiveReceipt()) {
            $this['productSelect']->suggestProducts($this->getActiveReceipt());
         }
      };
      $comp->onFinishSell[] = function () {
         $this->redirect("FinishSell:");
      };

      return $comp;
   }

   /**
    * @return ReceiptImmediateSale
    */
   protected function createComponentReceiptManualCreation() : ReceiptImmediateSale
   {
      if(($receipt = $this->getActiveReceipt()) === null) {
         //create new receipt
         $receipt = $this->receiptCacheService->createNewReceipt($this->getLoggedUser(), $this->getCash());
      }

      $comp = $this->receiptImmediateSaleFactory->create($receipt);

      $comp->onFinishSell[] = function () {
         $this->redirect("FinishSell:");
      };

      return $comp;
   }

   /********************************************************************************************************* helpers */
   public function getActiveView() : CashView
   {
      $cashSettings = $this->cashierCacheService->getCashierCashSettings();

      return $cashSettings->getSelectedView();
   }

   /**
    * @param Product $product
    */
   public function productSelected(Product $product)
   {
      if(is_null($this->getActiveReceipt())) {
         //create new receipt
         $receipt = $this->receiptCacheService->createNewReceipt($this->getLoggedUser(), $this->getCash());

         $this->flashMessageNotify("Vytvořen nový účet");
         $this->flashMessageNotify($product->getName(), FlashMessageNotify::TYPE_SUCCESS, "Přidána nová položka");

         //redraw
         $this->redrawControl("leftColumn");
      } else {
         $receipt = $this->getActiveReceipt();

         $this->flashMessageNotify($product->getName(), FlashMessageNotify::TYPE_SUCCESS, "Přidána nová položka");

         //redraw
         if($this->getActiveView()->equalsValue(CashView::IMMEDIATE_SALE)) {
            $this['receiptManualCreation']->redrawReceipt();
         } else {
            $this['receiptVisualization']->redrawReceipt();
         }
      }

      $receipt->addProduct($product, $this->isSellerTaxablePerson());
      $this->receiptCacheService->save();


      //update suggested products
      if(!$this->getActiveView()->equalsValue(CashView::IMMEDIATE_SALE)) {
         $this['productSelect']->suggestProducts($this->getActiveReceipt());
      }
   }
   /********************************************************************************************************* signals */

   /**
    * Scanned code by barcode scanner
    * @param string $code
    */
   public function handleBarcodeScanned(string $code)
   {
      /** @var ProductRepository $productRepo */
      $productRepo = $this->_em->getRepository(Product::class);

      if(BarcodeHelper::isEanValid($code) &&
         ($count = $productRepo->countBy(["ean" => $code]))
      ) {
         if($count === 1) {
            /** @var Product $product */
            $product = $productRepo->findOneBy(["ean" => $code]);

            $this->productSelected($product);
         }
      } else {
         $this->flashMessageNotify("Produkt nenalezen", FlashMessageNotify::TYPE_DANGER);
      }

      $this['flashMessageNotify']->redrawControl();
   }

   /**
    * Create and show new receipt
    */
   public function handleNewReceipt()
   {
      //create new receipt
      $this->receiptCacheService->createNewReceipt($this->getLoggedUser(), $this->getCash());

      $this->flashMessageNotify("Vytvořen nový účet");

      //redraw
      $this->redrawControl();
   }
}