<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Presenters;



use App\CashModule\Components\QuickAddReceiptItemModal\QuickAddReceiptItemModal;
use App\CashModule\Components\QuickAddReceiptItemModal\QuickAddReceiptItemModalFactory;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualization;
use App\CashModule\Components\ReceiptVisualization\ReceiptVisualizationFactory;
use App\CashModule\Components\RenameReceiptModal\RenameReceiptModal;
use App\CashModule\Components\RenameReceiptModal\RenameReceiptModalFactory;
use App\CashModule\Components\SelectActiveReceipt\SelectActiveReceipt;
use App\CashModule\Components\SelectActiveReceipt\SelectActiveReceiptFactory;
use App\Model\Services\CashierCacheService\CashierCacheService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;

class ChangeReceiptPresenter extends BasePresenter
{
   /** @var ReceiptVisualizationFactory @inject */
   public $receiptVisualizationFactory;

   /** @var ReceiptCacheService @inject */
   public $receiptCacheService;

   /** @var CashierCacheService @inject */
   public $cashierDataCacheService;

   /** @var SelectActiveReceiptFactory @inject */
   public $selectActiveReceiptFactory;

   public function actionDefault()
   {
      $this->breadcrumb->add("Změnit účet");
   }

   /****************************************************************************************************** components */
   /**
    * @return ReceiptVisualization
    */
   protected function createComponentReceiptVisualization() : ReceiptVisualization
   {
      $comp = $this->receiptVisualizationFactory->create($this->template->receipt);

      return $comp;
   }

   /**
    * @return SelectActiveReceipt
    */
   protected function createComponentSelectActiveReceipt() : SelectActiveReceipt
   {
      $comp = $this->selectActiveReceiptFactory->create($this->getCash());

      $comp->onSelected[] = function (CachedReceipt $cachedReceipt) {
         $this->receiptCacheService->setActiveReceipt($cachedReceipt, $this->getLoggedUser());
         $this->presenter->flashMessageNotify("Účet změněn");
         $this->redirect("MakeSell:");
      };

      $comp->onShowReceipt[] = function ($receipt) {
         $this->template->receipt = $receipt;

         //redraw
         $this->redrawControl("receipt");
      };

      $comp->onDeleted[] = function () {
         $this->presenter->flashMessageNotify("Účet smazán");
      };

      return $comp;
   }

}