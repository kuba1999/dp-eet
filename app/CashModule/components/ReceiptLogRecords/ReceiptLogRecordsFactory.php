<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptLogRecords;


use App\Components\DataGrid\DataGridFactory;
use App\Model\Entities\Receipt;
use Kdyby\Doctrine\EntityManager;

class ReceiptLogRecordsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var DataGridFactory */
   private $dataGridFactory;

   function __construct(EntityManager $em, DataGridFactory $dataGridFactory)
   {
      $this->em = $em;
      $this->dataGridFactory = $dataGridFactory;
   }

   public function create(?Receipt $receipt = null) : ReceiptLogRecords
   {
      $component = new ReceiptLogRecords(
         $receipt,
         $this->em,
         $this->dataGridFactory
      );

      return $component;
   }
}