<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptLogRecords;


use App\Components\BaseControl;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\DataGridFactory;
use App\Components\DataGrid\Utils\Sorting;
use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptLogRecord;
use App\Model\Repositories\ReceiptLogRecordRepository;
use Kdyby\Doctrine\EntityManager;

class ReceiptLogRecords extends BaseControl
{
   /** @var Receipt */
   private $receipt;

   /** @var EntityManager */
   private $em;

   /** @var DataGridFactory */
   private $dataGridFactory;

   /**
    * ReceiptLogRecords constructor.
    * @param Receipt $receipt
    * @param EntityManager $em
    * @param DataGridFactory $dataGridFactory
    */
   function __construct(?Receipt $receipt, EntityManager $em, DataGridFactory $dataGridFactory)
   {
      $this->receipt = $receipt;
      $this->em = $em;
      $this->dataGridFactory = $dataGridFactory;
   }

   /**
    * @return DataGrid
    */
   protected function createComponentGrid() : DataGrid
   {
      $comp = $this->dataGridFactory->create();

      if(!$this->receipt) {
         $comp->addColumnText("niceID", "Doklad")
            ->setVirtualColumn();
      }
      $comp->addColumnDateTime("created", "Vytvořeno");
      $comp->addColumnDateTime("type", "Typ")
         ->setRenderer(function (ReceiptLogRecord $record){
            return $record->getType()->getHtmlLabel();
         });
      $comp->addColumnText("message", "Zpráva");

      /** @var ReceiptLogRecordRepository $receiptLogRecordRepo */
      $receiptLogRecordRepo = $this->em->getRepository(ReceiptLogRecord::class);


      if($this->receipt) {
         $comp->setDataSource($receiptLogRecordRepo->getLogRecordsByReceiptQB($this->receipt));
      } else {
         $comp->setDataSource($receiptLogRecordRepo->getLogRecordsQB());
      }

      $comp->setDefaultSorting(["created" => Sorting::SORT_DESC]);

      return $comp;
   }


   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}