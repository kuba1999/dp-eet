<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\AddEditReceiptItemModal;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\SellerData;
use App\Model\Entities\Settings;
use App\Model\Entities\SettingsGlobal;
use App\Model\Entities\TaxRate;
use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\TaxRateRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class AddEditReceiptItemModal extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var CachedReceipt */
   private $cachedReceipt;

   /** @var SettingsRepository */
   private $settingsRepo;

   /** @var int|null @persistent */
   public $itemId;

   /** @var bool @persistent */
   public $show = false;

   /** @var callable */
   public $onCreated;

   /** @var callable */
   public $onSaved;

   /**
    * @param EntityManager $em
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      CachedReceipt $cachedReceipt
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->cachedReceipt = $cachedReceipt;

      $this->settingsRepo = $this->em->getRepository(SettingsGlobal::class);
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("name", "Název")
         ->setMaxLength(256)
         ->setRequired();

      $form->addText("quantity", "Množství")
         ->addRule(BaseForm::FLOAT)
         ->setRequired();

      $form->addPrice("price", $this->isSellerTaxablePerson() ? "Cena vč. DPH":"Cena")
         ->setRequired();

      if($this->isSellerTaxablePerson()) {
         $form->addSelect("taxRate", "Sazba DPH", $this->getTaxRates())
            ->setPrompt("vyberte")
            ->setRequired();
      }

      $form->addTextArea("note", "Poznámka", $this->getTaxRates())
         ->setMaxLength(256)
         ->setNullable()
         ->setAttribute("placeholder", "Text bude uveden na dokladu");

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };

      if($this->getItem()) {
         $form->setDefaults([
            "name"            =>    $this->getItem()->getName(),
            "quantity"        =>    $this->getItem()->getQuantity(),
            "note"            =>    $this->getItem()->getNote()
         ]);

         if($this->isSellerTaxablePerson() && $this->getItem()->getTaxRateId()) {
            /** @var TaxRate $taxRate */
            $taxRate = $this->em->find(TaxRate::class, $this->getItem()->getTaxRateId());

            if(!$taxRate->isArchived()) {
               $form['taxRate']->setDefaultValue($this->getItem()->getTaxRateId());
            }

            $form['price']->setDefaultValue(PriceHelper::calculatePriceWithVat($this->getItem()->getTaxRateValue(), $this->getItem()->getPrice()));
         } else {
            $form['price']->setDefaultValue($this->getItem()->getPrice());
         }

         $form['submit']->caption = "Uložit";
      }

      $form->onAnchor[] = function (BaseForm $form) {
         if($this->isSellerTaxablePerson() && $this->getItem() && $this->getItem()->getTaxRateId()) {
            /** @var TaxRate $taxRate */
            $taxRate = $this->em->find(TaxRate::class,$this->getItem()->getTaxRateId());

            if($taxRate->isArchived() && !$form->isSubmitted()) {
               $this->flashMessage("Vyberte novou sazbu DPH", FlashMessage::TYPE_WARNING);
               $form['taxRate']->addError("Zvolená sazba DPH již neexistuje. Zvolte novou.");
            }
         }
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->isSellerTaxablePerson()) {
         $taxRate = $this->getTaxRate( $form['taxRate']->getValue() );

         $price = PriceHelper::calculatePriceWithoutVat($taxRate, $form['price']->getValue(), false);

         $taxRateId = $taxRate->getId();
         $taxRateValue = $taxRate->getValue();
         $taxRateType = $taxRate->getType();
      } else {
         $price = $form['price']->getValue();
         $taxRateId = null;
         $taxRateValue = null;
         $taxRateType = TaxRateType::get(TaxRateType::_NONE_);
      }

      if($this->getItem()) {
         $item = $this->getItem();

         $item->setName($form['name']->getValue());
         $item->setQuantity($form['quantity']->getValue());
         $item->setPrice($price);
         $item->setTaxRateId($taxRateId);
         $item->setTaxRateValue($taxRateValue);
         $item->setTaxRateType($taxRateType);

         $item->setNote($form['note']->getValue());

         $this->receiptCacheService->save($this->cachedReceipt);
         $this->onSaved($item);
      }
      else {
         $item = new CachedReceiptItem(
            $this->cachedReceipt,
            null,
            $form['name']->getValue(),
            $form['quantity']->getValue(),
            $price
         );

         $item->setTaxRateId($taxRateId);
         $item->setTaxRateValue($taxRateValue);
         $item->setTaxRateType($taxRateType);

         $item->setNote($form['note']->getValue());

         $this->cachedReceipt->addItem($item);
         $this->receiptCacheService->save($this->cachedReceipt);
         $this->onCreated($item);
      }
   }

   /********************************************************************************************************* helpers */
   public function show($itemId = null)
   {
      $this->show = true;
      $this->itemId = $itemId;

      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->itemId = null;

      $this->redrawControl();
   }

   public function getItem() : ?CachedReceiptItem
   {
      if(!$this->itemId) {
         return null;
      }

      return $this->cachedReceipt->getItem($this->itemId);
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * Get all vat rates
    * @return array
    */
   private function getTaxRates() : array
   {
      /** @var TaxRateRepository $vatRepo */
      $vatRepo = $this->em->getRepository(TaxRate::class);

      return $vatRepo->getAllForSelectBox();
   }


   private function getTaxRate(int $taxRateId) : TaxRate
   {
      /** @var TaxRateRepository $vatRepo */
      $vatRepo = $this->em->getRepository(TaxRate::class);

      return $vatRepo->find($taxRateId);
   }


   /********************************************************************************************************** render */
   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}