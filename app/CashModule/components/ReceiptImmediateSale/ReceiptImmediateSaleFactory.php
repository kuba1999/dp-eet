<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModalFactory;
use App\CashModule\Components\ProductSelect\ProductSelectFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModalFactory;
use App\Model\Services\ProductSuggesterService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class ReceiptImmediateSaleFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var AddEditReceiptItemModalFactory */
   private $addEditReceiptItemModalFactory;

   /** @var ProductSelectFactory */
   private $productSelectFactory;

   /** @var ProductSuggesterService */
   private $productSuggesterService;

   /** @var RenameReceiptModalFactory */
   private $renameReceiptModalFactory;

   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory,
      ProductSelectFactory $productSelectFactory,
      ProductSuggesterService $productSuggesterService,
      RenameReceiptModalFactory $renameReceiptModalFactory
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->addEditReceiptItemModalFactory = $addEditReceiptItemModalFactory;
      $this->productSelectFactory = $productSelectFactory;
      $this->productSuggesterService = $productSuggesterService;
      $this->renameReceiptModalFactory = $renameReceiptModalFactory;
   }

   public function create(CachedReceipt $cachedReceipt) : ReceiptImmediateSale
   {
      $component = new ReceiptImmediateSale(
         $this->em,
         $this->receiptCacheService,
         $this->addEditReceiptItemModalFactory,
         $this->productSelectFactory,
         $this->productSuggesterService,
         $this->renameReceiptModalFactory,
         $cachedReceipt
      );

      return $component;
   }
}