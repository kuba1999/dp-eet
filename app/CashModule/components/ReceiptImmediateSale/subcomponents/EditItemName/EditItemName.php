<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class EditItemName extends Control
{
   /** @var CachedReceiptItem */
   private $item;

   /** @var callable */
   public $onSuccess;

   /**
    * EditItemName constructor.
    * @param CachedReceiptItem $item
    */
   function __construct(CachedReceiptItem $item)
   {
      $this->item = $item;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("value")
         ->setNullable()
         ->setDefaultValue($this->item->getName() !== "Nepojmenovaná položka" ? $this->item->getName():null)
         ->setAttribute("placeholder", "Nepojmenovaná položka")
         ->addClass("autocomplete");

      $form->onSuccess[] = [$this, "save"];
      $form->onError[] = function () {
         $this->redrawControl();
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function save(BaseForm $form)
   {
      $this->item->setName($form['value']->getValue());

      //$this->redrawControl();

      $this->onSuccess();
   }

   public function handleProductsAutocomplete()
   {

   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}