<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\TaxRate;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\TaxRateRepository;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class EditItemTaxRate extends Control
{
   /** @var CachedReceiptItem */
   private $item;

   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSuccess;

   /**
    * EditItemTaxRate constructor.
    * @param EntityManager $em
    * @param CachedReceiptItem $item
    */
   function __construct(EntityManager $em, CachedReceiptItem $item)
   {
      $this->item = $item;
      $this->em = $em;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addSelect("value", null, $this->getTaxRates())
         ->setRequired("vyberte sazbu")
         ->setPrompt("vyberte sazbu")
         ->setAttribute("placeholder", "Zadejte cenu");

      $form->onSuccess[] = [$this, "save"];
      $form->onError[] = function () {
         $this->redrawControl();
      };

      if($this->item->getTaxRateId()) {
         /** @var TaxRate $taxRate */
         $taxRate = $this->em->find(TaxRate::class, $this->item->getTaxRateId());

         if (!$taxRate->isArchived()) {
            $form['value']->setDefaultValue($this->item->getTaxRateId());
         }
      }

      $form->onAnchor[] = function (BaseForm $form) {
         if($this->item->getTaxRateId()) {
            /** @var TaxRate $taxRate */
            $taxRate = $this->em->find(TaxRate::class,$this->item->getTaxRateId());

            if($taxRate->isArchived() && !$form->isSubmitted()) {
               $this->flashMessage("Vyberte novou sazbu DPH", FlashMessage::TYPE_WARNING);
               $form['value']->addError("Zvolená sazba DPH již neexistuje. Zvolte novou.");
            }
         }
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function save(BaseForm $form)
   {
      $taxRate = $this->getTaxRate($form['value']->getValue());

      $tmpTaxRateValue = $this->item->getTaxRateValue();

      $this->item->setTaxRateId($taxRate->getId());
      $this->item->setTaxRateType($taxRate->getType());
      $this->item->setTaxRateValue($taxRate->getValue());

      //update price
      if(is_null($tmpTaxRateValue)) {
         $this->item->setPrice(PriceHelper::calculatePriceWithoutVat($this->item->getTaxRateValue(), $this->item->getPrice()));
      } else {
         $price = PriceHelper::calculatePriceWithVat($tmpTaxRateValue, $this->item->getPrice());
         $this->item->setPrice(PriceHelper::calculatePriceWithoutVat($this->item->getTaxRateValue(), $price));
      }

      $this->redrawControl();

      $this->onSuccess();
   }

   /**
    * @return array
    */
   private function getTaxRates() : array
   {
      /** @var TaxRateRepository $vatRepo */
      $vatRepo = $this->em->getRepository(TaxRate::class);

      return $vatRepo->getAllForSelectBox();
   }


   private function getTaxRate(int $taxRateId) : TaxRate
   {
      /** @var TaxRate $taxRate */
      $taxRate = $this->em->find(TaxRate::class, $taxRateId);

      return $taxRate;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}