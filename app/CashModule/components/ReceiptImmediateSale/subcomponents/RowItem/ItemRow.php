<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\SellerData;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Http\FileUpload;

class ItemRow extends Control
{
   /** @var CachedReceiptItem */
   private $item;

   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onChanged;

   /** @var callable */
   public $onRemoved;

   /** @var callable */
   public $onEdit;

   /**
    * EditItemTaxRate constructor.
    * @param EntityManager $em
    * @param CachedReceiptItem $item
    */
   function __construct(EntityManager $em, CachedReceiptItem $item)
   {
      $this->item = $item;
      $this->em = $em;
   }

   //////////////////////////////////////////////////////////////////////////////////// edit
   protected function createComponentItemNameForm() : EditItemName
   {
      $control = new EditItemName($this->item);
      $control->onSuccess[] = function () {
         $this->onChanged($this->item);
      };

      return $control;
   }

   protected function createComponentItemPriceForm() : EditItemPrice
   {
      $control = new EditItemPrice($this->item, $this->isSellerTaxablePerson());
      $control->onSuccess[] = function () {
         $this->onChanged($this->item);
         $this->redrawControl("partialSum");
      };

      return $control;
   }

   protected function createComponentItemTaxRateForm() : EditItemTaxRate
   {
      $control = new EditItemTaxRate($this->em, $this->item);
      $control->onSuccess[] = function () {
         $this->onChanged($this->item);
         $this->redrawControl("partialSum");
      };

      return $control;
   }

   protected function createComponentItemQuantityForm() : EditItemQuantity
   {
      $control = new EditItemQuantity($this->item);
      $control->onSuccess[] = function () {
         if($this->item->getQuantity() <= 0.0) {
            $this->handleRemove();
         } else {
            $this->onChanged($this->item);
         }

         $this->redrawControl("partialSum");
      };

      return $control;
   }
   ////////////////////////////////////////////////////////////////////////////////////// remove
   public function handleRemove()
   {
      $this->template->removed = true;
      $this->redrawControl("removed");
      $this->onRemoved();
   }

   public function handleEdit()
   {
      $this->onEdit();
   }
   ////////////////////////////////////////////////////////////////////////////////////// helpers
   /**
    * @return bool
    */
   private function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->taxablePerson = $this->isSellerTaxablePerson();
      $tpl->partialSum = $this->item->getPartialSum();
      $tpl->id = $this->item->getId();

      return $tpl->render();
   }
}