<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Helpers\PriceHelper;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class EditItemPrice extends Control
{
   /** @var CachedReceiptItem */
   private $item;

   /** @var callable */
   public $onSuccess;

   /** @var bool */
   private $taxablePerson;

   /**
    * EditItemName constructor.
    * @param CachedReceiptItem $item
    * @param bool $taxablePerson
    */
   function __construct(CachedReceiptItem $item, bool $taxablePerson)
   {
      $this->item = $item;
      $this->taxablePerson = $taxablePerson;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addPrice("value")
         ->setRequired("Zadejte cenu")
         ->setAttribute("placeholder", "Zadejte cenu");

      if($this->taxablePerson && $this->item->getTaxRateValue()) {
         $form['value']->setDefaultValue(PriceHelper::calculatePriceWithVat($this->item->getTaxRateValue(), $this->item->getPrice()));
      } else {
         $form['value']->setDefaultValue($this->item->getPrice());
      }

      $form->onSuccess[] = [$this, "save"];
      $form->onError[] = function () {
         $this->redrawControl();
      };
      $form->onAnchor[] = function (BaseForm $form) {
         if($form['value']->isFilled()) {
            $form['value']->setDefaultValue(round($form['value']->getValue(), 2));
         }
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function save(BaseForm $form)
   {
      if($this->taxablePerson && $this->item->getTaxRateValue()) {
         $priceWithTax = $form['value']->getValue();
         $price = PriceHelper::calculatePriceWithoutVat($this->item->getTaxRateValue(), $priceWithTax);
      } else {
         $price = $form['value']->getValue();
      }

      $this->item->setPrice($price);

      $this->redrawControl();

      $this->onSuccess();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}