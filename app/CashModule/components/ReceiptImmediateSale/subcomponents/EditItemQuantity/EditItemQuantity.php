<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class EditItemQuantity extends Control
{
   /** @var CachedReceiptItem */
   private $item;

   /** @var callable */
   public $onSuccess;

   /**
    * EditItemName constructor.
    * @param CachedReceiptItem $item
    */
   function __construct(CachedReceiptItem $item)
   {
      $this->item = $item;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("value")
         ->setType("text")
         ->addRule(BaseForm::NUMERIC, "Zadejte číselnou hodnotu")
         ->setRequired("Zadejte množství")
         ->setDefaultValue($this->item->getQuantity())
         ->setAttribute("placeholder", "Zadejte množství")
         ->addClass("text-center");

      $form->addSubmit("plus");
      $form->addSubmit("minus");

      $form->onSuccess[] = [$this, "save"];
      $form->onError[] = function () {
         $this->redrawControl();
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function save(BaseForm $form)
   {
      if($form->isSubmitted() === $form['minus'])  {
         $this->item->decrementQuantity();
      }
      elseif($form->isSubmitted() === $form['plus'])  {
         $this->item->incrementQuantity();
      }
      else {
         $this->item->setQuantity($form['value']->getValue());
      }

      $form['value']->setValue($this->item->getQuantity());

      $this->redrawControl();
      $this->onSuccess();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}