<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptImmediateSale;


use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModal;
use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModalFactory;
use App\CashModule\Components\ProductSelect\ProductSelect;
use App\CashModule\Components\ProductSelect\ProductSelectFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModal;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModalFactory;
use App\Model\Entities\Product;
use App\Model\Entities\SellerData;
use App\Model\Enums\TaxRateType;
use App\Model\Repositories\ProductRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\ProductSuggesterService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Utils\ArrayHash;

class ReceiptImmediateSale extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onFinishSell;

   /** @var CachedReceipt */
   private $cachedReceipt;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var AddEditReceiptItemModalFactory */
   private $addEditReceiptItemModalFactory;

   /** @var ProductSelectFactory */
   public $productSelectFactory;

   /** @var bool @persistent */
   public $productSelectShown = false;

   /** @var ProductSuggesterService */
   private $productSuggesterService;

   /** @var RenameReceiptModalFactory */
   private $renameReceiptModalFactory;

   /**
    * ReceiptImmediateSale constructor.
    * @param EntityManager $em
    * @param ReceiptCacheService $receiptCacheService
    * @param AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory
    * @param CachedReceipt $cachedReceipt
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory,
      ProductSelectFactory $productSelectFactory,
      ProductSuggesterService $productSuggesterService,
      RenameReceiptModalFactory $renameReceiptModalFactory,
      CachedReceipt $cachedReceipt
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->cachedReceipt = $cachedReceipt;
      $this->addEditReceiptItemModalFactory = $addEditReceiptItemModalFactory;
      $this->productSuggesterService = $productSuggesterService;
      $this->productSelectFactory = $productSelectFactory;
      $this->renameReceiptModalFactory = $renameReceiptModalFactory;
   }

   private function getItems() : ArrayHash
   {
      if($this->cachedReceipt) {
         return $this->cachedReceipt->getItems();
      }

      return new ArrayHash();
   }

   //////////////////////////////////////////////////////////////////////////////////// edit items
   protected function createComponentItemRow() : Multiplier
   {
      return new Multiplier(function (int $itemId) {
         $item = $this->cachedReceipt->getItem($itemId);

         if(!$item) {
            $this->presenter->terminate();
         }

         $control = new ItemRow($this->em, $item);
         $control->onChanged[] = function () {
            $this->receiptCacheService->save();
            $this->redrawControl("overview");
         };
         $control->onRemoved[] = function () use($item) {
            $this->cachedReceipt->removeItem($item->getId());
            $this->receiptCacheService->save();
            $this->redrawControl("overview");
         };
         $control->onEdit[] = function () use($item) {
            $this->editRow($item);
         };

         return $control;
      });
   }

   //////////////////////////////////////////////////////////////////////////////////// edit row
   /**
    * @return AddEditReceiptItemModal
    */
   public function createComponentAddEditReceiptItemModal() : AddEditReceiptItemModal
   {
      $comp = $this->addEditReceiptItemModalFactory->create($this->cachedReceipt);

      $comp->onSaved[] = function (CachedReceiptItem $item) use ($comp) {
         $this['itemRow'][$item->getId()]->redrawControl();

         $this->presenter->flashMessageNotify("Položka uložena");
         $this->redrawControl("overview");
         $comp->hide();
      };

      return $comp;
   }

   private function editRow(CachedReceiptItem $item)
   {
      /** @var AddEditReceiptItemModal $comp */
      $comp = $this['addEditReceiptItemModal'];

      $comp->show($item->getId());
   }

   //////////////////////////////////////////////////////////////////////////////////// add row
   public function handleAddRow()
   {
      $this->createRow();
   }

   private function createRow()
   {
      $item = new CachedReceiptItem(
         $this->cachedReceipt,
         null,
         "Nepojmenovaná položka",
         1.0,
         0.0
      );

      $item->setTaxRateType(TaxRateType::get(TaxRateType::_NONE_));
      $item->setTaxRateValue(0);

      $this->cachedReceipt->addItem($item);
      $this->receiptCacheService->save();

      $this->template->items = [$item];

      $this->redrawControl("items");
      $this->redrawControl("overview");
   }

   //////////////////////////////////////////////////////////////////////////////////// select product
   /**
    * @return ProductSelect
    */
   protected function createComponentProductSelect() : ProductSelect
   {
      $comp = $this->productSelectFactory->createTableView();

      $comp->onProductSelected[] = [$this, "productSelected"];

      $comp->isProductInReceiptCallback = function (Product $product) {
         return $this->cachedReceipt->getItemByProduct($product) !== null;
      };

      //update suggested products
      $comp->suggestProducts($this->cachedReceipt);

      return $comp;
   }

   public function handleSelectProduct()
   {
      $this->productSelectShown = true;
      $this->redrawControl("productSelectModal");
   }

   public function productSelected(Product $product)
   {
      $item = $this->cachedReceipt->addProduct($product, $this->isSellerTaxablePerson());
      $this->receiptCacheService->save();

      $this->productSelectShown = false;
      $this->template->items = [$item];

      $this->redrawControl("items");
      $this->redrawControl("productSelectModal");
      $this->redrawControl("overview");
   }

   ////////////////////////////////////////////////////////////////////////////////////// autocomplete product name
   public function handleProductsAutocomplete(string $term = "")
   {
      $ret = [];

      /** @var ProductRepository $productRepo */
      $productRepo = $this->em->getRepository(Product::class);

      //suggest products
      $suggested = $this->productSuggesterService->suggest($this->cachedReceipt, 5, $term);
      $suggestedIDs = [];
      foreach ($suggested AS $suggestedProduct) {
         $ret[$suggestedProduct->getName()] = [
            "id"     =>    $suggestedProduct->getId(),
            "name"   =>    $suggestedProduct->getName(),
            "suggested"   =>    true,
         ];

         $suggestedIDs[] = $suggestedProduct->getId();
      }
      sort($ret);
      $ret = array_values($ret);

      $qb = $productRepo->searchProducts($term)
         ->setMaxResults(10 - count($suggestedIDs));

      if(count($suggestedIDs)) {
         $qb->andWhere("p.id NOT IN (:productIDs)")->setParameter("productIDs", $suggestedIDs);
      }

      /** @var Product[] $products */
      $products = $qb
         ->orderBy("p.name", "asc")
         ->getQuery()->getResult();

      foreach ($products AS $product) {
         $ret[] = [
            "id"     =>    $product->getId(),
            "name"   =>    $product->getName()
         ];
      }

      $this->presenter->sendJson($ret);
   }

   public function handleSelectedSuggestedProduct(int $itemId = null, int $productId = null)
   {
      if(!$productId || !$itemId) {
         return;
      }

      /** @var Product $product */
      $product = $this->em->find(Product::class, $productId);

      if(!$product) {
         return;
      }

      $item = $this->cachedReceipt->getItem($itemId);
      $item->setProductId($productId);
      $item->setName($product->getName());
      $item->setPrice($product->getPrice());

      if($this->isSellerTaxablePerson()) {
         $item->setTaxRateId($product->getTaxRate()->getId());
         $item->setTaxRateValue($product->getTaxRate()->getValue());
         $item->setTaxRateType($product->getTaxRate()->getType());
      }

      $this->receiptCacheService->save();

      $this['itemRow'][$itemId]->redrawControl();
      $this->redrawControl("overview");
   }


   ////////////////////////////////////////////////////////////////////////////////////// helpers

   /**
    * @return RenameReceiptModal
    */
   protected function createComponentRenameReceiptModal() : RenameReceiptModal
   {
      $comp = $this->renameReceiptModalFactory->create($this->cachedReceipt);

      $comp->onSaved[] = function () use ($comp) {
         $comp->hide();
         $this->redrawControl("receiptName");
         $this->presenter->flashMessageNotify("Uloženo");
      };

      return $comp;
   }

   public function handleRenameReceipt()
   {
      /** @var RenameReceiptModal $comp */
      $comp = $this['renameReceiptModal'];

      $comp->show();
   }

   ////////////////////////////////////////////////////////////////////////////////////// helpers
   /**
    * @return bool
    */
   private function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * Calculate items sum
    * @param bool $taxIncluded
    * @return float
    */
   public function getReceiptSum($taxIncluded = true) : float
   {
      return $this->cachedReceipt->getReceiptSum($taxIncluded);
   }

   /**
    * Calculate items sum
    * @return float
    */
   public function getTaxAmount() : float
   {
      return $this->cachedReceipt->getTaxAmount();
   }

   public function handleFinishSell()
   {
      $this->onFinishSell();
   }

   public function redrawReceipt()
   {
      $this->redrawControl("receipt");
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->receipt = $this->cachedReceipt;
      $tpl->taxablePerson = $this->isSellerTaxablePerson();

      if(!isset($tpl->items)) {
         $tpl->items = $this->getItems();
      }

      return $tpl->render();
   }
}