<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SelectCashModal;


use App\Model\Entities\Cash;
use App\Model\Entities\User;
use App\Model\Repositories\CashRepository;
use App\Model\Services\CashierCacheService\CashierCacheService;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;

class SelectCashModal extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var CashierCacheService */
   private $cashierCacheService;

   /** @var User */
   private $cashier;

   /** @var CashRepository */
   private $cashRepo;

   /** @var callable */
   public $onSelected;

   /** @var bool @persistent */
   public $show = false;

   /**
    * ChangeViewModal constructor.
    * @param CashierCacheService $cashierCacheService
    */
   function __construct(EntityManager $em, CashierCacheService $cashierCacheService, User $cashier)
   {
      $this->em = $em;
      $this->cashierCacheService = $cashierCacheService;
      $this->cashier = $cashier;

      $this->cashRepo = $em->getRepository(Cash::class);
   }

   public function handleChange(int $cashId)
   {
      $this->cashierCacheService->getCashierData()->setActiveCashId($cashId);
      $this->cashierCacheService->saveCashierData();
      $this->redrawControl("content");
      $this->onSelected();
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }

   public function isCashActive(Cash $cash) : bool
   {
      $cashierData = $this->cashierCacheService->getCashierData();

      return $cashierData->getActiveCashId() === $cash->getId();
   }

   public function getItems() : array
   {
      return $this->cashRepo->getAvailableCash($this->cashier);
   }
}