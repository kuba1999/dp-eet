<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SelectCashModal;


use App\Model\Services\CashierCacheService\CashierCacheService;
use Doctrine\ORM\EntityManager;
use Nette\Security\User;

class SelectCashModalFactory
{
   /** @var CashierCacheService */
   private $cashierCacheService;

   /** @var EntityManager */
   private $em;

   /** @var \App\Model\Entities\User */
   private $cashier;

   function __construct(EntityManager $em, CashierCacheService $cashierCacheService, User $user)
   {
      if(!$user->isLoggedIn()) {
         return null;
      }

      $this->em = $em;
      $this->cashierCacheService = $cashierCacheService;
      $this->cashier = $this->em->find(\App\Model\Entities\User::class, $user->getId());
   }

   public function create() : SelectCashModal
   {
      $component = new SelectCashModal(
         $this->em,
         $this->cashierCacheService,
         $this->cashier
      );

      return $component;
   }
}