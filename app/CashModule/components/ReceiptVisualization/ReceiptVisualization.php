<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization;


use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModal;
use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModalFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\QuickAddReceiptItemModal\QuickAddReceiptItemModal;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\QuickAddReceiptItemModal\QuickAddReceiptItemModalFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModal;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModalFactory;
use App\Model\Entities\Product;
use App\Model\Entities\SellerData;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Utils\ArrayHash;

class ReceiptVisualization extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var CachedReceipt */
   private $cachedReceipt;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var AddEditReceiptItemModalFactory */
   private $addEditReceiptItemModalFactory;

   /** @var QuickAddReceiptItemModalFactory */
   private $quickAddReceiptItemModalFactory;

   /** @var RenameReceiptModalFactory */
   private $renameReceiptModalFactory;

   /** @var bool */
   private $readOnly = false;

   /** @var bool */
   private $overview = false;

   /** @var callable */
   public $onFinishSell = [];

   /** @var callable */
   public $onProductRemoved;

   /**
    * ReceiptVisualization constructor.
    * @param EntityManager $em
    * @param ReceiptCacheService $receiptCacheService
    * @param AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory
    * @param QuickAddReceiptItemModalFactory $quickAddReceiptItemModalFactory
    * @param RenameReceiptModalFactory $renameReceiptModalFactory
    * @param CachedReceipt $cachedReceipt
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory,
      QuickAddReceiptItemModalFactory $quickAddReceiptItemModalFactory,
      RenameReceiptModalFactory $renameReceiptModalFactory,
      CachedReceipt $cachedReceipt
   ) {
      $this->em = $em;
      $this->cachedReceipt = $cachedReceipt;
      $this->receiptCacheService = $receiptCacheService;
      $this->addEditReceiptItemModalFactory = $addEditReceiptItemModalFactory;
      $this->quickAddReceiptItemModalFactory = $quickAddReceiptItemModalFactory;
      $this->renameReceiptModalFactory = $renameReceiptModalFactory;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->readOnly = $this->readOnly;
      $tpl->overview = $this->overview;
      $tpl->receipt = $this->cachedReceipt;

      if (!isset($tpl->items)) {
         $tpl->items = array_reverse((array) $this->getItems());
      }

      return $tpl->render();
   }


   public function renderReadonly()
   {
      $this->readOnly = true;

      return $this->render();
   }


   public function renderOverview()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/overview.latte");
      $tpl->receipt = $this->cachedReceipt;
      $tpl->items = array_reverse((array) $this->getItems());

      return $tpl->render();
   }

   /****************************************************************************************************** components */
   /**
    * @return AddEditReceiptItemModal
    */
   public function createComponentAddEditReceiptItemModal() : AddEditReceiptItemModal
   {
      $comp = $this->addEditReceiptItemModalFactory->create($this->cachedReceipt);

      $comp->onSaved[] = function (CachedReceiptItem $item) use ($comp) {
         if($this->presenter->isAjax()) {
            $this->template->items = [];
            $this->template->items[$item->getId()] = $item;

            $this->redrawReceiptItemChanged();
         }

         $this->presenter->flashMessageNotify("Položka uložena");
         $comp->hide();
      };
      $comp->onCreated[] = function () use ($comp) {
         $this->redrawReceipt();
         $this->presenter->flashMessageNotify("Položka přidána");
         $comp->hide();
      };

      return $comp;
   }

   /**
    * @return QuickAddReceiptItemModal
    */
   public function createComponentQuickAddEditReceiptItemModal() : QuickAddReceiptItemModal
   {
      $comp = $this->quickAddReceiptItemModalFactory->create($this->cachedReceipt);

      $comp->onCreated[] = function () use ($comp) {
         $comp->hide();
         $this->redrawReceipt();
         $this->presenter->flashMessageNotify("Položka přidána");
      };

      return $comp;
   }


   /**
    * @return RenameReceiptModal
    */
   protected function createComponentRenameReceiptModal() : RenameReceiptModal
   {
      $comp = $this->renameReceiptModalFactory->create($this->cachedReceipt);

      $comp->onSaved[] = function () use ($comp) {
         $comp->hide();
         $this->redrawControl("receiptName");
         $this->presenter->flashMessageNotify("Uloženo");
      };

      return $comp;
   }

   /********************************************************************************************************* helpers */

   /**
    * @param Product $product
    */
   public function addItem(Product $product)
   {
      $this->cachedReceipt->addProduct($product, $this->isSellerTaxablePerson());
      $this->receiptCacheService->save();

      $this->redrawReceipt();
   }

   public function getItems() : ArrayHash
   {
      return $this->cachedReceipt->getItems();
   }

   public function getProduct($productId)
   {
      return $this->em->find(Product::class, $productId);
   }

   /**
    * Calculate items sum
    * @param bool $taxIncluded
    * @return float
    */
   public function getReceiptSum($taxIncluded = true) : float
   {
      return $this->cachedReceipt->getReceiptSum($taxIncluded);
   }

   /**
    * Calculate items sum
    * @return float
    */
   public function getTaxAmount() : float
   {
      return $this->cachedReceipt->getTaxAmount();
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   public function redrawReceiptItemChanged()
   {
      $this->redrawControl("table");
      $this->redrawControl("itemsContainer");
      $this->redrawControl("sum");
      $this->redrawControl("vat");
      $this->redrawControl("count");
      $this->redrawControl("finishButton");
   }

   public function redrawReceiptItemRemoved()
   {
      if($this->cachedReceipt->countItems() > 0) {
         $this->redrawControl("deleted"); //hide/remove item
      } else {
         $this->redrawControl("itemsContainer"); //show message "without items"
      }

      $this->redrawControl("table");
      $this->redrawControl("sum");
      $this->redrawControl("vat");
      $this->redrawControl("count");
      $this->redrawControl("finishButton");
   }

   public function redrawReceipt()
   {
      $this->redrawControl();
   }

   public function setReadOnly(bool $value = true)
   {
      $this->readOnly = $value;
   }

   private function receiptItemRemoved(CachedReceiptItem $removedItem)
   {
      if($removedItem->getProductId()) {
         $product = $this->em->find(Product::class, $removedItem->getProductId());

         $this->onProductRemoved($product);
      }
   }

   /********************************************************************************************************* signals */

   /**
    * @param $itemId
    */
   public function handleQuantityPlus($itemId)
   {
      if($this->readOnly) {
         return;
      }

      $this->cachedReceipt->incrementItemQuantity($itemId);
      $this->receiptCacheService->save($this->cachedReceipt);

      if($this->presenter->isAjax()) {
         $this->template->items = [];
         $this->template->items[] = $this->cachedReceipt->getItem($itemId);
      }

      $this->redrawReceiptItemChanged();
   }

   /**
    * @param $itemId
    */
   public function handleQuantityMinus($itemId)
   {
      if($this->readOnly) {
         return;
      }

      $itemTemp = $this->cachedReceipt->getItem($itemId);

      if(!$itemTemp) {
         return;
      }

      $this->cachedReceipt->decrementItemQuantity($itemId);
      $this->receiptCacheService->save($this->cachedReceipt);

      if($this->presenter->isAjax()) {
         //Has been deleted after decrement?
         if(!$this->cachedReceipt->getItem($itemId)) {
            $this->template->deleted = $itemId;
            $this->redrawReceiptItemRemoved();
            $this->receiptItemRemoved($itemTemp);

         } else {
            $this->template->items = [];
            $this->template->items[$itemId] = $this->cachedReceipt->getItem($itemId);
            $this->redrawReceiptItemChanged();
         }
      }
   }

   /**
    * @param $itemId
    */
   public function handleRemoveItem($itemId)
   {
      if($this->readOnly) {
         return;
      }

      $removedItem = $this->cachedReceipt->removeItem($itemId);

      if(!$removedItem) {
         return;
      }

      $this->receiptCacheService->save($this->cachedReceipt);

      if($this->presenter->isAjax()) {
         $this->template->deleted = $itemId;
         $this->redrawReceiptItemRemoved();
         $this->receiptItemRemoved($removedItem);
      }

      $this->presenter->flashMessageNotify("Položka smazána");
   }

   public function handleEditItem($itemId)
   {
      if($this->readOnly) {
         return;
      }

      /** @var AddEditReceiptItemModal $comp */
      $comp = $this['addEditReceiptItemModal'];

      $comp->show($itemId);
   }

   public function handleAddReceiptItem()
   {
      /** @var AddEditReceiptItemModal $comp */
      $comp = $this['addEditReceiptItemModal'];

      $comp->show();
   }

   public function handleQuickAddReceiptItem()
   {
      /** @var QuickAddReceiptItemModal $comp */
      $comp = $this['quickAddEditReceiptItemModal'];

      $comp->show();
   }

   public function handleRenameReceipt()
   {
      /** @var RenameReceiptModal $comp */
      $comp = $this['renameReceiptModal'];

      $comp->show();
   }

   public function handleFinishSell()
   {
      $this->onFinishSell();
   }
}