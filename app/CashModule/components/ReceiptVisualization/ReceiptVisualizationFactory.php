<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization;


use App\CashModule\Components\AddEditReceiptItemModal\AddEditReceiptItemModalFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\QuickAddReceiptItemModal\QuickAddReceiptItemModalFactory;
use App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal\RenameReceiptModalFactory;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class ReceiptVisualizationFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var AddEditReceiptItemModalFactory */
   private $addEditReceiptItemModalFactory;

   /** @var QuickAddReceiptItemModalFactory */
   private $quickAddReceiptItemModalFactory;

   /** @var RenameReceiptModalFactory */
   private $renameReceiptModalFactory;

   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      AddEditReceiptItemModalFactory $addEditReceiptItemModalFactory,
      QuickAddReceiptItemModalFactory $quickAddReceiptItemModalFactory,
      RenameReceiptModalFactory $renameReceiptModalFactory
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->addEditReceiptItemModalFactory = $addEditReceiptItemModalFactory;
      $this->quickAddReceiptItemModalFactory = $quickAddReceiptItemModalFactory;
      $this->renameReceiptModalFactory = $renameReceiptModalFactory;
   }

   public function create(CachedReceipt $receipt = null) : ReceiptVisualization
   {
      $component = new ReceiptVisualization(
         $this->em,
         $this->receiptCacheService,
         $this->addEditReceiptItemModalFactory,
         $this->quickAddReceiptItemModalFactory,
         $this->renameReceiptModalFactory,
         $receipt
      );

      return $component;
   }
}