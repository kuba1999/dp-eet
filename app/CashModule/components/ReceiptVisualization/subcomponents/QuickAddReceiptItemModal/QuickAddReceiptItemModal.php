<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization\Subcomponents\QuickAddReceiptItemModal;


use App\Components\BaseControl;
use App\Forms\BaseForm;
use App\Model\Entities\SellerData;
use App\Model\Entities\Settings;
use App\Model\Entities\SettingsGlobal;
use App\Model\Entities\TaxRate;
use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\TaxRateRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class QuickAddReceiptItemModal extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var CachedReceipt */
   private $cachedReceipt;

   /** @var SettingsRepository */
   private $settingsRepo;

   /** @var bool @persistent */
   public $show = false;

   /** @var callable */
   public $onCreated;

   /**
    * @param EntityManager $em
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      CachedReceipt $cachedReceipt
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->cachedReceipt = $cachedReceipt;

      $this->settingsRepo = $this->em->getRepository(SettingsGlobal::class);
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addPrice("price", $this->isSellerTaxablePerson() ? "Cena vč. DPH":"Cena")
         ->setRequired();

      $form->addText("quantity", "Množství")
         ->setDefaultValue(1)
         ->setRequired();

      if($this->isSellerTaxablePerson()) {
         $form->addSelect("taxRate", "Sazba DPH", $this->getTaxRates())
            ->setPrompt("vyberte")
            ->setRequired();
      }

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->isSellerTaxablePerson()) {
         $taxRate = $this->getTaxRate( $form['taxRate']->getValue() );

         $price = PriceHelper::calculatePriceWithoutVat($taxRate, $form['price']->getValue(), false);

         $taxRateId = $taxRate->getId();
         $taxRateValue = $taxRate->getValue();
         $taxRateType = $taxRate->getType();
      } else {
         $price = $form['price']->getValue();
         $taxRateId = null;
         $taxRateValue = null;
         $taxRateType = TaxRateType::get(TaxRateType::_NONE_);
      }

      $item = new CachedReceiptItem(
         $this->cachedReceipt,
         null,
         "Nepojmenovaná položka",
         $form['quantity']->getValue(),
         $price
      );

      $item->setTaxRateId($taxRateId);
      $item->setTaxRateValue($taxRateValue);
      $item->setTaxRateType($taxRateType);

      $this->cachedReceipt->addItem($item);
      $this->receiptCacheService->save($this->cachedReceipt);

      $this->onCreated($item);
   }

   /********************************************************************************************************* helpers */
   public function show()
   {
      $this->show = true;

      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;

      $this->redrawControl();
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * Get all vat rates
    * @return array
    */
   private function getTaxRates() : array
   {
      /** @var TaxRateRepository $vatRepo */
      $vatRepo = $this->em->getRepository(TaxRate::class);

      return $vatRepo->getAllForSelectBox();
   }


   private function getTaxRate(int $taxRateId) : TaxRate
   {
      /** @var TaxRate $taxRate */
      $taxRate = $this->em->find(TaxRate::class, $taxRateId);

      return $taxRate;
   }


   /********************************************************************************************************** render */
   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}