<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization\Subcomponents\QuickAddReceiptItemModal;


use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class QuickAddReceiptItemModalFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
   }

   public function create(CachedReceipt $cachedReceipt) : QuickAddReceiptItemModal
   {
      $component = new QuickAddReceiptItemModal(
         $this->em,
         $this->receiptCacheService,
         $cachedReceipt
      );

      return $component;
   }
}