<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal;


use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class RenameReceiptModalFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
   }

   public function create(CachedReceipt $cachedReceipt) : RenameReceiptModal
   {
      $component = new RenameReceiptModal(
         $this->em,
         $this->receiptCacheService,
         $cachedReceipt
      );

      return $component;
   }
}