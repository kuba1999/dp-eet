<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptVisualization\Subcomponents\RenameReceiptModal;


use App\Components\BaseControl;
use App\Forms\BaseForm;
use App\Model\Entities\Settings;
use App\Model\Entities\TaxRate;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\TaxRateRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class RenameReceiptModal extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var CachedReceipt */
   private $cachedReceipt;

   /** @var bool @persistent */
   public $show = false;

   /** @var callable */
   public $onSaved;

   /**
    * RenameReceiptModal constructor.
    * @param EntityManager $em
    * @param ReceiptCacheService $receiptCacheService
    * @param CachedReceipt $cachedReceipt
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      CachedReceipt $cachedReceipt
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->cachedReceipt = $cachedReceipt;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("name", "Název")
         ->setDefaultValue($this->cachedReceipt->getName())
         ->setRequired(false);

      $form->addSubmit("submit", "Uložit");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $this->cachedReceipt->setName($form['name']->getValue());

      $this->receiptCacheService->save($this->cachedReceipt);
      $this->onSaved();
   }

   /********************************************************************************************************* helpers */
   public function show()
   {
      $this->show = true;

      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;

      $this->redrawControl();
   }

   /********************************************************************************************************** render */
   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}