<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ProductSelect;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Model\Services\ProductSuggesterService;
use Kdyby\Doctrine\EntityManager;

class ProductSelectFactory
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationSchemeFactory;

   /** @var ProductSuggesterService */
   private $productSuggesterService;

   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      ProductSuggesterService $productSuggesterService
   ) {
      $this->em = $em;
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
      $this->productSuggesterService = $productSuggesterService;
   }

   /**
    * @return GridView
    */
   public function createGridView() : GridView
   {
      $component = new GridView(
         $this->em,
         $this->categoryLocationSchemeFactory,
         $this->productSuggesterService
      );

      return $component;
   }

   /**
    * @return TableView
    */
   public function createTableView() : TableView
   {
      $component = new TableView(
         $this->em,
         $this->categoryLocationSchemeFactory,
         $this->productSuggesterService
      );

      return $component;
   }
}