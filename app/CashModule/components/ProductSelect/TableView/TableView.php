<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ProductSelect;

use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationScheme;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Forms\BaseForm;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;
use App\Model\Helpers\BarcodeHelper;
use App\Model\Repositories\ProductCategoryRepository;
use App\Model\Repositories\ProductRepository;
use App\Model\Services\ProductSuggesterService;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Controls\SubmitButton;

class TableView extends ProductSelect
{
   /** @var CategoryLocationSchemeFactory */
   protected $categoryLocationSchemeFactory;

   /**
    * ID of actual category
    * @var int @persistent
    */
   public $categoryId = null;

   /** @var string @persistent */
   public $searchedTerm = null;

   /**
    * GridView constructor.
    * @param EntityManager $em
    * @param CategoryLocationSchemeFactory $categoryLocationSchemeFactory
    * @param ProductSuggesterService $productSuggesterService
    */
   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      ProductSuggesterService $productSuggesterService
   ) {
      parent::__construct($em, $productSuggesterService);
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
   }

   /************************************************************************************************************ Form */
   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax spinner ajax-history";

      $form->addText("searchedTerm")
         ->setAttribute("placeholder", "Hledat")
         ->setValue($this->searchedTerm);

      $form->addSubmit("search")->onClick[] = [$this, "formSearch"];

      return $form;
   }

   public function formSearch(SubmitButton $button)
   {
      $form = $button->getForm();
      $searchedTerm = $form['searchedTerm']->getValue();

      $this->searchedTerm = $searchedTerm;
      $this->redrawControl();
   }

   /********************************************************************************************************* Signals */
   public function handleChangeCategory($categoryId)
   {
      $this->changeCategory($categoryId);
   }

   public function handleUp()
   {
      if($this->getActualCategory()->getParent()) {
         $categoryId = $this->getActualCategory()->getParent()->getId();
      } else {
         $categoryId = null;
      }

      $this->changeCategory($categoryId);
   }

   public function handleResetSearching()
   {
      $this->searchedTerm = null;
      $this['form']["searchedTerm"]->setValue(null);
      $this->redrawControl();
   }

   /**
    * Scanned code by barcode scanner
    * @param string $code
    */
   public function handleBarcodeScanned(string $code)
   {
      if(BarcodeHelper::isEanValid($code) &&
         ($count = $this->productRepo->countBy(["ean" => $code]))
      ) {
         if($count === 1) {
            /** @var Product $product */
            $product = $this->productRepo->findOneBy(["ean" => $code]);

            if(isset($this->sessionSection->products[$product->getId()])) {
               return; //product is already shown => duplicity
            }


            $this->handleSelectProduct($product->getId());
            return;
         }
      }

      $this['form']["searchedTerm"]->setValue($code); //try search if EAN does not exist
      $this->searchedTerm = $code;
      $this->redrawControl();
   }


   /********************************************************************************************************** Helpers*/
   /**
    * @return array
    */
   public function getCategories() : array
   {
      if(!empty($this->searchedTerm)) { //if searching is active
         $qb = $this->productCategoryRepo->searchCategories($this->searchedTerm);
      } else {
         $qb = $this->productCategoryRepo->getAll();
      }

      if($this->getActualCategory()) { //get products in root
         $qb->andWhere("c.parent = :parent")
            ->setParameter("parent", $this->getActualCategory());
      } else {
         $qb->andWhere("c.parent IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * @return array
    */
   public function getProducts() : array
   {
      if(!empty($this->searchedTerm)) { //if searching is active
         $qb = $this->productRepo->searchProducts($this->searchedTerm);
      } else {
         $qb = $this->productRepo->getAll();
      }

      if($this->getActualCategory()) { //get categories in root
         $qb->andWhere("p.category = :category")
            ->setParameter("category", $this->getActualCategory());
      } else {
         $qb->andWhere("p.category IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * Search products and categories
    * searching is also executed in subcategories
    * @return array
    */
   public function searchProductsAndCategories()
   {
      /** @var Product[] $products */
      $products = $this->productRepo->searchProducts($this->searchedTerm, $this->getActualCategory())->getQuery()->getResult();
      $ret = [];
      foreach ($products AS $product) {
         if(is_null($product->getCategory())) {
            if(!isset($ret[""])) {
               $ret[null] = [
                  "location"      => null,
                  "sort"          => null,
                  "category"      => null
               ];
            }

            $ret[null]['products'][] = $product;
            continue;
         }

         /** @var string[] $location */
         $location = $this->productCategoryRepo->getNamedCategoryLocation($product->getCategory());

         if($this->getActualCategory()) {
            unset($location[$this->getActualCategory()->getId()]); //unset actual category name
         }

         if(!isset($ret[implode(",", array_keys($location))])) {
            $ret[implode(",", array_keys($location))] = [
               "location"      => $location,
               "sort"          => str_replace(" ", "", implode("", $location)),
               "category"      => $product->getCategory()
            ];
         }

         $ret[implode(",", array_keys($location))]['products'][] = $product;
      }

      //sort products
      foreach ($ret AS $key=>$item) {
         usort($ret[$key]['products'], function (Product $a,Product $b) {
            return ($a->getName() > $b->getName())  ? +1 : -1;
         });
      }

      /** @var ProductCategory[] $categories */
      $categories = $this->productCategoryRepo->searchCategories($this->searchedTerm, $this->getActualCategory())->getQuery()->getResult();

      foreach ($categories AS $category) {
         /** @var string[] $location */
         $location = $this->productCategoryRepo->getNamedCategoryLocation($category);

         if($this->getActualCategory()) {
            unset($location[$this->getActualCategory()->getId()]); //unset actual category name
         }

         if(!isset($ret[implode(",", array_keys($location))])) {
            $ret[implode(",", array_keys($location))] = [
               "location"      => $location,
               "sort"          => str_replace(" ", "", implode("", $location)),
               "category"      => $category,
            ];
         }
      }

      //sort categories
      usort($ret, function ($a,$b) {
         return $a['sort'] > $b['sort']  ? +1 : -1;
      });

      return $ret;
   }

   /**
    * @return ProductCategory|null
    */
   public function getActualCategory() : ?ProductCategory
   {
      if(is_null($this->categoryId)) { //root
         return null;
      }

      /** @var ProductCategory $category */
      $category = $this->productCategoryRepo->find($this->categoryId);

      return $category;
   }

   /**
    * @param int|null $categoryId
    */
   public function changeCategory(int $categoryId = null)
   {
      $this->categoryId = $categoryId;

      $this->redrawControl();
      $this['categoryLocationScheme']->redrawControl();
   }

   /**
    * Shows where is subcategory/product located
    * @return CategoryLocationScheme
    */
   public function createComponentCategoryLocationScheme() : CategoryLocationScheme
   {
      $comp = $this->categoryLocationSchemeFactory->create($this->getActualCategory());

      $comp->setItemLink(function(ProductCategory $category = null){
         if($category) {
            return $this->link("changeCategory!", $category->getId());
         } else {
            return $this->link("changeCategory!", null);
         }
      });

      $comp->setItemLinkClass("ajax ajax-history spinner");

      return $comp;
   }

   public function getColspanValue() : int
   {
      $colspan = 7;

      if($this->isSellerTaxablePerson()) {
         $colspan++;
      }

      if($this->isWarehouseEnabled()) {
         $colspan++;
      }

      return $colspan;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}