<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ProductSelect;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Components\BaseControl;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;
use App\Model\Entities\SellerData;
use App\Model\Entities\Settings;
use App\Model\Entities\SettingsGlobal;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\ProductCategoryRepository;
use App\Model\Repositories\ProductRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Services\ProductSuggesterService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use Kdyby\Doctrine\EntityManager;

abstract class ProductSelect extends BaseControl
{
   /** @var EntityManager */
   protected $_em;

   /** @var ProductRepository */
   protected $productRepo;

   /** @var ProductCategoryRepository */
   protected $productCategoryRepo;

   /** @var CategoryLocationSchemeFactory */
   protected $categoryLocationSchemeFactory;

   /** @var SettingsRepository */
   private $settingsRepo;

   /** @var ProductSuggesterService */
   private $productSuggesterService;

   /** @var callable */
   public $onProductSelected = [];

   /** @var callable */
   public $isProductInReceiptCallback = [];

   /** @var Product[] */
   private $suggestedProducts = [];

   /**
    * @param EntityManager $em
    */
   function __construct(
      EntityManager $em,
      ProductSuggesterService $productSuggesterService
   ) {
      $this->_em = $em;
      $this->productSuggesterService = $productSuggesterService;

      $this->productRepo = $this->_em->getRepository(Product::class);
      $this->productCategoryRepo = $this->_em->getRepository(ProductCategory::class);
      $this->settingsRepo = $this->_em->getRepository(SettingsGlobal::class);
   }

   /**
    * @param Product $product
    * @return float
    * @throws \Exception
    */
   public function getUnitPriceWithVat(Product $product) : float
   {
      $unitPrice = PriceHelper::calculatePriceWithVat($product->getTaxRate(), $product->getPrice());

      return $unitPrice;
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->_em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * @return bool
    */
   public function isWarehouseEnabled() : bool
   {
      return $this->settingsRepo->isWarehouseEnabled();
   }

   /**
    * @param Product $product
    * @return bool
    */
   public function isProductOnReceipt(Product $product) : bool
   {
      if(is_callable($this->isProductInReceiptCallback)) {
         return call_user_func($this->isProductInReceiptCallback, $product);
      }

      return false;
   }

   /**
    * @param Product $product
    */
   public function productRemovedFromReceipt(Product $product)
   {
      if($this->presenter->isAjax()) {
         $this->template->redrawProduct = $product;
         $this->redrawControl("content");
      }
   }

   public function suggestProducts(CachedReceipt $cachedReceipt)
   {
      $products = $this->productSuggesterService->suggest($cachedReceipt);
      $this->suggestedProducts = $products;
      $this->redrawControl("suggested");
   }

   public function getSuggestedProducts() : array
   {
      return $this->suggestedProducts;
   }

   /******************************************************************************************************** handlers */
   /**
    * @param int $productId
    */
   public function handleSelectProduct(int $productId)
   {
      /** @var Product $product */
      $product = $this->_em->find(Product::class, $productId);

      if($this->presenter->isAjax() && !$this->isProductOnReceipt($product)) {
         $this->template->redrawProduct = $product;
         $this->redrawControl("content");
      }

      $this->onProductSelected($product);
   }
}