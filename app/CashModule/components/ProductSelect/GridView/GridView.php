<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ProductSelect;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationScheme;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;
use App\Model\Services\ProductSuggesterService;
use Doctrine\ORM\EntityManager;

class GridView extends ProductSelect
{
   /** @var CategoryLocationSchemeFactory */
   protected $categoryLocationSchemeFactory;

   /**
    * ID of actual category
    * @var int @persistent
    */
   public $categoryId = null;

   /**
    * GridView constructor.
    * @param EntityManager $em
    * @param CategoryLocationSchemeFactory $categoryLocationSchemeFactory
    * @param ProductSuggesterService $productSuggesterService
    */
   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      ProductSuggesterService $productSuggesterService
   ) {
      parent::__construct($em, $productSuggesterService);
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
   }


   /******************************************************************************************************** handlers */

   /**
    * @param int $categoryId
    */
   public function handleOpenCategory(int $categoryId = null)
   {
      $this->categoryId = $categoryId;

      $this->redrawControl("items");
      $this['categoryLocationScheme']->redrawControl();
   }

   /****************************************************************************************************** components */

   /**
    * Shows where is subcategory/product located
    * @return CategoryLocationScheme
    */
   public function createComponentCategoryLocationScheme() : CategoryLocationScheme
   {
      $comp = $this->categoryLocationSchemeFactory->create($this->getActualCategory());

      $comp->setItemLink(function(ProductCategory $category = null){
         if($category) {
            return $this->link("openCategory!", $category->getId());
         } else {
            return $this->link("openCategory!", null);
         }
      });

      $comp->setItemLinkClass("ajax ajax-history spinner");

      return $comp;
   }

   /********************************************************************************************************* helpers */

   /**
    * @return array
    */
   public function getCategories() : array
   {
      $qb = $this->productCategoryRepo->getAll();

      if($this->getActualCategory()) { //get products in root
         $qb->andWhere("c.parent = :parent")
            ->setParameter("parent", $this->getActualCategory());
      } else {
         $qb->andWhere("c.parent IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * @return array
    */
   public function getProducts() : array
   {
      $qb = $this->productRepo->getAll();

      if($this->getActualCategory()) { //get categories in root
         $qb->andWhere("p.category = :category")
            ->setParameter("category", $this->getActualCategory());
      } else {
         $qb->andWhere("p.category IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * @return ProductCategory|null
    */
   public function getActualCategory() : ?ProductCategory
   {
      if(is_null($this->categoryId)) { //root
         return null;
      }

      /** @var ProductCategory|null $category */
      $category = $this->productCategoryRepo->find($this->categoryId);

      return $category;
   }


   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}