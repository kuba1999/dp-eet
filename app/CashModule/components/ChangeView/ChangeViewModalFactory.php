<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ChangeView;


use App\Model\Services\CashierCacheService\CashierCacheService;

class ChangeViewModalFactory
{
   /** @var CashierCacheService */
   private $cashierCacheService;

   function __construct(CashierCacheService $cashierCacheService)
   {
      $this->cashierCacheService = $cashierCacheService;
   }

   public function create() : ChangeViewModal
   {
      $component = new ChangeViewModal(
         $this->cashierCacheService
      );

      return $component;
   }
}