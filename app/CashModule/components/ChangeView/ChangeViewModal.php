<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ChangeView;


use App\Model\Enums\CashView;
use App\Model\Services\CashierCacheService\CashierCacheService;
use Nette\Application\UI\Control;

class ChangeViewModal extends Control
{
   /** @var CashierCacheService */
   private $cashierCacheService;

   /** @var callable */
   public $onSelected;

   /** @var bool @persistent */
   public $show = false;

   /**
    * ChangeViewModal constructor.
    * @param CashierCacheService $cashierCacheService
    */
   function __construct(CashierCacheService $cashierCacheService)
   {
      $this->cashierCacheService = $cashierCacheService;
   }

   public function isViewSelected(int $view) : bool
   {
      return $this->cashierCacheService->getCashierCashSettings()->getSelectedView()->equalsValue($view);
   }

   public function handleChangeView(int $view)
   {
      $cashView = CashView::get($view);

      $this->cashierCacheService->getCashierCashSettings()->setSelectedView($cashView);
      $this->cashierCacheService->saveCashierCashSettings();
      $this->redrawControl("content");
      $this->onSelected();
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}