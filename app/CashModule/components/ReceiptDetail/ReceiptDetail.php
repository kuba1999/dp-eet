<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\CashModule\Components\ReceiptLogRecords\ReceiptLogRecords;
use App\CashModule\Components\ReceiptLogRecords\ReceiptLogRecordsFactory;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialog;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialogFactory;
use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Model\Entities\Receipt;
use App\Model\Entities\SettingsEet;
use App\Model\Entities\User;
use App\Model\Enums\TypeOfDocument;
use App\Model\Enums\UserRole;
use App\Model\Repositories\SettingsRepository;
use App\Model\Services\EETService;
use App\Model\Services\PDFGeneratorService\PDFGeneratorServiceFactory;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\ForbiddenRequestException;

class ReceiptDetail extends BaseControl
{
   const VIEW_CASHIER   = 0;
   const VIEW_CUSTOMER  = 1;

   /** @var EntityManager */
   private $em;

   /** @var Receipt */
   private $receipt;

   /** @var EETService */
   private $EETService;

   /** @var PDFGeneratorServiceFactory */
   private $PDFGeneratorServiceFactory;

   /** @var SendReceiptModalDialogFactory */
   private $sendReceiptModalDialogFactory;

   /** @var ReceiptLogRecordsFactory */
   private $receiptLogRecordsFactory;

   /** @var int */
   public $view = self::VIEW_CASHIER;

   public $onReceiptCancellation = [];

   public $onCreateTicket = [];

   /**
    * ReceiptDetail constructor.
    * @param EntityManager $em
    */
   public function __construct(
      EntityManager $em,
      EETService $EETService,
      PDFGeneratorServiceFactory $PDFGeneratorServiceFactory,
      SendReceiptModalDialogFactory $sendReceiptModalDialogFactory,
      ReceiptLogRecordsFactory $receiptLogRecordsFactory,
      Receipt $receipt
   ) {
      $this->em = $em;
      $this->receipt = $receipt;
      $this->EETService = $EETService;
      $this->PDFGeneratorServiceFactory = $PDFGeneratorServiceFactory;
      $this->sendReceiptModalDialogFactory = $sendReceiptModalDialogFactory;
      $this->receiptLogRecordsFactory = $receiptLogRecordsFactory;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . ($this->view === self::VIEW_CASHIER ? "/templateCashier.latte" : "/templateCustomer.latte"));
      $tpl->receipt = $this->receipt;
      $tpl->seller = $this->receipt->getSellerData();
      $tpl->typeOfDocument = $this->getTypeOfDocument();
      $tpl->customerData = $this->receipt->getCustomerData();

      if($this->receipt->getEetData()) {
         $tpl->pkp = $this->EETService->generatePKPCode($this->receipt);
         $tpl->bkp = $this->EETService->generateBKPCode($this->receipt);
      }

      return $tpl->render();
   }

   ///////////////////////////////////////////////////////////////////// assign customer
   protected function createComponentAssignCustomer() : AssignCustomerModal
   {
      $this->checkPrivileges();

      $comp = new AssignCustomerModal($this->em, $this->receipt);
      $comp->onSaved[] = function () use ($comp) {
         $this->presenter->flashMessageNotify("Uloženo", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();
         $this->redrawControl("box-general");
         $this->redrawControl("box-customer-details");

         if(!$this->presenter->isAjax()) {
            $this->redirect("this");
         }
      };

      return $comp;
   }

   public function handleAssignCustomer()
   {
      $this['assignCustomer']->show();
   }

   ///////////////////////////////////////////////////////////////////// EET
   public function handleProcessEet()
   {
      $this->checkPrivileges();

      if(!$this->isEetEnabled()) {
         $this->presenter->flashMessageNotify(
            "EET není zapnuto",
            FlashMessageNotify::TYPE_DANGER
         );

         return;
      }

      if($this->EETService->send($this->receipt)) {
         $this->presenter->flashMessageNotify(
            "Úspěšně zaevidováno",
            FlashMessageNotify::TYPE_SUCCESS
         );
      } else {
         $this->presenter->flashMessageNotify(
            "Nastala chyba",
            FlashMessageNotify::TYPE_WARNING
         );
      }

      $this->redrawControl("box-eet");

      if(!$this->presenter->isAjax()) {
         $this->redirect("this");
      }
   }

   ///////////////////////////////////////////////////////////////////// Load customer data
   public function handleLoadCustomerDataFromAccount()
   {
      $this->checkPrivileges();

      if(!$this->receipt->getCustomer()) {
         return;
      }

      $customerData = $this->receipt->getCustomerData();
      $customer = $this->receipt->getCustomer();

      if($customer->getCompany()) {
         $customerData->setName($customer->getName() . " (" . $customer->getCompany() . ")");
      } else {
         $customerData->setName($customer->getName());
      }

      $customerData->setStreet($customer->getStreet());
      $customerData->setCity($customer->getCity());
      $customerData->setZip($customer->getZip());
      $customerData->setPhone($customer->getPhone());
      $customerData->setEmail($customer->getEmail());
      $customerData->setVatId($customer->getVatId());
      $customerData->setVatNo($customer->getVatNo());

      $this->em->flush();

      $this->redrawControl("box-customer-details");

      if(!$this->presenter->isAjax()) {
         $this->redirect("this");
      }
   }

   ///////////////////////////////////////////////////////////////////// change customer data
   protected function createComponentChangeCustomerData() : ChangeCustomerData
   {
      $this->checkPrivileges();

      $comp = new ChangeCustomerData($this->em, $this->receipt);
      $comp->onSaved[] = function () use ($comp) {
         $this->presenter->flashMessageNotify("Uloženo", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();
         $this->redrawControl("box-customer-details");

         if(!$this->presenter->isAjax()) {
            $this->redirect("this");
         }
      };

      return $comp;
   }

   public function handleChangeCustomerData()
   {
      $this['changeCustomerData']->show();
   }

   ///////////////////////////////////////////////////////////////////// send receipt
   protected function createComponentSendReceiptModalDialog() : SendReceiptModalDialog
   {
      $comp = $this->sendReceiptModalDialogFactory->create();
      $comp->onSent[] = function () use ($comp) {
         $this->presenter->flashMessageNotify("Odesláno", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();

         if(!$this->presenter->isAjax()) {
            $this->redirect("this");
         }
      };

      return $comp;
   }

   public function handleSendReceipt()
   {
      $this['sendReceiptModalDialog']->show($this->receipt);
   }

   ///////////////////////////////////////////////////////////////////// receipt link receipt
   protected function createComponentShowReceiptLink() : ShowReceiptLink
   {
      return new ShowReceiptLink($this->receipt);
   }

   public function handleShowReceiptLink()
   {
      $this['showReceiptLink']->show();
   }

   ///////////////////////////////////////////////////////////////////// cancel receipt
   protected function createComponentCancelReceiptDialog() : CancelReceiptDialog
   {
      $comp =  new CancelReceiptDialog($this->em, $this->receipt);

      $comp->onSuccess[] = function (Receipt $receipt) use ($comp) {
         $this->presenter->flashMessageNotify("Storno provedeno", FlashMessageNotify::TYPE_SUCCESS);

         //send to EET
         if($this->isEetEnabled()) {
            if($this->EETService->send($receipt)) {
               $this->presenter->flashMessageNotify(
                  "Úspěšně zaevidováno",
                  FlashMessageNotify::TYPE_INFO,
                  "EET"
               );
            } else {
               $this->presenter->flashMessageNotify(
                  "Nastala chyba - bude proveden další pokus o zaevidování",
                  FlashMessageNotify::TYPE_WARNING,
                  "EET"
               );
            }
         }

         $this->onReceiptCancellation($receipt);
      };

      $comp->hide();

      return $comp;
   }

   public function handleCancelReceiptDialog()
   {
      $this['cancelReceiptDialog']->show();
   }

   public function isPossibleToMakeCancellation() : bool
   {
      return !in_array(
         $this->receipt->getTypeOfDocument()->getValue(),
         [
            TypeOfDocument::CANCELLATION,
            TypeOfDocument::PRO_FORMA_INVOICE,
         ]
      );
   }

   ///////////////////////////////////////////////////////////////////// receipt buttons
   public function handlePrintReceipt()
   {
      $generator = $this->PDFGeneratorServiceFactory->receipt($this->receipt);

      $generator->show(true);
   }

   public function handleDownloadReceipt()
   {
      $generator = $this->PDFGeneratorServiceFactory->receipt($this->receipt);

      $generator->download();
   }

   public function handleCreateTicket()
   {
      $this->onCreateTicket($this->receipt);
   }

   ///////////////////////////////////////////////////////////////////// log
   protected function createComponentLog() : ReceiptLogRecords
   {
      return $this->receiptLogRecordsFactory->create($this->receipt);
   }

   ///////////////////////////////////////////////////////////////////// helpers

   public function isSellerTaxablePerson() : bool
   {
      return $this->receipt->getSellerData()->isTaxablePerson();
   }

   private function getTypeOfDocument() : string
   {
      return TypeOfDocument::getName($this->receipt->getTypeOfDocument());
   }

   /**
    * Some operations are only allowed to ADMIN and CUSTOMER role
    * @param bool $throwException
    * @return bool
    * @throws ForbiddenRequestException
    */
   public function checkPrivileges($throwException = false) : bool
   {
      /** @var User $loggedUser */
      $loggedUser = $this->presenter->getLoggedUser();

      if(!$loggedUser || !UserRole::isUserOfBackend($loggedUser)) {
         if($throwException) {
            throw new ForbiddenRequestException();
         }

         return false;
      }

      return true;
   }

   private function isEetEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsEet::class);

      return $settingsRepo->isEetEnabled();
   }
}