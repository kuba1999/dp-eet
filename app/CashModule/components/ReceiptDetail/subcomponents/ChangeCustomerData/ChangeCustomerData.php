<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;
use Nette\Http\FileUpload;

class ChangeCustomerData extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSaved;

   /** @var bool @persistent */
   public $show = false;

   /** @var Receipt */
   private $receipt;

   /** @var UserRepository */
   private $userRepo;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em, Receipt $receipt)
   {
      $this->em = $em;
      $this->receipt = $receipt;

      $this->userRepo = $this->em->getRepository(User::class);
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("name", "Jméno / firma")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getName())
         ->setMaxLength(256);
      $form->addText("street", "Ulice a číslo")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getStreet())
         ->setMaxLength(256);
      $form->addText("city", "Město")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getCity())
         ->setMaxLength(256);
      $form->addText("zip", "PSČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getZip())
         ->setMaxLength(256);
      $form->addText("vatNo", "IČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getVatNo())
         ->setMaxLength(256);
      $form->addText("vatId", "DIČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getVatId())
         ->setMaxLength(256);
      $form->addText("phone", "Telefon")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getPhone())
         ->setMaxLength(256);
      $form->addText("email", "Email")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getEmail())
         ->setMaxLength(256)
         ->addCondition(BaseForm::FILLED)
         ->addRule(BaseForm::EMAIL);

      $form->addSubmit("save", "Uložit");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      $customerData = $this->receipt->getCustomerData();

      $customerData->setName($form['name']->getValue());
      $customerData->setStreet($form['street']->getValue());
      $customerData->setCity($form['city']->getValue());
      $customerData->setZip($form['zip']->getValue());
      $customerData->setVatNo($form['vatNo']->getValue());
      $customerData->setVatId($form['vatId']->getValue());
      $customerData->setEmail($form['email']->getValue());
      $customerData->setPhone($form['phone']->getValue());

      $this->em->flush($customerData);
      $this->onSaved();
   }


   /**
    * Request for getting data from ARES
    * @param string|null $vatNo
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->flashMessage("Načtení dat proběhlo úspěšně");
      }
      catch (UnknownVatNoException $e) {
         $this->flashMessage("Neexistující IČ", FlashMessage::TYPE_WARNING);
      }
      catch (UnableToConnect $e) {
         $this->flashMessage("Nepodařilo se spojit s portálem ARES", FlashMessage::TYPE_WARNING);
      }
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}