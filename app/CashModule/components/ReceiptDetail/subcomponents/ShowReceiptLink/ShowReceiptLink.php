<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;
use Nette\Http\FileUpload;

class ShowReceiptLink extends BaseControl
{
   /** @var bool @persistent */
   public $show = false;

   /** @var Receipt */
   private $receipt;

   /**
    * ShowReceiptLink constructor.
    * @param Receipt $receipt
    */
   function __construct(Receipt $receipt)
   {
      $this->receipt = $receipt;
   }

   public function getLink() : string
   {
      $link = $this->presenter->link(
         "//:Portal:ReceiptToken:default",
         [
            $this->receipt->getToken(),
            "backLink" => null
         ]
      );

      return $link;
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}