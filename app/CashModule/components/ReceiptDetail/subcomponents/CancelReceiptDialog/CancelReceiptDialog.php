<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\SellerData;
use App\Model\Entities\SettingsGlobal;
use App\Model\Entities\StockMovement;
use App\Model\Entities\User;
use App\Model\Enums\StockMovementType;
use App\Model\Enums\TypeOfDocument;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;
use Nette\Http\FileUpload;

class CancelReceiptDialog extends BaseControl
{
   /** @var bool @persistent */
   public $show = false;

   /** @var Receipt */
   private $receipt;

   /** @var EntityManager */
   private $em;

   public $onSuccess = [];

   /**
    * ShowReceiptLink constructor.
    * @param Receipt $receipt
    */
   function __construct(EntityManager $em, Receipt $receipt)
   {
      $this->em = $em;
      $this->receipt = $receipt;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      if($this->isWarehouseEnabled()) {
         $form->addCheckbox("stockIn", "zpětně naskladnit produkty z dokladu")
            ->setDefaultValue(true);
      }

      $form->addSubmit("makeCancel");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->isWarehouseEnabled() && $form['stockIn']->getValue()) {
         $this->makeStockMovement();
      }

      $receipt = $this->generateReceiptForCancellation();

      //attach receipt
      //$this->receipt->setAttachedReceipt($receipt);

      /** @var ReceiptRepository $receiptRepo */
      $receiptRepo = $this->em->getRepository(Receipt::class);

      $this->em->beginTransaction();
      $receiptRepo->generateDocumentId($receipt);

      $this->em->flush();
      $this->em->commit();

      $this->onSuccess($receipt);
   }

   private function generateReceiptForCancellation() : Receipt
   {
      $receipt = new Receipt(
         TypeOfDocument::get(TypeOfDocument::CANCELLATION),
         $this->receipt->getCash(),
         $this->presenter->getLoggedUser(),
         $this->getSellerData()
      );
      $this->em->persist($receipt);

      //add items
      foreach ($this->receipt->getItems() AS $item) {
         $i = $receipt->addItem(
            $item->getName(),
            $item->getUnitPriceWithTax(),
            $item->getQuantity() * -1
         );

         $i->setProduct($item->getProduct());
         $i->setTaxRateValue($item->getTaxRateValue());
         $i->setTaxRateType($item->getTaxRateType());
         $i->setNote($item->getNote());
         $i->setMeasurementUnit($item->getMeasurementUnit());

         $this->em->persist($i);
      }

      return $receipt;
   }

   private function makeStockMovement()
   {
      //make stock taking
      $stockMovement = new StockMovement(
         StockMovementType::get(StockMovementType::STOCK_TAKING),
         $this->presenter->getLoggedUser()
      );

      foreach ($this->receipt->getItems() AS $item) {
         if($item->getProduct()) { //insert only items with existing association with product
            //item
            $stockItem = $stockMovement->addItem(
               $item->getProduct(),
               $item->getQuantity()
            );

            $this->em->persist($stockItem);
         }
      }

      if(count($stockMovement->getItems())) {
         $this->em->persist($stockMovement);
      }
   }

   /**
    * @return bool
    */
   public function isWarehouseEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsGlobal::class);

      return $settingsRepo->isWarehouseEnabled();
   }

   /**
    * @return bool
    */
   public function isEetEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsGlobal::class);

      return $settingsRepo->isEetEnabled();
   }

   private function getSellerData() : SellerData
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);
      return $sellerDataRepo->getSellerData();
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}