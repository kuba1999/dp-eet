<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;
use Nette\Http\FileUpload;

class AssignCustomerModal extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSaved;

   /** @var bool @persistent */
   public $show = false;

   /** @var Receipt */
   private $receipt;

   /** @var UserRepository */
   private $userRepo;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em, Receipt $receipt)
   {
      $this->em = $em;
      $this->receipt = $receipt;

      $this->userRepo = $this->em->getRepository(User::class);
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addSelect("customer", "Zákazník")
         ->setPrompt("-");

      if($this->receipt->getCustomer()) { //set selected customer
         $customer = $this->receipt->getCustomer();

         $form['customer']->setItems([$customer->getId() => $customer->getName()]);
         $form['customer']->setDefaultValue($customer->getId());
      }

      $form->addSubmit("submit", "Uložit");
      $form->addSubmit("remove", "Odebrat")
         ->setValidationScope()
         ->onClick[] = [$this, "removeCustomer"];

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };
      $form->onValidate[] = function (BaseForm $form) {
         $customerId = $form['customer']->getRawValue();

         if(!empty($customerId)) {
            $customer = $this->userRepo->getCustomer($customerId);

            if($customer) {
               $form['customer']->setItems([$customer->getId() => $customer->getName()]);
            }
         }
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($form->isSubmitted() === $form['remove'])
      {
         return;
      }

      if($form['customer']->isFilled()) {
         $customer = $this->userRepo->getCustomer($form['customer']->getValue());

         $this->receipt->setCustomer($customer);
         $this->em->flush();
      }

      $this->onSaved();
   }

   /**
    * Remove assigned customer
    */
   public function removeCustomer()
   {
      $this->receipt->setCustomer(null);
      $this->em->flush();

      $this->onSaved();
   }

   /**
    * Load customers to selectbox by ajax
    * @param string|null $term
    */
   public function handleLoadCustomers(string $term = null)
   {
      $customers = $this->userRepo->searchCustomers($term);

      $results = [];

      foreach ($customers AS $customer) {
         $results[] = [
            "id"     => $customer->getId(),
            "text"   => $customer->getName()
         ];
      }

      $this->presenter->sendJson(["results"  => $results]);
   }

   public function show()
   {
      $this->show = true;
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;
      $tpl->removeButton = !is_null($this->receipt->getCustomer());

      return $tpl->render();
   }
}