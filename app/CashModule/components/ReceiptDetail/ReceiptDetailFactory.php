<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\ReceiptDetail;


use App\CashModule\Components\ReceiptLogRecords\ReceiptLogRecordsFactory;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialogFactory;
use App\Model\Entities\Receipt;
use App\Model\Services\EETService;
use App\Model\Services\PDFGeneratorService\PDFGeneratorServiceFactory;
use Kdyby\Doctrine\EntityManager;

class ReceiptDetailFactory
{
   /** @var EntityManager */
   private $em;

   /** @var EETService */
   private $EETService;

   /** @var PDFGeneratorServiceFactory */
   private $PDFGeneratorServiceFactory;

   /** @var SendReceiptModalDialogFactory */
   private $sendReceiptModalDialogFactory;

   /** @var ReceiptLogRecordsFactory */
   private $receiptLogRecordsFactory;

   function __construct(
      EntityManager $em,
      EETService $EETService,
      PDFGeneratorServiceFactory $PDFGeneratorServiceFactory,
      SendReceiptModalDialogFactory $sendReceiptModalDialogFactory,
      ReceiptLogRecordsFactory $receiptLogRecordsFactory
   ) {
      $this->em = $em;
      $this->EETService = $EETService;
      $this->PDFGeneratorServiceFactory = $PDFGeneratorServiceFactory;
      $this->sendReceiptModalDialogFactory = $sendReceiptModalDialogFactory;
      $this->receiptLogRecordsFactory = $receiptLogRecordsFactory;
   }

   public function create(Receipt $receipt) : ReceiptDetail
   {
      $component = new ReceiptDetail(
         $this->em,
         $this->EETService,
         $this->PDFGeneratorServiceFactory,
         $this->sendReceiptModalDialogFactory,
         $this->receiptLogRecordsFactory,
         $receipt
      );

      return $component;
   }

   public function createCustomerView(Receipt $receipt) : ReceiptDetail
   {
      $component = $this->create($receipt);
      $component->view = ReceiptDetail::VIEW_CUSTOMER;

      return $component;
   }
}