<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SelectActiveReceipt;


use App\Components\BaseControl;
use App\Model\Entities\Cash;
use App\Model\Entities\Settings;
use App\Model\Entities\User;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class SelectActiveReceipt extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var Cash */
   private $cash;

   /** @var bool @persistent */
   public $show = false;

   /** @var callable */
   public $onSelected;

   /** @var callable */
   public $onDeleted;

   /** @var callable */
   public $onShowReceipt;

   /**
    * RenameReceiptModal constructor.
    * @param EntityManager $em
    * @param ReceiptCacheService $receiptCacheService
    */
   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService,
      Cash $cash
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->cash = $cash;
   }

   /********************************************************************************************************* helpers */
   public function getReceipts() : array
   {
      return $this->receiptCacheService->getReceiptsByCash($this->cash);
   }

   public function getUser(int $userId) : User
   {
      return $this->em->find(User::class, $userId);
   }

   public function show()
   {
      $this->show = true;

      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;

      $this->redrawControl();
   }

   public function isShowed() : bool
   {
      return $this->show;
   }

   public function canReceiptBeOpened(CachedReceipt $cachedReceipt)
   {
      return is_null($cachedReceipt->getBlockedByCashierId()) ||
         $cachedReceipt->getBlockedByCashierId() == $this->presenter->getLoggedUser()->getId();
   }

   public function canReceiptBeDeleted(CachedReceipt $cachedReceipt) : bool
   {
      return is_null($cachedReceipt->getBlockedByCashierId()) ||
         $cachedReceipt->getBlockedByCashierId() == $this->presenter->getLoggedUser()->getId();
   }

   /********************************************************************************************************* signals */
   public function handleShowReceipt(int $receiptId)
   {
      $this->onShowReceipt($this->receiptCacheService->getReceipt($receiptId));
   }

   public function handleOpenReceipt(int $receiptId)
   {
      $this->onSelected($this->receiptCacheService->getReceipt($receiptId));
   }

   public function handleDeleteReceipt(int $receiptId)
   {
      $this->receiptCacheService->removeReceipt($receiptId);
      $this->redrawControl("items");

      $this->onDeleted();
   }

   /********************************************************************************************************** render */

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}