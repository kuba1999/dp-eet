<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SelectActiveReceipt;


use App\Model\Entities\Cash;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class SelectActiveReceiptFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   function __construct(
      EntityManager $em,
      ReceiptCacheService $receiptCacheService
   ) {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
   }

   public function create(Cash $cash) : SelectActiveReceipt
   {
      $component = new SelectActiveReceipt(
         $this->em,
         $this->receiptCacheService,
         $cash
      );

      return $component;
   }
}