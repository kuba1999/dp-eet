<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\FinishReceipt\Subcomponents;

use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;

class EditCustomerDetails extends BaseControl
{
   /** @var CachedReceipt */
   private $receipt;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /**
    * @param CachedReceipt $receipt
    */
   public function __construct(ReceiptCacheService $receiptCacheService, CachedReceipt $receipt)
   {
      $this->receipt = $receipt;
      $this->receiptCacheService = $receiptCacheService;
   }

   public function createComponentForm()
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax ajaxSpinnerEditCustomerBox";

      $form->addText("name", "Jméno / firma")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getName())
         ->setMaxLength(256);
      $form->addText("street", "Ulice a číslo")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getStreet())
         ->setMaxLength(256);
      $form->addText("city", "Město")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getCity())
         ->setMaxLength(256);
      $form->addText("zip", "PSČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getZip())
         ->setMaxLength(256);
      $form->addText("vatNo", "IČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getVatNo())
         ->setMaxLength(256);
      $form->addText("vatId", "DIČ")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getVatId())
         ->setMaxLength(256);
      $form->addText("phone", "Telefon")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getPhone())
         ->setMaxLength(256);
      $form->addText("email", "Email")
         ->setNullable()
         ->setDefaultValue($this->receipt->getCustomerData()->getEmail())
         ->setMaxLength(256)
         ->addCondition(BaseForm::FILLED)
         ->addRule(BaseForm::EMAIL);

      $form->addSubmit("save", "Uložit");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      $customerDetails = $this->receipt->getCustomerData();
      $customerDetails->setName($form['name']->getValue());
      $customerDetails->setStreet($form['street']->getValue());
      $customerDetails->setCity($form['city']->getValue());
      $customerDetails->setZip($form['zip']->getValue());
      $customerDetails->setVatNo($form['vatNo']->getValue());
      $customerDetails->setVatId($form['vatId']->getValue());
      $customerDetails->setPhone($form['phone']->getValue());
      $customerDetails->setEmail($form['email']->getValue());

      $this->receiptCacheService->save($this->receipt);
   }

   /**
    * Request for getting data from ARES
    * @param string|null $vatNo
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessageNotify("Načtení dat proběhlo úspěšně");
         $this->presenter->redrawControl();
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessageNotify("Neexistující IČ", FlashMessageNotify::TYPE_WARNING);
      }
      catch (UnableToConnect $e) {
         $this->presenter->flashMessageNotify("Nepodařilo se spojit s portálem ARES", FlashMessageNotify::TYPE_WARNING);
      }
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }

}