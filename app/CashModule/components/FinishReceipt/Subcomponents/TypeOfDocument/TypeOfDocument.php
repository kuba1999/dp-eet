<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\FinishReceipt\Subcomponents;

use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\SellerData;
use App\Model\Entities\TaxRate;
use App\Model\Enums\TypeOfPayment;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\CachedReceiptItem;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Controls\TextInput;

class TypeOfDocument extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var CachedReceipt */
   private $receipt;

   /** @var callable[] */
   public $onFinished = [];

   /**
    * TypeOfDocument constructor.
    * @param EntityManager $em
    * @param bool $isSellerTaxablePerson
    * @param CachedReceipt $receipt
    */
   public function __construct(EntityManager $em, CachedReceipt $receipt)
   {
      $this->receipt = $receipt;
      $this->em = $em;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->sellerTaxablePerson = $this->isSellertaxablePerson();

      if(!isset($tpl->activeTab)) {
         $tpl->activeTab = 1;
      }

      return $tpl->render();
   }
   /***************************************************************************** types */
   protected function createComponentSimplifiedTaxInvoiceForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("receivedAmount")
         ->setRequired()
         ->addRule(BaseForm::FLOAT);
      $form->addSubmit("paid");

      $form->onValidate[] = [$this, "receivedAmountValidator"];
      $form->onSuccess[] = [$this, "simplifiedTaxInvoice"];
      $form->onError[] = function (){
         $this->template->activeTab = 1;
      };

      return $form;
   }

   protected function createComponentTaxInvoiceForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("receivedAmount")
         ->setRequired()
         ->addRule(BaseForm::FLOAT);

      $form->addSelect("typeOfPayment", "Platební metoda", TypeOfPayment::getNamedTypes())
         ->setRequired();

      $form->addDatePicker("creationDate", "Datum vystavení")
         ->setDefaultValue(new \DateTime())
         ->setAttribute("readonly", "readonly")
         ->setOmitted()
         ->setRequired();

      $form->addDatePicker("dueDate", "Datum splatnosti")
         ->setDefaultValue((new \DateTime())->add(new \DateInterval("P15D")))
         ->setRequired("Zadejte datum splatnosti");

      $form->addDatePicker("dateOfTaxableSupply", "Datum zdanitelného plnění")
         ->setDefaultValue(new \DateTime())
         ->setRequired("Zadejte datum zdanitelného plnění");

      $form->addSubmit("paid");

      $form->onValidate[] = [$this, "receivedAmountValidator"];
      $form->onValidate[] = [$this, "customerDetailsValidator"];
      $form->onSuccess[] = [$this, "taxInvoice"];
      $form->onError[] = function (){
         $this->template->activeTab = 2;
      };

      return $form;
   }

   protected function createComponentInvoiceForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addSelect("typeOfPayment", "Platební metoda", TypeOfPayment::getNamedTypes())
         ->setRequired();

      $form->addDatePicker("creationDate", "Datum vystavení")
         ->setDefaultValue(new \DateTime())
         ->setAttribute("readonly", "readonly")
         ->setOmitted()
         ->setRequired();

      $form->addDatePicker("dueDate", "Datum splatnosti")
         ->setDefaultValue((new \DateTime())->add(new \DateInterval("P15D")))
         ->setRequired("Zadejte datum splatnosti");

      $form->addSubmit("create");

      $form->onSuccess[] = [$this, "invoice"];
      $form->onError[] = function (){
         $this->template->activeTab = 2;
      };

      return $form;
   }

   protected function createComponentProFormaInvoiceForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addSelect("typeOfPayment", "Platební metoda", TypeOfPayment::getNamedTypes())
         ->setRequired();

      $form->addDatePicker("creationDate", "Datum vystavení")
         ->setDefaultValue(new \DateTime())
         ->setAttribute("readonly", "readonly")
         ->setOmitted()
         ->setRequired();

      $form->addDatePicker("dueDate", "Datum splatnosti")
         ->setDefaultValue((new \DateTime())->add(new \DateInterval("P15D")))
         ->setRequired("Zadejte datum splatnosti");

      $form->addSubmit("create");

      $form->onSuccess[] = [$this, "proFormaInvoice"];
      $form->onError[] = function (){
         $this->template->activeTab = 3;
      };

      return $form;
   }

   /**
    * Check amount of received cash
    * @param BaseForm $form
    */
   public function receivedAmountValidator(BaseForm $form)
   {
      $valid = $form['receivedAmount']->getValue() >= $this->receipt->getReceiptSum( $this->isSellertaxablePerson() );

      if(!$valid) {
         $this->presenter->flashMessageNotify("Nebyla přijata dostatečná hotovost", FlashMessageNotify::TYPE_DANGER);
      }
   }

   /**
    * Check if customer details are filled
    * @param BaseForm $form
    */
   public function customerDetailsValidator(BaseForm $form)
   {
      $details = $this->receipt->getCustomerData();
      $valid = true;

      if(empty($details->getName())) {
         $valid = false;
      }
      elseif(empty($details->getStreet())) {
         $valid = false;
      }
      elseif(empty($details->getCity())) {
         $valid = false;
      }
      elseif(empty($details->getZip())) {
         $valid = false;
      }

      if(!$valid && $this->isSellertaxablePerson()) {
         $this->presenter->flashMessageNotify(
            "Daňový doklad musí obsahovat údaje o zákazníkovi (příjemci). Je nutné je vyplnit.",
            FlashMessageNotify::TYPE_DANGER
         );
         $form->addError(null);
      }
      elseif(!$valid && !$this->isSellertaxablePerson()) {
         $this->presenter->flashMessageNotify(
            "Faktura musí obsahovat údaje o zákazníkovi (příjemci). Je nutné je vyplnit.",
            FlashMessageNotify::TYPE_DANGER
         );
         $form->addError(null);
      }
   }

   /***************************************************************************** process forms */
   public function simplifiedTaxInvoice(BaseForm $form)
   {
      if($this->isSellertaxablePerson()) {
         $this->onFinished(\App\Model\Enums\TypeOfDocument::get(\App\Model\Enums\TypeOfDocument::SIMPLIFIED_TAX_INVOICE));
      } else {
         $this->onFinished(\App\Model\Enums\TypeOfDocument::get(\App\Model\Enums\TypeOfDocument::CASH_RECEIPT));
      }
   }

   public function taxInvoice(BaseForm $form)
   {
      $this->receipt->setDueDate($form['dueDate']->getValue());
      $this->receipt->setDateOfTaxableSupply($form['dateOfTaxableSupply']->getValue());
      $this->receipt->setTypeOfPayment(TypeOfPayment::get($form['typeOfPayment']->getValue()));

      $this->onFinished(\App\Model\Enums\TypeOfDocument::get(\App\Model\Enums\TypeOfDocument::TAX_INVOICE));
   }

   public function invoice(BaseForm $form)
   {
      $this->receipt->setDueDate($form['dueDate']->getValue());
      $this->receipt->setTypeOfPayment(TypeOfPayment::get($form['typeOfPayment']->getValue()));

      $this->onFinished(\App\Model\Enums\TypeOfDocument::get(\App\Model\Enums\TypeOfDocument::INVOICE));
   }

   public function proFormaInvoice(BaseForm $form)
   {
      $this->receipt->setDueDate($form['dueDate']->getValue());
      $this->receipt->setTypeOfPayment(TypeOfPayment::get($form['typeOfPayment']->getValue()));

      $this->onFinished(\App\Model\Enums\TypeOfDocument::get(\App\Model\Enums\TypeOfDocument::PRO_FORMA_INVOICE));
   }

   /***************************************************************************** helpers */
   /**
    * Calculate items sum
    * @return float
    */
   public function getReceiptSum() : float
   {
      return $this->receipt->getReceiptSum($this->isSellertaxablePerson());
   }
   
   /**
    * @return bool
    */
   protected function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }
}