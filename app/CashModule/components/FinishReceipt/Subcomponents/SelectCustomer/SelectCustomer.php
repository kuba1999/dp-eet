<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\FinishReceipt\Subcomponents;

use App\Components\BaseControl;
use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Services\CashierCacheService\CashierCacheService;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class SelectCustomer extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var CachedReceipt */
   private $receipt;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var callable[] */
   public $onCustomerSelected = [];

   /**
    * @param CachedReceipt $receipt
    */
   public function __construct(EntityManager $em, ReceiptCacheService $receiptCacheService, CachedReceipt $receipt)
   {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
      $this->receipt = $receipt;
   }

   public function createComponentForm()
   {
      $form = new BaseForm();

      $form->addSelect("customer", "Zákazník")
         ->setPrompt("-");

      if($this->receipt->getCustomerId()) { //set selected customer
         /** @var User|null $customer */
         $customer = $this->em->find(User::class, $this->receipt->getCustomerId());

         if($customer) {
            $form['customer']->setItems([$customer->getId() => $customer->getName()]);
            $form['customer']->setDefaultValue($customer->getId());
         } else {
            $this->receipt->setCustomerId(null);
            $this->receiptCacheService->save($this->receipt);
         }
      }

      return $form;
   }

   /**
    * Load customers to selectbox by ajax
    * @param string|null $term
    */
   public function handleLoadCustomers(string $term = null)
   {
      /** @var UserRepository $userRepo */
      $userRepo = $this->em->getRepository(User::class);
      $customers = $userRepo->searchCustomers($term);

      $results = [];

      foreach ($customers AS $customer) {
         $results[] = [
            "id"     => $customer->getId(),
            "text"   => $customer->getName()
         ];
      }

      $this->presenter->sendJson(["results"  => $results]);
   }

   /**
    * Attach selected customer to the receipt
    * @param int $customerId
    * @throws \Doctrine\ORM\ORMException
    * @throws \Doctrine\ORM\OptimisticLockException
    * @throws \Doctrine\ORM\TransactionRequiredException
    * @throws \Nette\Application\BadRequestException
    */
   public function handleCustomerSelected($customerId = null)
   {
      if(empty($customerId)) {
         $this->receipt->clearCustomer();
         $this->onCustomerSelected(null);
         $this->receiptCacheService->save($this->receipt);
         return;
      }

      /** @var User $customer */
      $customer = $this->em->find(User::class, $customerId);

      if(!$customer) {
         $this->presenter->error();
      }

      $this->receipt->setCustomerId($customerId);
      $customerDetails = $this->receipt->getCustomerData();
      $customerDetails->setName($customer->getName());
      $customerDetails->setStreet($customer->getStreet());
      $customerDetails->setCity($customer->getCity());
      $customerDetails->setZip($customer->getZip());
      $customerDetails->setVatNo($customer->getVatNo());
      $customerDetails->setVatId($customer->getVatId());
      $customerDetails->setPhone($customer->getPhone());
      $customerDetails->setEmail($customer->getEmail());

      $this->receiptCacheService->save($this->receipt);
      $this->onCustomerSelected($customer);
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }

}