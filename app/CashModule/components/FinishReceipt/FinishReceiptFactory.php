<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\FinishReceipt;


use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;

class FinishReceiptFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   function __construct(EntityManager $em, ReceiptCacheService $receiptCacheService)
   {
      $this->em = $em;
      $this->receiptCacheService = $receiptCacheService;
   }

   public function create(CachedReceipt $receipt) : FinishReceipt
   {
      $component = new FinishReceipt(
         $this->em,
         $this->receiptCacheService,
         $receipt
      );

      return $component;
   }
}