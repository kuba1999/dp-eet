<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\FinishReceipt;


use App\CashModule\Components\FinishReceipt\Subcomponents\EditCustomerDetails;
use App\CashModule\Components\FinishReceipt\Subcomponents\SelectCustomer;
use App\CashModule\Components\FinishReceipt\Subcomponents\TypeOfDocument;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use App\Model\Services\ReceiptCacheService\ReceiptCacheService;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class FinishReceipt extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var CachedReceipt */
   private $receipt;

   /** @var ReceiptCacheService */
   private $receiptCacheService;

   /** @var callable[] */
   public $onFinished = [];

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em, ReceiptCacheService $receiptCacheService, CachedReceipt $receipt)
   {
      $this->em = $em;
      $this->receipt = $receipt;
      $this->receiptCacheService = $receiptCacheService;
   }

   /**
    * @return TypeOfDocument
    */
   protected function createComponentTypeOfDocument() : TypeOfDocument
   {
      $comp = new TypeOfDocument(
         $this->em,
         $this->receipt
      );

      $comp->onFinished = $this->onFinished;

      return $comp;
   }

   /**
    * @return EditCustomerDetails
    */
   protected function createComponentEditCustomerDetails() : EditCustomerDetails
   {
      $comp = new EditCustomerDetails(
         $this->receiptCacheService,
         $this->receipt
      );

      return $comp;
   }

   /**
    * @return SelectCustomer
    */
   protected function createComponentSelectCustomer() : SelectCustomer
   {
      $comp = new SelectCustomer(
         $this->em,
         $this->receiptCacheService,
         $this->receipt
      );

      $comp->onCustomerSelected[] = function () {
         $this['editCustomerDetails']->redrawControl();
      };

      return $comp;
   }


   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}