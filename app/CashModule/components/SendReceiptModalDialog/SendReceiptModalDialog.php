<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SendReceiptModalDialog;


use App\Components\BaseControl;
use App\Forms\BaseForm;
use App\Model\Entities\Receipt;
use App\Model\Services\Mailing\ReceiptMailingService;
use Kdyby\Doctrine\EntityManager;

class SendReceiptModalDialog extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSent;

   /** @var bool @persistent */
   public $show = false;

   /** @var int @persistent */
   public $receiptId;

   /** @var ReceiptMailingService */
   private $receiptMailingService;

   /**
    * SendReceiptModalDialog constructor.
    * @param ReceiptMailingService $receiptMailingService
    */
   function __construct(EntityManager $em, ReceiptMailingService $receiptMailingService) {
      $this->em = $em;
      $this->receiptMailingService = $receiptMailingService;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("email", "Email")
         ->setMaxLength(256)
         ->setRequired()
         ->addRule(BaseForm::EMAIL);

      $form->addSubmit("send", "Poslat");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function (){
         $this->redrawControl();
      };

      $this->setDefaultEmail($form);

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $this->receiptMailingService->sendReceiptAsAttachment(
         $this->getReceipt(),
         $form['email']->getValue()
      );

      $this->onSent();
   }

   private function getReceipt() : ?Receipt
   {
      /** @var Receipt $receipt */
      $receipt = $this->em->find(Receipt::class, $this->receiptId);

      return $receipt;
   }

   private function setDefaultEmail(BaseForm $form)
   {
      $receipt = $this->getReceipt();

      if(!$receipt) {
         $this->presenter->error();
      }

      if($receipt->getCustomerData() && $receipt->getCustomerData()->getEmail()) {
         $form['email']->setDefaultValue($receipt->getCustomerData()->getEmail());
      }
      elseif ($receipt->getCustomer()) {
         $form['email']->setDefaultValue($receipt->getCustomer()->getEmail());
      }
   }

   public function show(Receipt $receipt)
   {
      $this->show = true;
      $this->receiptId = $receipt->getId();
      $this->redrawControl();
   }

   public function hide()
   {
      $this->show = false;
      $this->receiptId = null;
      $this->redrawControl();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}