<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\CashModule\Components\SendReceiptModalDialog;


use App\Model\Services\Mailing\ReceiptMailingService;
use Kdyby\Doctrine\EntityManager;

class SendReceiptModalDialogFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ReceiptMailingService */
   private $receiptMailingService;

   function __construct(EntityManager $em, ReceiptMailingService $receiptMailingService)
   {
      $this->em = $em;
      $this->receiptMailingService = $receiptMailingService;
   }

   public function create() : SendReceiptModalDialog
   {
      $component = new SendReceiptModalDialog(
         $this->em,
         $this->receiptMailingService
      );

      return $component;
   }
}