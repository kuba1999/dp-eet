<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\DashboardTicketsTimeline;


use App\Model\Entities\Company;
use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\Language;
use App\Model\Repositories\TicketMessageRepository;
use App\Model\Repositories\TicketRepository;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class DashboardTicketsTimeline extends Control
{
   const VIEW_USER      = 1;
   const VIEW_CUSTOMER  = 1;

   /** @var EntityManager */
   private $em;

   /** @var TicketRepository */
   private $ticketRepository;

   /** @var TicketMessageRepository */
   private $ticketMessageRepository;

   /** @var User */
   private $user;

   /** @var AvatarStorage */
   private $avatarStorage;

   /** @var array */
   public $onTicketOpen = [];

   public function __construct(
      User $user = null,
      EntityManager $em,
      AvatarStorage $avatarStorage
   ) {
      $this->user = $user;
      $this->em = $em;
      $this->ticketRepository = $this->em->getRepository(Ticket::class);
      $this->ticketMessageRepository = $this->em->getRepository(TicketMessage::class);
      $this->avatarStorage = $avatarStorage;
   }

   public function getMessages()
   {
      if($this->user) {
         $qb =  $this->ticketMessageRepository->getAllTicketsMessageByParticipantInTicket($this->user)
            ->orderBy("m.created", "desc")
            ->addOrderBy("m.id", "desc")
            ->setMaxResults(10);
      } else {
         $qb =  $this->ticketMessageRepository->getAll()
            ->orderBy("m.created", "desc")
            ->addOrderBy("m.id", "desc")
            ->setMaxResults(10);
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * @param User $user
    * @return string
    */
   public function getAvatar(User $user) : string
   {
      return $this->avatarStorage->getFilePath($user, true);
   }

   /**
    * @param User $user
    * @return bool
    */
   public function hasAvatar(User $user) : bool
   {
      return $this->avatarStorage->isExists($user);
   }

   public function handleOpenTicket(int $ticketId)
   {
      $this->onTicketOpen($ticketId);
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}