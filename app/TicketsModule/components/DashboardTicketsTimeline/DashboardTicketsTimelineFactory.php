<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\DashboardTicketsTimeline;


use App\Model\Entities\User;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;

class DashboardTicketsTimelineFactory
{
   /** @var EntityManager */
   private $em;

   /** @var AvatarStorage */
   private $avatarStorage;

   function __construct(
      EntityManager $em,
      AvatarStorage $avatarStorage
   ) {
      $this->em = $em;
      $this->avatarStorage = $avatarStorage;
   }

   public function create(User $user = null) : DashboardTicketsTimeline
   {
      $component = new DashboardTicketsTimeline(
         $user,
         $this->em,
         $this->avatarStorage
      );

      return $component;
   }
}