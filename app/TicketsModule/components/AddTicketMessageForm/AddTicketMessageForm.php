<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\CreateTicketForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\TicketStatus;
use App\Model\Repositories\TicketRepository;
use App\TicketsModule\Components\TicketDetail\AttachmentsUploadControl;
use App\TicketsModule\Components\TicketDetail\AttachmentsUploadControlFactory;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class AddTicketMessageForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $loggedUser;

   /** @var AttachmentsUploadControlFactory */
   private $attachmentsUploadControlFactory;

   /** @var Ticket */
   private $ticket;

   /** @var callable */
   public $onSuccess = [];

   public function __construct(
      Ticket $ticket,
      EntityManager $em,
      User $loggedUser,
      AttachmentsUploadControlFactory $attachmentsUploadControlFactory
   ) {
      $this->ticket = $ticket;
      $this->em = $em;
      $this->loggedUser = $loggedUser;
      $this->attachmentsUploadControlFactory = $attachmentsUploadControlFactory;
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addEditor("content", "Obsah")
         ->setRequired(true)
         ->addRule(BaseForm::FILLED, "Obsah je povinný");

      $this['attachments']->attach($form);

      $form->addSubmit("create", "Přidat")
         ->addClass("btn btn-primary");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = [$this, "formError"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      //check if ticket is opened
      if($this->ticket->getStatus()->getValue() == TicketStatus::CLOSED) {
         $this->presenter->flashMessage("Ticket je již uzavřen", FlashMessage::TYPE_DANGER);
         return;
      }

      //create message
      $message = new TicketMessage(
         $form['content']->getValue(),
         $this->loggedUser
      );

      //assign message to ticket
      $this->ticket->assignMessage($message);

      //change status
      TicketRepository::changeStatusOnReply($this->ticket, $message);

      //save it
      $this->em->flush($this->ticket);

      //add attachments
      if(!$this['attachments']->save($message)) {
         $this->presenter->flashMessage("Některé přílohy nebyly nahrány", FlashMessage::TYPE_DANGER);
      }

      $this->onSuccess($this->ticket);
   }

   /**
    * @param BaseForm $form
    */
   public function formError(BaseForm $form)
   {
      if($form['attachments']->isFilled()){
         $form['attachments']->addError("Je nutné znovu vybrat přílohy");
      }
   }

   public function createComponentAttachments() : AttachmentsUploadControl
   {
      $control = $this->attachmentsUploadControlFactory->create();

      return $control;
   }

   public function hasError() : bool
   {
      return $this['form']->hasErrors();
   }

   public function isSubmitted() : bool
   {
      return $this['form']->isSubmitted() != null;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}