<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\TicketDetail;


use App\Forms\BaseForm;
use App\Model\Services\ParametersService;
use App\Model\Storage\TicketAttachmentStorage;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;

class AttachmentsUploadControlFactory
{
   /** @var EntityManager */
   private $em;

   /** @var ParametersService */
   private $parameters;

   /** @var TicketAttachmentStorage */
   private $attachmentStorage;

   function __construct(
      EntityManager $em,
      ParametersService $parameters,
      TicketAttachmentStorage $attachmentStorage
   ){
      $this->em = $em;
      $this->parameters = $parameters;
      $this->attachmentStorage = $attachmentStorage;
   }

   /**
    * @param BaseForm $form
    * @return AttachmentsUploadControl
    */
   public function create() : AttachmentsUploadControl
   {
      return new AttachmentsUploadControl(
         $this->em,
         $this->parameters,
         $this->attachmentStorage
      );
   }
}