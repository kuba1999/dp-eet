<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\TicketDetail;


use App\Forms\BaseForm;
use App\Model\Entities\TicketMessage;
use App\Model\Services\ParametersService;
use App\Model\Storage\TicketAttachmentStorage;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\UploadControl;

class AttachmentsUploadControl extends Control
{
   /** @var EntityManager */
   private $em;
   /** @var ParametersService */
   private $parameters;

   /** @var TicketAttachmentStorage */
   private $attachmentStorage;

   /** @var UploadControl */
   private $control;


   public function __construct(
      EntityManager $em,
      ParametersService $parameters,
      TicketAttachmentStorage $attachmentStorage
   ) {
      $this->em = $em;
      $this->parameters = $parameters;
      $this->attachmentStorage = $attachmentStorage;
   }

   public function attach(BaseForm $form)
   {
      $form->addMultiUpload("attachments", "Přílohy")
         ->addRule(
            BaseForm::MAX_FILE_SIZE,
            "Maximální velikost je %dMB",
            $this->parameters->getParameter("ticketAttachmentMaxSize")*1024*1024
         )
         ->setRequired(false);

      $form->onValidate[] = function (BaseForm $form){
         foreach ($form['attachments']->getValue() as $attachemnt) {
            if(!$this->attachmentStorage->isAllowedFile($attachemnt)) {
               $form['attachments']->addError("Problém se souborem '" . $attachemnt->getName() . "'");
            }
         }
      };

      $this->control = $form['attachments'];
   }

   /**
    * @param TicketMessage $message
    * @return bool
    */
   public function save(TicketMessage $message) : bool
   {
      $ret = true;

      foreach ($this->control->getValue() AS $file)
      {
         $this->em->beginTransaction();

         $attachment = $message->addAttachment($file->getName());
         $this->em->persist($attachment);
         $this->em->flush($attachment);

         //save file to storage
         if($this->attachmentStorage->saveFile($file, $attachment)) {
            $this->em->commit();
         } else {
            $ret = false; //error occurred
         }
      }

      return $ret;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->control = $this->control;

      return $tpl->render();
   }
}