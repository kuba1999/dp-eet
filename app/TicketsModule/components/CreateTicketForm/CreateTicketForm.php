<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\CreateTicketForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Receipt;
use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\TicketMessageType;
use App\Model\Enums\TicketType;
use App\Model\Enums\UserRole;
use App\Model\Repositories\ReceiptRepository;
use App\TicketsModule\Components\TicketDetail\AttachmentsUploadControl;
use App\TicketsModule\Components\TicketDetail\AttachmentsUploadControlFactory;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class CreateTicketForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $loggedUser;

   /** @var AttachmentsUploadControlFactory */
   private $attachmentsUploadControlFactory;

   /** @var callable */
   public $onSuccess = [];

   public function __construct(
      EntityManager $em,
      User $loggedUser,
      AttachmentsUploadControlFactory $attachmentsUploadControlFactory
   ) {
      $this->em = $em;
      $this->loggedUser = $loggedUser;
      $this->attachmentsUploadControlFactory = $attachmentsUploadControlFactory;
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();


      $form->addText("subject", "Předmět")
         ->setRequired(true)
         ->addRule(BaseForm::FILLED, "Předmět je povinný");

      $form->addSelect("type", "Typ", TicketType::getNamedTypes())
         ->setTranslator(null)
         ->setPrompt(null)
         ->setRequired(true)
         ->addRule(BaseForm::FILLED, "Typ je povinný");

      $form->addSelect("receipt", "Doklad", $this->getReceipts())
         ->setPrompt("-");

      $form->addEditor("content", "Zpráva")
         ->setRequired(true)
         ->addRule(BaseForm::FILLED, "Zpráva je povinná");

      $this['attachments']->attach($form);

      $form->addSubmit("create", "Založit")
         ->addClass("btn btn-primary");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      $ticket = new Ticket($form['subject']->getValue(), $form['type']->getValue(), $this->loggedUser);

      $ticket->addLogMessage(TicketMessageType::CREATED, $this->loggedUser);

      $message = new TicketMessage(
         $form['content']->getValue(),
         $this->loggedUser
      );

      $message->setType(TicketMessageType::FIRST_MESSAGE);

      $ticket->assignMessage($message);

      //assign receipt
      if($form['receipt']->isFilled()) {
         $receipt = $this->em->find(Receipt::class, $form['receipt']->getValue());
         $ticket->setReceipt($receipt);
      }

      //save
      $this->em->persist($ticket);
      $this->em->flush($ticket);

      //add attachments
      if(!$this['attachments']->save($message)) {
         $this->presenter->flashMessage("Některé přílohy se nepodařilo nahrát", FlashMessage::TYPE_DANGER);
      }

      $this->onSuccess($ticket);
   }

   public function createComponentAttachments() : AttachmentsUploadControl
   {
      $control = $this->attachmentsUploadControlFactory->create();

      return $control;
   }

   /**
    * @return array
    */
   private function getReceipts() : array
   {
      /** @var ReceiptRepository $receiptRepo */
      $receiptRepo = $this->em->getRepository(Receipt::class);
      $qb = $receiptRepo->getAll();

      if(!UserRole::isUserOfBackend($this->loggedUser)) {
         $qb->andWhere("r.customer = :customer")
            ->setParameter("customer", $this->loggedUser);
      }

      $ret = [];
      foreach ($qb->getQuery()->getResult() AS $receipt) {
         $ret[$receipt->getId()] = $receipt->getNiceId();
      }

      return $ret;
   }

   public function setSelectedReceiptId(int $id)
   {
      $this['form-receipt']->setDefaultValue($id);
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}