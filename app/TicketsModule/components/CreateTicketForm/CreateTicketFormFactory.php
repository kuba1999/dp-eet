<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\CreateTicketForm;


use App\TicketsModule\Components\TicketDetail\AttachmentsUploadControlFactory;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Security\User;

class CreateTicketFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $user;

   /** @var AttachmentsUploadControlFactory */
   private $attachmentsUploadControlFactory;

   function __construct(
      EntityManager $em,
      User $user,
      AttachmentsUploadControlFactory $attachmentsUploadControlFactory
   ) {
      $this->em = $em;
      $this->user = $user;
      $this->attachmentsUploadControlFactory = $attachmentsUploadControlFactory;
   }

   /**
    * @return CreateTicketForm
    */
   public function create() : CreateTicketForm
   {
      /** @var \App\Model\Entities\User $user */
      $loggedUser = $this->em->getRepository(\App\Model\Entities\User::class)->find($this->user->getId());

      return new CreateTicketForm(
         $this->em,
         $loggedUser,
         $this->attachmentsUploadControlFactory
      );
   }
}