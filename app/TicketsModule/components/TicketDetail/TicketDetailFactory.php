<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\TicketDetail;


use App\Components\SetProjectFormModal\SetProjectFormModalFactory;
use App\Model\Entities\Ticket;
use App\Model\Entities\User;
use App\Model\Storage\AvatarStorage;
use App\Model\Storage\TicketAttachmentStorage;
use App\TicketsModule\Components\CreateTicketForm\AddTicketMessageFormFactory;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;

class TicketDetailFactory
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $user;

   /** @var AddTicketMessageFormFactory */
   private $addTicketMessageFormFactory;

   /** @var AvatarStorage */
   public $avatarStorage;

   /** @var TicketAttachmentStorage */
   private $attachmentStorage;

   function __construct(
      EntityManager $em,
      \Nette\Security\User $user,
      AddTicketMessageFormFactory $addTicketMessageFormFactory,
      AvatarStorage $avatarStorage,
      TicketAttachmentStorage $attachmentStorage
   ) {
      $this->em = $em;
      $this->user = $user;
      $this->addTicketMessageFormFactory = $addTicketMessageFormFactory;
      $this->avatarStorage = $avatarStorage;
      $this->attachmentStorage = $attachmentStorage;
   }

   /**
    * @param Ticket $ticket
    * @return TicketDetail
    */
   public function create(Ticket $ticket) : TicketDetail
   {
      /** @var \App\Model\Entities\User $user */
      $loggedUser = $this->em->getRepository(\App\Model\Entities\User::class)->find($this->user->getId());

      return new TicketDetail(
         $ticket,
         $this->em,
         $loggedUser,
         $this->addTicketMessageFormFactory,
         $this->avatarStorage,
         $this->attachmentStorage
      );
   }
}