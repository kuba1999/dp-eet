<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Components\TicketDetail;


use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Receipt;
use App\Model\Entities\Ticket;
use App\Model\Entities\TicketAttachment;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\TicketStatus;
use App\Model\Enums\UserRole;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\TicketMessageRepository;
use App\Model\Storage\AvatarStorage;
use App\Model\Storage\TicketAttachmentStorage;
use App\TicketsModule\components\CreateTicketForm\AddTicketMessageForm;
use App\TicketsModule\components\CreateTicketForm\AddTicketMessageFormFactory;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\Responses\FileResponse;
use Nette\Application\Responses\TextResponse;
use Nette\Application\UI\Control;

class TicketDetail extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $loggedUser;

   /** @var Ticket */
   private $ticket;

   /** @var AddTicketMessageFormFactory */
   private $addTicketMessageFormFactory;

   /** @var AvatarStorage */
   private $avatarStorage;

   /** @var TicketAttachmentStorage */
   private $attachmentStorage;

   /** @var bool @persistent */
   public $showAll = false;

   private $templateFile = __DIR__ . "/template.latte";

   /** @var callable */
   public $onSuccess = [];

   public function __construct(
      Ticket $ticket,
      EntityManager $em,
      User $loggedUser,
      AddTicketMessageFormFactory $addTicketMessageFormFactory,
      AvatarStorage $avatarStorage,
      TicketAttachmentStorage $attachmentStorage
   ) {
      $this->em = $em;
      $this->loggedUser = $loggedUser;
      $this->ticket = $ticket;
      $this->addTicketMessageFormFactory = $addTicketMessageFormFactory;
      $this->avatarStorage = $avatarStorage;
      $this->attachmentStorage = $attachmentStorage;
   }

   /**
    * @return TicketMessage[]
    */
   public function getMessages()
   {
      /** @var TicketMessageRepository $ticketMessageRepo */
      $ticketMessageRepo = $this->em->getRepository(TicketMessage::class);
      $qb = $ticketMessageRepo->getAllByTicket($this->ticket);
      $qb->orderBy("m.created", "desc");
      $qb->addOrderBy("m.id", "desc");

      /*if(!\App\Model\Enums\UserRole::isUserOfBackend($this->loggedUser)) {
         $qb->andWhere('m.type IN (:type)')
            ->setParameter('type', [
               TicketMessageType::FIRST_MESSAGE,
               TicketMessageType::MESSAGE
            ]);
      }*/

      if(!$this->showAll) {
         $qb->setMaxResults(7);
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * @return TicketMessage|null
    */
   public function getLastMessage() : ?TicketMessage
   {
      /** @var TicketMessageRepository $ticketMessageRepo */
      $ticketMessageRepo = $this->em->getRepository(TicketMessage::class);

      return $ticketMessageRepo->getLastTicketMessage($this->ticket);
   }

   /**
    * @return bool
    */
   public function isShowMoreButton() : bool
   {
      /** @var TicketMessageRepository $ticketMessageRepo */
      $ticketMessageRepo = $this->em->getRepository(TicketMessage::class);

      $count = $ticketMessageRepo->countMessages($this->ticket, !\App\Model\Enums\UserRole::isUserOfBackend($this->loggedUser));

      return $count > 7 && $this->showAll == false;
   }

   /**
    * @param User $user
    * @return string
    */
   public function getAvatar(User $user) : string
   {
      return $this->avatarStorage->getFilePath($user, true);
   }

   /**
    * @param User $user
    * @return bool
    */
   public function hasAvatar(User $user) : bool
   {
      return $this->avatarStorage->isExists($user);
   }

   /**
    * @param TicketAttachment $attachment
    * @return bool
    */
   public function isImageAttachment(TicketAttachment $attachment) : bool
   {
      return $this->attachmentStorage->isImage($attachment);
   }

   /**
    * @return User
    */
   public function getLoggedUser() : User
   {
      return $this->loggedUser;
   }

   public function handleImage($attachmentID, $thumbnail = false)
   {
      $this->checkFileRequirements();

      /** @var TicketAttachment $attachment */
      $attachment = $this->em->find(TicketAttachment::class, $attachmentID);

      $this->attachmentStorage->getImage($attachment, $thumbnail)->send();
   }

   public function handleFile($attachmentID)
   {
      $this->checkFileRequirements();

      /** @var TicketAttachment $attachment */
      $attachment = $this->em->find(TicketAttachment::class, $attachmentID);

      $this->presenter->sendResponse(new FileResponse($this->attachmentStorage->getFilePath($attachment), $attachment->getName()));
   }

   public function handleShowOlder()
   {
      $this->showAll = true;
      $this->redrawControl("ticket");
   }

   public function handleCloseTicket()
   {
      $this->ticket->changeStatusWithNotification(TicketStatus::CLOSED, $this->loggedUser);

      $this->em->persist($this->ticket);
      $this->em->flush($this->ticket);

      $this->presenter->flashMessage("Tiket byl uzavřen", FlashMessage::TYPE_INFO);
      $this->presenter->redirect("this");
   }

   public function handleReopenTicket()
   {
      $this->ticket->changeStatusWithNotification(TicketStatus::REOPENED, $this->loggedUser);

      $this->em->persist($this->ticket);
      $this->em->flush($this->ticket);

      $this->presenter->flashMessage("Tiket byl otevřen", FlashMessage::TYPE_INFO);
      $this->presenter->redirect("this");
   }

   private function checkFileRequirements()
   {
      if(
         !in_array($this->loggedUser->getRole()->getValue(), [UserRole::ROLE_ADMIN, UserRole::ROLE_CASHIER]) &&
         $this->ticket->getAuthor() !== $this->loggedUser
      ) {
         $this->presenter->sendResponse(new TextResponse("not allowed"));
      }
   }

   /**
    * @return AddTicketMessageForm
    */
   public function createComponentAddMessage() : AddTicketMessageForm
   {
      $component = $this->addTicketMessageFormFactory->create($this->ticket);

      $component->onSuccess[] = function (){
         $this->presenter->flashMessage("Zpráva přidána", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this");
      };

      return $component;
   }

   /**
    * @return string
    */
   public function getTemplateFile(): string
   {
      return $this->templateFile;
   }

   /**
    * @param string $templateFile
    */
   public function setTemplateFile(string $templateFile)
   {
      $this->templateFile = $templateFile;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile($this->templateFile);
      $tpl->ticket = $this->ticket;
      $tpl->loggedUser = $this->loggedUser;

      return $tpl->render();
   }
}