<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\SetProjectFormModal;


use App\Model\Entities\Company;
use App\Model\Entities\Project;
use App\Model\Entities\Ticket;
use Kdyby\Doctrine\EntityManager;

class SetProjectFormModalFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return SetProjectFormModal
    */
   public function create(Ticket $ticket) : SetProjectFormModal
   {
      return new SetProjectFormModal($ticket, $this->em);
   }
}