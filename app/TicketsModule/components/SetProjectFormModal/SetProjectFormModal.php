<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\SetProjectFormModal;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Company;
use App\Model\Entities\Project;
use App\Model\Entities\Ticket;
use App\Model\Repositories\ProjectRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class SetProjectFormModal extends Control
{
   /** @var bool @persistent */
   private $visible = false;

   /** @var callable[] */
   public $onSuccess = [];

   /** @var EntityManager */
   private $em;

   /** @var int @persistent */
   public $projectID = null;

   /** @var Ticket */
   private $ticket;

   public function __construct(Ticket $ticket, EntityManager $em)
   {
      $this->ticket = $ticket;
      $this->em = $em;
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->setShowFlashErrorMessage(false);
      $form->getElementPrototype()->class = "ajax";

      /** @var ProjectRepository $projectRepo */
      $projectRepo = $this->em->getRepository(Project::class);
      /** @var Project[] $projects */
      $projects = $projectRepo->getAllByCompany($this->ticket->getAuthor()->getCompany())->getQuery()->getResult();

      $items = [];
      foreach ($projects AS $project) {
         $items[$project->getId()] = $project->getName();
      }

      $form->addSelect("project", "Project", $items)
         ->setPrompt("-");
      $form->addSubmit("submit", "Uložit");

      $form->onSuccess[] = [$this, 'formSuccess'];
      $form->onError[] = function () {
        $this->setVisible(true);
      };

      if($this->ticket->getProject()) {
         $form['project']->setDefaultValue($this->ticket->getProject()->getId());
      }

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      //we can not set project without company
      if(!$this->ticket->getAuthor()->getCompany()) {
         return;
      }

      if($form['project']->getValue() == null) {
         $this->ticket->setProject(null);
      } else {
         /** @var ProjectRepository $projectRepo */
         $projectRepo = $this->em->getRepository(Project::class);
         $project = $projectRepo->find($form['project']->getValue());
         $this->ticket->setProject($project);
      }

      $this->em->flush($this->ticket);
      $this->presenter->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);

      $this->setVisible(false);
      $this->onSuccess();
   }

   /**
    * @return bool
    */
   public function isVisible(): bool
   {
      return $this->visible;
   }

   /**
    * @param bool $visible
    */
   public function setVisible(bool $visible)
   {
      $this->visible = $visible;

      $this->redrawControl();
   }

   /**
    * @param Project $project
    */
   public function setProject(Project $project)
   {
      $this->setProjectID($project->getId());
   }

   /**
    * @return Project
    */
   public function getProject(): ?Project
   {
      if(is_null($this->projectID)) {
         return null;
      }

      /** @var ProjectRepository $projectRepository */
      $projectRepository = $this->em->getRepository(Project::class);
      $project = $projectRepository->find($this->projectID);

      return $project;
   }

   /**
    * @return int
    */
   public function getProjectID(): int
   {
      return $this->projectID;
   }

   /**
    * @param int $projectID
    */
   public function setProjectID(int $projectID)
   {
      $this->projectID = $projectID;

      if($this->getProject()) {
         $this['form-submit']->caption = "Uožit";
         $this['form']->setDefaults([
            "name"   => $this->getProject()->getName(),
            "note"   => $this->getProject()->getNote(),
            "serviceContractValidUntil"   => $this->getProject()->getServiceContractValidUntil(),
         ]);
      }
   }

   public function render()
   {
      $tpl = $this->template;

      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}