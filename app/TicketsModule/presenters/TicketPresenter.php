<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Ticket;
use App\Model\Enums\UserRole;
use App\Model\Repositories\TicketRepository;
use App\TicketsModule\Components\TicketDetail\TicketDetail;
use App\TicketsModule\Components\TicketDetail\TicketDetailFactory;

class TicketPresenter extends BasePresenter
{
   /** @var TicketDetailFactory @inject */
   public $ticketDetailFactory;

   /** @var Ticket */
   private $ticket;

   protected function startup()
   {
      parent::startup();

      if($this->getLoggedUser()->getRole() == UserRole::ROLE_CUSTOMER) {
         $this->breadcrumb->add("Moje tikety", "TicketList:my");
      } else {
         $this->breadcrumb->add("Tikety", "TicketList:default");
      }

      /** @var TicketRepository $ticketRepository */
      $ticketRepository = $this->_em->getRepository(Ticket::class);

      /** @var Ticket $ticket */
      $this->ticket = $ticketRepository->find($this->getParameter("id"));

      if(is_null($this->ticket)) {
         $this->flashMessage("Požadovaný tiket neexistuje", FlashMessage::TYPE_DANGER);

         if($this->getLoggedUser()->getRole() == UserRole::ROLE_CUSTOMER) {
            $this->redirect("TicketList:my");
         } else {
            $this->redirect("TicketList:default");
         }
      }

      if($this->getLoggedUser()->getRole() == UserRole::ROLE_CUSTOMER && $this->getLoggedUser() != $this->ticket->getAuthor()) {
         $this->flashMessage("Nemáte opravnění pro přístup k danému tiketu", FlashMessage::TYPE_DANGER);

         if($this->getLoggedUser()->getRole() == UserRole::ROLE_CUSTOMER) {
            $this->redirect("TicketList:my");
         } else {
            $this->redirect("TicketList:default");
         }
      }
   }

   public function actionDefault($id)
   {
      $this->breadcrumb->add("Tiket #" . $id . "|" . $this->ticket->getSubject(), ":Tickets:Ticket:", $id);
   }

   public function createComponentTicketDetail() : TicketDetail
   {
      $component = $this->ticketDetailFactory->create($this->ticket);

      return $component;
   }
}