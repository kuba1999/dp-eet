<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Presenters;


use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\DataGridFactory;
use App\Components\DataGrid\Utils\Sorting;
use App\Model\Entities\Ticket;
use App\Model\Enums\TicketStatus;
use App\Model\Enums\TicketType;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\TicketRepository;
use Nette\Utils\Html;

class TicketListPresenter extends BasePresenter
{
   /** @var DataGridFactory @inject */
   public $dataGridFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Tiketovací systém");
   }

   public function actionDefault()
   {
      $this->breadcrumb->add("Správa tiketů", "CreateTicket:");
   }

   public function actionMy()
   {
      $this->breadcrumb->add("Moje tikety", "CreateTicket:My");
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnNumber("id", "ID")
         ->setWidth(1)
         ->setFilterText();

      $grid->addColumnText("subject", "Předmět")
         ->setWidth(10)
         ->setFilterText();

      $grid->addColumnText("status", "Status")
         ->setWidth(1)
         ->setRenderer(function (Ticket $ticket) {
            switch ($ticket->getStatus()->getValue()){
               case TicketStatus::NEW:
                  return Html::el('span')
                     ->setClass("label label-success lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("nový");
                  break;

               case TicketStatus::REOPENED:
                  return Html::el('span')
                     ->setClass("label label-success lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("znovuotevřen");
                  break;

               case TicketStatus::NEW_MESSAGE:
                  return Html::el('span')
                     ->setClass("label label-warning lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("nová zpráva");
                  break;

               case TicketStatus::ANSWERED:
                  return Html::el('span')
                     ->setClass("label label-primary lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("odpovězeno");
                  break;

               case TicketStatus::CLOSED:
                  return Html::el('span')
                     ->setClass("label label-danger lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("uzavřen");
                  break;
            }
         });

      $grid->addColumnText("type", "Typ")
         ->setWidth(1)
         ->setReplacement(TicketType::getNamedTypes());
      $grid->addColumnText("niceID", "Doklad")
         ->setVirtualColumn()
         ->setWidth(2)
         ->setFilterText();
      $grid->addColumnText("author.name", "Autor")
         ->setWidth(2)
         ->setFilterText();
      $grid->addColumnDateTime("lastMessage", "Poslední zpráva")
         ->setVirtualColumn()
         ->setWidth(2);

      $grid->addFilterSelect("status", TicketStatus::getNamedStatuses())
         ->setMultiple(false);

      $grid->addFilterSelect("type", TicketType::getNamedTypes())
         ->setMultiple(true);

      $grid->addAction("edit", "zobrazit", "Ticket:default")
         ->setIcon("fa fa-eye")
         ->setClass("btn btn-primary");

      /** @var TicketRepository $ticketRepo */
      $ticketRepo = $this->_em->getRepository(Ticket::class);

      if($this->getAction() == "my") {
         $qb = $ticketRepo->getAllTicketsByParticipant($this->getLoggedUser());
      } else {
         $qb = $ticketRepo->getAllQB();
      }

      ReceiptRepository::addSelectForNiceId($qb, "receipt");

      $grid->setDataSource(
         $qb
            ->leftJoin("t.author", "author")
            ->leftJoin("t.receipt", "receipt")
      );

      $grid->addDefaultSorting("status", Sorting::SORT_ASC);
      $grid->addDefaultSorting("lastMessage", Sorting::SORT_DESC);

      return $grid;
   }
}