<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\TicketsModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Ticket;
use App\TicketsModule\components\CreateTicketForm\CreateTicketForm;
use App\TicketsModule\components\CreateTicketForm\CreateTicketFormFactory;

class CreateTicketPresenter extends BasePresenter
{
   /** @var CreateTicketFormFactory @inject */
   public $createTicketFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Tiketovací systém");
   }

   public function actionDefault(int $receiptId = null)
   {
      $this->breadcrumb->add("Založit tiket", ":Admin:Tickets:CreateTicket:");
   }

   public function createComponentCreateTicketForm() : CreateTicketForm
   {
      $control = $this->createTicketFormFactory->create();
      $control->onSuccess[] = function (Ticket $ticket){
         $this->flashMessage(
            "Tiket byl založen",
            FlashMessage::TYPE_SUCCESS
         );
         $this->redirect("Ticket:default", $ticket->getId());
      };

      if($receiptId = $this->getParameter("receiptId")) {
         $control->setSelectedReceiptId($receiptId);
      }

      return $control;
   }
}