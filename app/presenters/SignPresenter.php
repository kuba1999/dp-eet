<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Presenters;

use App\Components\RecoveryPasswordForm\PasswordRecoveryFormFactory;
use App\Components\SignInForm\SignInFormFactory;

abstract class SignPresenter extends BasePresenter
{
   /** @var SignInFormFactory @inject */
   public $signInFormFactory;

   /** @var PasswordRecoveryFormFactory @inject */
   public $passwordRecoveryFormFactory;

   abstract public function actionIn();
   abstract public function actionOut();
}