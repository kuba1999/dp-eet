<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Presenters;

use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageFactory;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Model\Entities\User;
use App\Model\Enums\Language;
use App\Model\Enums\UserRole;
use App\Model\Repositories\UserRepository;
use App\Model\Storage\AvatarStorage;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\ComponentReflection;
use Nette\Application\UI\MethodReflection;
use Nette\Application\UI\Presenter;

/**
 * Class BasePresenter
 * @package App\Presenters
 * @secured
 */
class BasePresenter extends Presenter
{
   /** @var string @persistent */
   public $backLink = '';

   /** @var UserRepository */
   public $userRepo;

   /** @var AvatarStorage @inject */
   public $avatarStorage;

   /** @var EntityManager @inject */
   public $_em;

   protected function startup()
   {
      parent::startup();

      $this->userRepo = $this->_em->getRepository(User::class);
   }

   public function getLoggedUser() : ?User
   {
      if(!$this->user->isLoggedIn()) {
         return null;
      }

      if(is_null($this->userRepo)) {
         $this->userRepo = $this->_em->getRepository(User::class);
      }

      return $this->userRepo->find($this->getUser()->getId());
   }

   public function getAvatar() : string
   {
      return $this->avatarStorage->getFilePath($this->getLoggedUser(), true);
   }

   /**
    * @return FlashMessage
    */
   public function createComponentFlashMessage() : FlashMessage
   {
      return new FlashMessage();
   }

   /**
    * @return FlashMessageNotify
    */
   public function createComponentFlashMessageNotify() : FlashMessageNotify
   {
      return new FlashMessageNotify();
   }

   /**
    * @param $message
    * @param string $type
    * @param null $title
    * @return void
    */
   public function flashMessage($message, $type = FlashMessage::TYPE_INFO, $title = null)
   {
      $this['flashMessage']->addMessage($message, $type, $title);
   }

   /**
    * @param $message
    * @param string $type
    * @param null $title
    */
   public function flashMessageNotify($message, $type = FlashMessage::TYPE_INFO, $title = null)
   {
      $this['flashMessageNotify']->addMessage($message, $type, $title);
   }


   public function checkRequirements($element)
   {
      $requiredLoggedIn = true;

      //onlyGuests
      if($this->hasClassAnnotation($element, "onlyGuests") && $this->user->isLoggedIn()) {
         $this->flashMessage("Sekce je určena pouze pro nepřihlášené uživatele", FlashMessage::TYPE_DANGER);
         $this->redirectOnMainPage();
      }
      elseif($this->hasClassAnnotation($element, "notSecured")) {
         $requiredLoggedIn = false;
      }

      if($requiredLoggedIn && $this->user->isLoggedIn() === false) {
         $this->backLink = $this->storeRequest();

         $this->flashMessage("Je nutné se přihlásit", FlashMessage::TYPE_DANGER);
         $this->redirectOnSignPage();
      }

      if (!$this->user->isAllowed($this->getName(), $this->getAction()))
      {
         $this->flashMessage("Nemáte potřebná oprávnění pro tuto akci", FlashMessage::TYPE_DANGER);
         $this->redirectOnMainPage();
      }
   }

   private function redirectOnSignPage()
   {
      if(strpos($this->getName(), "Admin:") !== false || strpos($this->getName(), "Cash:") !== false) {
         $this->redirect(":Admin:Sign:in");
      }
      elseif(strpos($this->getName(), "Portal:") !== false) {
         $this->redirect(":Portal:Sign:in");
      }
      else {
         $this->terminate();
      }
   }

   private function redirectOnMainPage()
   {
      if(UserRole::isUserOfBackend($this->getLoggedUser())) {
         $this->redirect(":Admin:Dashboard:");
      }
      else {
         $this->redirect(":Portal:Dashboard:");
      }
   }

   private function hasClassAnnotation($element, string $class) : bool
   {
      if($element instanceof ComponentReflection && $element->hasAnnotation($class)) {
         return true;
      }
      elseif($element instanceof MethodReflection && ComponentReflection::parseAnnotation($element->getDeclaringClass(), $class)) {
         return true;
      }

      return false;
   }
}