<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Components\UserSettings;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Exceptions\NotUniqueUsernameException;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;

class UserSettings extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var AvatarStorage */
   private $avatarStorage;

   /** @var User */
   private $user;

   /** @var callable */
   public $onSuccess;

   /** @var UserRepository */
   private $userRepository;

   /**
    * UserSettings constructor.
    * @param User $user
    * @param EntityManager $em
    */
   public function __construct(
      User $user,
      EntityManager $em
   ) {
      $this->user = $user;
      $this->em = $em;

      $this->userRepository = $this->em->getRepository(User::class);
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Jméno")
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("phone", "Telefon")
         ->setMaxLength(255);

      $form->addText("email", "Email")
         ->setType("email")
         ->addRule(BaseForm::EMAIL)
         ->setRequired(true)
         ->setMaxLength(255);

      $form->addText("username", "Uživatelské jméno")
         ->setType("username")
         ->setRequired(true)
         ->setMaxLength(255);

      $form->addPassword("password", "Nové heslo")
         ->addRule(BaseForm::MIN_LENGTH, "Minimální délka hesla je %d znaků", 6)
         ->setRequired(false);

      $form->addPassword("passwordRepeat", "Zopakujte heslo")
         ->setRequired(false)
         ->addConditionOn($form['password'], BaseForm::REQUIRED)
         ->setRequired(true)
         ->addRule(BaseForm::EQUAL, 'Hesla se neshodují', $form['password']);

      $form->addText("street", "Ulice a číslo")
         ->setMaxLength(64);
      $form->addText("city", "Město")
         ->setMaxLength(64);
      $form->addText("zip", "PSČ")
         ->setMaxLength(5);
      $form->addText("company", "Firma")
         ->setMaxLength(255);
      $form->addText("vatNo", "IČ")
         ->setMaxLength(8);
      $form->addText("vatId", "DIČ")
         ->setMaxLength(12);


      $form->addSubmit("submit", "Uložit");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];


      $form->setDefaults([
         'name' => $this->user->getName(),
         'phone' => $this->user->getPhone(),
         'email' => $this->user->getEmail(),

         'street' => $this->user->getStreet(),
         'city' => $this->user->getCity(),
         'zip' => $this->user->getZip(),
         'company' => $this->user->getCompany(),
         'vatNo' => $this->user->getVatNo(),
         'vatId' => $this->user->getVatId(),

         'username' => $this->user->getUsername()
      ]);

      return $form;
   }

   /**
    * Validate form
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      //if user wants to change password, check old password validity
      if($form['password']->getValue()) {
         if(!UserRepository::passwordVerify($form['oldPassword']->getValue(), $this->user)) {
            $form['oldPassword']->addError($this->translator->trans("backend.profilePage.incorrectActualPassword"));
         }
      }
   }

   /**
    * Process form and create user
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $user = $this->user;

      $user->setName($form['name']->getValue());
      $user->setEmail($form['email']->getValue());
      $user->setPhone($form['phone']->getValue());

      $user->setUsername($form['username']->getValue());

      //if user wants to change password
      if($form['password']->getValue()) {
         $user->setPassword($form['password']->getValue());
      }

      $user->setStreet($form['street']->getValue());
      $user->setCity($form['city']->getValue());
      $user->setZip($form['zip']->getValue());
      $user->setCompany($form['company']->getValue());
      $user->setVatNo($form['vatNo']->getValue());
      $user->setVatId($form['vatId']->getValue());

      try {
         $this->userRepository->save($user);
      } catch (NotUniqueEmailException $e) {
         $form['email']->addError("Email je již přiřazen k jinému uživateli");
         return;
      } catch (NotUniqueUsernameException $e) {
         $form['username']->addError("Uživatelské jméno je již přiřazeno k jinému uživateli");
         return;
      }

      $this->onSuccess();
   }



   /**
    * Request for getting data from ARES
    * @param string|null $vat
    */
   public function handleLoadCompanyData(string $vat = null)
   {
      try{
         $data = AresSellerDataService::getData($vat);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessageNotify("Načtení dat proběhlo úspěšně");
         $this->redrawControl();
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessageNotify("Neexistující IČ", FlashMessage::TYPE_WARNING);
         $this->redrawControl();
      }
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}