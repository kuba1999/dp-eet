<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Components\UserSettings;


use App\Model\Entities\User;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;

class UserSettingsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $user;

   function __construct(
      EntityManager $em,
      \Nette\Security\User $user
   ) {
      $this->em = $em;

      $userRepo = $this->em->getRepository(User::class);

      if($user->isLoggedIn()) {
         $this->user = $userRepo->find($user->getId());
      }
   }

   public function create() : UserSettings
   {
      $component = new UserSettings(
         $this->user,
         $this->em
      );

      return $component;
   }
}