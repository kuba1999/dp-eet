<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Components\UserSettings;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Exceptions\NotUniqueUsernameException;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Services\Mailing\UserAccountMailingService;
use Doctrine\ORM\EntityManager;

class RegistrationForm extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSuccess;

   /** @var UserRepository */
   private $userRepository;

   /** @var UserAccountMailingService */
   public $userAccountMailingService;

   /**
    * RegistrationForm constructor.
    * @param EntityManager $em
    */
   public function __construct(EntityManager $em, UserAccountMailingService $userAccountMailingService)
   {
      $this->em = $em;
      $this->userAccountMailingService = $userAccountMailingService;
      $this->userRepository = $this->em->getRepository(User::class);
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Jméno")
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("phone", "Telefon")
         ->setMaxLength(255);

      $form->addText("email", "Email")
         ->setType("email")
         ->addRule(BaseForm::EMAIL)
         ->setRequired(true)
         ->setMaxLength(255);

      $form->addText("username", "Uživatelské jméno")
         ->setType("username")
         ->setRequired(true)
         ->setMaxLength(255);

      $form->addPassword("password", "Heslo")
         ->addRule(BaseForm::MIN_LENGTH, "Minimální délka hesla je %d znaků", 6)
         ->setRequired(true);

      $form->addPassword("passwordRepeat", "Zopakujte heslo")
         ->setRequired(true)
         ->addRule(BaseForm::EQUAL, 'Hesla se neshodují', $form['password']);

      $form->addText("street", "Ulice a číslo")
         ->setMaxLength(64);
      $form->addText("city", "Město")
         ->setMaxLength(64);
      $form->addText("zip", "PSČ")
         ->setMaxLength(5);
      $form->addText("company", "Firma")
         ->setMaxLength(255);
      $form->addText("vatNo", "IČ")
         ->setMaxLength(8);
      $form->addText("vatId", "DIČ")
         ->setMaxLength(12);

      $form->addSubmit("submit", "Registrovat se");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   /**
    * Process form and create user
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $user = new User($form['name']->getValue(), UserRole::get(UserRole::ROLE_CUSTOMER));

      $user->setEmail($form['email']->getValue());
      $user->setPhone($form['phone']->getValue());

      $user->setUsername($form['username']->getValue());
      $user->setPassword($form['password']->getValue());

      $user->setStreet($form['street']->getValue());
      $user->setCity($form['city']->getValue());
      $user->setZip($form['zip']->getValue());
      $user->setCompany($form['company']->getValue());
      $user->setVatNo($form['vatNo']->getValue());
      $user->setVatId($form['vatId']->getValue());

      try {
         $this->userRepository->create($user);
      } catch (NotUniqueEmailException $e) {
         $form['email']->addError("Email je již přiřazen k jinému uživateli");
         return;
      } catch (NotUniqueUsernameException $e) {
         $form['username']->addError("Uživatelské jméno je již přiřazeno k jinému uživateli");
         return;
      }

      $this->userAccountMailingService->sendActivationEmail($user);

      $this->onSuccess($user);
   }



   /**
    * Request for getting data from ARES
    * @param string|null $vat
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      if(is_null($vatNo)) {
         $this->presenter->flashMessageNotify("Zadejte IČ", FlashMessageNotify::TYPE_DANGER);
         return;
      }

      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessageNotify("Načtení dat proběhlo úspěšně");
         $this->redrawControl();
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessageNotify("Neexistující IČ", FlashMessage::TYPE_WARNING);
         $this->redrawControl();
      }
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}