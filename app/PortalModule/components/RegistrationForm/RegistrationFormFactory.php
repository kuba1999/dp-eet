<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Components\UserSettings;


use App\Model\Services\Mailing\UserAccountMailingService;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;

class RegistrationFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var UserAccountMailingService */
   private $userAccountMailingService;

   function __construct(EntityManager $em, UserAccountMailingService $userAccountMailingService)
   {
      $this->em = $em;
      $this->userAccountMailingService = $userAccountMailingService;
   }

   public function create() : RegistrationForm
   {
      $component = new RegistrationForm($this->em, $this->userAccountMailingService);

      return $component;
   }
}