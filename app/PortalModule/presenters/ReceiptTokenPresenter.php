<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\CashModule\Components\ReceiptDetail\ReceiptDetail;
use App\CashModule\Components\ReceiptDetail\ReceiptDetailFactory;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Receipt;
use App\Model\Helpers\ReceiptTokenHelper;

/**
 * @package App\PortalModule\Presenters
 * @notSecured
 */
class ReceiptTokenPresenter extends BasePresenter
{
   /** @var Receipt|null */
   private $receipt;

   /** @var ReceiptDetailFactory @inject */
   public $receiptDetailFactory;

   protected function startup()
   {
      parent::startup();

      $receiptId = ReceiptTokenHelper::decode($this->getParameter("receiptToken"));

      if(is_null($receiptId)) {
         $this->flashMessage("Doklad neexsituje", FlashMessage::TYPE_DANGER);
      }
      else {
         /** @var Receipt|null receipt */
         $this->receipt = $this->_em->find(Receipt::class, $receiptId);
      }

      if($this->receipt) {
         if($this->receipt->getCustomer() && $this->receipt->getCustomer() === $this->getLoggedUser()) {
            $this->redirect("Receipts:detail", $receiptId);
         }

         $this->breadcrumb->add("Doklad " . $this->receipt->getNiceId(), "this");
      } else {
         $this->breadcrumb->add("Doklad neexistuje", "this");
      }
   }

   public function actionDefault(string $receiptToken)
   {
      $this->template->receipt = $this->receipt;
   }

   protected function createComponentReceiptDetail() : ReceiptDetail
   {
      return $this->receiptDetailFactory->createCustomerView($this->receipt);
   }
}