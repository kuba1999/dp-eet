<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\PortalModule\Components\UserSettings\RegistrationForm;
use App\PortalModule\Components\UserSettings\RegistrationFormFactory;

/**
 * Class RegistrationPresenter
 * @package App\PortalModule\Presenters
 * @notSecured
 * @onlyGuests
 */
class RegistrationPresenter extends BasePresenter
{
   /** @var RegistrationFormFactory @inject */
   public $registrationFormFactory;

   public function actionDefault()
   {
      $this->breadcrumb->add("Registrace", ":Portal:Welcome:");
   }

   /**
    * @return RegistrationForm
    */
   public function createComponentRegistrationForm() : RegistrationForm
   {
      $comp = $this->registrationFormFactory->create();
      $comp->onSuccess[] = function () {
         $this->flashMessage(
            "Na zadaný email byly zaslány instrukce pro aktivaci účtu",
            FlashMessage::TYPE_SUCCESS,
            "Registrace proběhla úspěšně"
         );
         $this->redirect(":Portal:Welcome:");
      };

      return $comp;
   }
}