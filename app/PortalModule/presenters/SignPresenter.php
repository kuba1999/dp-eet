<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\Components\RecoveryPasswordForm\PasswordRecoveryFormFactory;
use App\Components\SignInForm\SignInForm;
use App\Components\SignInForm\SignInFormFactory;
use App\Model\Entities\User;
use App\Model\Enums\UserAccountStatus;
use App\Model\Repositories\UserRepository;

/**
 * Class SignPresenter
 * @package App\PortalModule\Presenters
 * @notSecured
 */
class SignPresenter extends BasePresenter
{
   /** @var SignInFormFactory @inject */
   public $signInFormFactory;

   /** @var PasswordRecoveryFormFactory @inject */
   public $passwordRecoveryFormFactory;

   /**
    * Login
    */
   public function actionIn()
   {
      $this->breadcrumb->add("Přihlášení");

      if($this->user->isLoggedIn()) {
         $this->redirect(":Portal:Dashboard:");
      }
   }

   /**
    * Logout
    */
   public function actionOut()
   {
      $this->breadcrumb->add("Odlášení");

      $this->getUser()->logout(true);
      $this->flashMessage("Byl jste úspěšně odhlášen", FlashMessage::TYPE_SUCCESS);
      $this->redirect(":Portal:Welcome:");
   }

   /**
    * @return SignInForm
    */
   public function createComponentSignInForm() : SignInForm
   {
      $control = $this->signInFormFactory->create();
      $control->onSuccess[] = function () {
         $this->restoreRequest($this->backLink);
         $this->redirect(":Portal:Dashboard:default");
      };

      return $control;
   }

   public function actionAccountActivation($userID, $hash)
   {
      $this->setView("in");

      /** @var UserRepository $userRepository */
      $userRepository = $this->_em->getRepository(User::class);

      /** @var User $user */
      $user = $userRepository->find($userID);

      if(!$user) {
         //unknown user
         $this->flashMessage("Aktivace účtu se nezdařila", FlashMessage::TYPE_DANGER);
         $this->redirect("in");
      }

      if($user->getHash() === $hash) {
         $user->setHash(null);
         $user->setAccountStatus(UserAccountStatus::ACTIVE);
         $this->_em->flush($user);

         $this->flashMessage("Účet byl úspěšně aktivován", FlashMessage::TYPE_SUCCESS);
         $this->redirect("in");
      } else {
         $this->flashMessage("Aktivace účtu se nezdařila", FlashMessage::TYPE_DANGER);
         $this->redirect("in");
      }

   }

   public function createComponentPasswordRecoveryForm() : PasswordRecoveryForm
   {
      $control = $this->passwordRecoveryFormFactory->create();
      $control->onSuccess[] = function () {
         $this->flashMessage("sign.passwordRecoveryPage.resettingSuccessful", FlashMessage::TYPE_SUCCESS);
        // $this->redirect(":Admin:Tickets:Sign:in");
      };

      return $control;
   }
}