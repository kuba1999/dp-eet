<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\Receipt;
use App\Model\Repositories\ReceiptRepository;
use App\TicketsModule\Components\DashboardTicketsTimeline\DashboardTicketsTimeline;
use App\TicketsModule\Components\DashboardTicketsTimeline\DashboardTicketsTimelineFactory;

class DashboardPresenter extends BasePresenter
{
   /**
    * @var DashboardTicketsTimelineFactory
    * @inject
    */
   public $ticketsTimelineFactory;

   public function actionDefault()
   {
      $this->breadcrumb->add("Přehled", ":Portal:Dashboard:");
   }

   /**
    * @return DashboardTicketsTimeline
    */
   public function createComponentTicketsTimeline() : DashboardTicketsTimeline
   {
      $component = $this->ticketsTimelineFactory->create($this->getLoggedUser());

      $component->onTicketOpen[] = function (int $ticketId) {
         $this->redirect("Support:ticketDetail", $ticketId);
      };

      return $component;
   }

   public function getLastReceipts() : array
   {
      /** @var ReceiptRepository $receiptRepo */
      $receiptRepo = $this->_em->getRepository(Receipt::class);

      $qb = $receiptRepo->getReceiptsByCustomer($this->getLoggedUser());
      $qb->select("r");
      $qb->orderBy("r.creationDate", "desc");
      $qb->setMaxResults(5);

      return $qb->getQuery()->getResult();
   }
}