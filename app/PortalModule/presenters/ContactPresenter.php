<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\SettingsGlobal;
use App\Model\Repositories\SettingsRepository;

/**
 * Class ContactPresenter
 * @package App\PortalModule\Presenters
 * @notSecured
 */
class ContactPresenter extends BasePresenter
{
   public function actionDefault()
   {
      $this->breadcrumb->add("Kontakt", ":Portal:Welcome:");

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->_em->getRepository(SettingsGlobal::class);
      $this->template->content = $settingsRepo->getGlobalSettings()->getContactPageContent();
   }
}