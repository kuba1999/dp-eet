<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\CashModule\Components\ReceiptDetail\ReceiptDetail;
use App\CashModule\Components\ReceiptDetail\ReceiptDetailFactory;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialog;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialogFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Utils\Sorting;
use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Model\Entities\Receipt;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Services\PDFGeneratorService\PDFGeneratorServiceFactory;

class ReceiptsPresenter extends BasePresenter
{
   /** @var ReceiptRepository */
   private $receiptRepo;

   /** @var ReceiptDetailFactory @inject */
   public $receiptDetailFactory;

   /** @var PDFGeneratorServiceFactory @inject */
   public $pdfGeneratorServiceFactory;

   /** @var SendReceiptModalDialogFactory @inject */
   public $sendReceiptModalDialogFactory;

   protected function startup()
   {
      parent::startup();

      $this->receiptRepo = $this->_em->getRepository(Receipt::class);
      $this->breadcrumb->add("Doklady", ":Portal:Receipts:");
   }


   protected function createComponentReceipts() : DataGrid
   {
      $datagrid = $this->dataGridFactory->create();

      $datagrid->addColumnText("niceID", "ID")
         ->setVirtualColumn()
         ->setWidth(15)
         ->setFilterText();
      $datagrid->addColumnDateTime("creationDate", "Vytvořeno")
         ->setWidth(13)
         ->setFilterDate();
      $datagrid->addColumnText("cashier.name", "Vystavil")
         ->setWidth(10)
         ->setFilterText();
      $datagrid->addColumnText("typeOfDocument", "Typ")
         ->setWidth(20)
         ->setRenderer(function (Receipt $receipt) {
            $type = $receipt->getTypeOfDocument();

            return \App\Model\Enums\TypeOfDocument::getName($type);
         })
         ->setFilterSelect(\App\Model\Enums\TypeOfDocument::getNamedTypes());

      $datagrid->addColumnNumber("itemsCount", "Položek")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFilterNumber();

      $datagrid->addColumnNumber("itemsSum", "Suma")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setValueSuffix(" Kč")
         ->setFilterNumber();

      $datagrid->addAction("detail", "", "detail")
         ->setClass("btn btn-default")
         ->setIcon("fas fa-search");
      $datagrid->addAction("print", "")
         ->setClass("btn btn-success openNewWindow")
         ->setIcon("fas fa-print")
         ->onClick[] = [$this, "printReceipt"];
      $datagrid->addAction("download", "")
         ->setClass("btn btn-warning")
         ->setIcon("fas fa-download")
         ->onClick[] = [$this, "downloadReceipt"];
      $datagrid->addAction("send", "")
         ->setClass("btn btn-primary")
         ->setIcon("fas fa-envelope")
         ->setAjax(true)
         ->onClick[] = [$this, "sendReceipt"];

      $datagrid->setDefaultSorting(["creationDate" => Sorting::SORT_DESC]);

      $data = $this->receiptRepo->getReceiptsByCustomer($this->getLoggedUser());

      $datagrid->setDataSource($data);

      return $datagrid;
   }

   public function printReceipt(array $receipt)
   {
      $generator = $this->pdfGeneratorServiceFactory->receipt($receipt[0]);

      $generator->show(true);
   }

   public function downloadReceipt(array $receipt)
   {
      $generator = $this->pdfGeneratorServiceFactory->receipt($receipt[0]);

      $generator->download();
   }

   /****************************************************************************************************** receipt detail */
   public function actionDetail(int $id)
   {
      /** @var Receipt $receipt */
      $receipt = $this->receiptRepo->find($id);

      if(!$receipt) {
         $this->flashMessage("Doklad neexistuje", FlashMessage::TYPE_DANGER);
         $this->redirect("default");
      }

      if($this->getLoggedUser() !== $receipt->getCustomer()) {
         $this->flashMessage("K dokladu nemáte právo přistoupit", FlashMessage::TYPE_DANGER);
         $this->redirect("default");
      }

      $this->breadcrumb->add("Doklad " . $receipt->getNiceId());
   }

   protected function createComponentReceiptDetail() : ReceiptDetail
   {
      /** @var Receipt $receipt */
      $receipt = $this->receiptRepo->find($this->getParameter("id"));

      $comp = $this->receiptDetailFactory->createCustomerView($receipt);

      $comp->onCreateTicket[] = function (Receipt $receipt) {
         $this->redirect("Support:createTicket", $receipt->getId());
      };

      return $comp;
   }


   ///////////////////////////////////////////////////////////////////// send receipt
   protected function createComponentSendReceiptModalDialog() : SendReceiptModalDialog
   {
      $comp = $this->sendReceiptModalDialogFactory->create();

      $comp->onSent[] = function () use ($comp) {
         $this->flashMessageNotify("Odesláno", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();

         if(!$this->presenter->isAjax()) {
            $this->redirect("this");
         }
      };

      return $comp;
   }

   public function sendReceipt(array $receipt)
   {
      $this['sendReceiptModalDialog']->show($receipt[0]);
   }
}