<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\SettingsGlobal;
use App\Model\Repositories\SettingsRepository;

/**
 * Class WelcomePresenter
 * @package App\PortalModule\Presenters
 * @notSecured
 */
class WelcomePresenter extends BasePresenter
{
   protected function startup()
   {
      parent::startup();

      if($this->user->isLoggedIn()) {
         $this->redirect(":Portal:Dashboard:");
      }
   }

   public function actionDefault()
   {
      $this->breadcrumb->add("Vítejte", ":Portal:Welcome:");

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->_em->getRepository(SettingsGlobal::class);
      $this->template->content = $settingsRepo->getGlobalSettings()->getWelcomePageContent();
   }
}