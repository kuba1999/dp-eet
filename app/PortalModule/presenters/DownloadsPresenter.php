<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\DownloadsFile;
use App\Model\Repositories\DownloadsRepository;
use App\Model\Storage\DownloadsFileStorage;
use Nette\Application\Responses\FileResponse;

class DownloadsPresenter extends BasePresenter
{
   /** @var DownloadsFileStorage @inject */
   public $downloadsFileStorage;

   public function actionDefault()
   {
      $this->breadcrumb->add("Ke stažení", ":Portal:Downloads:");
   }

   /**
    * Get all files
    * @return DownloadsFile[]
    */
   public function getFiles()
   {
      /** @var DownloadsRepository $downRepo */
      $downRepo = $this->_em->getRepository(DownloadsFile::class);

      return $downRepo->getAll()->getQuery()->getResult();
   }

   public function handleDownload(int $id)
   {
      /** @var DownloadsRepository $downloadsRepository */
      $downloadsRepository = $this->_em->getRepository(DownloadsFile::class);
      $file = $downloadsRepository->find($id);

      if(!$file) {
         return;
      }

      $this->sendResponse(new FileResponse(
         $this->downloadsFileStorage->getFilePath($file),
         $file->getFileName()
      ));
   }
}