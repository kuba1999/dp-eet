<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\PortalModule\Components\UserSettings\UserSettings;
use App\PortalModule\Components\UserSettings\UserSettingsFactory;

class SettingsPresenter extends BasePresenter
{
   /** @var UserSettingsFactory @inject */
   public $userSettingsFactory;

   public function actionDefault()
   {
      $this->breadcrumb->add("Nastavení", ":Portal:Settings:");
   }

   /**
    * Component for user settings form
    * @return UserSettings
    */
   public function createComponentUserSettings() : UserSettings
   {
      $comp = $this->userSettingsFactory->create();

      $comp->onSuccess[] = function () {
         $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this");
      };

      return $comp;
   }

}