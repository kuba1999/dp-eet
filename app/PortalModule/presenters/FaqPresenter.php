<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\Faq;
use App\Model\Repositories\FaqRepository;
use App\Model\Storage\FaqFileStorage;
use Nette\Application\Responses\FileResponse;

class FaqPresenter extends BasePresenter
{
   /** @var FaqFileStorage @inject */
   public $faqFileStorage;

   public function actionDefault()
   {
      $this->breadcrumb->add("Často kladné otázky", ":Portal:Dashboard:");
   }

   /**
    * Get all questions
    * @return Faq[]
    */
   public function getQuestions()
   {
      /** @var FaqRepository $faqRepo */
      $faqRepo = $this->_em->getRepository(Faq::class);

      return $faqRepo->getAll()->getQuery()->getResult();
   }

   /**
    * @param Faq $faq
    * @return bool
    */
   public function hasQuestionFile(Faq $faq) : bool
   {
      return $this->faqFileStorage->hasFile($faq);
   }

   /**
    * Download file attached to question
    * @param int $questionId
    */
   public function handleDownload(int $questionId)
   {
      /** @var FaqRepository $faqRepository */
      $faqRepository = $this->_em->getRepository(Faq::class);
      $question = $faqRepository->find($questionId);

      if(!$question || !$this->hasQuestionFile($question)) {
         return;
      }

      $this->sendResponse(new FileResponse(
         $this->faqFileStorage->getFilePath($question),
         $question->getFileName()
      ));
   }
}