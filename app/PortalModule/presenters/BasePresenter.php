<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Components\Breadcrumb\Breadcrumb;
use App\Components\Breadcrumb\BreadcrumbFactory;
use App\Components\DataGrid\DataGridFactory;
use App\Model\Entities\SellerData;
use App\Model\Repositories\SellerDataRepository;

/**
 * Class BasePresenter
 * @package App\PortalModule\Presenters
 */
class BasePresenter extends \App\Presenters\BasePresenter
{
   /** @var BreadcrumbFactory @inject */
   public $breadcrumbFactory;

   /** @var Breadcrumb */
   protected $breadcrumb;

   /** @var DataGridFactory @inject */
   public $dataGridFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb = $this->breadcrumbFactory->create();
   }

   protected function beforeRender()
   {
      parent::beforeRender();

      $this->template->pageTitle = $this['breadcrumb']->getLast()->getName();
   }

   /**
    * Component for breadcrumb navigation
    * @return Breadcrumb
    */
   public function createComponentBreadcrumb() : Breadcrumb
   {
      return $this->breadcrumb;
   }

   public function getCompanyName() : string
   {
      /** @var SellerDataRepository $sellerRepo */
      $sellerRepo = $this->_em->getRepository(SellerData::class);
      $data = $sellerRepo->getSellerData();

      return $data->getCompany() ? $data->getCompany() : "FÉR POKLADNA";
   }
}