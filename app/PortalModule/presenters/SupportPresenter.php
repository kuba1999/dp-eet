<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Utils\Sorting;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Ticket;
use App\Model\Enums\TicketStatus;
use App\Model\Enums\TicketType;
use App\Model\Enums\UserRole;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\TicketRepository;
use App\TicketsModule\components\CreateTicketForm\CreateTicketForm;
use App\TicketsModule\components\CreateTicketForm\CreateTicketFormFactory;
use App\TicketsModule\Components\TicketDetail\TicketDetail;
use App\TicketsModule\Components\TicketDetail\TicketDetailFactory;
use Nette\Utils\Html;

class SupportPresenter extends BasePresenter
{
   /** @var CreateTicketFormFactory @inject */
   public $createTicketFormFactory;

   /** @var TicketDetailFactory @inject */
   public $ticketDetailFactory;

   /** @var Ticket */
   private $ticket;

   public function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Podpora", ":Portal:Support:");
   }


   /********************************************************************************** Create ticket */
   public function actionCreateTicket(int $receiptId = null)
   {
      $this->breadcrumb->add("Nový dotaz", ":Portal:Support:createTicket");
   }

   /**
    * @return CreateTicketForm
    */
   public function createComponentCreateTicketForm() : CreateTicketForm
   {
      $control = $this->createTicketFormFactory->create();

      if($receiptId = $this->getParameter("receiptId")) {
         $control->setSelectedReceiptId($receiptId);
      }

      $control->onSuccess[] = function (Ticket $ticket){
         $this->flashMessage(
            "Tiket byl založen",
            FlashMessage::TYPE_SUCCESS
         );

         $this->redirect("Support:ticketDetail", $ticket->getId());
      };

      return $control;
   }

   /********************************************************************************** My tickets */
   public function actionDefault()
   {

   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnNumber("id", "ID")
         ->setWidth(1)
         ->setFilterText();

      $grid->addColumnText("subject", "Předmět")
         ->setWidth(8)
         ->setFilterText();

      $grid->addColumnText("status", "Status")
         ->setWidth(1)
         ->setRenderer(function (Ticket $ticket) {
            switch ($ticket->getStatus()->getValue()){
               case TicketStatus::NEW:
                  return Html::el('span')
                     ->setClass("label label-success lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("nový");
                  break;

               case TicketStatus::REOPENED:
                  return Html::el('span')
                     ->setClass("label label-success lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("znovuotevřen");
                  break;

               case TicketStatus::NEW_MESSAGE:
                  return Html::el('span')
                     ->setClass("label label-warning lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("nová zpráva");
                  break;

               case TicketStatus::ANSWERED:
                  return Html::el('span')
                     ->setClass("label label-primary lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("odpovězeno");
                  break;

               case TicketStatus::CLOSED:
                  return Html::el('span')
                     ->setClass("label label-danger lb-md")
                     ->setStyle("font-size: 15px;")
                     ->setText("uzavřen");
                  break;
            }
         });

      $grid->addColumnText("type", "Typy")
         ->setWidth(1)
         ->setReplacement(TicketType::getNamedTypes());
      $grid->addColumnText("niceID", "Doklad")
         ->setVirtualColumn()
         ->setWidth(2)
         ->setFilterText();
      $grid->addColumnDateTime("lastMessage", "Poslední zpráva")
         ->setVirtualColumn()
         ->setWidth(2);

      $grid->addFilterSelect("status", TicketStatus::getNamedStatuses())
         ->setMultiple(false);

      $grid->addFilterSelect("type", TicketType::getNamedTypes())
         ->setMultiple(true);

      $grid->addAction("edit", "zobrazit", "Support:ticketDetail")
         ->setIcon("fa fa-eye")
         ->setClass("btn btn-primary");

      /** @var TicketRepository $ticketRepo */
      $ticketRepo = $this->_em->getRepository(Ticket::class);
      $qb = $ticketRepo->getAllTicketsByParticipant($this->getLoggedUser())
               ->leftJoin("t.author", "author")
               ->leftJoin("t.receipt", "receipt");


      ReceiptRepository::addSelectForNiceId($qb, "receipt");

      $grid->setDataSource($qb);

      $grid->addDefaultSorting("status", Sorting::SORT_ASC);
      $grid->addDefaultSorting("lastMessage", Sorting::SORT_DESC);

      return $grid;
   }

   /********************************************************************************** Ticket detail */
   public function actionTicketDetail(int $id)
   {
      $this->breadcrumb->add("Dotaz #" . $id, ":Portal:Support:ticketDetail", $id);


      /** @var TicketRepository $ticketRepository */
      $ticketRepository = $this->_em->getRepository(Ticket::class);

      /** @var Ticket $ticket */
      $this->ticket = $ticketRepository->find($id);

      if(is_null($this->ticket)) {
         $this->flashMessage("Požadovaný ticket neexistuje", FlashMessage::TYPE_DANGER);

         $this->redirect("default");
      }

      if($this->getLoggedUser()->getRole() == UserRole::ROLE_CUSTOMER && $this->getLoggedUser() != $this->ticket->getAuthor()) {
         $this->flashMessage("Nemáte opravnění pro přístup k danému ticketu", FlashMessage::TYPE_DANGER);

         $this->redirect("default");
      }
   }

   public function createComponentTicketDetail() : TicketDetail
   {
      $component = $this->ticketDetailFactory->create($this->ticket);
      $component->setTemplateFile(__DIR__ . "/templates/Support/ticketDetailComponent.latte");

      return $component;
   }
}