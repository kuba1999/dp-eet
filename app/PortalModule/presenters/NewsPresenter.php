<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\PortalModule\Presenters;


use App\Model\Entities\News;
use App\Model\Repositories\NewsRepository;
use App\Model\Storage\FaqFileStorage;

/**
 * Class NewsPresenter
 * @package App\PortalModule\Presenters
 * @notSecured
 */
class NewsPresenter extends BasePresenter
{
   /** @var FaqFileStorage @inject */
   public $faqFileStorage;

   public function actionDefault()
   {
      $this->breadcrumb->add("Aktuality", ":Portal:News:");
   }

   /**
    * Get all news
    * @return News[]
    */
   public function getNews()
   {
      /** @var NewsRepository $newsRepo */
      $newsRepo = $this->_em->getRepository(News::class);

      return $newsRepo->getAll()->getQuery()->getResult();
   }
}