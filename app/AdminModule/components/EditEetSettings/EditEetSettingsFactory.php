<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditEetSettings;


use App\Model\Services\EETService;
use App\Model\Storage\EetCertificateStorage;
use Kdyby\Doctrine\EntityManager;

class EditEetSettingsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var EetCertificateStorage */
   private $certificateStorage;

   /** @var EETService */
   private $EETService;

   function __construct(
      EntityManager $em,
      EetCertificateStorage $certificateStorage,
      EETService $EETService
   ) {
      $this->em = $em;
      $this->certificateStorage = $certificateStorage;
      $this->EETService = $EETService;
   }

   public function create() : EditEetSettings
   {
      $component = new EditEetSettings(
         $this->em,
         $this->certificateStorage,
         $this->EETService
      );

      return $component;
   }
}