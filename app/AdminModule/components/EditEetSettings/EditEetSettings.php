<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditEetSettings;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\SellerData;
use App\Model\Entities\SettingsEet;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Services\EETService;
use App\Model\Storage\EetCertificateStorage;
use Kdyby\Doctrine\EntityManager;

class EditEetSettings extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSubmit;

   /** @var callable */
   public $onSuccess;

   /** @var SettingsEet */
   private $settings;

   /** @var SellerData */
   private $sellerData;

   /** @var EetCertificateStorage */
   private $certificateStorage;

   /** @var EETService */
   private $EETService;

   /**
    * EditEetSettings constructor.
    * @param EntityManager $em
    * @param EetCertificateStorage $certificateStorage
    * @param EETService $EETService
    */
   function __construct(
      EntityManager $em,
      EetCertificateStorage $certificateStorage,
      EETService $EETService
   ) {
      $this->em = $em;
      $this->certificateStorage = $certificateStorage;
      $this->EETService = $EETService;

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $em->getRepository(SettingsEet::class);
      $this->settings = $settingsRepo->getEetSettings();

      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $em->getRepository(SellerData::class);
      $this->sellerData = $sellerDataRepo->getSellerData();
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "form-horizontal";

      $form->addCheckbox("enabled", "EET povoleno")
         ->setRequired(false);

      $form->addCheckbox("verificationMode", "Ověřovací mód")
         ->setRequired(false);

      $form->addCheckbox("playgroundEnvironment", "Neprodukční prostředí")
         ->setRequired(false);

      $form->addText("vatId", "DIČ")
         ->setAttribute("readonly", "readonly")
         ->setOmitted()
         ->setRequired();

      $form->addText("defaultPremiseId", "Výchozí číslo provozovny")
         ->setMaxLength(6)
         ->setNullable()
         ->addConditionOn($form['enabled'], BaseForm::EQUAL, true)
         ->setRequired();

      $form->addText("defaultCashRegisterId", "Výchozí číslo pokladny")
         ->setMaxLength(6)
         ->setNullable()
         ->addConditionOn($form['enabled'], BaseForm::EQUAL, true)
         ->setRequired();

      $form->addPassword("privateKeyPassword", "Heslo k certifikátu")
         ->setNullable();

      $form->addUpload("certificate", "Certifikát")
         ->addConditionOn($form['enabled'], BaseForm::EQUAL, true)
         ->setRequired(!$this->certificateStorage->isExists());
         $form["certificate"]->addConditionOn($form['privateKeyPassword'], BaseForm::FILLED)
         ->setRequired();


      $form->addSubmit("submit", "Uložit");
      $form->addSubmit("test", "Test spojení");

      $form->onSuccess[] = function () {$this->onSubmit();};
      $form->onSubmit[] = function () {$this->onSubmit();};
      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onValidate[] = [$this, "certificateValidate"];

      return $form;
   }

   private function setDefaults(BaseForm $form)
   {
      $form->setDefaults([
         "enabled"               =>  $this->settings->isEnabled(),
         "verificationMode"      =>  $this->settings->isVerificationMode(),
         "playgroundEnvironment" =>  $this->settings->isPlaygroundEnvironment(),
         "defaultPremiseId"      =>  $this->settings->getDefaultPremiseId(),
         "defaultCashRegisterId" =>  $this->settings->getDefaultCashRegisterId(),
         "vatId"                 =>  $this->sellerData->getVatId()
      ]);
   }

   public function formSuccess(BaseForm $form)
   {
      if($form->isSubmitted() === $form['submit']) {
         $this->save($form);
      }
      elseif($form->isSubmitted() === $form['test']) {
         $form->reset();
         $this->testConnection($form);
      }
   }

   /**
    * @param BaseForm $form
    */
   public function save(BaseForm $form)
   {
      $this->settings->setEnabled($form['enabled']->getValue());
      $this->settings->setVerificationMode($form['verificationMode']->getValue());
      $this->settings->setPlaygroundEnvironment($form['playgroundEnvironment']->getValue());
      $this->settings->setDefaultPremiseId($form['defaultPremiseId']->getValue());
      $this->settings->setDefaultCashRegisterId($form['defaultCashRegisterId']->getValue());

      if($form['certificate']->isOk()) {
         $this->settings->setPrivateKeyPassword($form['privateKeyPassword']->getValue());
         $this->saveCertificate($form);
      }

      $this->em->persist($this->settings);
      $this->em->flush($this->settings);

      $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);

      $this->onSuccess();
   }
   /**
    * @param BaseForm $form
    */
   public function testConnection(BaseForm $form)
   {
      if(!$this->certificateStorage->isExists()) {
         $this->flashMessage("Není nahrán certifikát", FlashMessage::TYPE_DANGER);
         return;
      }
      elseif(empty($this->sellerData->getVatNo())) {
         $this->flashMessage("Není zadáno DIČ", FlashMessage::TYPE_DANGER);
         return;
      }
      elseif(empty($this->settings->getDefaultCashRegisterId())) {
         $this->flashMessage("Není zadáno výchozí číslo pokladny ", FlashMessage::TYPE_DANGER);
         return;
      }
      elseif(empty($this->settings->getDefaultPremiseId())) {
         $this->flashMessage("Není zadáno výchozí číslo provozovny ", FlashMessage::TYPE_DANGER);
         return;
      }

      $valid = $this->EETService->testConnection();

      if($valid !== true) {
         $this->flashMessage("Nepodařilo se navázat spojení s EET", FlashMessage::TYPE_DANGER, "Chyba");
         $this->flashMessage($valid, FlashMessage::TYPE_WARNING);
      } else {
         $this->flashMessage("Spojení OK", FlashMessage::TYPE_SUCCESS);
         $this->onSuccess();
      }
   }

   public function certificateValidate(BaseForm $form)
   {
      if(!$form['certificate']->isOk()) {
         return;
      }

      $certStore = $form['certificate']->getValue()->getContents();
      $password = $form['privateKeyPassword']->getValue();

      if (!openssl_pkcs12_read($certStore, $certInfo, $password)) {
         $form['certificate']->addError("Heslo není správné nebo byl nahrán nesprávný certifikát");
         $form['privateKeyPassword']->addError("Heslo není správné nebo byl nahrán nesprávný certifikát");
      }
   }

   private function saveCertificate(BaseForm $form)
   {
      $certStore = $form['certificate']->getValue()->getContents();
      $password = $form['privateKeyPassword']->getValue();

      openssl_pkcs12_read($certStore, $certInfo, $password);

      $this->certificateStorage->save($certInfo['cert'], $certInfo['pkey']);
   }

   public function isCertificateExists() : bool
   {
      return $this->certificateStorage->isExists();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $this->setDefaults($this['form']);

      return $tpl->render();
   }
}