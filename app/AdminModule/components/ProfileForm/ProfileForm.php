<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\ProfileForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Company;
use App\Model\Entities\User;
use App\Model\Enums\Language;
use App\Model\Enums\TicketType;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Exceptions\NotUniqueUsernameException;
use App\Model\Repositories\UserRepository;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class ProfileForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var AvatarStorage */
   private $avatarStorage;

   /** @var User */
   private $user;

   /** @var callable */
   public $onSuccess;

   /** @var UserRepository */
   private $userRepository;

   public function __construct(
      User $user,
      EntityManager $em,
      AvatarStorage $avatarStorage
   ) {
      $this->user = $user;
      $this->em = $em;
      $this->avatarStorage = $avatarStorage;
      $this->userRepository = $this->em->getRepository(User::class);
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Jméno")
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("phone", "Telefon")
         ->setRequired()
         ->setMaxLength(255);

      $form->addUpload("avatar", "Fotografie / avatar")
         ->setRequired(false)
         ->addRule(BaseForm::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF');

      $form->addText("email", "Email")
         ->setType("email")
         ->addRule(BaseForm::EMAIL)
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("username", "Uživatelské jméno")
         ->setType("username")
         ->setRequired()
         ->setMaxLength(255);

      $form->addPassword("oldPassword", "Aktuální heslo");

      $form->addPassword("newPassword", "Nové heslo")
         ->addRule(BaseForm::MIN_LENGTH, "Minimální délka hesla je %d znaků", 6)
         ->setRequired(false);

      $form->addPassword("newPasswordRepeat", "Zopakujte nové heslo")
         ->setRequired(false)
         ->addConditionOn($form['newPassword'], BaseForm::FILLED)
         ->setRequired(true)
         ->addRule(BaseForm::EQUAL, 'Hesla se neshodují', $form['newPassword']);

      if(UserRole::isUserOfBackend($this->user)) {
         $form->addMultiSelect("ticketTypeEmailNotification", "Emailová notifikace - typ ticketu", TicketType::getNamedTypes());
      }

      $form->addSubmit("submit", "Uložit");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];

      $ticketTypeNotifications = [];
      foreach ($this->user->getTicketTypeNotifications() AS $ticketTypeNotification) {
         $ticketTypeNotifications[] = $ticketTypeNotification->getType();
      }

      $form->setDefaults([
         'username' => $this->user->getUsername(),
         'name' => $this->user->getName(),
         'phone' => $this->user->getPhone(),
         'email' => $this->user->getEmail(),
         'ticketTypeEmailNotification' => $ticketTypeNotifications
      ]);

      return $form;
   }

   /**
    * Validate form
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      /** @var FileUpload $avatar */
      $avatar = $form['avatar']->getValue();
      $width = $avatar->getImageSize()[0];
      $height = $avatar->getImageSize()[1];

      if($avatar->hasFile() && ($width != 215 && $height != 215)) {
         $form['avatar']->addError("Rozměr musí být 215px x 215px");
      }

      //if user wants to change password, check old password validity
      if($form['newPassword']->getValue()) {
         if(!UserRepository::passwordVerify($form['oldPassword']->getValue(), $this->user)) {
            $form['oldPassword']->addError("Heslo není správné");
         }
      }
   }

   /**
    * Process form and create user
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $user = $this->user;

      $user->setName($form['name']->getValue());
      $user->setUsername($form['username']->getValue());
      $user->setEmail($form['email']->getValue());
      if($form['newPassword']->getValue()) {
         $user->setPassword($form['newPassword']->getValue());
      }

      $user->setPhone($form['phone']->getValue());

      //ticket notifications
      if(UserRole::isUserOfBackend($this->user)) {
         $selectedTypes = $form['ticketTypeEmailNotification']->getValue();

         //unsubscribe
         foreach ($user->getTicketTypeNotifications() as $ticketTypeNotification) {
            if(!in_array($ticketTypeNotification->getType(), $selectedTypes)) {
               $user->removeTicketTypeNotification($ticketTypeNotification);
               $this->em->remove($ticketTypeNotification);
            } else {
               unset($selectedTypes[array_search($ticketTypeNotification->getType(), $selectedTypes)]);
            }
         }

         //subscribe
         foreach ($selectedTypes AS $type) {
            $user->addTicketTypeNotification($type);
         }
      }

      try{
         $this->userRepository->save($user);
      } catch (NotUniqueEmailException $e) {
         $form['email']->addError("Email je již přiřazen k jinému uživateli");
         return;
      } catch (NotUniqueUsernameException $e) {
         $form['username']->addError("Uživatelské jméno je již přiřazeno k jinému uživateli");
         return;
      }

      /** @var FileUpload $avatar */
      $avatar = $form['avatar']->getValue();

      //save avatar if creating user was successfully
      if($user->getId() && $avatar->hasFile()) {
         $this->avatarStorage->saveFile($avatar, $user);
      }

      $this->onSuccess();
   }

   /**
    * @return string
    */
   public function getAvatarPath() : string
   {
      return $this->avatarStorage->getFilePath($this->user);
   }

   /**
    * @return string
    */
   public function hasAvatar() : string
   {
      if(is_null($this->user)) {
         return false;
      }

      return $this->avatarStorage->isExists($this->user);
   }

   public function handleRemoveAvatar()
   {
      $this->avatarStorage->deleteFile($this->user);
      $this->getPresenter()->flashMessage("Avatar smazán", FlashMessage::TYPE_SUCCESS);
      $this->redrawControl("avatar");
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}