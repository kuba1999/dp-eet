<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditFaqForm;


use App\Forms\BaseForm;
use App\Model\Entities\Faq;
use App\Model\Storage\FaqFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class AddEditFaqForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var FaqFileStorage */
   private $faqFileStorage;

   /** @var Faq */
   private $faq;

   /** @var callable */
   public $onSuccess;

   function __construct(EntityManager $em, FaqFileStorage $faqFileStorage)
   {
      $this->em = $em;
      $this->faqFileStorage = $faqFileStorage;
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("subject", "Předmět")
         ->setRequired(true)
         ->setMaxLength(255);

      $form->addEditor("content", "Obsah")
         ->setRequired(true);

      $form->addUpload("file", "Soubor")
         ->setRequired(false);

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->faq) {
         $form['subject']->setDefaultValue($this->faq->getSubject());
         $form['content']->setDefaultValue($this->faq->getContent());

         $form['submit']->caption = "Uložit";
      }

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      if($this->faq) { //update existing question
         $faq = $this->faq;
      } else { //new news
         $faq = new Faq(
            $form['subject']->getValue(),
            $form['content']->getValue()
         );
      }

      if($this->faq) {
         $this->em->flush($faq);
      } else {
         $this->em->persist($faq);
         $this->em->flush($faq);
      }

      /** @var FileUpload $file */
      $file = $form['file']->getValue();

      //save file if creating question was successful
      if($faq->getId() && $file->hasFile()) {
         $this->faqFileStorage->saveFile($file, $faq);

         $faq->setFileName($file->getName());
         $this->em->flush($faq);
      }

      $this->onSuccess($faq);
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }

   public function hasFile() : bool
   {
      if(!$this->faq) {
         return false;
      }

      return $this->faqFileStorage->hasFile($this->faq);
   }
   /**
    * @return Faq
    */
   public function getFaq(): Faq
   {
      return $this->faq;
   }

   /**
    * @param Faq $faq
    */
   public function setFaq(Faq $faq)
   {
      $this->faq = $faq;
   }
}