<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditFaqForm;


use App\Model\Storage\FaqFileStorage;
use Kdyby\Doctrine\EntityManager;

class AddEditFaqFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var FaqFileStorage */
   private $faqFileStorage;

   function __construct(EntityManager $em, FaqFileStorage $faqFileStorage)
   {
      $this->em = $em;
      $this->faqFileStorage = $faqFileStorage;
   }

   public function create() : AddEditFaqForm
   {
      $component = new AddEditFaqForm(
         $this->em,
         $this->faqFileStorage
      );

      return $component;
   }
}