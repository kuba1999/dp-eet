<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditSellerData;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\SellerData;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\TextInput;

class EditSellerData extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSubmit;

   /** @var callable */
   public $onSuccess;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "form-horizontal";

      $form->addText("company", "Název firmy")
         ->setMaxLength(255)
         ->setRequired();

      $form->addText("street", "Ulice a číslo")
         ->setMaxLength(64)
         ->setRequired();
      $form->addText("city", "Město")
         ->setMaxLength(64)
         ->setRequired();
      $form->addText("zip", "PSČ")
         ->setMaxLength(32)
         ->setRequired();
      $form->addText("vatNo", "IČ")
         ->setMaxLength(32)
         ->setRequired();
      $form->addText("vatId", "DIČ")
         ->setMaxLength(32)
         ->setRequired();
      $form->addCheckbox("taxablePerson", "Jsem plátce DPH")
         ->setRequired(false);
      $form->addText("bankAccountNumber", "Číslo bankovního účtu")
         ->setMaxLength(256)
         ->setRequired(false);
      $form->addTextArea("documentNote", "Poznámka na dokladu")
         ->setMaxLength(256)
         ->setRequired(false);

      $form->addText("phone", "Telefon")
         ->setMaxLength(256);
      $form->addText("email", "Email")
         ->setMaxLength(256)
         ->addCondition(BaseForm::FILLED)
         ->addRule(BaseForm::EMAIL);
      $form->addText("web", "Web")
         ->setMaxLength(256);

      $form->addSubmit("submit", "Uložit");

      $form->onSuccess[] = function () {$this->onSubmit();};
      $form->onSubmit[] = function () {$this->onSubmit();};
      $form->onSuccess[] = [$this, "formSuccess"];

      $form->setDefaults($this->getDefaults());

      return $form;
   }

   private function getDefaults()
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);
      $data = $sellerDataRepo->getSellerData();

      if($data) {
         return [
            "company"   =>  $data->getCompany(),
            "street"    =>  $data->getStreet(),
            "city"      =>  $data->getCity(),
            "zip"       =>  $data->getZip(),
            "vatNo"     =>  $data->getVatNo(),
            "vatId"     =>  $data->getVatId(),
            "taxablePerson" =>  $data->isTaxablePerson(),
            "phone"     =>  $data->getPhone(),
            "email"     =>  $data->getEmail(),
            "web"       =>  $data->getWeb(),
            "bankAccountNumber" =>  $data->getBankAccountNumber(),
            "documentNote" =>  $data->getDocumentNote(),
         ];
      }

      return [];
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->isDataChanged($form)) {
         $data = new SellerData();

         $data->setCompany($form['company']->getValue());
         $data->setStreet($form['street']->getValue());
         $data->setCity($form['city']->getValue());
         $data->setZip($form['zip']->getValue());
         $data->setVatNo($form['vatNo']->getValue());
         $data->setVatId($form['vatId']->getValue());
         $data->setTaxablePerson($form['taxablePerson']->getValue());
         $data->setPhone($form['phone']->getValue());
         $data->setEmail($form['email']->getValue());
         $data->setWeb($form['web']->getValue());
         $data->setBankAccountNumber($form['bankAccountNumber']->getValue());
         $data->setDocumentNote($form['documentNote']->getValue());

         $this->em->persist($data);
         $this->em->flush($data);
      }

      $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);
      $this->onSuccess();
   }

   private function isDataChanged(BaseForm $form) : bool
   {
      if($this->getDefaults() === []) {
         return true;
      }

      foreach ($form->getControls() AS $control) {
         if($control instanceof TextInput || $control instanceof Checkbox) {
            if($control->getValue() !== $this->getDefaults()[$control->getName()])
            {
               return true;
            }
         }
      }

      return false;
   }

   /**
    * Request for getting data from ARES
    * @param string|null $vatNo
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessageNotify("Načtení dat proběhlo úspěšně");
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessageNotify("Neexistující IČ", FlashMessageNotify::TYPE_WARNING);
      }
      catch (UnableToConnect $e) {
         $this->presenter->flashMessageNotify("Nepodařilo se spojit s portálem ARES", FlashMessageNotify::TYPE_WARNING);
      }
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}