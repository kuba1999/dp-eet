<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditSellerData;


use Kdyby\Doctrine\EntityManager;

class EditSellerDataFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create() : EditSellerData
   {
      $component = new EditSellerData(
         $this->em
      );

      return $component;
   }
}