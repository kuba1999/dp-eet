<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\AddEditUserForm;


use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;

class AddEditUserFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var AvatarStorage */
   private $avatarStorage;

   function __construct(EntityManager $em, AvatarStorage $avatarStorage)
   {
      $this->em = $em;
      $this->avatarStorage = $avatarStorage;
   }

   public function create(string $formType) : AddEditUserForm
   {
      $component = new AddEditUserForm(
         $formType,
         $this->em,
         $this->avatarStorage
      );

      return $component;
   }
}