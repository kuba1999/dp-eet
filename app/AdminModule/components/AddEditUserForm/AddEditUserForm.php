<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\AddEditUserForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Enums\TicketType;
use App\Model\Enums\UserAccountStatus;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\MissingAdminException;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Exceptions\NotUniqueUsernameException;
use App\Model\Repositories\UserRepository;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnableToConnect;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class AddEditUserForm extends Control
{
   const FORM_CUSTOMER       =  "cusotmer";
   const FORM_USER_OF_SYSTEM =  "userOfSystem";

   const ACTION_CREATED      =  "created";
   const ACTION_SAVED        =  "saved";

   private $formType = self::FORM_CUSTOMER;

   /** @var EntityManager */
   private $em;

   /** @var AvatarStorage */
   private $avatarStorage;

   /** @var User */
   private $user;

   /** @var callable */
   public $onSuccess;

   /** @var UserRepository */
   private $userRepository;

   public function __construct($formType, EntityManager $em, AvatarStorage $avatarStorage)
   {
      $this->formType = $formType;
      $this->em = $em;
      $this->avatarStorage = $avatarStorage;
      $this->userRepository = $this->em->getRepository(User::class);
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Jméno")
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("phone", "Telefon")
         ->setMaxLength(255);

      $form->addText("email", "Email")
         ->setType("email")
         ->addRule(BaseForm::EMAIL)
         ->setRequired($this->formType == self::FORM_USER_OF_SYSTEM)
         ->setMaxLength(255);

      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         $form->addUpload("avatar", "Fotografie / avatar")
            ->setRequired(false)
            ->addRule(BaseForm::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.');
      }

      $form->addEditor("note", "Poznámka");

      $form->addText("username", "Uživatelské jméno")
         ->setType("username")
         ->setRequired($this->formType == self::FORM_USER_OF_SYSTEM || ($this->user && $this->user->getUsername()))
         ->setMaxLength(255);

      $form->addPassword("password", "Heslo")
         ->addRule(BaseForm::MIN_LENGTH, "Minimální délka hesla je %d znaků", 6)
         ->setRequired(false);

      $form['username']->addConditionOn($form['password'], BaseForm::FILLED)
         ->setRequired(true);

      if($this->user && is_null($this->user->getPassword())) {
         $form['password']->addConditionOn($form['username'], BaseForm::FILLED)
            ->setRequired(true);
      }
      elseif (!$this->user) {
         $form['password']->addConditionOn($form['username'], BaseForm::REQUIRED)
            ->setRequired(true);
      }

      $form->addPassword("passwordRepeat", "Zopakujte heslo")
         ->setRequired(false)
         ->addConditionOn($form['password'], BaseForm::REQUIRED)
         ->setRequired(true)
         ->addRule(BaseForm::EQUAL, 'Hesla se neshodují', $form['password']);

      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         $roles = UserRole::getNamedRoles();
         unset($roles[UserRole::ROLE_CUSTOMER]);
         unset($roles[UserRole::ROLE_GUEST]);

         $form->addSelect("role", "Role", $roles);
      }

      $form->addSelect("accountStatus", "Stav účtu", UserAccountStatus::getStatuses())
         ->setRequired(true)
         ->setDefaultValue(UserAccountStatus::ACTIVE);

      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         $form->addMultiSelect("ticketTypeEmailNotification", "Emailová notifikace - typ ticketu", TicketType::getNamedTypes());
      }

      if($this->formType == self::FORM_CUSTOMER) {
         $form->addText("street", "Ulice a číslo")
            ->setMaxLength(256);
         $form->addText("city", "Město")
            ->setMaxLength(256);
         $form->addText("zip", "PSČ")
            ->setMaxLength(256);
         $form->addText("company", "Firma")
            ->setMaxLength(256);
         $form->addText("vatNo", "IČ")
            ->setMaxLength(256);
         $form->addText("vatId", "DIČ")
            ->setMaxLength(256);
      }

      $form->addSubmit("submit", "Přidat");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->user) {
         $form->setDefaults([
            'name' => $this->user->getName(),
            'phone' => $this->user->getPhone(),
            'email' => $this->user->getEmail(),
            'note' => $this->user->getNote(),

            'street' => $this->user->getStreet(),
            'city' => $this->user->getCity(),
            'zip' => $this->user->getZip(),
            'company' => $this->user->getCompany(),
            'vatNo' => $this->user->getVatNo(),
            'vatId' => $this->user->getVatId(),

            'username' => $this->user->getUsername(),

            'accountStatus' => $this->user->getAccountStatus()->getValue(),
         ]);

         if($this->formType == self::FORM_USER_OF_SYSTEM) {
            $form['role']->setDefaultValue($this->user->getRole()->getValue());


            $ticketTypeNotifications = [];
            foreach ($this->user->getTicketTypeNotifications() AS $ticketTypeNotification) {
               $ticketTypeNotifications[] = $ticketTypeNotification->getType();
            }
            $form['ticketTypeEmailNotification']->setDefaultValue($ticketTypeNotifications);
         }

         $form['password']->setRequired(false);
         $form['submit']->caption = "Uložit";
      }

      return $form;
   }

   /**
    * Validate form
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      //avatar is only for users of system (not customers)
      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         /** @var FileUpload $avatar */
         $avatar = $form['avatar']->getValue();
         $width = $avatar->getImageSize()[0];
         $height = $avatar->getImageSize()[1];


         if($avatar->hasFile() && ($width != 215 && $height != 215)) {
            $form['avatar']->addError("rozměr musí být 215px x 215px");
         }
      }
   }

   /**
    * Process form and create user
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->user) { //update existing user
         $user = $this->user;
      } else { //new user
         $user = new User(
            $form['name']->getValue(),
            $this->formType == self::FORM_USER_OF_SYSTEM ? UserRole::get($form['role']->getValue()) : UserRole::get(UserRole::ROLE_CUSTOMER)
         );
      }

      $user->setAccountStatus($form['accountStatus']->getValue());
      $user->setName($form['name']->getValue());
      $user->setEmail($form['email']->getValue());
      $user->setUsername($form['username']->getValue());
      if($form['password']->isFilled()) {
         $user->setPassword($form['password']->getValue());
      }
      $user->setPhone($form['phone']->getValue());
      $user->setNote($form['note']->getValue());

      if($this->formType == self::FORM_CUSTOMER) {
         $user->setStreet($form['street']->getValue());
         $user->setCity($form['city']->getValue());
         $user->setZip($form['zip']->getValue());
         $user->setCompany($form['company']->getValue());
         $user->setVatNo($form['vatNo']->getValue());
         $user->setVatId($form['vatId']->getValue());
      }

      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         $this->processTicketNotificationSubscribing($form, $user);
      }

      try{
         if($this->user) {
            $this->userRepository->save($user);
            $action = self::ACTION_SAVED;
         } else {
            $this->userRepository->create($user);
            $action = self::ACTION_CREATED;
         }

      } catch (NotUniqueEmailException $e) {
         $form['email']->addError("Email je již přiřazen k jinému uživatNoeli");
         return;
      } catch (NotUniqueUsernameException $e) {
         $form['username']->addError("UživatNoelské jméno je již přiřazeno k jinému uživatNoeli");
         return;
      } catch (MissingAdminException $e) {
         $form['role']->addError("Roli nelze změnit, jelikož by v systému nebyl žádný uživatNoel s oprávněním administrátora");
         return;
      }

      if($this->formType == self::FORM_USER_OF_SYSTEM) {
         /** @var FileUpload $avatar */
         $avatar = $form['avatar']->getValue();

         //save avatar if creating user was successfully
         if($user->getId() && $avatar->hasFile()) {
            $this->avatarStorage->saveFile($avatar, $user);
         }
      }

      $this->onSuccess($user, $action);
   }

   private function processTicketNotificationSubscribing(BaseForm $form, User $user)
   {
      $selectedTypes = $form['ticketTypeEmailNotification']->getValue();

      //unsubscribe
      foreach ($user->getTicketTypeNotifications() as $ticketTypeNotification) {
         if(!in_array($ticketTypeNotification->getType(), $selectedTypes)) {
            $user->removeTicketTypeNotification($ticketTypeNotification);
            $this->em->remove($ticketTypeNotification);
         } else {
            unset($selectedTypes[array_search($ticketTypeNotification->getType(), $selectedTypes)]);
         }
      }

      //subscribe
      foreach ($selectedTypes AS $type) {
         $user->addTicketTypeNotification($type);
      }
   }

   /**
    * Remove user avatar
    */
   public function handleRemoveAvatNoar()
   {
      $this->avatarStorage->deleteFile($this->user);
      $this->getPresenter()->flashMessage("Avatar smazán", FlashMessage::TYPE_SUCCESS);
      $this->redrawControl("avatar");
   }

   /**
    * Request for getting data from ARES
    * @param string|null $vatNo
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessage("Načtení dat proběhlo úspěšně");
         $this->redrawControl();
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessage("Neexistující IČ", FlashMessage::TYPE_WARNING);
         $this->redrawControl();
      }
      catch (UnableToConnect $e) {
         $this->presenter->flashMessageNotify("Nepodařilo se spojit s portálem ARES", FlashMessageNotify::TYPE_WARNING);
      }
   }

   public function render()
   {
      $tpl = $this->template;

      if($this->formType == self::FORM_CUSTOMER) {
         $tpl->setFile(__DIR__ . "/templateCustomer.latte");
      } else {
         $tpl->setFile(__DIR__ . "/templateUser.latte");
      }

      return $tpl->render();
   }

   /**
    * @return User
    */
   public function getUser(): ?User
   {
      return $this->user;
   }

   /**
    * @param User $user
    */
   public function setUser(User $user)
   {
      $this->user = $user;
   }

   /**
    * @return string
    */
   public function getAvatarPath() : string
   {
      return $this->avatarStorage->getFilePath($this->getUser());
   }

   /**
    * @return string
    */
   public function hasAvatar() : string
   {
      if(is_null($this->getUser())) {
         return false;
      }

      return $this->avatarStorage->isExists($this->getUser());
   }

   public function handleRemoveAvatar()
   {
      $this->avatarStorage->deleteFile($this->user);
      $this->getPresenter()->flashMessage("Avatar smazán", FlashMessage::TYPE_SUCCESS);
      $this->redrawControl("avatar");
   }
}