<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditTaxRateForm;


use App\Forms\BaseForm;
use App\Forms\Controls\ColorPicker\ColorPicker;
use App\Model\Entities\TaxRate;
use App\Model\Enums\TaxRateType;
use App\Model\Repositories\TaxRateRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class AddEditTaxRateForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSuccess;

   /** @var TaxRate|null */
   private $taxRate;

   /**
    * AddEditTaxRateForm constructor.
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Název")
         ->setRequired()
         ->setMaxLength(32);

      $form->addSelect("type", "Typ", TaxRateType::getNamedTypes())
         ->setRequired();

      $form->addText("value", "Sazba v %")
         ->addRule(BaseForm::FLOAT)
         ->setRequired();

      $form->addColorPicker("color", "Barevné označení", ColorPicker::EXTENDED_COLORS)
         ->setNullable()
         ->setRequired(false);

      $form->addSubmit("submit", "Přidat");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->taxRate) { //edit existing entity
         $form['submit']->caption = "Uložit";

         $form->setDefaults([
            "name"    =>    $this->taxRate->getName(),
            "type"    =>    $this->taxRate->getType()->getValue(),
            "value"    =>    $this->taxRate->getValue(),
            "color"   =>    $this->taxRate->getColor(),
         ]);
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      //check validity of tax rate
      if($form['value']->getValue() < 0 || $form['value']->getValue() > 100) {
         $form['value']->addError("Hodnota musí být v intervalu 0..100");
      }
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $this->em->beginTransaction();

      if($this->taxRate) { //edit existing taxRate
         $taxRate = $this->taxRate;
         $taxRate->setName($form['name']->getValue());
         $taxRate->setType(TaxRateType::get($form['type']->getValue()));
         $taxRate->setValue($form['value']->getValue());
      } else { //create new taxRate
         $taxRate = new TaxRate(
            $form['name']->getValue(),
            TaxRateType::get($form['type']->getValue()),
            $form['value']->getValue()
         );
         $this->em->persist($taxRate);
      }

      $taxRate->setColor($form['color']->getValue());

      /** @var TaxRateRepository $taxRepo */
      $taxRepo = $this->em->getRepository(TaxRate::class);
      $taxRepo->synchronizeValuesByType($taxRate);

      $this->em->flush($taxRate);
      $this->em->commit();

      $this->onSuccess($taxRate);
   }

   /**
    * @return TaxRate|null
    */
   public function getTaxRate() : ?TaxRate
   {
      return $this->taxRate;
   }

   /**
    * @param TaxRate|null $taxRate
    */
   public function setTaxRate($taxRate)
   {
      $this->taxRate = $taxRate;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}