<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditTaxRateForm;


use Kdyby\Doctrine\EntityManager;

class AddEditTaxRateFormFactory
{
   /** @var EntityManager */
   private $em;

   /**
    * AddEditTaxRateFormFactory constructor.
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return AddEditTaxRateForm
    */
   public function create() : AddEditTaxRateForm
   {
      $component = new AddEditTaxRateForm($this->em);

      return $component;
   }
}