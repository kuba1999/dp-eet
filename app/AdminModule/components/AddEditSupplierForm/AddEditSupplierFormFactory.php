<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditSupplierForm;


use Kdyby\Doctrine\EntityManager;

class AddEditSupplierFormFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create() : AddEditSupplierForm
   {
      $component = new AddEditSupplierForm(
         $this->em
      );

      return $component;
   }
}