<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditSupplierForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Settings;
use App\Model\Entities\Supplier;
use App\Model\Services\AresSellerDataService\AresSellerDataService;
use App\Model\Services\AresSellerDataService\UnknownVatNoException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class AddEditSupplierForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSuccess;

   /** @var Supplier|null */
   private $supplier;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Jméno")
         ->setRequired()
         ->setMaxLength(255);

      $form->addText("address", "Adresa")
         ->setRequired(false)
         ->setMaxLength(255);

      $form->addText("vatNo", "IČ")
         ->setRequired(false)
         ->setMaxLength(8);

      $form->addText("vatId", "DIČ")
         ->setRequired(false)
         ->setMaxLength(12);

      $form->addText("phone", "Telefon")
         ->setRequired(false)
         ->setMaxLength(255);

      $form->addText("email", "Email")
         ->setRequired(false)
         ->setType("email")
         ->addRule(BaseForm::EMAIL)
         ->setMaxLength(255);

      $form->addCheckbox("visible", "Zobrazovat")
         ->setRequired(false)
         ->setDefaultValue(true);


      $form->addEditor("note", "Poznámka")
         ->setRequired(false);

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->supplier) { //edit existing entity
         $form['submit']->caption = "Uložit";

         $form->setDefaults([
            "name"      =>    $this->supplier->getName(),
            "address"   =>    $this->supplier->getAddress(),
            "vatNo"     =>    $this->supplier->getVatNo(),
            "vatId"     =>    $this->supplier->getVatId(),
            "phone"     =>    $this->supplier->getPhone(),
            "email"     =>    $this->supplier->getEmail(),
            "visible"   =>    $this->supplier->isVisible(),
            "note"      =>    $this->supplier->getNote(),
         ]);
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->supplier) { //edit existing company
         $supplier = $this->supplier;
         $supplier->setName($form['name']->getValue());
      } else { //create new company
         $supplier = new Supplier($form['name']->getValue());
         $this->em->persist($supplier);
      }

      $supplier->setAddress($form['address']->getValue());
      $supplier->setVatId($form['vatId']->getValue());
      $supplier->setVatNo($form['vatNo']->getValue());
      $supplier->setEmail($form['email']->getValue());
      $supplier->setPhone($form['phone']->getValue());
      $supplier->setVisible($form['visible']->getValue());
      $supplier->setNote($form['note']->getValue());

      $this->em->flush($supplier);

      $this->onSuccess($supplier);
   }

   /**
    * Request for getting data from ARES
    * @param string|null $vatNo
    */
   public function handleLoadCompanyData(string $vatNo = null)
   {
      try{
         $data = AresSellerDataService::getData($vatNo);

         $this->presenter->payload->ares = $data;
         $this->presenter->flashMessage("Načtení dat proběhlo úspěšně");
         $this->redrawControl();
      }
      catch (UnknownVatNoException $e) {
         $this->presenter->flashMessage("Neexistující IČ", FlashMessage::TYPE_WARNING);
         $this->redrawControl();
      }
   }

   /**
    * @return Supplier|null
    */
   public function getSupplier(): ?Supplier
   {
      return $this->supplier;
   }

   /**
    * @param Supplier $supplier
    */
   public function setSupplier(Supplier $supplier)
   {
      $this->supplier = $supplier;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}