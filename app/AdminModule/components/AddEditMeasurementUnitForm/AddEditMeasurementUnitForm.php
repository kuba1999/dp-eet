<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditMeasurementUnitForm;


use App\Forms\BaseForm;
use App\Model\Entities\MeasurementUnit;
use App\Model\Enums\UnitFormat;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class AddEditMeasurementUnitForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSuccess;

   /** @var MeasurementUnit|null */
   private $measurementUnit;

   /**
    * AddEditMeasurementUnitForm constructor.
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Označení")
         ->setRequired()
         ->setMaxLength(32);

      $form->addText("measurementUnit", "Měrná jednotka")
         ->setNullable()
         ->setMaxLength(16)
         ->setRequired(false);

      $form->addSelect("measurementUnitFormat", "Formát jednotky", UnitFormat::getNamedFormats())
         ->setPrompt("-")
         ->setRequired(false);

      $form->addText("saleUnit", "Prodejní jednotka")
         ->setMaxLength(16)
         ->setRequired();

      $form->addSelect("saleUnitFormat", "Formát jednotky", UnitFormat::getNamedFormats())
         ->setPrompt("-")
         ->setRequired(false);

      $form->addText("coefficient", "Konverzní koeficient")
         ->setNullable()
         ->setRequired(false)
         ->addConditionOn($form['measurementUnit'], BaseForm::FILLED)
         ->setRequired(true);

      $form->addSubmit("submit", "Přidat");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->measurementUnit) { //edit existing entity
         $form['submit']->caption = "Uložit";

         $form->setDefaults([
            "name"    =>    $this->measurementUnit->getName(),
            "measurementUnit"    =>    $this->measurementUnit->getMeasurementUnit(),
            "measurementUnitFormat"   =>    $this->measurementUnit->getMeasurementUnitFormat() ? $this->measurementUnit->getMeasurementUnitFormat()->getValue() : null,
            "saleUnit"    =>    $this->measurementUnit->getSaleUnit(),
            "saleUnitFormat"   =>    $this->measurementUnit->getSaleUnitFormat() ? $this->measurementUnit->getSaleUnitFormat()->getValue() : null,
            "coefficient"   =>    $this->measurementUnit->getConversionCoefficient(),
         ]);
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      //check validity of coefficient
      if($form['coefficient']->getValue() < 0) {
         $form['value']->addError("Hodnota koeficientu nesmí být záporné číslo");
      }
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->measurementUnit) { //edit existing measurementUnit
         $measurementUnit = $this->measurementUnit;
         $measurementUnit->setName($form['name']->getValue());
      } else { //create new measurementUnit
         $measurementUnit = new MeasurementUnit(
            $form['name']->getValue(),
            $form['saleUnit']->getValue()
         );
         $this->em->persist($measurementUnit);
      }

      $measurementUnit->setSaleUnit($form['saleUnit']->getValue());
      if(!$form['saleUnitFormat']->isFilled()) {
         $measurementUnit->setSaleUnitFormat(null);
      } else {
         $measurementUnit->setSaleUnitFormat(UnitFormat::get((int)$form['saleUnitFormat']->getValue()));
      }

      if(!$form['measurementUnit']->isFilled()) {
         $measurementUnit->setMeasurementUnit(null);
         $measurementUnit->setMeasurementUnitFormat(null);
         $measurementUnit->setConversionCoefficient(null);
      } else {
         $measurementUnit->setMeasurementUnit($form['measurementUnit']->getValue());
         $measurementUnit->setConversionCoefficient($form['coefficient']->getValue());

         if(!$form['measurementUnitFormat']->isFilled()) {
            $measurementUnit->setMeasurementUnitFormat(null);
         } else {
            $measurementUnit->setMeasurementUnitFormat(UnitFormat::get((int)$form['measurementUnitFormat']->getValue()));
         }
      }

      $this->em->flush($measurementUnit);

      $this->onSuccess($measurementUnit);
   }

   /**
    * @return MeasurementUnit|null
    */
   public function getMeasurementUnit() : ?MeasurementUnit
   {
      return $this->measurementUnit;
   }

   /**
    * @param MeasurementUnit|null $measurementUnit
    */
   public function setMeasurementUnit($measurementUnit)
   {
      $this->measurementUnit = $measurementUnit;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}