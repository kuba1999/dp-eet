<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditMeasurementUnitForm;


use Kdyby\Doctrine\EntityManager;

class AddEditMeasurementUnitFormFactory
{
   /** @var EntityManager */
   private $em;

   /**
    * AddEditMeasurementUnitFormFactory constructor.
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return AddEditMeasurementUnitForm
    */
   public function create() : AddEditMeasurementUnitForm
   {
      $component = new AddEditMeasurementUnitForm($this->em);

      return $component;
   }
}