<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditNewsForm;


use Kdyby\Doctrine\EntityManager;

class AddEditNewsFormFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create() : AddEditNewsForm
   {
      $component = new AddEditNewsForm(
         $this->em
      );

      return $component;
   }
}