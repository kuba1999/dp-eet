<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditNewsForm;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\News;
use App\Model\Enums\Language;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\DateTime;

class AddEditNewsForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var News */
   private $news;

   /** @var callable */
   public $onSuccess;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("title", "Titulek")
         ->setRequired(true)
         ->setMaxLength(64);

      $form->addEditor("content", "Obsah")
         ->setRequired(true);

      $form->addDatePicker("date", "Datum")
         ->setDefaultValue(new DateTime())
         ->setRequired(true);

      $form->addUpload("image", "Obrázek")
         ->addRule(BaseForm::IMAGE)
         ->setRequired(false);

      $form->addCheckbox("published", "publikováno")
         ->setDefaultValue(true)
         ->setRequired(false);

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->news) {
         $form->setDefaults([
            'date' => $this->news->getDate(),
            'published' => $this->news->isPublished(),
            'title'  => $this->news->getTitle(),
            'content'   => $this->news->getContent()
         ]);

         $form['submit']->caption = "Uložit";
      }

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      if($this->news) { //update existing news
         $news = $this->news;
         $news->setTitle($form['title']->getValue());
         $news->setContent($form['content']->getValue());
         $news->setDate($form['date']->getValue());
      } else { //new news
         $news = new News(
            $form['title']->getValue(),
            $form['content']->getValue(),
            $form['date']->getValue()
         );
      }

      $news->setPublished($form['published']->getValue());


      if($this->news) {
         $this->em->flush($news);
      } else {
         $this->em->persist($news);
         $this->em->flush($news);
      }

      $this->onSuccess($news);
   }

   public function handleDeleteImage()
   {
      $this->newsImageStorage->deleteFile($this->news);
      $this->presenter->flashMessage("Obrázek smazán", FlashMessage::TYPE_SUCCESS);
      $this->redrawControl("image");
   }


   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }

   /**
    * @return News
    */
   public function getNews(): News
   {
      return $this->news;
   }

   /**
    * @param News $news
    */
   public function setNews(News $news)
   {
      $this->news = $news;
   }
}