<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditCashForm;


use Kdyby\Doctrine\EntityManager;

class AddEditCashFormFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create() : AddEditCashForm
   {
      $component = new AddEditCashForm(
         $this->em
      );

      return $component;
   }
}