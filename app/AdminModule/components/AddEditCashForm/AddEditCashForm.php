<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditCashForm;


use App\Forms\BaseForm;
use App\Model\Entities\Cash;
use App\Model\Entities\SettingsEet;
use App\Model\Entities\User;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\UserRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class AddEditCashForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var Cash|null */
   private $cash;

   /** @var callable */
   public $onSuccess;

   /** @var SettingsEet */
   private $settingsEet;

   /** @var UserRepository */
   private $userRepo;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsEet::class);
      $this->settingsEet = $settingsRepo->getEetSettings();

      $this->userRepo = $this->em->getRepository(User::class);
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[]  = "form-horizontal";

      $form->addText("name", "Název")
         ->setMaxLength(64)
         ->setRequired();

      if($this->settingsEet->isEnabled()) {
         $form->addText("premiseId", "Číslo provozovny")
            ->setAttribute("placeholder", $this->settingsEet->getDefaultPremiseId())
            ->setMaxLength(6)
            ->setNullable();

         $form->addText("cashRegisterId", "Číslo pokladny")
            ->setAttribute("placeholder", $this->settingsEet->getDefaultCashRegisterId())
            ->setMaxLength(6)
            ->setNullable();
      }

      $form->addMultiSelect("privileges", "Přiřazení uživatelé", $this->getAllUsersOfSystem());

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->cash) {
         $form['submit']->caption = "Uložit";

         $form->setDefaults([
            "name"            =>    $this->cash->getName(),
            "premiseId"       =>    $this->cash->getPremiseId(),
            "cashRegisterId"  =>    $this->cash->getCashRegisterId(),
            "privileges"      =>    $this->getPrivileges(),
         ]);
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->cash) {
         $cash = $this->cash;
         $cash->setName($form['name']->getValue());
      } else {
         $cash = new Cash($form['name']->getValue());
         $this->em->persist($cash);
      }

      if($this->settingsEet->isEnabled()) {
         $cash->setPremiseId($form['premiseId']->getValue());
         $cash->setCashRegisterId($form['cashRegisterId']->getValue());
      }

      //add or remove privilege
      /** @var User[] $users */
      $users = $this->userRepo->getAllUsersOfSystem()
         ->select("PARTIAL u.{id}")
         ->getQuery()->getResult();
      foreach ($users AS $user) {
         if(in_array($user->getId(), $form['privileges']->getValue())) {
            $cash->addPrivilege($user);
         } else {
            $cash->removePrivilege($user);
         }
      }

      $this->em->flush($cash);
      $this->onSuccess($cash);
   }

   private function getAllUsersOfSystem() : array
   {
      /** @var User[] $users */
      $users = $this->userRepo->getAllUsersOfSystem()
                  ->orderBy("u.name", "asc")
                  ->select("PARTIAL u.{id, name}")
                  ->getQuery()
                  ->getResult();

      $ret = [];
      foreach ($users AS $user) {
         $ret[$user->getId()] = $user->getName();
      }

      return $ret;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->eetEnabled = $this->settingsEet->isEnabled();

      return $tpl->render();
   }

   /**
    * @return Cash|null
    */
   public function getCash() : ?Cash
   {
      return $this->cash;
   }

   /**
    * @param Cash $cash
    */
   public function setCash(Cash $cash)
   {
      $this->cash = $cash;
   }

   private function getPrivileges() : array
   {
      $ret = [];

      foreach ($this->cash->getPrivileges() AS $privilege) {
         $ret[] = $privilege->getId();
      }

      return $ret;
   }
}