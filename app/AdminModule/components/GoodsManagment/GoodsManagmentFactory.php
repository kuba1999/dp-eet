<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\GoodsManagment;


use App\AdminModule\Components\AddEditProductCategoryModal\AddEditProductCategoryModalFactory;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use Kdyby\Doctrine\EntityManager;

class GoodsManagmentFactory
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationSchemeFactory;

   /** @var AddEditProductCategoryModalFactory */
   private $addEditProductCategoryModalFactory;

   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      AddEditProductCategoryModalFactory $addEditProductCategoryModalFactory
   ) {
      $this->em = $em;
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
      $this->addEditProductCategoryModalFactory = $addEditProductCategoryModalFactory;
   }

   public function create() : GoodsManagment
   {
      $component = new GoodsManagment(
         $this->em,
         $this->categoryLocationSchemeFactory,
         $this->addEditProductCategoryModalFactory
      );

      return $component;
   }
}