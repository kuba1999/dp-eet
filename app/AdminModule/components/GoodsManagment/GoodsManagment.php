<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\GoodsManagment;


use App\AdminModule\Components\AddEditProductCategoryModal\AddEditProductCategoryModal;
use App\AdminModule\Components\AddEditProductCategoryModal\AddEditProductCategoryModalFactory;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationScheme;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;
use App\Model\Entities\SettingsGlobal;
use App\Model\Helpers\BarcodeHelper;
use App\Model\Repositories\ProductCategoryRepository;
use App\Model\Repositories\ProductRepository;
use App\Model\Repositories\SettingsRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Controls\SubmitButton;

class GoodsManagment extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationSchemeFactory;

   /** @var AddEditProductCategoryModalFactory */
   private $addEditProductCategoryModalFactory;

   /** @var ProductRepository */
   private $productRepo;

   /** @var ProductCategoryRepository */
   private $productCategoryRepo;

   /**
    * ID of actual category
    * @var int @persistent
    */
   public $categoryId = null;

   /** @var string @persistent */
   public $searchedTerm = null;

   /** @var callable */
   public $onSuccess;

   /** @var callable */
   public $onCreateProduct;

   /** @var callable */
   public $onEditProduct;

   /**
    * @param EntityManager $em
    */
   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      AddEditProductCategoryModalFactory $addEditProductCategoryModalFactory
   ) {
      $this->em = $em;
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
      $this->addEditProductCategoryModalFactory = $addEditProductCategoryModalFactory;

      $this->productRepo = $this->em->getRepository(Product::class);
      $this->productCategoryRepo = $this->em->getRepository(ProductCategory::class);
   }

   /************************************************************************************************************ Form */
   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax spinner ajax-history";

      $form->addText("searchedTerm")
         ->setAttribute("placeholder", "Hledat")
         ->setValue($this->searchedTerm);

      $form->addSubmit("search")->onClick[] = [$this, "formSearch"];

      return $form;
   }

   public function formSearch(SubmitButton $button)
   {
      $form = $button->getForm();
      $searchedTerm = $form['searchedTerm']->getValue();

      $this->searchedTerm = $searchedTerm;
      $this->redrawControl("table");
      $this->redrawControl("searchForm");
   }

   /********************************************************************************************************* Signals */
   public function handleChangeCategory($categoryId)
   {
      $this->changeCategory($categoryId);
   }

   public function handleUp()
   {
      if($this->getActualCategory()->getParent()) {
         $categoryId = $this->getActualCategory()->getParent()->getId();
      } else {
         $categoryId = null;
      }

      $this->changeCategory($categoryId);
   }

   public function handleCreateCategory()
   {
      $this['addEditProductCategoryModal']->setParentCategory($this->getActualCategory());
      $this['addEditProductCategoryModal']->show();
   }

   public function handleCreateProduct()
   {
      $this->onCreateProduct($this->getActualCategory());
   }

   public function handleEditProduct(int $productId)
   {
      $product = $this->productRepo->find($productId);

      $this->onEditProduct($product);
   }

   public function handleRemoveProduct(int $productId)
   {
      /** @var Product $product */
      $product = $this->em->find(Product::class, $productId);

      if(!$product) {
         return;
      }

      $product->setArchived();
      $this->em->flush($product);

      try {
         $this->em->remove($product);
         $this->em->flush();

         $this->presenter->flashMessageNotify("Produkt smazán", FlashMessageNotify::TYPE_SUCCESS);
      }
      catch (ForeignKeyConstraintViolationException $e) {
         $this->presenter->flashMessageNotify("Produkt archivován", FlashMessageNotify::TYPE_WARNING);
      }
      $this->redrawControl();
   }

   public function handleEditCategory(int $categoryIdForEditing)
   {
      $category = $this->em->find(ProductCategory::class, $categoryIdForEditing);

      if(!$category) {
         return;
      }

      $this['addEditProductCategoryModal']->setCategory($category);
      $this['addEditProductCategoryModal']->show();
   }

   public function handleRemoveCategory(int $categoryId)
   {
      $category = $this->em->find(ProductCategory::class, $categoryId);

      if(!$category) {
         return;
      }

      $this->em->remove($category);

      try {
         $this->em->flush();
      }
      catch (ForeignKeyConstraintViolationException $e) {
         $this->presenter->flashMessageNotify("Kategorii nelze smazat, jelikož obsahuje položky", FlashMessageNotify::TYPE_DANGER);
      }
   }

   public function handleResetSearching()
   {
      $this->searchedTerm = null;
      $this['form']["searchedTerm"]->setValue(null);
      $this->redrawControl();
   }

   /**
    * Scanned code by barcode scanner
    * @param string $code
    */
   public function handleBarcodeScanned(string $code)
   {
      if(BarcodeHelper::isEanValid($code) &&
         ($count = $this->productRepo->countBy(["ean" => $code]))
      ) {
         if($count === 1) {
            $product = $this->productRepo->findOneBy(["ean" => $code]);

            $this->onEditProduct($product); //show product
         }
      }

      $this['form']["searchedTerm"]->setValue($code); //try search if EAN does not exist
      $this->searchedTerm = $code;
      $this->redrawControl();
   }

   /********************************************************************************************************** Helpers*/
   public function getCategories() : array
   {
      if(!empty($this->searchedTerm)) { //if searching is active
         $qb = $this->productCategoryRepo->searchCategories($this->searchedTerm);
      } else {
         $qb = $this->productCategoryRepo->getAll();
      }

      if($this->getActualCategory()) { //get products in root
         $qb->andWhere("c.parent = :parent")
            ->setParameter("parent", $this->getActualCategory());
      } else {
         $qb->andWhere("c.parent IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   public function getProducts() : array
   {
      if(!empty($this->searchedTerm)) { //if searching is active
         $qb = $this->productRepo->searchProducts($this->searchedTerm);
      } else {
         $qb = $this->productRepo->getAll();
      }

      if($this->getActualCategory()) { //get categories in root
         $qb->andWhere("p.category = :category")
            ->setParameter("category", $this->getActualCategory());
      } else {
         $qb->andWhere("p.category IS NULL");
      }

      return $qb->getQuery()->getResult();
   }

   /**
    * Search products and categories
    * searching is also executed in subcategories
    * @return array
    */
   public function searchProductsAndCategories()
   {
      /** @var Product[] $products */
      $products = $this->productRepo->searchProducts($this->searchedTerm, $this->getActualCategory())->getQuery()->getResult();
      $ret = [];
      foreach ($products AS $product) {
         if(is_null($product->getCategory())) {
            if(!isset($ret[""])) {
               $ret[null] = [
                  "location"      => null,
                  "sort"          => null,
                  "category"      => null
               ];
            }

            $ret[null]['products'][] = $product;
            continue;
         }

         /** @var string[] $location */
         $location = $this->productCategoryRepo->getNamedCategoryLocation($product->getCategory());

         if($this->getActualCategory()) {
            unset($location[$this->getActualCategory()->getId()]); //unset actual category name
         }

         if(!isset($ret[implode(",", array_keys($location))])) {
            $ret[implode(",", array_keys($location))] = [
               "location"      => $location,
               "sort"          => str_replace(" ", "", implode("", $location)),
               "category"      => $product->getCategory()
            ];
         }

         $ret[implode(",", array_keys($location))]['products'][] = $product;
      }

      //sort products
      foreach ($ret AS $key=>$item) {
         usort($ret[$key]['products'], function (Product $a,Product $b) {
            return ($a->getName() > $b->getName())  ? +1 : -1;
         });
      }

      /** @var ProductCategory[] $categories */
      $categories = $this->productCategoryRepo->searchCategories($this->searchedTerm, $this->getActualCategory())->getQuery()->getResult();

      foreach ($categories AS $category) {
         /** @var string[] $location */
         $location = $this->productCategoryRepo->getNamedCategoryLocation($category);

         if($this->getActualCategory()) {
            unset($location[$this->getActualCategory()->getId()]); //unset actual category name
         }

         if(!isset($ret[implode(",", array_keys($location))])) {
            $ret[implode(",", array_keys($location))] = [
               "location"      => $location,
               "sort"          => str_replace(" ", "", implode("", $location)),
               "category"      => $category,
            ];
         }
      }

      //sort categories
      usort($ret, function ($a,$b) {
         return $a['sort'] > $b['sort']  ? +1 : -1;
      });

      return $ret;
   }

   /**
    * @return ProductCategory|null
    */
   public function getActualCategory() : ?ProductCategory
   {
      if(is_null($this->categoryId)) { //root
         return null;
      }

      return $this->productCategoryRepo->find($this->categoryId);
   }

   /**
    * @param int|null $categoryId
    */
   public function changeCategory(int $categoryId = null)
   {
      $this->categoryId = $categoryId;

      $this->redrawControl();
      $this['categoryLocationScheme']->redrawControl();
   }

   /**
    * Shows where is subcategory/product located
    * @return CategoryLocationScheme
    */
   public function createComponentCategoryLocationScheme() : CategoryLocationScheme
   {
      $comp = $this->categoryLocationSchemeFactory->create($this->getActualCategory());

      $comp->setItemLink(function(ProductCategory $category = null){
         if($category) {
            return $this->link("changeCategory!", $category->getId());
         } else {
            return $this->link("changeCategory!", null);
         }
      });

      $comp->setItemLinkClass("ajax ajax-history spinner");

      return $comp;
   }

   public function createComponentAddEditProductCategoryModal() : AddEditProductCategoryModal
   {
      $comp = $this->addEditProductCategoryModalFactory->create();
      $comp->onCreated[] = function (ProductCategory $category) use($comp) {
         $this->flashMessageNotify("Kategorie *" . $category->getName() . "* vytvořena", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();
         $this->redrawControl();
      };
      $comp->onSaved[] = function (ProductCategory $category) use($comp) {
         $this->flashMessageNotify("Kategorie *" . $category->getName() . "* uložena", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();
         $this->redrawControl();
      };

      return $comp;
   }

   /**
    * @return bool
    */
   public function isWarehouseEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsGlobal::class);

      return $settingsRepo->isWarehouseEnabled();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}