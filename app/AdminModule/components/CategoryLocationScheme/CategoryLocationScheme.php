<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\CategoryLocationScheme;


use App\Model\Entities\ProductCategory;
use App\Model\Repositories\ProductCategoryRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class CategoryLocationScheme extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var ProductCategory|null */
   private $category;

   /** @var callable */
   private $itemLink;

   /** @var string|null */
   private $itemLinkClass;

   /** @var callable */
   public $onSuccess;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em, ProductCategory $category = null)
   {
      $this->em = $em;
      $this->category = $category;
   }

   public function getItems() : array
   {
      /** @var ProductCategoryRepository $productCategoryRepo */
      $productCategoryRepo = $this->em->getRepository(ProductCategory::class);

      return $productCategoryRepo->getCategoryLocation($this->category);
   }

   public function setItemLink(callable $itemLink)
   {
      $this->itemLink = $itemLink;
   }

   public function hasItemLink() : bool
   {
      return is_callable($this->itemLink);
   }

   public function renderItemLink(ProductCategory $category = null)
   {
      return call_user_func($this->itemLink, $category);
   }

   /**
    * @return string
    */
   public function getItemLinkClass(): ?string
   {
      return $this->itemLinkClass;
   }

   /**
    * @param string $itemLinkClass
    */
   public function setItemLinkClass(string $itemLinkClass)
   {
      $this->itemLinkClass = $itemLinkClass;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}