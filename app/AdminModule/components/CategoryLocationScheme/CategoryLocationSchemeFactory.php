<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\CategoryLocationScheme;


use App\Model\Entities\ProductCategory;
use Kdyby\Doctrine\EntityManager;

class CategoryLocationSchemeFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create(ProductCategory $category = null) : CategoryLocationScheme
   {
      $component = new CategoryLocationScheme(
         $this->em,
         $category
      );

      return $component;
   }
}