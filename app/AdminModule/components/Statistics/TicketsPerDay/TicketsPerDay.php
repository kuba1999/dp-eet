<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\TicketsPerDay;

use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\Company;
use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Entities\UserTicketTypeNotification;
use App\Model\Enums\Language;
use App\Model\Enums\TicketType;
use App\Model\Enums\UserAccountStatus;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\MissingAdminException;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Repositories\CompanyRepository;
use App\Model\Repositories\TicketMessageRepository;
use App\Model\Repositories\TicketRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Base;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class TicketsPerDay extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var int @persistent */
   public $month;

   /** @var int @persistent */
   public $year;

   public function __construct(EntityManager $em)
   {
      $this->em = $em;

      $this->year = date("Y");
      $this->month = date("n");
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "form-inline ajax";

      $form->addSelect("year", null, $this->getYears())
         ->setAttribute('data-autosubmit-change', TRUE)
         ->setDefaultValue($this->year);
      $form->addSelect("month", null, $this->getMonths())
         ->setAttribute('data-autosubmit-change', TRUE)
         ->setDefaultValue($this->month);

      $form->onSuccess[] = [$this, "changeMonth"];

      return $form;
   }

   public function changeMonth(BaseForm $form)
   {
      $this->month = $form['month']->getValue();
      $this->year = $form['year']->getValue();

      $this->redrawControl("content");
   }

   public function getLabelsForGraph()
   {
      $numberOfDays = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);

      $ret = [];
      for ($i = 1; $i <= $numberOfDays; $i++) {
         $ret[] = $i . " ." . $this->month . " . (" . $this->getDays()[date("l", strtotime($i.".".$this->month.".".$this->year))] . ")";
      }

      return $ret;
   }

   public function getDataForGraph()
   {
      $sql = "
         SELECT DAY(created) AS day, COUNT(id) AS `count`  
         FROM ticket  
         WHERE MONTH(created) = :month and YEAR(created) = :year 
         GROUP BY DAY(created) 
      ";

      $stmt = $this->em->getConnection()->prepare($sql);
      $stmt->bindValue(':month',$this->month);
      $stmt->bindValue(':year',$this->year);
      $stmt->execute();

      $ret = [];
      for ($i = 1; $i <= date("t"); $i++) {
         $ret[] = 0;
      }

      foreach ($stmt->fetchAll() AS $value) {
         $ret[$value['day'] - 1] = (int)$value['count'];
      }

      return $ret;
   }

   private function getMonths()
   {
      return [
         1  =>   "leden",
         2  =>   "únor",
         3  =>   "březen",
         4  =>   "duben",
         5  =>   "květen",
         6  =>   "červen",
         7  =>   "červenec",
         8  =>   "srpen",
         9  =>   "září",
         10  =>   "říjen",
         11  =>   "listopad",
         12  =>   "prosinec",
      ];
   }

   private function getDays()
   {
      return [
         "Monday"  =>   "po",
         "Tuesday"  =>   "út",
         "Wednesday"  =>   "st",
         "Thursday"  =>   "čt",
         "Friday"  =>   "pá",
         "Saturday"  =>   "so",
         "Sunday"  =>   "ne",
      ];
   }

   private function getYears()
   {
      $ret = [];
      for ($i = $this->getMaxYear(); $i >= $this->getMinYear(); $i--) {
         $ret[$i] = $i;
      }

      return $ret;
   }

   private function getMaxYear()
   {
      $ticketRepo = $this->em->getRepository(Ticket::class);
      $qb = $ticketRepo->createQueryBuilder("t")
               ->select("MAX(YEAR(t.created))");

      return $qb->getQuery()->getSingleScalarResult();
   }

   private function getMinYear()
   {
      $ticketRepo = $this->em->getRepository(Ticket::class);
      $qb = $ticketRepo->createQueryBuilder("t")
               ->select("MIN(YEAR(t.created))");

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}