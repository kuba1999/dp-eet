<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\TicketsPerDay;


use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Storage\AvatarStorage;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class TicketsPerDayFactory
{
   /** @var EntityManager */
   private $em;

   /**
    * TicketsPerMonthFactory constructor.
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   /**
    * @return TicketsPerDay
    */
   public function create() : TicketsPerDay
   {
      $component = new TicketsPerDay($this->em);

      return $component;
   }
}