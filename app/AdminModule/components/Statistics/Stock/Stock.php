<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Stock;


use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDay;
use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDayFactory;
use App\Components\BaseControl;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\DataGridFactory;
use App\Components\DataGrid\Row;
use App\Forms\BaseForm;
use App\Model\Entities\Cash;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Product;
use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Entities\SellerData;
use App\Model\Entities\TaxRate;
use App\Model\Entities\Ticket;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TicketStatus;
use App\Model\Helpers\DateHelper;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\TicketRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;
use Nette\Utils\Json;

class Stock extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var DataGridFactory */
   private $dataGridFactory;

   function __construct(EntityManager $em, DataGridFactory $dataGridFactory)
   {
      $this->em = $em;
      $this->dataGridFactory = $dataGridFactory;
   }


   protected function createComponentProducts() : DataGrid
   {
      $comp = $this->dataGridFactory->create();

      $comp->addColumnText("name", "Produkt")
         ->setFilterText();
      $comp->addColumnNumber("price", $this->isSellerTaxablePerson() ? "Cena MJ bez DPH" : "Cena MJ")
         ->setFormat(2)
         ->setValueSuffix(" Kč")
         ->addCellClass("text-right");
      if($this->isSellerTaxablePerson()) {
         $comp->addColumnNumber("priceWithTax", "Cena MJ s DPH")
            ->setVirtualColumn()
            ->setFormat(2)
            ->setValueSuffix(" Kč")
            ->addCellClass("text-right");
      }
      $comp->addColumnNumber("quantityInStock", "Stav skladu")
         ->setFormat(2)
         ->addCellClass("text-right")
         ->addCellClass(function (Product $product, Row $row) {
            if ($product->getQuantityInStock() === 0.0) {
               return "bg-warning";
            } elseif ($product->getQuantityInStock() > 0.0) {
               return "bg-success";
            } elseif ($product->getQuantityInStock() < 0.0) {
               return "bg-red";
            }
         });
      $comp->addColumnNumber("priceSum", $this->isSellerTaxablePerson() ? "Celkem bez DPH" : "Celkem")
         ->setVirtualColumn()
         ->setFormat(2)
         ->setValueSuffix(" Kč")
         ->addCellClass("text-right");

      if($this->isSellerTaxablePerson()) {
         $comp->addColumnNumber("priceSumWithTax", "Celkem s DPH")
            ->setVirtualColumn()
            ->setFormat(2)
            ->setValueSuffix(" Kč")
            ->addCellClass("text-right");
      }

      $qb = $this->em->createQueryBuilder()
         ->select("PARTIAL p.{id, name, price, quantityInStock}")
         ->addSelect("taxRate.value AS tax")
         ->addSelect("(p.price*(100+taxRate.value) /100) AS priceWithTax")
         ->addSelect("(p.price*p.quantityInStock) AS priceSum")
         ->addSelect("((p.price*p.quantityInStock)*(100+taxRate.value) /100) AS priceSumWithTax")
         ->from(Product::class, "p")
         ->leftJoin("p.taxRate", "taxRate");

      $comp->setDataSource($qb);

      return $comp;
   }

   public function getSum($withTax) : float
   {
      if($withTax) {
         $qb = $this->em->createQueryBuilder()
            ->select("SUM((p.price*p.quantityInStock)*(100+taxRate.value) /100)")
            ->from(Product::class, "p")
            ->leftJoin("p.taxRate", "taxRate");

         return $qb->getQuery()->getSingleScalarResult();
      }
      else {
         $qb = $this->em->createQueryBuilder()
            ->select("SUM(p.price*p.quantityInStock)")
            ->from(Product::class, "p")
            ->leftJoin("p.taxRate", "taxRate");

         return $qb->getQuery()->getSingleScalarResult();
      }
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}