<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Stock;


use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDayFactory;
use App\Components\DataGrid\DataGridFactory;
use App\Model\Storage\DownloadsFileStorage;
use App\Model\Storage\FaqFileStorage;
use App\Model\Storage\NewsImageStorage;
use Kdyby\Doctrine\EntityManager;

class StockFactory
{
   /** @var EntityManager */
   private $em;

   /** @var DataGridFactory */
   private $dataGridFactory;

   function __construct(EntityManager $em, DataGridFactory $dataGridFactory)
   {
      $this->em = $em;
      $this->dataGridFactory = $dataGridFactory;
   }

   public function create() : Stock
   {
      $component = new Stock(
         $this->em,
         $this->dataGridFactory
      );

      return $component;
   }
}