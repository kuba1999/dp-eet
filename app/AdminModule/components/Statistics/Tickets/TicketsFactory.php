<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Tickets;


use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDayFactory;
use App\Model\Storage\DownloadsFileStorage;
use App\Model\Storage\FaqFileStorage;
use App\Model\Storage\NewsImageStorage;
use Kdyby\Doctrine\EntityManager;

class TicketsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var TicketsPerDayFactory */
   private $ticketsPerDayFactory;

   function __construct(EntityManager $em, TicketsPerDayFactory $ticketsPerDayFactory)
   {
      $this->em = $em;
      $this->ticketsPerDayFactory = $ticketsPerDayFactory;
   }

   public function create() : Tickets
   {
      $component = new Tickets(
         $this->em,
         $this->ticketsPerDayFactory
      );

      return $component;
   }
}