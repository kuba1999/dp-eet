<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Tickets;


use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDay;
use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDayFactory;
use App\Forms\BaseForm;
use App\Model\Entities\Cash;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Entities\SellerData;
use App\Model\Entities\Ticket;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TicketStatus;
use App\Model\Helpers\DateHelper;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\TicketRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;
use Nette\Utils\Json;

class Tickets extends Control
{
   /** @var TicketRepository */
   private $ticketRepo;

   /** @var TicketsPerDayFactory */
   private $ticketsPerDayFactory;

   public function __construct(EntityManager $em, TicketsPerDayFactory $ticketsPerDayFactory)
   {
      /** @var TicketRepository $ticketRepo */
      $this->ticketRepo = $em->getRepository(Ticket::class);

      $this->ticketsPerDayFactory = $ticketsPerDayFactory;
   }

   public function createComponentTicketsPerDay() : TicketsPerDay
   {
      $component = $this->ticketsPerDayFactory->create();

      return $component;
   }

   public function getNewTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::NEW)
         ->andWhere("t.author = :author")
         ->setParameter("author", $this->presenter->getLoggedUser());

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getClosedTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::CLOSED)
         ->andWhere("t.author = :author")
         ->setParameter("author", $this->presenter->getLoggedUser());

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getOpenedTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status != :status")
         ->setParameter("status", TicketStatus::CLOSED)
         ->andWhere("t.author = :author")
         ->setParameter("author", $this->presenter->getLoggedUser());

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getNewMessageTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::ANSWERED)
         ->andWhere("t.author = :author")
         ->setParameter("author", $this->presenter->getLoggedUser());

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}