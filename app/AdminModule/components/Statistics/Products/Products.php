<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Products;


use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDay;
use App\AdminModule\Components\Statistics\TicketsPerDay\TicketsPerDayFactory;
use App\Components\BaseControl;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\DataGridFactory;
use App\Components\DataGrid\Row;
use App\Forms\BaseForm;
use App\Model\Entities\Cash;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Product;
use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Entities\SellerData;
use App\Model\Entities\TaxRate;
use App\Model\Entities\Ticket;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TicketStatus;
use App\Model\Helpers\DateHelper;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\TicketRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;
use Nette\Utils\Json;

class Products extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var DataGridFactory */
   private $dataGridFactory;

   function __construct(EntityManager $em, DataGridFactory $dataGridFactory)
   {
      $this->em = $em;
      $this->dataGridFactory = $dataGridFactory;
   }


   protected function createComponentProducts() : DataGrid
   {
      $comp = $this->dataGridFactory->create();

      $comp->addColumnText("name", "Produkt")
         ->setFilterText();
      $comp->addColumnNumber("count", "Počet")
         ->setVirtualColumn()
         ->setFormat(2)
         ->addCellClass("text-right");
      $comp->addColumnNumber("priceSum", "Celková cena")
         ->setReplacement([null => '0'])
         ->setVirtualColumn()
         ->setFormat(2)
         ->addCellClass("text-right");


      $qb = $this->em->createQueryBuilder()
         ->select("PARTIAL p.{id, name}")
         ->addSelect("COUNT(i.id * i.quantity) AS count")
         ->addSelect("SUM(i.unitPriceWithTax * i.quantity) AS priceSum")
         ->from(Product::class, "p")
         ->leftJoin(ReceiptItem::class, "i", "with", "i.product = p")
         ->groupBy("p.id");

      $comp->setDataSource($qb);

      return $comp;
   }

   public function productsCount() : int
   {
      $qb = $this->em->createQueryBuilder()
         ->select("COUNT(p.id)")
         ->from(Product::class, "p");

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}