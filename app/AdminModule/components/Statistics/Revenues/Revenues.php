<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Revenues;


use App\Forms\BaseForm;
use App\Model\Entities\Cash;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Entities\SellerData;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TypeOfDocument;
use App\Model\Helpers\DateHelper;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;
use Nette\Utils\Json;

class Revenues extends Control
{
   const GROUP_BY_DAY      =  0;
   const GROUP_BY_WEEK     =  1;
   const GROUP_BY_MONTH    =  2;
   const GROUP_BY_QUARTER  =  3;
   const GROUP_BY_YEAR     =  4;

   /** @var EntityManager */
   private $em;

   /** @var Group[] */
   private $groups = [];

   /** @var array @persistent */
   public $settings = [
      "default_from" => true
   ];

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function getGroups() : array
   {
      if($this->groups != []) {
         return $this->groups;
      }

      $this->loadItems();

      $this->groups = array_reverse($this->groups);

      return $this->groups;
   }

   private function loadItems()
   {
      $qb = $this->em->createQueryBuilder();
      $qb->select("PARTIAL i.{id, quantity, unitPriceWithTax, taxRateType, taxRateValue}, 
                     r.creationDate AS created, 
                     r.id AS receiptId,
                     c.name AS cashName
         ")
         ->from(ReceiptItem::class, "i")
         ->join("i.receipt", "r")
         ->join("r.cash", "c")
         ->orderBy("created", "desc")
         ->addOrderBy("c.name", "asc")
         ->where("r.typeOfDocument IN (:docTypes)")
         ->setParameter("docTypes", [
            TypeOfDocument::INVOICE,
            TypeOfDocument::TAX_INVOICE,
            TypeOfDocument::SIMPLIFIED_TAX_INVOICE,
            TypeOfDocument::CASH_RECEIPT,
            TypeOfDocument::CANCELLATION,
         ]);

      if($from = $this->getSettingsValue("from")) {
         $qb->andWhere("DATE(r.creationDate) >= DATE(:f)")->setParameter("f", $from);
      }
      elseif($this->settings['default_from']) {
         $from = (new \DateTime())->sub(new \DateInterval("P1M"));
         $qb->andWhere("DATE(r.creationDate) >= DATE(:f)")->setParameter("f", $from);
      }

      if($to = $this->getSettingsValue("to")) {
         $qb->andWhere("DATE(r.creationDate) <= DATE(:t)")->setParameter("t", $to);
      }

      if($cashIDs = $this->getSettingsValue("cash")) {
         $qb->andWhere("c.id IN (:c)")->setParameter("c", $cashIDs);
      }

      $items = $qb->getQuery()->getArrayResult();

      foreach ($items AS $item) {
         $this->addItem($item);
      }
   }

   private function addItem(array $data)
   {
      /** @var \DateTime $created */
      $created = $data['created'];

      //group by cash?
      if(is_null($this->getSettingsValue("cash"))) {
         $key = $this->getGroupKey($created); //no
      } else {
         $key = $this->getGroupKey($created) . $data['cashName']; //yes
      }

      if(isset($this->groups[$key])) {
         $group = $this->groups[$key];
      }
      elseif(is_null($this->getSettingsValue("cash"))) {
         $group = new Group($created, null);

         $this->groups[$key] = $group;
      }
      else {
         $group = new Group($created, $data['cashName']);

         $this->groups[$key] = $group;
      }

      $group->addItem(
         $data['receiptId'],
         TaxRateType::get((int)$data[0]['taxRateType']),
         $data[0]['taxRateValue'],
         $data[0]['unitPriceWithTax'],
         $data[0]['quantity']
      );
   }

   public function getTitle(Group $group) : string
   {
      switch ($this->getSettingsValue("groupBy")) {
         case self::GROUP_BY_DAY:
            return $group->getDate()->format("d. m. Y") . " | " . DateHelper::czechDay($group->getDate()->format("w"));
         case self::GROUP_BY_WEEK:
            return  "týden č. " . $group->getDate()->format("W") . " | " . $group->getDate()->format("Y");
         case self::GROUP_BY_MONTH:
            return  DateHelper::czechMonth($group->getDate()->format("m")) . " | " . $group->getDate()->format("Y");
         case self::GROUP_BY_QUARTER:
            return  DateHelper::quarter($group->getDate()->format("m")) . ". kvartál | " . $group->getDate()->format("Y");
         case self::GROUP_BY_YEAR:
            return $group->getDate()->format("Y");
      }
   }

   public function getGroupKey(\DateTime $created) : string
   {
      switch ($this->getSettingsValue("groupBy")) {
         case self::GROUP_BY_DAY:
            return self::GROUP_BY_DAY . "-" . $created->format("dmY");
         case self::GROUP_BY_WEEK:
            return  self::GROUP_BY_WEEK . "-" . $created->format("WY");
         case self::GROUP_BY_MONTH:
            return  self::GROUP_BY_MONTH . "-" . $created->format("mY");
         case self::GROUP_BY_QUARTER:
            return  self::GROUP_BY_QUARTER . "-" . DateHelper::quarter($created->format("m")) . $created->format("Y");
         case self::GROUP_BY_YEAR:
            return self::GROUP_BY_YEAR . "-" . $created->format("Y");
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////// chart
   private function getChartData()
   {
      $ret = [];
      /** @var Group $group */
      foreach ($this->getGroups() AS $group) {
         $ret[] = [
            "date" => $this->getChartDateValue($group),
            "value" => $group->getPriceSum(true)
         ];
      }


      return $ret;
   }

   /**
    * @param Group $group
    * @return string
    */
   public function getChartDateValue(Group $group) : string
   {
      switch ($this->getSettingsValue("groupBy")) {
         case self::GROUP_BY_DAY:
            return $group->getDate()->format(DATE_ATOM);
         case self::GROUP_BY_WEEK:
            $date = clone $group->getDate();
            return $date->modify("this week")->format(DATE_ATOM);
         case self::GROUP_BY_MONTH:
            $date = clone $group->getDate();
            return $date->modify("first day of this month")->format(DATE_ATOM);
         case self::GROUP_BY_QUARTER:
            $quarter = DateHelper::quarter($group->getDate()->format("m")) - 1;
            $date = clone $group->getDate();
            return $date->modify("first day of January")
               ->add(new \DateInterval("P".$quarter*3 . "M"))
               ->sub(new \DateInterval("P1D"))
               ->format(DATE_ATOM);
         case self::GROUP_BY_YEAR:
            return $group->getDate()->format("Y");
      }
   }

   /////////////////////////////////////////////////////////////////////////////////////// filter
   public function handleChangeGroupBy(int $type)
   {
      $this->settings['groupBy'] = $type;

      $this->redrawControl("filter");
      $this->redrawControl("content");
   }

   /**
    * @return BaseForm
    */
   protected function createComponentFilter() : BaseForm
   {
      $form = new BaseForm();
      $form->addDatePicker("from", "datum (rozsah)")
         ->setAttribute("placeholder", "od")
         ->setNullable();
      $form->addDatePicker("to")
         ->setAttribute("placeholder", "do")
         ->setDefaultValue($this->getSettingsValue("to"))
         ->setNullable();
      $form->addMultiSelect("cash", null, $this->getCashItems())
         ->setDefaultValue($this->getSettingsValue("cash"));
      if($this->settings['default_from']) {
         $form['from']->setDefaultValue((new \DateTime())->sub(new \DateInterval("P1M")));
      }
      else {
         $form['from']->setDefaultValue($this->getSettingsValue("from"));
      }


      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      /** @var \DateTime $from */
      $from = $form['from']->getValue();

      /** @var \DateTime $to */
      $to = $form['to']->getValue();

      /** @var array $cash */
      $cash = $form['cash']->getValue();

      $this->settings['default_from'] = false;
      $this->settings['from'] = $from ? $from->format("d-m-Y") : null;
      $this->settings['to']   = $to ? $to->format("d-m-Y") : null;
      $this->settings['cash'] = $cash;

      $this->presenter->payload->url = $this->link('this');
      $this->redrawControl("content");
      $this->redrawControl("filter");
   }

   ////////////////////////////////////////////////////////////// helpers

   /**
    * Get actual value of settings item
    * @param string $key
    * @return bool|\DateTime|int|mixed|null|static
    */
   public function getSettingsValue(string $key)
   {
      if($key === "from") {
         if(!isset($this->settings['from']) || is_null($this->settings['from'])) {
            return null;
         }

         return \DateTime::createFromFormat("d-m-Y", $this->settings['from']);
      }
      elseif($key === "to") {
         if(!isset($this->settings['to']) || is_null($this->settings['to'])) {
            return null;
         }
         return \DateTime::createFromFormat("d-m-Y", $this->settings['to']);
      }
      elseif($key === "groupBy") {
         if(!isset($this->settings['groupBy'])) {
            return self::GROUP_BY_DAY;
         }
         elseif(!in_array($this->settings['groupBy'], [self::GROUP_BY_DAY, self::GROUP_BY_WEEK, self::GROUP_BY_MONTH, self::GROUP_BY_QUARTER, self::GROUP_BY_YEAR])) {
            return self::GROUP_BY_DAY;
         }

         return $this->settings['groupBy'];
      }
      elseif($key === "cash") {
         if(!isset($this->settings['cash']) || $this->settings['cash'] === []) {
            return null;
         }

         return $this->settings['cash'];
      }
   }

   private function getCashItems() : array
   {
      $ret = [];

      /** @var CashRepository $cashRepo */
      $cashRepo = $this->em->getRepository(Cash::class);
      /** @var Cash[] $cashItems */
      $cashItems = $cashRepo->getAll()->select("PARTIAL c.{id, name}")->getQuery()->getResult();

      foreach ($cashItems AS $cash) {
         $ret[$cash->getId()] = $cash->getName();
      }

      $this->em->clear(Cash::class);

      return $ret;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->chartData = $this->getChartData();

      return $tpl->render();
   }
}