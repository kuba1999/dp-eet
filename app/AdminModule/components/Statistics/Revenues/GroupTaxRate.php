<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Revenues;


use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;

class GroupTaxRate
{
   /** @var Group */
   private $group;

   /** @var TaxRateType */
   private $taxRateType;

   /** @var float|null */
   private $taxRateValue;

   /** @var float */
   private $priceWithTaxSum;

   /** @var int */
   private $itemsCount = 0;

   public function __construct(Group $group, TaxRateType $taxRateType, ?float $taxRateValue)
   {
      $this->group = $group;
      $this->taxRateType = $taxRateType;
      $this->taxRateValue = $taxRateValue;
   }

   /**
    * @param string $cashName
    * @param int $receiptId
    * @param float $unitPriceWithTax
    * @param float $quantity
    */
   public function addItem(int $receiptId, float $unitPriceWithTax, float $quantity)
   {
      $this->group->addReceipt($receiptId);
      $this->itemsCount++;
      $this->priceWithTaxSum += $unitPriceWithTax * $quantity;
   }

   /**
    * @param bool $withTax
    * @return float
    * @throws \Exception
    */
   public function getPriceSum(bool $withTax) : float
   {
      if(!$withTax && !$this->taxRateType->equalsValue(TaxRateType::_NONE_)) {
         return PriceHelper::calculatePriceWithoutVat($this->taxRateValue, $this->priceWithTaxSum);
      }

      return $this->priceWithTaxSum;
   }

   /**
    * @return float
    */
   public function getTaxAmount() : float
   {
      return $this->getPriceSum(true) - $this->getPriceSum(false);
   }

   /**
    * @return TaxRateType
    */
   public function getTaxRateType(): TaxRateType
   {
      return $this->taxRateType;
   }

   /**
    * @return float|null
    */
   public function getTaxRateValue(): ?float
   {
      return $this->taxRateValue;
   }

   /**
    * @return int
    */
   public function getItemsCount(): int
   {
      return $this->itemsCount;
   }
}