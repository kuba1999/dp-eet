<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Revenues;


use App\Model\Storage\DownloadsFileStorage;
use App\Model\Storage\FaqFileStorage;
use App\Model\Storage\NewsImageStorage;
use Kdyby\Doctrine\EntityManager;

class RevenuesFactory
{
   /** @var EntityManager */
   private $em;

   function __construct(EntityManager $em)
   {
      $this->em = $em;
   }

   public function create() : Revenues
   {
      $component = new Revenues(
         $this->em
      );

      return $component;
   }
}