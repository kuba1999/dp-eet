<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\Statistics\Revenues;


use App\Model\Enums\TaxRateType;

class Group
{
   /** @var GroupTaxRate[] */
   private $taxRates = [];

   /** @var array */
   private $receiptIDs = [];

   /** @var \DateTime */
   private $date;

   /** @var string|null */
   private $cashName;

   /**
    * Group constructor.
    * @param \DateTime $date
    */
   public function __construct(\DateTime $date, ?string $cashName)
   {
      $this->date = $date;
      $this->cashName = $cashName;
   }

   /**
    * @param string $cashName
    * @param int $receiptId
    * @param TaxRateType $taxRateType
    * @param float|null $taxRateValue
    * @param float $unitPriceWithTax
    * @param float $quantity
    */
   public function addItem(int $receiptId, TaxRateType $taxRateType, ?float $taxRateValue, float $unitPriceWithTax, float $quantity)
   {
      $taxRateGroup = $this->getTaxRateGroup($taxRateType, $taxRateValue);

      $taxRateGroup->addItem($receiptId, $unitPriceWithTax, $quantity);
   }

   /**
    * @param string $cashName
    * @param TaxRateType $taxRateType
    * @param float $taxRateValue
    * @return GroupTaxRate
    */
   private function getTaxRateGroup(TaxRateType $taxRateType, ?float $taxRateValue) : GroupTaxRate
   {
      $key = $taxRateType->getValue() . $taxRateValue;

      if(isset($this->taxRates[$key])) {
         return $this->taxRates[$key];
      } else {
         return $this->taxRates[$key] = new GroupTaxRate($this, $taxRateType, $taxRateValue);
      }
   }

   public function getTaxRates() : array
   {
      ksort($this->taxRates);

      return $this->taxRates;
   }

   /**
    * @return int
    */
   public function getReceiptsCount() : int
   {
      return count($this->receiptIDs);
   }

   public function getPriceSum(bool $withTax) : float
   {
      $sum = 0.0;

      foreach ($this->getTaxRates() AS $taxRateGroup)
      {
         $sum += $taxRateGroup->getPriceSum($withTax);
      }

      return $sum;
   }

   public function getTaxAmount() : float
   {
      return $this->getPriceSum(true) - $this->getPriceSum(false);
   }

   public function addReceipt(int $receiptId)
   {
      $this->receiptIDs[$receiptId] = null;
   }

   /**
    * @return string|null
    */
   public function getCashName(): ?string
   {
      return $this->cashName;
   }

   /**
    * @return \DateTime
    */
   public function getDate(): \DateTime
   {
      return $this->date;
   }
}