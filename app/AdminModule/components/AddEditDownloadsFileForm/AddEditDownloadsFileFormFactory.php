<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditDownloadsFileForm;


use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;

class AddEditDownloadsFileFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var DownloadsFileStorage */
   private $downloadsFileStorage;

   function __construct(EntityManager $em, DownloadsFileStorage $downloadsFileStorage)
   {
      $this->em = $em;
      $this->downloadsFileStorage = $downloadsFileStorage;
   }

   public function create() : AddEditDownloadsFileForm
   {
      $component = new AddEditDownloadsFileForm(
         $this->em,
         $this->downloadsFileStorage
      );

      return $component;
   }
}