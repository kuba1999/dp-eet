<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditDownloadsFileForm;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class AddEditDownloadsFileForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var DownloadsFileStorage */
   private $downloadsFileStorage;

   /** @var DownloadsFile */
   private $file;

   /** @var callable */
   public $onSuccess;

   /**
    * AddEditDownloadsFileForm constructor.
    * @param EntityManager $em
    * @param DownloadsFileStorage $downloadsFileStorage
    */
   function __construct(EntityManager $em, DownloadsFileStorage $downloadsFileStorage)
   {
      $this->em = $em;
      $this->downloadsFileStorage = $downloadsFileStorage;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Název")
            ->setRequired(true)
            ->setMaxLength(255);

      $form->addUpload("file", "Soubor")
         ->setRequired(true);

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];

      if($this->file) {
         $form['name']->setDefaultValue($this->file->getName());

         $form['file']->setRequired(false);

         $form['submit']->caption = "Uložit";
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->file) { //update existing file
         $file = $this->file;
         $file->setName($form['name']->getValue());
      } else { //new file
         $file = new DownloadsFile(
            $form['name']->getValue()
         );
      }


      if($this->file) {
         $this->em->flush($file);
      } else {
         $this->em->persist($file);
         $this->em->flush($file);
      }

      /** @var FileUpload $file */
      $uploadedFile = $form['file']->getValue();

      //save file if creating question was successful
      if($file->getId() && $uploadedFile->hasFile()) {
         $this->downloadsFileStorage->saveFile($uploadedFile, $file);

         $file->setFileName($uploadedFile->getName());
         $this->em->flush($file);
      }

      $this->onSuccess($file);
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }

   /**
    * @return DownloadsFile
    */
   public function getFile(): DownloadsFile
   {
      return $this->file;
   }

   /**
    * @param DownloadsFile $file
    */
   public function setFile(DownloadsFile $file)
   {
      $this->file = $file;
   }
}