<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditProductForm;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationScheme;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Forms\BaseForm;
use App\Model\Entities\MeasurementUnit;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;
use App\Model\Entities\SellerData;
use App\Model\Entities\Settings;
use App\Model\Entities\SettingsGlobal;
use App\Model\Entities\StockMovement;
use App\Model\Entities\TaxRate;
use App\Model\Entities\User;
use App\Model\Enums\StockMovementType;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\MeasurementUnitRepository;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Repositories\TaxRateRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;

class AddEditProductForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationScheme;

   /** @var ProductCategory|null */
   private $category;

   /** @var Product|null */
   private $product;

   /** @var User */
   private $loggedUser;

   /** @var callable */
   public $onSuccess;

   /**
    * AddEditProductForm constructor.
    * @param EntityManager $em
    * @param CategoryLocationSchemeFactory $categoryLocationScheme
    * @param ProductCategory|null $category
    * @param User $loggedUser
    */
   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationScheme,
      ProductCategory $category = null,
      User $loggedUser
   ) {
      $this->em = $em;
      $this->categoryLocationScheme = $categoryLocationScheme;
      $this->category = $category;
      $this->loggedUser = $loggedUser;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $form->addText("name", "Název")
         ->setMaxLength(256)
         ->setRequired();

      $form->addText("ean", "EAN")
         ->setMaxLength(256)
         ->setRequired(false);

      $form->addText("plu", "PLU")
         ->setMaxLength(16)
         ->setRequired(false);

      $form->addColorPicker("color", "Barva")
         ->setRequired(false);

      $form->addEditor("note", "Poznámka")
         ->setRequired(false);

      //price box
      if($this->isSellerTaxablePerson()) {
         $form->addSelect("taxRate", "Sazba DPH", $this->getTaxRates())
            ->setRequired();

         $form->addPrice("priceWithoutTax", "Cena bez DPH")
            ->setRequired();

         $form->addPrice("priceWithVat", "Cena s DPH")
            ->setRequired();
      } else {
         $form->addPrice("priceWithoutTax", "Cena")
            ->setRequired();
      }

      $form->addText("grossMargin", "Marže")
         ->setRequired(false);

      $form->addText("quantityInStock", "Sklad")
         ->setDisabled(!$this->isWarehouseEnabled())
         ->addRule(BaseForm::FLOAT)
         ->setNullable()
         ->setRequired(false);

      //measure unit box
      $form->addSelect("measureUnit", "Měrná jednotka", $this->getMeasureUnits())
         ->setRequired(true);

      $form->addText("amountInSellUnit", "Množství produktu ve vybrané jednotce", $this->getMeasureUnits())
         ->addRule(BaseForm::FLOAT)
         ->setDefaultValue(1)
         ->setRequired(true);

      $form->addSubmit("submit", "Přidat");

      $form->onValidate[] = [$this, "formValidate"];
      $form->onSuccess[] = [$this, "formSuccess"];

      $this->setFormDefaults($form);

      return $form;
   }

   private function setFormDefaults(BaseForm $form)
   {
      if($this->product) {
         $form->setDefaults([
            "name"               =>    $this->product->getName(),
            "ean"                =>    $this->product->getEan(),
            "plu"                =>    $this->product->getPlu(),
            "color"              =>    $this->product->getColor(),
            "note"               =>    $this->product->getNote(),
            "grossMargin"        =>    $this->product->getGrossMargin(),
            "quantityInStock"    =>    $this->product->getQuantityInStock(),
            "measureUnit"        =>    $this->product->getMeasurementUnit()->getId(),
            "amountInSellUnit"   =>    $this->product->getAmountInSellUnit(),
         ]);

         if($this->isSellerTaxablePerson()) {
            if($this->product->getTaxRate() && !$this->product->getTaxRate()->isArchived()) {
               $form['taxRate']->setDefaultValue($this->product->getTaxRate()->getId());
            } else {
               $form->onAnchor[] = function (BaseForm $form) {
                  if(!$form->isSubmitted()) {
                     $form['taxRate']->addError("Došlo ke změně sazby");
                  }
               };
            }

            $form['priceWithoutTax']->setDefaultValue(round($this->product->getPrice(), 2));

            if($this->product->getTaxRate()) {
               $form['priceWithVat']->setDefaultValue(PriceHelper::calculatePriceWithVat($this->product->getTaxRate(), $this->product->getPrice(), 2));
            }
         } else {
            $form['priceWithoutTax']->setDefaultValue(round($this->product->getPrice(), 2));
         }

         $form['submit']->caption = "Uložit";
      } else {
         //set defult VAT rate and measure unit
         if($this->isSellertaxablePerson() && $this->category && $this->category->getDefaultTaxRate()) {
            $form['taxRate']->setDefaultValue($this->category->getDefaultTaxRate()->getId());
         }

         if($this->category && $this->category->getDefaultMeasurementUnit()) {
            $form['measureUnit']->setDefaultValue($this->category->getDefaultMeasurementUnit()->getId());
         }
      }
   }

   /**
    * @param BaseForm $form
    */
   public function formValidate(BaseForm $form)
   {
      if(!is_numeric($form['grossMargin']->getValue()) && $form['grossMargin']->isFilled()) {
         $value = explode("%", $form['grossMargin']->getValue());

         if(!is_numeric($value[0])) {
            $form['grossMargin']->addError("Chybný formát");
         }
      }
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->isSellertaxablePerson()) {
         /** @var TaxRate $taxRate */
         $taxRate = $this->em->find(TaxRate::class, $form['taxRate']->getValue());

         $priceWithoutTax = PriceHelper::calculatePriceWithoutVat(
            $taxRate,
            $form['priceWithVat']->getValue(),
            false
         );
      } else {
         $priceWithoutTax = $form['priceWithoutTax']->getValue();
      }


      if($this->product) { //edit existing product
         $product = $this->product;

         $product->setName($form['name']->getValue());
         $product->setPrice($priceWithoutTax);
         $product->setMeasurementUnit($this->em->find(MeasurementUnit::class, $form['measureUnit']->getValue()));
      }
      else {
         $product = new Product(
            $form['name']->getValue(),
            $this->category,
            $priceWithoutTax,
            $this->em->find(MeasurementUnit::class, $form['measureUnit']->getValue())
         );

         $this->em->persist($product);
      }

      $product->setEan($form['ean']->getValue());
      $product->setPlu($form['plu']->getValue());
      $product->setColor($form['color']->getValue());
      $product->setNote($form['note']->getValue());
      $product->setGrossMargin($form['grossMargin']->getValue());
      $product->setAmountInSellUnit($form['amountInSellUnit']->getValue());

      if($this->isSellertaxablePerson()) {
         $product->setTaxRate($this->em->find(TaxRate::class, $form['taxRate']->getValue()));
      } else {
         $product->setTaxRate(null);
      }

      //stock
      $newQuantity = $form['quantityInStock']->getValue();

      //only if warehouse is enabled
      if($this->isWarehouseEnabled() && $product->getQuantityInStock() != $newQuantity) {

         $actual = (float) $product->getQuantityInStock(); //actual quantity
         $difference = $newQuantity - $actual; //difference

         //make stock taking
         $stockMovement = new StockMovement(
            StockMovementType::get(StockMovementType::STOCK_TAKING),
            $this->loggedUser
         );

         //item
         $stockItem = $stockMovement->addItem(
            $product,
            $difference
         );

         //save
         $this->em->persist($stockMovement);
         $this->em->persist($stockItem);
      }

      $this->em->flush($product);

      $this->onSuccess($product);
   }

   public function handlePriceChanged($taxRateId, $priceWithoutTax, $priceWithVat, $changed)
   {
      /** @var TaxRate $taxRate */
      $taxRate = $this->em->find(TaxRate::class, $taxRateId);

      $newPriceWithoutTax = null;
      $newPriceWithTax = null;

      if($changed == "taxRate") {
         $newPriceWithoutTax = $priceWithoutTax;

         if(is_numeric($priceWithoutTax)) {
            bdump("ok");
            $newPriceWithTax = PriceHelper::calculatePriceWithVat($taxRate, $priceWithoutTax);
         }
      }
      elseif($changed == "priceWithoutTax") {
         $newPriceWithoutTax = $priceWithoutTax;

         if(is_numeric($priceWithoutTax)) {
            $newPriceWithTax = PriceHelper::calculatePriceWithVat($taxRate, $priceWithoutTax);
         }
      }
      elseif($changed == "priceWithVat") {
         if(is_numeric($priceWithVat)) {
            $newPriceWithoutTax = PriceHelper::calculatePriceWithoutVat($taxRate, $priceWithVat);
         }

         $newPriceWithTax = $priceWithVat;
      }

      $this->presenter->payload->prices = [
         "priceWithoutTax" =>    $newPriceWithoutTax,
         "priceWithVat"    =>    $newPriceWithTax,
      ];
   }

   public function createComponentCategoryLocationScheme() : CategoryLocationScheme
   {
      $comp = $this->categoryLocationScheme->create($this->category);

      return $comp;
   }

   /**
    * Get all tax rates
    * @return array
    */
   private function getTaxRates() : array
   {
      /** @var TaxRateRepository $vatRepo */
      $vatRepo = $this->em->getRepository(TaxRate::class);

      return $vatRepo->getAllForSelectBox();
   }

   /**
    * Get all tax rates
    * @return array
    */
   private function getMeasureUnits() : array
   {
      /** @var MeasurementUnitRepository $measureRepo */
      $measureRepo = $this->em->getRepository(MeasurementUnit::class);

      return $measureRepo->getAllForSelectBox();
   }

   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * @return bool
    */
   public function isWarehouseEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsGlobal::class);

      return $settingsRepo->isWarehouseEnabled();
   }

   /**
    * @return Product|null
    */
   public function getProduct()
   {
      return $this->product;
   }

   /**
    * @param Product|null $product
    */
   public function setProduct($product)
   {
      $this->product = $product;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}