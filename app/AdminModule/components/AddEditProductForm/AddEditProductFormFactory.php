<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditProductForm;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Model\Entities\ProductCategory;
use Kdyby\Doctrine\EntityManager;
use Nette\Security\User;

class AddEditProductFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationScheme;

   /** @var User */
   private $user;

   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory,
      User $user
   ) {
      $this->em = $em;
      $this->categoryLocationScheme = $categoryLocationSchemeFactory;
      $this->user = $user;
   }

   public function create(ProductCategory $category = null) : AddEditProductForm
   {
      $loggedUser = $this->em->find(\App\Model\Entities\User::class, $this->user->getId());

      $component = new AddEditProductForm(
         $this->em,
         $this->categoryLocationScheme,
         $category,
         $loggedUser
      );

      return $component;
   }
}