<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditProductCategoryModal;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationScheme;
use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use App\Components\BaseControl;
use App\Forms\BaseForm;
use App\Model\Entities\MeasurementUnit;
use App\Model\Entities\ProductCategory;
use App\Model\Entities\TaxRate;
use App\Model\Repositories\MeasurementUnitRepository;
use App\Model\Repositories\ProductCategoryRepository;
use App\Model\Repositories\TaxRateRepository;
use Kdyby\Doctrine\EntityManager;

class AddEditProductCategoryModal extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationScheme;

   /** @var int|null @persistent */
   public $parentCategoryId;

   /** @var int|null @persistent */
   public $categoryId;

   /** @var bool @persistent */
   public $show = false;

   /** @var callable */
   public $onCreated;

   /** @var callable */
   public $onSaved;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em, CategoryLocationSchemeFactory $categoryLocationScheme)
   {
      $this->em = $em;
      $this->categoryLocationScheme = $categoryLocationScheme;
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("name", "Název")
         ->setMaxLength(255)
         ->setRequired();
      $form->addColorPicker("color", "Barva")
         ->setRequired(false);

      $form->addSelect("defaultTaxRate", "Výchozí sazba DPH", $this->getTaxRates())
         ->setPrompt("-")
         ->setRequired(false);

      $form->addSelect("defaultMeasurementUnit", "Výchozí měrná jednotka", $this->getMeasurementUnits())
         ->setPrompt("-")
         ->setRequired(false);

      $form->addSubmit("submit", "Přidat");

      $form->onSuccess[] = [$this, "formSuccess"];
      $form->onError[] = function () {
         $this->redrawControl();
      };

      //editing category
      if($this->getCategory()) {
         $category = $this->getCategory();

         $form->setDefaults([
            "name"                        =>    $category->getName(),
            "color"                       =>    $category->getColor(),
            "defaultTaxRate"              =>    $category->getDefaultTaxRate() ? $category->getDefaultTaxRate()->getId() : null,
            "defaultMeasurementUnit"      =>    $category->getDefaultMeasurementUnit() ? $category->getDefaultMeasurementUnit()->getId() : null,
         ]);

         $form['submit']->caption = "Uložit";
      }

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      if($this->categoryId) {
         $category = $this->getCategory();
         $category->setName($form['name']->getvalue());
      } else {
         $category = new ProductCategory(
            $form['name']->getvalue(),
            $this->getParentCategory()
         );
      }

      if($form['defaultTaxRate']->getValue()) {
         $taxRate = $this->em->find(TaxRate::class, $form['defaultTaxRate']->getValue());
         $category->setDefaultTaxRate($taxRate);
      }

      if($form['defaultMeasurementUnit']->getValue()) {
         $unit = $this->em->find(MeasurementUnit::class, $form['defaultMeasurementUnit']->getValue());
         $category->setDefaultMeasurementUnit($unit);
      }

      $category->setColor($form['color']->getValue());

      if($this->categoryId) {
         $this->em->flush($category);
         $this->onSaved($category);
      } else {
         $this->em->persist($category);
         $this->em->flush($category);
         $this->onCreated($category);
      }
   }

   /**
    * Array of tax rates
    * @return array
    */
   private function getTaxRates() : array
   {
      /** @var TaxRateRepository $repo */
      $repo = $this->em->getRepository(TaxRate::class);

      return $repo->getAllForSelectBox();
   }

   /**
    * Array of measurement units
    * @return array
    */
   private function getMeasurementUnits() : array
   {
      /** @var MeasurementUnitRepository $repo */
      $repo = $this->em->getRepository(MeasurementUnit::class);

      return $repo->getAllForSelectBox();
   }

   public function createComponentCategoryLocationScheme() : CategoryLocationScheme
   {
      $comp = $this->categoryLocationScheme->create($this->getParentCategory());

      return $comp;
   }

   /**
    * @param ProductCategory|null $parentCategory
    */
   public function setParentCategory(ProductCategory $parentCategory = null)
   {
      if(is_null($parentCategory)) {
         $this->parentCategoryId = null;
         return;
      }

      $this->parentCategoryId = $parentCategory->getId();
   }

   /**
    * Show modal window
    * @param ProductCategory|null $category
    */
   public function show(ProductCategory $category = null)
   {
      if($category) {
         $this->categoryId = $category->getId();
      }

      $this->show = true;
      $this->redrawControl();
   }

   /**
    * Hide modal window
    */
   public function hide()
   {
      $this->categoryId = null;
      $this->show = false;
      $this->redrawControl();
   }

   /**
    * @return ProductCategory|null
    */
   public function getParentCategory() : ?ProductCategory
   {
      //without parent = root
      if(is_null($this->parentCategoryId)) {
         return null;
      }

      /** @var ProductCategoryRepository $repo */
      $repo = $this->em->getRepository(ProductCategory::class);

      return $repo->find($this->parentCategoryId);
   }

   public function getCategory() : ?ProductCategory
   {
      if(is_null($this->categoryId)) {
         return null;
      }

      /** @var ProductCategoryRepository $repo */
      $repo = $this->em->getRepository(ProductCategory::class);

      return $repo->find($this->categoryId);
   }

   /**
    * Set category for editing
    * @param ProductCategory $category
    */
   public function setCategory(ProductCategory $category)
   {
      $this->categoryId = $category->getId();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->show = $this->show;

      return $tpl->render();
   }
}