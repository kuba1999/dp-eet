<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\AddEditProductCategoryModal;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use Kdyby\Doctrine\EntityManager;

class AddEditProductCategoryModalFactory
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationScheme;

   function __construct(EntityManager $em, CategoryLocationSchemeFactory $categoryLocationSchemeFactory)
   {
      $this->em = $em;
      $this->categoryLocationScheme = $categoryLocationSchemeFactory;
   }

   public function create() : AddEditProductCategoryModal
   {
      $component = new AddEditProductCategoryModal(
         $this->em,
         $this->categoryLocationScheme
      );

      return $component;
   }
}