<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditGlobalSettings;


use App\Model\Storage\EetCertificateStorage;
use Kdyby\Doctrine\EntityManager;

class EditGlobalSettingsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var EetCertificateStorage */
   private $certificateStorage;

   function __construct(EntityManager $em, EetCertificateStorage $certificateStorage)
   {
      $this->em = $em;
      $this->certificateStorage = $certificateStorage;
   }

   public function create() : EditGlobalSettings
   {
      $component = new EditGlobalSettings(
         $this->em,
         $this->certificateStorage
      );

      return $component;
   }
}