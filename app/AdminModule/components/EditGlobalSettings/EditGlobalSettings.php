<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\EditGlobalSettings;


use App\Components\BaseControl;
use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Model\Entities\SettingsEet;
use App\Model\Entities\SettingsGlobal;
use App\Model\Repositories\SettingsRepository;
use Kdyby\Doctrine\EntityManager;

class EditGlobalSettings extends BaseControl
{
   /** @var EntityManager */
   private $em;

   /** @var callable */
   public $onSubmit;

   /** @var callable */
   public $onSuccess;

   /** @var SettingsGlobal */
   private $settings;

   /**
    * @param EntityManager $em
    */
   function __construct(EntityManager $em)
   {
      $this->em = $em;

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $em->getRepository(SettingsEet::class);
      $this->settings = $settingsRepo->getGlobalSettings();
   }

   /**
    * @return BaseForm
    */
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "form-horizontal";

      $form->addCheckbox("warehouse", "Evidence skladu povolena")
         ->setRequired(false);

      $form->addSubmit("submit", "Uložit");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   private function setDefaults(BaseForm $form)
   {
      $form->setDefaults([
         "warehouse"      =>  $this->settings->isWarehouseEnabled()
      ]);
   }

   /**
    * @param BaseForm $form
    */
   public function formSuccess(BaseForm $form)
   {
      $this->settings->setWarehouseEnabled($form['warehouse']->getValue());

      $this->em->persist($this->settings);
      $this->em->flush();

      $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);

      $this->onSuccess();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $this->setDefaults($this['form']);

      return $tpl->render();
   }
}