<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\SelectProducts;


use App\AdminModule\Components\CategoryLocationScheme\CategoryLocationSchemeFactory;
use Kdyby\Doctrine\EntityManager;

class SelectProductsFactory
{
   /** @var EntityManager */
   private $em;

   /** @var CategoryLocationSchemeFactory */
   private $categoryLocationSchemeFactory;

   function __construct(
      EntityManager $em,
      CategoryLocationSchemeFactory $categoryLocationSchemeFactory
   ) {
      $this->em = $em;
      $this->categoryLocationSchemeFactory = $categoryLocationSchemeFactory;
   }

   public function create() : SelectProducts
   {
      $component = new SelectProducts(
         $this->em,
         $this->categoryLocationSchemeFactory
      );

      return $component;
   }
}