<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\StockInProducts;


use App\Forms\BaseForm;
use App\Model\Entities\DownloadsFile;
use App\Model\Entities\Product;
use App\Model\Entities\SellerData;
use App\Model\Entities\StockMovement;
use App\Model\Entities\StockMovementItem;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\StockMovementRepository;
use App\Model\Storage\DownloadsFileStorage;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Http\FileUpload;

class EditItem extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var Item */
   private $item;

   /** @var Product */
   private $product;

   /** @var callable */
   public $onChanged;

   /** @var callable */
   public $onRemoved;

   /** @var string[] */
   private $errors = [];

   /**
    * EditItem constructor.
    * @param StockInProducts $stockInProducts
    * @param EntityManager $em
    * @param Item $item
    */
   function __construct(EntityManager $em, Item $item)
   {
      $this->em = $em;
      $this->item = $item;

      /** @var Product $product */
      $product = $this->em->find(Product::class, $this->item->getId());
      $this->product = $product;
   }

   protected function createComponentPurchasePrice() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addPrice("value")
         ->setRequired()
         ->setHtmlAttribute("placeholder", "Kč vč. DPH")
         ->setDefaultValue($this->item->getPurchasePrice());

      $form->onSuccess[] = function (BaseForm $form) {
         $this->item->setPurchasePrice($form['value']->getValue());
         $this->onChanged($this->item);
         $this->redrawControl("purchasePrice");
      };

      $form->onError[] = function (BaseForm $form) {
         $this->redrawControl("purchasePrice");
      };

      $form->onValidate[] = function (BaseForm $form) {
         if($form['value']->isFilled() && $form['value']->getValue() < 0) {
            $form['value']->addError("Zadejte kladnou hodnotu");
         }
         $this->redrawControl("purchasePrice");
      };

      return $form;
   }

   protected function createComponentSellPrice() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("value")
         ->addRule(BaseForm::FLOAT)
         ->setRequired($this->isGrossMarginCheckerEnabled())
         ->setHtmlAttribute("placeholder", $this->isSellerTaxablePerson() ? "Kč vč. DPH":"Kč")
         ->setDefaultValue($this->item->getSellPrice());

      $form->onSuccess[] = function (BaseForm $form) {
         $this->item->setSellPrice($form['value']->getValue());
         $this->onChanged($this->item);
         $this->redrawControl("sellPrice");
      };

      $form->onError[] = function (BaseForm $form) {
         $this->redrawControl("sellPrice");
      };

      $form->onValidate[] = function (BaseForm $form) {
         if($form['value']->isFilled() && $form['value']->getValue() < 0) {
            $form['value']->addError("Zadejte kladnou hodnotu");
         }
         $this->redrawControl("sellPrice");
      };

      return $form;
   }


   protected function createComponentQuantity() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addText("value")
         ->setRequired()
         ->addRule(BaseForm::FLOAT)
         ->setHtmlAttribute("min", 1)
         ->setDefaultValue($this->item->getQuantity());

      $form->onSuccess[] = function (BaseForm $form) {
         $this->item->setQuantity($form['value']->getValue());
         $this->onChanged($this->item);
         $this->redrawControl("quantity");
      };

      $form->onError[] = function (BaseForm $form) {
         $this->redrawControl("quantity");
      };

      $form->onValidate[] = function (BaseForm $form) {
         if($form['value']->isFilled() && $form['value']->getValue() < 0) {
            $form['value']->addError("Zadejte kladnou hodnotu");
         }
         $this->redrawControl("quantity");
      };

      return $form;
   }

   private function showFormControlErrors()
   {
      if($this['purchasePrice-value']->isFilled()) {
         $this['purchasePrice']->validate();
      }
      if($this['sellPrice-value']->isFilled()) {
         $this['sellPrice']->validate();
      }
      if($this['quantity-value']->isFilled()) {
         $this['quantity']->validate();
      }
   }

   public function isValid() : bool
   {
      $this['purchasePrice']->validate();
      $this['sellPrice']->validate();
      $this['quantity']->validate();

      return $this['purchasePrice']->isValid() && $this['sellPrice']->isValid() && $this['quantity']->isValid();
   }

   private function checkGrossMargin()
   {
      if(!$this->isGrossMarginCheckerEnabled()) {
         return;
      }

      $sellPrice = $this->item->getSellPrice();
      $quantity = $this->item->getQuantity();
      $purchasePrice = $this->item->getPurchasePrice();

      if(!is_numeric($sellPrice) || !is_numeric($quantity) || !is_numeric($purchasePrice) || $quantity <= 0) {
         return;
      }

      $purchaseUnitPrice = $purchasePrice / $quantity;


      if(!PriceHelper::isGrossMarginValid($this->product, $sellPrice, $purchaseUnitPrice)) {
         $this->addError("Prodejní cena mimo nastavenou marži");
      }
   }

   /**
    * @param Product $product
    * @return StockMovementItem|null
    */
   public function getStockMovementWithLastBuyOfProduct() : ?StockMovementItem
   {
      /** @var StockMovementRepository $stockMovementRepo */
      $stockMovementRepo = $this->em->getRepository(StockMovement::class);

      return $stockMovementRepo->getLastBuyOfProduct($this->product);
   }

   public function getOldPurchaseUnitPrice() : ?float
   {
      if($lastBuy = $this->getStockMovementWithLastBuyOfProduct()) {
         return $lastBuy->getUnitPrice();
      }

      return null;
   }

   /**
    * @param Product $product
    * @return null|string
    */
   public function calculateNewPurchaseUnitPrice() : string
   {
      $lastBuy = $this->getStockMovementWithLastBuyOfProduct();

      if(!$lastBuy) { //product without last buy
         return "";
      }

      $price = $this->item->getPurchasePrice();
      $quantity = $this->item->getQuantity();

      if(is_numeric($price) && is_numeric($quantity) && $quantity > 0) {
         $newPurchaseUnitPrice = round($price / $quantity, 2);

         if($lastBuy->getUnitPrice() == 0.0) {
            return "-";
         }
         else {
            $percents = round(($lastBuy->getUnitPrice() - $newPurchaseUnitPrice) * 100 / $lastBuy->getUnitPrice(), 2);
            $percents = abs($percents);
         }

         if($lastBuy->getUnitPrice() > $newPurchaseUnitPrice) {
            return "Nová pořizovací cena " . $newPurchaseUnitPrice . " Kč / -" . $percents . " %";
         }
         else {
            return "Nová pořizovací cena " . $newPurchaseUnitPrice . " Kč / +" . $percents . " %";
         }
      }

      return "";
   }

   public function isGrossMarginCheckerEnabled() : bool
   {
      return $this->product->getGrossMargin() !== null;
   }

   public function getGrossMarginTooltipValue() : string
   {
      if(strpos($this->product->getGrossMargin(), "%") === false) {
         return $this->product->getGrossMargin(). " Kč";
      }

      return $this->product->getGrossMargin();
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * @param string $error
    */
   private function addError(string $error)
   {
      $this->errors[$error] = $error;
   }

   public function handleRemove()
   {
      $this->onRemoved($this->item);
   }

   public function render()
   {
      $this->checkGrossMargin();
      $this->showFormControlErrors();

      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->product = $this->product;
      $tpl->errors = $this->errors;

      $this->redrawControl("errors");
      $this->redrawControl("overview");

      return $tpl->render();
   }
}