<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\StockInProducts;


use App\AdminModule\Components\SelectProducts\SelectProductsFactory;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Storages\NewMemcachedStorage;
use Nette\Security\User;

class StockInProductsFactory
{
   /** @var NewMemcachedStorage */
   private $storage;

   /** @var EntityManager */
   private $em;

   /** @var SelectProductsFactory */
   private $selectProductsFactory;

   /** @var User */
   private $user;

   function __construct(
      NewMemcachedStorage $storage,
      EntityManager $em,
      SelectProductsFactory $selectProductsFactory,
      User $user
   ) {
      $this->storage = $storage;
      $this->em = $em;
      $this->selectProductsFactory = $selectProductsFactory;
      $this->user = $user;
   }

   public function create() : StockInProducts
   {
      /** @var \App\Model\Entities\User $loggedUser */
      $loggedUser = $this->em->find(\App\Model\Entities\User::class, $this->user->getId());

      $component = new StockInProducts(
         $this->storage,
         $this->em,
         $this->selectProductsFactory,
         $loggedUser
      );

      return $component;
   }
}