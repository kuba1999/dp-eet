<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\StockInProducts;


use App\Model\Entities\Product;

class Item
{
   /** @var int */
   private $id;

   /** @var float|null */
   private $purchasePrice;

   /** @var float|null */
   private $sellPrice;

   /** @var float|null */
   private $quantity;

   public function __construct(Product $product)
   {
      $this->id = $product->getId();
   }

   /**
    * @return int
    */
   public function getId(): int
   {
      return $this->id;
   }

   /**
    * @return float|null
    */
   public function getPurchasePrice(): ?float
   {
      return $this->purchasePrice;
   }

   /**
    * @param float|null $purchasePrice
    */
   public function setPurchasePrice(?float $purchasePrice): void
   {
      $this->purchasePrice = $purchasePrice;
   }

   /**
    * @return float|null
    */
   public function getSellPrice(): ?float
   {
      return $this->sellPrice;
   }

   /**
    * @param float|null $sellPrice
    */
   public function setSellPrice(?float $sellPrice): void
   {
      $this->sellPrice = $sellPrice;
   }

   /**
    * @return float|null
    */
   public function getQuantity(): ?float
   {
      return $this->quantity;
   }

   /**
    * @param float|null $quantity
    */
   public function setQuantity(?float $quantity): void
   {
      $this->quantity = $quantity;
   }
}