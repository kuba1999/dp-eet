<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Components\StockInProducts;


use App\AdminModule\Components\SelectProducts\SelectProducts;
use App\AdminModule\Components\SelectProducts\SelectProductsFactory;
use App\Components\FlashMessage\FlashMessageNotify;
use App\Forms\BaseForm;
use App\Model\Entities\Product;
use App\Model\Entities\SellerData;
use App\Model\Entities\Settings;
use App\Model\Entities\StockMovement;
use App\Model\Entities\Supplier;
use App\Model\Entities\User;
use App\Model\Enums\StockMovementType;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\StockMovementRepository;
use App\Model\Repositories\SupplierRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\Storages\NewMemcachedStorage;

class StockInProducts extends Control
{
   const KEY_NAMESPACE        = "stockIn";
   const KEY_ITEMS            = "items";
   const KEY_ITEM             = "item";
   const KEY_DATA             = "data";

   /** @var EntityManager */
   private $em;

   /** @var SelectProductsFactory */
   private $selectProductsFactory;

   /** @var callable */
   public $onSuccess;

   /** @var StockMovementRepository */
   private $stockMovementRepo;

   /** @var User */
   private $loggedUser;

   /** @var Cache */
   private $_cache;

   /** @var array */
   private $items;

   /** @var array */
   private $data;

   /**
    * @param EntityManager $em
    */
   function __construct(
      NewMemcachedStorage $storage,
      EntityManager $em,
      SelectProductsFactory $SelectProductsFactory,
      User $loggedUser
   ) {
      $this->em = $em;
      $this->selectProductsFactory = $SelectProductsFactory;
      $this->loggedUser = $loggedUser;

      $this->_cache = new Cache($storage, self::KEY_NAMESPACE);
      $this->stockMovementRepo = $this->em->getRepository(StockMovement::class);

      $this->loads();
   }

   ///////////////////////////////////////////////////////////////////////////////// form
   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addSelect("supplier", "Dodavatel", $this->getSuppliers())
         ->setPrompt("-")
         ->setDefaultValue($this->data['supplier']);
      $form->addText("invoiceNumber", "Číslo dokladu")
         ->setNullable()
         ->setDefaultValue($this->data['invoiceNumber']);
      $form->addTextArea("note", "Poznámka")
         ->setNullable()
         ->setDefaultValue($this->data['note']);

      $form->addSubmit("stockIn", "Naskladnit")
         ->addClass("btn btn-success btn-lg");


      $form->onSuccess[] = function (BaseForm $form) {
         $this->data['supplier'] = $form['supplier']->getValue();
         $this->data['invoiceNumber'] = $form['invoiceNumber']->getValue();
         $this->data['note'] = $form['note']->getValue();

         $this->saveData();


         if($form->isSubmitted() === $form['stockIn'])
         {
            $this->stockIn($form);
         }
      };

      return $form;
   }

   /**
    * @param BaseForm $form
    */
   private function stockIn(BaseForm $form)
   {
      if(!count($this->getItems())) {
         $this->presenter->flashMessageNotify("Žádné položky pro naskladnění!", FlashMessageNotify::TYPE_DANGER);
         return;
      }

      $stockMovement = new StockMovement(StockMovementType::get(StockMovementType::BUY), $this->loggedUser);

      //general info
      if(!empty($form['supplier']->getValue())) {
         $stockMovement->setSupplier($this->em->find(Supplier::class, $form['supplier']->getValue()));
      }
      $stockMovement->setInvoiceNumber($form['invoiceNumber']->getValue());
      $stockMovement->setNote($form['note']->getValue());

      //validate
      /** @var Item $item */
      $valid = true;
      foreach ($this->getItems() AS $item) {
         $item = $this->getItem($item->getId());

         if (!$this['editItem'][$item->getId()]->isValid()) {
            $this->redrawControl("items");
            $valid = false;
         }
      }

      if(!$valid) {
         $this->presenter->flashMessageNotify("Opravte chyby!", FlashMessageNotify::TYPE_DANGER);
         return;
      }

      /** @var Item $item */
      foreach ($this->getItems() AS $item) {
         $item = $this->getItem($item->getId());

         $sellPrice = $item->getSellPrice();
         $quantity = $item->getQuantity();
         $purchasePrice = $item->getPurchasePrice();

         if($quantity <= 0) {
            continue;
         }

         /** @var Product $product */
         $product = $this->em->find(Product::class, $item->getId());

         $item = $stockMovement->addItem(
            $product,
            $quantity
         );

         $item->setUnitPrice($purchasePrice / $quantity);

         //set product price
         if(!is_null($sellPrice)) {
            if($this->isSellerTaxablePerson()) {
               $product->setPrice(PriceHelper::calculatePriceWithoutVat($product->getTaxRate(), $sellPrice, false));
            } else {
               $product->setPrice($sellPrice);
            }
         }

         $this->em->persist($stockMovement);
      }

      $this->em->flush($stockMovement);
      $this->clear();
      $form->reset();
      $this->onSuccess();
      $this->redrawControl();
      $this['selectProducts']->redrawControl();
   }

   ///////////////////////////////////////////////////////////////////////////////// items
   protected function createComponentEditItem() : Multiplier
   {
      return new Multiplier(function (int $itemId){
         $comp = new EditItem($this->em, $this->getItem($itemId));
         $comp->onChanged[] = function (Item $item) {
            $this->saveItem($item);
            $this->redrawControl("table");
            $this->redrawControl("overview");
         };
         $comp->onRemoved[] = function (Item $item) {
            $this->removeItemById($item->getId());
            $this['selectProducts']->redrawControl();

            $this->redrawControl("table");
            $this->redrawControl("overview");
            $this->redrawControl("items");
         };

         return $comp;
      });
   }

   /**
    * @return Item[]
    */
   public function getItems() : array
   {
      $ret = [];
      foreach ($this->items AS $itemId) {
         $item = $this->_cache->load(self::KEY_ITEM . $itemId);

         if($item) {
            $ret[] = $item;
         }
      }

      return $ret;
   }

   public function getItem(int $id) : Item
   {
      $item = $this->_cache->load(self::KEY_ITEM . $id);

      return $item;
   }

   ///////////////////////////////////////////////////////////////////////////////// product select
   protected function createComponentSelectProducts() : SelectProducts
   {
      $comp = $this->selectProductsFactory->create();

      $comp->onSelected[] = [$this, "addProduct"];
      $comp->onUnselected[] = [$this, "removeProduct"];
      $comp->isSelectedCallback = function (Product $product) {
         return isset($this->items[$product->getId()]);
      };

      return $comp;
   }

   public function addProduct(Product $product)
   {
      $item = new Item($product);

      if(!isset($this->items[$item->getId()])) {
         $this->items[$item->getId()] = $item->getId();
         $this->_cache->save(self::KEY_ITEM . $item->getId(), $item);
      }

      $this->saveItems();
      $this->redrawControl("table");
      $this->redrawControl("items");
   }

   public function removeProduct(Product $product)
   {
      unset($this->items[$product->getId()]);

      $this->saveItems();
      $this->redrawControl("table");
      $this->redrawControl("items");
   }

   public function removeItemById(int $id)
   {
      unset($this->items[$id]);

      $this->saveItems();
      $this->redrawControl("table");
      $this->redrawControl("items");
   }

   ///////////////////////////////////////////////////////////////////////////////// helpers
   /**
    * @return array
    */
   private function getSuppliers() : array
   {
      /** @var SupplierRepository $supplierRepo */
      $supplierRepo = $this->em->getRepository(Supplier::class);

      return $supplierRepo->getAllForSelectBox();
   }

   /**
    * @return bool
    */
   public function isSellerTaxablePerson() : bool
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);

      return $sellerDataRepo->isSellerTaxablePerson();
   }

   /**
    * @return float
    */
   public function getProductPurchasePricesSum() : float
   {
      $sum = 0.0;

      foreach ($this->getItems() AS $item) {
         $sum += (float) $item->getPurchasePrice();
      }

      return $sum;
   }

   /**
    * @return float
    */
   public function getProductQuantitiesSum() : float
   {
      $quantity = 0.0;

      foreach ($this->getItems() AS $item) {
         $quantity += (float) $item->getQuantity();
      }

      return $quantity;
   }

   /////////////////////////////////////////////////////////////////// cache

   private function saveItems()
   {
      $this->_cache->save(self::KEY_ITEMS, $this->items);
   }

   private function saveItem(Item $item)
   {
      $this->_cache->save(self::KEY_ITEM . $item->getId(), $item);
   }

   private function saveData()
   {
      $this->_cache->save(self::KEY_DATA, $this->data);
   }

   private function clear()
   {
      $this->_cache->remove(self::KEY_DATA);
      $this->_cache->remove(self::KEY_ITEM);
      $this->_cache->remove(self::KEY_ITEMS);

      $this->loads();
   }

   private function loads()
   {
      $this->items = $this->_cache->load(self::KEY_ITEMS, function (){
         return [];
      });

      $this->data = $this->_cache->load(self::KEY_DATA, function (){
         return [
            "supplier" => null,
            "invoiceNumber" => null,
            "note" => null,
         ];
      });
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->taxablePerson = $this->isSellerTaxablePerson();

      return $tpl->render();
   }
}