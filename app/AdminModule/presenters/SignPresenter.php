<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\Components\SignInForm\SignInForm;

class SignPresenter extends \App\Presenters\SignPresenter
{
   protected function startup()
   {
      parent::startup();

      //set base layout
      $this->setLayout("layoutBase");
   }

   public function actionIn()
   {
      if($this->user->isLoggedIn()) {
         $this->redirect(":Admin:Dashboard:default");
      }
   }

   public function actionOut()
   {
      $this->getUser()->logout(true);
      $this->flashMessage("Byl jste úspěšně odhlášen", FlashMessage::TYPE_SUCCESS);
      $this->redirect(":Admin:Sign:in");
   }

   public function createComponentSignInForm() : SignInForm
   {
      $control = $this->signInFormFactory->create();
      $control->onSuccess[] = function () {
         $this->restoreRequest($this->backLink);
         $this->redirect(":Admin:Dashboard:default");
      };

      return $control;
   }

   public function checkRequirements($element)
   {

   }
}