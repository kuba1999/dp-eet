<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\AddEditDownloadsFileForm\AddEditDownloadsFileForm;
use App\AdminModule\Components\AddEditDownloadsFileForm\AddEditDownloadsFileFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\DownloadsFile;
use App\Model\Repositories\DownloadsRepository;
use App\Model\Storage\DownloadsFileStorage;
use Nette\Application\Responses\FileResponse;
use Nette\Utils\Html;

class DownloadsPresenter extends BasePresenter
{
   /** @var AddEditDownloadsFileFormFactory @inject */
   public $addEditDownloadsFileFormFactory;

   /** @var DownloadsFile */
   private $file;

   /** @var DownloadsFileStorage @inject */
   public $downloadsFileStorage;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Soubory ke stažení", "default");
   }

   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");

      $this['addEditDownloadsFileForm']->onSuccess[] = function (DownloadsFile $file){
         $this->flashMessage("Soubor <strong>" . $file->getName() . "</strong> byl přidán", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function actionEdit($id)
   {
      /** @var DownloadsRepository $downloadsRepo */
      $downloadsRepo = $this->_em->getRepository(DownloadsFile::class);
      $this->file = $downloadsRepo->find($id);

      $this['addEditDownloadsFileForm']->setFile($this->file);
      $this->breadcrumb->add("Editovat: " . $this->file->getName(), "edit");

      $this['addEditDownloadsFileForm']->onSuccess[] = function (DownloadsFile $file){
         $this->flashMessage("Soubor <strong>" . $file->getName() . "</strong> byl uložen", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Název")
         ->setWidth(40)
         ->setFilterText();

      $grid->addColumnText("file", "Soubor")
         ->setWidth(20)
         ->setRenderer(function (DownloadsFile $file) {
            return Html::el('a')->href($this->link("download!", $file->getId()))->setText("stáhnout soubor");
         })
         ->setSortable(false);

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat soubor %s?", "name")
         ->onClick[] = [$this, "deleteFile"];

      /** @var DownloadsRepository $faqRepo */
      $downloadsRepo = $this->_em->getRepository(DownloadsFile::class);

      $grid->setDataSource($downloadsRepo->getAll());

      return $grid;
   }

   /**
    * @param DownloadsFile $file
    */
   public function deleteFile(DownloadsFile $file)
   {
      $this->_em->remove($file);
      $this->downloadsFileStorage->deleteFile($file);
      $this->_em->flush();

      $this->flashMessage("Soubor <strong>" . $file->getName() . "</strong> byl smazán", FlashMessage::TYPE_SUCCESS);
   }

   /**
    * @return AddEditDownloadsFileForm
    */
   public function createComponentAddEditDownloadsFileForm() : AddEditDownloadsFileForm
   {
      $component = $this->addEditDownloadsFileFormFactory->create();

      return $component;
   }

   public function handleDownload(int $fileId)
   {
      /** @var DownloadsRepository $downloadsRepository */
      $downloadsRepository = $this->_em->getRepository(DownloadsFile::class);
      $file = $downloadsRepository->find($fileId);

      if(!$file) {
         return;
      }

      $this->sendResponse(new FileResponse(
         $this->downloadsFileStorage->getFilePath($file),
         $file->getFileName()
      ));
   }
}