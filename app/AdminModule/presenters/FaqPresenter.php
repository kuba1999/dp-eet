<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\AddEditFaqForm\AddEditFaqForm;
use App\AdminModule\Components\AddEditFaqForm\AddEditFaqFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Faq;
use App\Model\Enums\ProductType;
use App\Model\Repositories\FaqRepository;
use App\Model\Storage\FaqFileStorage;
use Nette\Application\Responses\FileResponse;
use Nette\Utils\Html;

class FaqPresenter extends BasePresenter
{
   /** @var AddEditFaqFormFactory @inject */
   public $addEditFaqFormFactory;

   /** @var Faq */
   private $faq;

   /** @var FaqFileStorage @inject */
   public $faqFileStorage;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("FAQ", "default");
   }

   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");

      $this['addEditFaqForm']->onSuccess[] = function (Faq $faq){
         $this->flashMessage("Otázka <strong>" . $faq->getSubject() . "</strong> byla přidána", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function actionEdit($id)
   {
      /** @var FaqRepository $faqRepo */
      $faqRepo = $this->_em->getRepository(Faq::class);
      $this->faq = $faqRepo->find($id);

      $this['addEditFaqForm']->setFaq($this->faq);
      $this->breadcrumb->add("Editovat: " . $this->faq->getSubject(), "edit");

      $this['addEditFaqForm']->onSuccess[] = function (Faq $faq){
         $this->flashMessage("Otázka <strong>" . $faq->getSubject() . "</strong> byla uložena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("subject", "Předmět")
         ->setWidth(40)
         ->setFilterText();

      $grid->addColumnText("file", "Soubor")
         ->setWidth(20)
         ->setRenderer(function (Faq $faq) {
            if($this->faqFileStorage->hasFile($faq)) {
               return Html::el('a')->href($this->link("download!", $faq->getId()))->setText("stáhnout soubor");
            }
         })
         ->setSortable(false);

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat otázku %s?", "subject")
         ->onClick[] = [$this, "deleteFaq"];

      /** @var FaqRepository $faqRepo */
      $faqRepo = $this->_em->getRepository(Faq::class);

      //$grid->setDefaultSorting(["date" => Sorting::SORT_DESC]);

      $grid->setDataSource($faqRepo->getAll());

      return $grid;
   }

   /**
    * @param Faq $faq
    */
   public function deleteFaq(Faq $faq)
   {
      $this->_em->remove($faq);
      $this->faqFileStorage->deleteFile($faq);
      $this->_em->flush();

      $this->flashMessage("Otázka <strong>" . $faq->getSubject() . "</strong> byla smazána", FlashMessage::TYPE_SUCCESS);
   }

   /**
    * @return AddEditFaqForm
    */
   public function createComponentAddEditFaqForm() : AddEditFaqForm
   {
      $component = $this->addEditFaqFormFactory->create();

      return $component;
   }

   public function handleDownload(int $questionId)
   {
      /** @var FaqRepository $faqRepository */
      $faqRepository = $this->_em->getRepository(Faq::class);
      $question = $faqRepository->find($questionId);

      if(!$question || !$this->faqFileStorage->hasFile($question)) {
         return;
      }

      $this->sendResponse(new FileResponse(
         $this->faqFileStorage->getFilePath($question),
         $question->getFileName()
      ));
   }
}