<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Utils\Sorting;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\StockMovement;
use App\Model\Entities\StockMovementItem;
use App\Model\Entities\Supplier;
use App\Model\Enums\StockMovementType;
use App\Model\Helpers\MeasurementUnitHelper;
use App\Model\Repositories\StockMovementRepository;
use App\Model\Repositories\SupplierRepository;
use Nette\Utils\Html;

class StockMovementHistoryPresenter extends BasePresenter
{
   /** @var StockMovementRepository */
   public $stockMovementRepo;

   protected function startup()
   {
      parent::startup();

      $this->stockMovementRepo = $this->_em->getRepository(StockMovement::class);

      $this->breadcrumb->add("Skladové pohyby", "StockMovementHistory:");

      if(!$this->isWarehouseEnabled()) {
         $this->flashMessage("Evidence skladu není zapnuta", FlashMessage::TYPE_DANGER);
      }
   }

   /**
    * @return DataGrid
    */
   public function createComponentGridMovements() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnDateTime("created", "Datum a čas")
         ->setWidth(10)
         ->setFilterDate();

      $grid->addColumnText("type", "Operace")
         ->setWidth(10)
         ->setRenderer(function (StockMovement $movement) {
            switch ($movement->getType()->getValue()) {
               case StockMovementType::BUY:
                  return Html::el('span')->setText($movement->getType())->setClass("label label-success");
               case StockMovementType::SALE:
                  return Html::el('span')->setText($movement->getType())->setClass("label label-warning");
               case StockMovementType::STOCK_TAKING:
                  return Html::el('span')->setText($movement->getType())->setClass("label label-primary");
            }
         })
         ->setFilterSelect(StockMovementType::getNamedTypes());

      $grid->addColumnText("supplier.name", "Dodavatel")
         ->setWidth(15)
         ->setFilterSelect($this->getSuppliers(), ["supplier.id"]);

      $grid->addColumnNumber("quantitySum", "Počet položek")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFormatDecimals(2)
         ->setSortable(false);

      $grid->addColumnNumber("priceSum", "Cena celkem vč. DPH")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setValueSuffix(" Kč")
         ->setSortable(false);


      $grid->addAction("detail", "Detail", "detail")
         ->setClass("btn btn-primary");

      $grid->addDefaultSorting("created", Sorting::SORT_DESC);

      $grid->setDataSource($this->stockMovementRepo->getMovementsQB()->leftJoin("m.supplier", "supplier"));

      return $grid;
   }

   private function getSuppliers() : array
   {
      /** @var SupplierRepository $supplierRepo */
      $supplierRepo = $this->_em->getRepository(Supplier::class);

      return $supplierRepo->getAllForSelectBox();
   }

   /*********************************************************************************************************** detail*/
   public function actionDetail(int $id)
   {
      $stockMovement = $this->stockMovementRepo->getMovement($id);

      if(is_null($stockMovement)) {
         $this->flashMessage("Neexistující detail pohybu!", FlashMessage::TYPE_DANGER);
         $this->redirect("default");
      }

      $this->breadcrumb->add("Detail pohybu");

      $this->template->stockMovement = $stockMovement[0];
      $this->template->itemsCount = $stockMovement['quantitySum'];
      $this->template->itemsPurchasePriceSum = $stockMovement['priceSum'];
   }

   /**
    * @return DataGrid
    */
   public function createComponentGridMovementItems() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnDateTime("product.name", "Produkt")
         ->setWidth(10)
         ->setFilterText();

      $grid->addColumnNumber("quantity", "Počet jednotek")
         ->setWidth(10)
         ->setFormatDecimals(2)
         ->setValueSuffix(function(StockMovementItem $item) {
            return " " . MeasurementUnitHelper::getSellUnitText($item->getProduct()->getMeasurementUnit());
         })
         ->setFilterNumber();

      $grid->addColumnNumber("unitPrice", "Cena za jednotku vč. DPH")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFormatDecimals(2)
         ->setValueSuffix(" Kč")
         ->setFilterNumber();

      $grid->addColumnNumber("priceSum", "Cena celkem vč. DPH")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFormatDecimals(2)
         ->setValueSuffix(" Kč")
         ->setFilterNumber();

      $grid->addDefaultSorting("product.name", Sorting::SORT_ASC);

      $grid->setDataSource(
         $this->stockMovementRepo->getStockMovementItems($this->template->stockMovement)
            ->leftJoin("i.product", "product")
      );

      return $grid;
   }

   /********************************************************************************************************** helpers*/


}