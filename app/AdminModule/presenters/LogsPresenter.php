<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;
use App\CashModule\Components\ReceiptLogRecords\ReceiptLogRecords;
use App\CashModule\Components\ReceiptLogRecords\ReceiptLogRecordsFactory;

/**
 * Class LogsPresenter
 * @package App\AdminModule\Presenters
 */
class LogsPresenter extends BasePresenter
{
   /** @var ReceiptLogRecordsFactory @inject */
   public $receiptLogRecordsFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Log", "Logs:");
   }

   public function actionReceipts()
   {
      $this->breadcrumb->add("Doklady", "Logs:receipts");
   }

   protected function createComponentReceipts() : ReceiptLogRecords
   {
      return $this->receiptLogRecordsFactory->create();
   }
}