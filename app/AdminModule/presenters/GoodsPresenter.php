<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\AddEditProductForm\AddEditProductForm;
use App\AdminModule\Components\AddEditProductForm\AddEditProductFormFactory;
use App\AdminModule\Components\GoodsManagment\GoodsManagment;
use App\AdminModule\Components\GoodsManagment\GoodsManagmentFactory;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Product;
use App\Model\Entities\ProductCategory;

class GoodsPresenter extends BasePresenter
{
   /** @var GoodsManagmentFactory @inject */
   public $goodsManagmentFactory;

   /** @var AddEditProductFormFactory @inject */
   public $addEditProductFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Zboží", "Goods:");
   }

   /********************************************************************************* list of categories and products */
   public function renderDefault()
   {
      $this->backLink = $this->storeRequest();
   }


   /************************************************************************************************** create product */
   public function actionCreateProduct(int $categoryId = null)
   {
      $this->breadcrumb->add("Nový produkt", "Goods:createProduct");

      $this['addEditProductForm']->onSuccess[] = function (Product $product) {
         $this->flashMessage("Produkt *" . $product->getName() . "* vytvořen", FlashMessage::TYPE_SUCCESS);
         //$this->redirectBack();
         $this->redirect("editProduct", $product->getId());
      };
   }

   /************************************************************************************************** edit product */
   public function actionEditProduct(int $productId)
   {
      /** @var Product $product */
      $product = $this->_em->find(Product::class, $productId);

      $this->breadcrumb->add($product->getName() . "|editace produktu", "Goods:editProduct", $productId);

      $this['addEditProductForm']->setProduct($product);
      $this['addEditProductForm']->onSuccess[] = function (Product $product) {
         $this->flashMessage("Produkt *" . $product->getName() . "* uložen", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this");
      };
   }

   /************************************************************************************************* create category */



   /****************************************************************************************************** components */
   public function createComponentGoodsManagment() : GoodsManagment
   {
      $comp = $this->goodsManagmentFactory->create();

      $comp->onCreateProduct[] = function (ProductCategory $category = null) {
         $this->redirect("createProduct", $category ? $category->getId():null);
      };

      $comp->onEditProduct[] = function (Product $product) {
         $this->redirect("editProduct", $product->getId());
      };

      return $comp;
   }

   public function createComponentAddEditProductForm() : AddEditProductForm
   {
      if($this->getParameter("categoryId")) {
         $category = $this->_em->find(ProductCategory::class, $this->getParameter("categoryId"));
      } else {
         $category = null;
      }

      $comp = $this->addEditProductFormFactory->create($category);

      /*$comp->onCreateProduct[] = function (ProductCategory $category = null) {
         $this->redirect("createProduct", $category ? $category->getId():null);
      };*/

      return $comp;
   }

   /********************************************************************************************************* signals */
   public function handleGoBack()
   {
      $this->redirectBack();
   }

   /********************************************************************************************************* helpers */
   private function redirectBack()
   {
      $this->restoreRequest($this->backLink);
      $this->redirect("default");
   }
}