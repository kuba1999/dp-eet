<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

/**
 * Created by PhpStorm.
 * User: jakub
 * Date: 5.3.18
 * Time: 20:55
 */

namespace App\AdminModule\Presenters;


use App\CashModule\Components\ReceiptDetail\ReceiptDetail;
use App\CashModule\Components\ReceiptDetail\ReceiptDetailFactory;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialog;
use App\CashModule\Components\SendReceiptModalDialog\SendReceiptModalDialogFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Row;
use App\Components\DataGrid\Utils\Sorting;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Cash;
use App\Model\Entities\Receipt;
use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\UserRole;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Services\PDFGeneratorService\PDFGeneratorServiceFactory;
use Nette\Utils\Html;

class ReceiptsPresenter extends BasePresenter
{
   /** @var ReceiptRepository */
   private $receiptRepo;

   /** @var ReceiptDetailFactory @inject */
   public $receiptDetailFactory;

   /** @var PDFGeneratorServiceFactory @inject */
   public $pdfGeneratorServiceFactory;

   /** @var SendReceiptModalDialogFactory @inject */
   public $sendReceiptModalDialogFactory;

   protected function startup()
   {
      parent::startup();

      $this->receiptRepo = $this->_em->getRepository(Receipt::class);
      $this->breadcrumb->add("Doklady", "default");
   }

   public function actionDetail(int $id)
   {
      /** @var Receipt $receipt */
      $receipt = $this->receiptRepo->find($id);

      if(!$receipt) {
         $this->flashMessage("Doklad neexistuje", FlashMessage::TYPE_DANGER);
         $this->redirect("default");
      }

      $allowed = $this->receiptRepo->isCashierPrivilegedAccessToReceipt($this->getLoggedUser(), $receipt);
      if(!$allowed && !$this->getLoggedUser()->getRole()->equalsValue(UserRole::ROLE_ADMIN)) {
         $this->flashMessage("K dokladu nemáte právo přistoupit", FlashMessage::TYPE_DANGER);
         $this->redirect("default");
      }

      $this->breadcrumb->add("Doklad " . $receipt->getNiceId());
   }

   /****************************************************************************************************** components */

   protected function createComponentReceipts() : DataGrid
   {
      $datagrid = $this->dataGridFactory->create();

      $datagrid->addColumnText("niceID", "ID")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFilterText();
      $datagrid->addColumnDateTime("creationDate", "Vytvořeno")
         ->setWidth(13)
         ->setFilterDate();
      $datagrid->addColumnText("cash.name", "Pokladna")
         ->setWidth(10)
         ->setFilterSelect($this->getCashForFilter(), ['cash.id']);
      $datagrid->addColumnText("customer.name", "Zákazník")
         ->setWidth(10)
         ->setFilterText();
      $datagrid->addColumnText("typeOfDocument", "Typ")
         ->setWidth(15)
         ->setRenderer(function (Receipt $receipt) {
            $type = $receipt->getTypeOfDocument();

            return \App\Model\Enums\TypeOfDocument::getName($type);
         })
         ->setFilterSelect(\App\Model\Enums\TypeOfDocument::getNamedTypes());
      $datagrid->addColumnText("eetData.state", "Stav EET")
         ->setRenderer(function (Receipt $receipt) {
            $eetData = $receipt->getEetData();

            if(is_null($eetData)) {
               return "";
            }

            $state = $eetData->getState();

            if($state->getValue() == ReceiptEETState::WAITING) {
               return Html::el("span")->setText(ReceiptEETState::getName($state))->setClass("label label-primary");
            }
            elseif($state->getValue() == ReceiptEETState::OK) {
               return Html::el("span")->setText(ReceiptEETState::getName($state))->setClass("label label-success");
            }
            elseif($state->getValue() == ReceiptEETState::WAITING_NEXT_TRY) {
               return Html::el("span")->setText(ReceiptEETState::getName($state))->setClass("label label-warning");
            }
            elseif($state->getValue() == ReceiptEETState::ERROR) {
               return Html::el("span")->setText(ReceiptEETState::getName($state))->setClass("label label-danger");
            }

            return "";
         })
         ->setFilterSelect(ReceiptEETState::getNamedStates())
         ->setWidth(7);

      $datagrid->addColumnNumber("itemsCount", "Položek")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setFilterNumber();

      $datagrid->addColumnNumber("itemsSum", "Suma")
         ->setVirtualColumn()
         ->setWidth(10)
         ->setValueSuffix(" Kč")
         ->setFilterNumber();

      $datagrid->addAction("detail", "", "detail")
         ->setClass(function (Row $row){
            if($row->getValue("accessAllowed") || $this->getLoggedUser()->getRole()->equalsValue(UserRole::ROLE_ADMIN)) {
               return "btn btn-primary";
            }

            return "btn btn-primary disabled";
         })
         ->setIcon("fas fa-search");
      $datagrid->addAction("print", "")
         ->setClass("btn btn-success openNewWindow")
         ->setIcon("fas fa-print")
         ->onClick[] = [$this, "printReceipt"];
      $datagrid->addAction("send", "")
         ->setClass("btn btn-warning")
         ->setIcon("fas fa-envelope")
         ->setAjax(true)
         ->onClick[] = [$this, "sendReceipt"];

      $datagrid->setDefaultSorting(["creationDate" => Sorting::SORT_DESC]);

      $data = $this->receiptRepo->getReceiptsWithAccessFlagQb($this->getLoggedUser());

      $datagrid->setDataSource($data);

      return $datagrid;
   }

   public function printReceipt(array $receipt)
   {
      $generator = $this->pdfGeneratorServiceFactory->receipt($receipt[0]);

      $generator->show(true);
   }

   private function getCashForFilter() : array
   {
      /** @var CashRepository $cashRepo */
      $cashRepo = $this->_em->getRepository(Cash::class);
      $items = $cashRepo->getAll()->getQuery()->getResult();

      $ret = [];
      foreach ($items AS $item) {
         $ret[$item->getId()] = $item->getName();
      }

      return $ret;
   }

   ///////////////////////////////////////////////////////////////////// send receipt
   protected function createComponentSendReceiptModalDialog() : SendReceiptModalDialog
   {
      $comp = $this->sendReceiptModalDialogFactory->create();

      $comp->onSent[] = function () use ($comp) {
         $this->presenter->flashMessageNotify("Odesláno", FlashMessageNotify::TYPE_SUCCESS);
         $comp->hide();

         if(!$this->presenter->isAjax()) {
            $this->redirect("this");
         }
      };

      return $comp;
   }

   public function sendReceipt(array $receipt)
   {
      $this['sendReceiptModalDialog']->show($receipt[0]);
   }

   /****************************************************************************************************** receipt detail */
   protected function createComponentReceiptDetail() : ReceiptDetail
   {
      /** @var Receipt $receipt */
      $receipt = $this->receiptRepo->find($this->getParameter("id"));

      $comp = $this->receiptDetailFactory->create($receipt);

      $comp->onReceiptCancellation[] = function (Receipt $receipt) {
         $this->redirect("detail", $receipt->getId());
      };

      $comp->onCreateTicket[] = function (Receipt $receipt) {
         $this->redirect(":Tickets:CreateTicket:", $receipt->getId());
      };


      return $comp;
   }
}