<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\AddEditCashForm\AddEditCashForm;
use App\AdminModule\Components\AddEditCashForm\AddEditCashFormFactory;
use App\Components\AddEditUserForm\EditCompanyForm;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Cash;
use App\Model\Entities\Company;
use App\Model\Repositories\CashRepository;
use App\Model\Repositories\UsersRepository;

class CashPresenter extends BasePresenter
{
   /** @var AddEditCashFormFactory @inject */
   public $addEditCashFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Pokladny", "default");
   }

   /**
    * Create new cash
    */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");

      $this['addEditCashForm']->onSuccess[] = function (Cash $cash) {
         $this->flashMessage("Pokladna *" . $cash->getName() . "* vytvořena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   /**
    * Edit cash
    */
   public function actionEdit($id)
   {
      /** @var Cash $cash */
      $cash = $this->_em->getRepository(Cash::class)->find($id);

      //entity does not exist
      if(!$cash) {
         $this->flashMessage("Pokladna neexistuje");
         $this->redirect("default");
      }

      $this->breadcrumb->add($cash->getName() . "|editace pokladny");
      $this['addEditCashForm']->setCash($cash);
      $this['addEditCashForm']->onSuccess[] = function (Cash $cash) {
         $this->flashMessage("Pokladna *" . $cash->getName() . "* upravena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   /**
    * @return AddEditCashForm
    */
   public function createComponentAddEditCashForm() : AddEditCashForm
   {
      $form = $this->addEditCashFormFactory->create();

      /*$form->onSuccess[] = function (User $user, $action) {
         if($action == AddEditUserForm::ACTION_CREATED) {
            $this->flashMessage("Uživatel <strong>" . $user->getName() . "</strong> vytvořen", FlashMessage::TYPE_SUCCESS);
         }
         else if($action == AddEditUserForm::ACTION_SAVED) {
            $this->flashMessage("Uživatel <strong>" . $user->getName() . "</strong> upraven", FlashMessage::TYPE_SUCCESS);
         }
         $this->redirect("default");
      };*/

      return $form;
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Název")
         ->setFilterText()
         ->setWidth(20);
      $grid->addColumnText("premiseId", "Označení pokladny")
         ->setFilterText()
         ->setWidth(20);
      $grid->addColumnText("cashRegisterId", "Provozovna")
         ->setFilterText()
         ->setWidth(20);

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat pokladnu %s?", "name")
         ->onClick[] = [$this, "deleteCash"];

      /** @var CashRepository $cashRepo */
      $cashRepo = $this->_em->getRepository(Cash::class);

      $grid->setDataSource(
         $cashRepo->getAll()
      );

      return $grid;
   }

   public function deleteCash(Cash $cash)
   {
      //firstly mark as archived
      $cash->setArchived();
      $this->_em->flush($cash);

      try {
         $this->_em->remove($cash);
         $this->_em->flush();
         $this->flashMessage(
            "Pokladna *" . $cash->getName() . "* smazána",
            FlashMessage::TYPE_SUCCESS
         );
      } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
         $this->flashMessage(
            "Pokladna *" . $cash->getName() . "* archivována",
            FlashMessage::TYPE_WARNING
         );
      }
   }
}