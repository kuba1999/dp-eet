<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\AddEditUserForm\AddEditUserForm;
use App\Components\AddEditUserForm\AddEditUserFormFactory;
use App\Components\AddEditUserForm\EditCompanyForm;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Company;
use App\Model\Entities\User;
use App\Model\Enums\UserAccountStatus;
use App\Model\Exceptions\UserForeignKeyException;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\UsersRepository;

class CustomersPresenter extends BasePresenter
{
   /** @var AddEditUserFormFactory @inject */
   public $addEditUserFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Zákazníci", "default");
   }

   /**
    * List of customers
    */
   public function actionDefault()
   {

   }

   /**
    * Create new customer
    */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");
   }

   /**
    * Edit customer
    */
   public function actionEdit($id)
   {
      $user = $this->_em->getRepository(User::class)->find($id);
      $this->breadcrumb->add("Zákazník: " . $user->getName());
      $this['addEditUserForm']->setUser($user);
   }

   /**
    * @return AddEditUserForm
    */
   public function createComponentAddEditUserForm() : AddEditUserForm
   {
      $form = $this->addEditUserFormFactory->create(AddEditUserForm::FORM_CUSTOMER);

      $form->onSuccess[] = function (User $user, $action) {
         if($action == AddEditUserForm::ACTION_CREATED) {
            $this->flashMessage("Uživatel <strong>" . $user->getName() . "</strong> vytvořen", FlashMessage::TYPE_SUCCESS);
         }
         else if($action == AddEditUserForm::ACTION_SAVED) {
            $this->flashMessage("Uživatel <strong>" . $user->getName() . "</strong> upraven", FlashMessage::TYPE_SUCCESS);
         }
         $this->redirect("default");
      };

      return $form;
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Jméno")
         ->setFilterText();

      $grid->addColumnStatus("accountStatus", "Aktivní")
         ->addOption(0, "aktivní", "btn-success")
         ->addOption(1, "neaktivní", "btn-danger")
         ->addOption(2, "archivován", "btn-warning")
         ->onChange[] = [$this, 'changeAccountActivity'];

      $grid->addFilterSelect("accountStatus", UserAccountStatus::getStatuses())
         ->setMultiple(true);

      $grid->addColumnDateTime("lastLoginDate", "Poslední přihlášní")
         ->setReplacement([null => "n/a"]);

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-xs btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-xs btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat uživatele %s?", "name")
         ->onClick[] = [$this, "deleteUser"];

      /** @var UserRepository $userRepo */
      $userRepo = $this->_em->getRepository(User::class);

      $grid->setDataSource(
         $userRepo->getAllCustomers()
      );

      return $grid;
   }

   /**
    * @param User $user
    * @param $value
    */
   public function changeAccountActivity(User $user, $value)
   {
      $user->setAccountStatus((int)$value);

      /** @var UserRepository $userRepo */
      $userRepo = $this->_em->getRepository(User::class);
      $userRepo->save($user);
   }

   public function deleteUser(User $user)
   {
      /** @var UserRepository $userRepository */
      $userRepository = $this->_em->getRepository(User::class);

      try{
         $userRepository->delete($user);
      }catch (UserForeignKeyException $e) {
         $this->flashMessage("Uživatele nelze smazat", FlashMessage::TYPE_DANGER);
         return;
      }

      $this->flashMessage("Zákazník " . $user->getName() . " smazán", FlashMessage::TYPE_SUCCESS);
   }
}