<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;
use App\AdminModule\Components\AddEditTaxRateForm\AddEditTaxRateForm;
use App\AdminModule\Components\AddEditTaxRateForm\AddEditTaxRateFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\TaxRate;
use App\Model\Enums\TaxRateType;
use App\Model\Repositories\TaxRateRepository;

/**
 * Class TaxRatePresenter
 * @package App\AdminModule\Presenters
 *
 * @description Managing warehouses
 */
class TaxRatePresenter extends BasePresenter
{
   /** @var TaxRateRepository */
   private $taxRateRepo;

   /** @var AddEditTaxRateFormFactory @inject */
   public $addEditTaxRateFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->taxRateRepo = $this->_em->getRepository(TaxRate::class);

      $this->breadcrumb->add("Sazby DPH", "TaxRate:");
   }

   /*********************************************************************************************** List of suppliers */
   public function createComponentTaxRateGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("color", "")
         ->setWidth(2)
         ->setSortable(false)
         ->setRenderer(function (){return "";})
         ->addCellStyle(function(TaxRate $rate) {
            return "background-color: " . $rate->getColor();
         });
      $grid->addColumnText("name", "Název")
         ->setWidth(20)
         ->setFilterText();
      $grid->addColumnNumber("type", "Typ")
         ->setRenderer(function (TaxRate $taxRate) {
            return TaxRateType::getName($taxRate->getType());
         })
         ->setWidth(20)
         ->setFilterText();
      $grid->addColumnNumber("value", "Hodnota sazby")
         ->setValueSuffix("%")
         ->setWidth(20)
         ->setFilterText();

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");

      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat sazbu %s?", "name")
         ->onClick[] = [$this, "delete"];

      $grid->setDataSource($this->taxRateRepo->getAll());

      return $grid;
   }

   /*********************************************************************************************** Add tax */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat sazbu", "TaxRate:add");

      $this['addEditTaxRateForm']->onSuccess[] = function (TaxRate $taxRate) {
         $this->flashMessage("Sazba *" . $taxRate->getName() . "* vytvořena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function createComponentAddEditTaxRateForm() : AddEditTaxRateForm
   {
      $comp = $this->addEditTaxRateFormFactory->create();

      return $comp;
   }

   /*********************************************************************************************** Archive tax */
   public function delete(TaxRate $taxRate)
   {
      //firstly mark as archived
      $taxRate->setArchived();
      $this->_em->flush($taxRate);

      try {
         $this->_em->remove($taxRate);
         $this->_em->flush();
         $this->flashMessage(
            "Sazba *" . $taxRate->getName() . "* smazána",
            FlashMessage::TYPE_SUCCESS
         );

      } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
         $this->flashMessage(
            "Sazba *" . $taxRate->getName() . "* archivována",
            FlashMessage::TYPE_WARNING
         );
      }
   }

   /*********************************************************************************************** Edit tax */
   public function actionEdit(int $id)
   {
      /** @var TaxRate $taxRate */
      $taxRate = $this->taxRateRepo->find($id);

      //entity does not exist
      if(!$taxRate || $taxRate->isArchived()) {
         $this->flashMessage("Sazba neexistuje");
         $this->redirect("default");
      }

      $this->breadcrumb->add($taxRate->getName(), "TaxRate:add", null, "editace sazby");

      $this['addEditTaxRateForm']->setTaxRate($taxRate);
      $this['addEditTaxRateForm']->onSuccess[] = function (TaxRate $taxRate) {
         $this->flashMessage("Sazba *" . $taxRate->getName() . "* aktualizována", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }
}