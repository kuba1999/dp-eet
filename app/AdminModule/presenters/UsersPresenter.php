<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\AddEditUserForm\AddEditUserForm;
use App\Components\AddEditUserForm\AddEditUserFormFactory;
use App\Components\AddEditUserForm\EditCompanyForm;
use App\Components\AddEditUserForm\EditCompanyFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\User;
use App\Model\Enums\UserAccountStatus;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\MissingAdminException;
use App\Model\Exceptions\UserForeignKeyException;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\UsersRepository;

class UsersPresenter extends BasePresenter
{
   /** @var AddEditUserFormFactory @inject */
   public $addEditUserFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Uživatelé", "default");
   }

   /**
    * List of users
    */
   public function actionDefault()
   {

   }

   /**
    * Create new user
    */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");
   }

   /**
    * Create new user
    */
   public function actionEdit($id)
   {
      $user = $this->_em->getRepository(User::class)->find($id);
      $this->breadcrumb->add("Uživatel: " . $user->getName());
      $this['addEditUserForm']->setUser($user);
   }

   /**
    * @return AddEditUserForm
    */
   public function createComponentAddEditUserForm() : AddEditUserForm
   {
      $form = $this->addEditUserFormFactory->create(AddEditUserForm::FORM_USER_OF_SYSTEM);

      $form->onSuccess[] = function (User $user, $action) {
         if($action == AddEditUserForm::ACTION_CREATED) {
            $this->flashMessage("Uživatel *" . $user->getName() . "* vytvořen", FlashMessage::TYPE_SUCCESS);
         }
         else if($action == AddEditUserForm::ACTION_SAVED) {
            $this->flashMessage("Uživatel *" . $user->getName() . "* upraven", FlashMessage::TYPE_SUCCESS);
         }
         $this->redirect("default");
      };

      return $form;
   }

   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Jméno")
            ->setFilterText();

      $roles = UserRole::getNamedRoles();
      unset($roles[UserRole::ROLE_CUSTOMER]);
      unset($roles[UserRole::ROLE_GUEST]);

      $grid->addColumnText("role", "Role")
            ->setRenderer(function (User $user){
               return UserRole::getName($user->getRole());
            })
            ->setFilterSelect($roles);

      $grid->addColumnStatus("accountStatus", "Aktivní")
         ->addOption(0, "aktivní", "btn-success")
         ->addOption(1, "neaktivní", "btn-danger")
         ->addOption(2, "archivován", "btn-warning")
         ->onChange[] = [$this, 'changeAccountActivity'];

      $grid->addFilterSelect("accountStatus", UserAccountStatus::getStatuses())
         ->setMultiple(true);

      $grid->addColumnDateTime("lastLoginDate", "Poslední přihlášní")
         ->setReplacement([null => "n/a"]);

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-xs btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-xs btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat uživatele %s?", "name")
         ->onClick[] = [$this, "deleteUser"];

      /** @var UserRepository $userRepo */
      $userRepo = $this->_em->getRepository(User::class);

      $grid->setDataSource($userRepo->getAllUsersOfSystem());

      $grid->setFilterDefaults(
         [
            "accountStatus"   =>    [UserAccountStatus::ACTIVE, UserAccountStatus::INACTIVE]
         ]
      );

      return $grid;
   }

   /**
    * @param User $user
    * @param $value
    */
   public function changeAccountActivity(User $user, $value)
   {
      $user->setAccountStatus((int)$value);

      /** @var UserRepository $userRepo */
      $userRepo = $this->_em->getRepository(User::class);
      $userRepo->save($user);
   }

   public function deleteUser(User $user)
   {
      /** @var UserRepository $userRepository */
      $userRepository = $this->_em->getRepository(User::class);

      if($this->getLoggedUser() === $user) {
         $this->flashMessage("Nelze smazat sám sebe", FlashMessage::TYPE_DANGER);
         return;
      }

      try{
         $userRepository->delete($user);
      } catch (MissingAdminException $e) {
         $this->flashMessage("Uživatele nelze smazat, jelikož by v systému nebyl žádný uživatel s oprávněním administrátora", FlashMessage::TYPE_DANGER);
         return;
      } catch (UserForeignKeyException $e) {
         $this->flashMessage("Uživatele nelze smazat", FlashMessage::TYPE_DANGER);
         return;
      }

      $this->flashMessage("Uživatel " . $user->getName() . " smazán", FlashMessage::TYPE_SUCCESS);
   }
}