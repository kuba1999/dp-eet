<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\FlashMessage\FlashMessage;
use App\Forms\BaseForm;
use App\Forms\Controls\CKeditor\CKeditor;
use App\Model\Entities\Settings;
use App\Model\Entities\SettingsGlobal;
use App\Model\Repositories\SettingsRepository;

class PortalPageContentPresenter extends BasePresenter
{
   /** @var int */
   public $activeTab = 1;

   /** @var SettingsRepository */
   private $settingsRepo;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Obsah stránek", "PortalPageContent:");

      $this->settingsRepo = $this->_em->getRepository(SettingsGlobal::class);
   }

   public function actionDefault($activeTab = 1)
   {
      $this->template->activeTab = $activeTab;
   }

   protected function createComponentForm() : BaseForm
   {
      $form = new BaseForm();

      $globalSettings = $this->settingsRepo->getGlobalSettings();

      $form->addEditor("welcomePageContent")
         ->setView(CKeditor::VIEW_FULL)
         ->setDefaultValue($globalSettings->getWelcomePageContent());
      $form->addEditor("contactPageContent")
         ->setView(CKeditor::VIEW_FULL)
         ->setDefaultValue($globalSettings->getContactPageContent());

      $form->addSubmit("save_1", "Uložit");
      $form->addSubmit("save_2", "Uložit");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      $globalSettings = $this->settingsRepo->getGlobalSettings();

      if($form->isSubmitted() === $form['save_1']) {
         $globalSettings->setWelcomePageContent($form['welcomePageContent']->getValue());
         $this->_em->flush();

         $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this", 1);
      }
      elseif($form->isSubmitted() === $form['save_2']) {
         $globalSettings->setContactPageContent($form['contactPageContent']->getValue());
         $this->_em->flush();

         $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this", 2);
      }
   }
}