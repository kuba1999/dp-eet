<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;




use App\AdminModule\Components\ProfileForm\ProfileForm;
use App\AdminModule\Components\ProfileForm\ProfileFormFactory;
use App\Components\FlashMessage\FlashMessage;

class ProfilePresenter extends BasePresenter
{
   /** @var ProfileFormFactory @inject */
   public $profileFormFactory;

   public function actionDefault()
   {
      $this->breadcrumb->add("Můj profil");
   }

   /**
    * @return ProfileForm
    */
   public function createComponentProfileForm() : ProfileForm
   {
      $component = $this->profileFormFactory->create($this->getLoggedUser());
      $component->onSuccess[] = function () {
         $this->flashMessage("Uloženo", FlashMessage::TYPE_SUCCESS);
         $this->redirect("this");
      };

      return $component;
   }
}