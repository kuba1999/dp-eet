<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\StockInProducts\StockInProducts;
use App\AdminModule\Components\StockInProducts\StockInProductsFactory;
use App\Components\FlashMessage\FlashMessage;

class StockInProductsPresenter extends BasePresenter
{
   /** @var StockInProductsFactory @inject */
   public $stockInProductsFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Naskladnění", "StockIn:");

      if(!$this->isWarehouseEnabled()) {
         $this->flashMessage("Evidence skladu není zapnuta", FlashMessage::TYPE_DANGER);
      }
   }

   public function createComponentStockInProducts() : StockInProducts
   {
      $comp = $this->stockInProductsFactory->create();
      $comp->onSuccess[] = function () use ($comp) {
        $this->flashMessage("Položky naskladněny", FlashMessage::TYPE_SUCCESS);

        if(!$this->isAjax()) {
           $this->redirect("this");
        }
      };

      return $comp;
   }
}