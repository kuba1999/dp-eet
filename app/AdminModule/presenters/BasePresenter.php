<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\Components\Breadcrumb\Breadcrumb;
use App\Components\Breadcrumb\BreadcrumbFactory;
use App\Components\DataGrid\DataGridFactory;
use App\Model\Entities\SettingsGlobal;
use App\Model\Helpers\Base62Helper;
use App\Model\Repositories\SettingsRepository;

class BasePresenter extends \App\Presenters\BasePresenter
{
   /** @var BreadcrumbFactory @inject */
   public $breadcrumbFactory;

   /** @var Breadcrumb */
   public $breadcrumb;

   /** @var DataGridFactory @inject */
   public $dataGridFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb = $this->breadcrumbFactory->create();
      $this->setLayout(__DIR__ . "/templates/@layoutAdmin.latte");

      $this->breadcrumb->add("Domů", ":Admin:Dashboard:default");

      $this['flashMessage']->redrawControl();
      $this['flashMessageNotify']->redrawControl();
   }

   /**
    * Component for breadcrumb navigation
    * @return Breadcrumb
    */
   public function createComponentBreadcrumb() : Breadcrumb
   {
      return $this->breadcrumb;
   }

   /**
    * @return bool
    */
   public function isWarehouseEnabled() : bool
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->_em->getRepository(SettingsGlobal::class);

      return $settingsRepo->isWarehouseEnabled();
   }
}