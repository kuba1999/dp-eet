<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\AddEditNewsForm\AddEditNewsForm;
use App\AdminModule\Components\AddEditNewsForm\AddEditNewsFormFactory;
use App\Components\AddEditUserForm\EditCompanyForm;
use App\Components\AddEditUserForm\EditCompanyFormFactory;
use App\Components\DataGrid\Column\ColumnDateTime;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Utils\Sorting;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Company;
use App\Model\Entities\News;
use App\Model\Entities\Project;
use App\Model\Enums\Language;
use App\Model\Repositories\NewsRepository;

class NewsPresenter extends BasePresenter
{
   /** @var AddEditNewsFormFactory @inject */
   public $addEditNewsFormFactory;

   /** @var News */
   private $news;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Aktuality", "default");
   }

   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat", "add");

      $this['addEditNewsForm']->onSuccess[] = function (News $news){
         $this->flashMessage("Aktualita <strong>" . $news->getTitle() . "</strong> byla přidána", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function actionEdit($id)
   {
      /** @var NewsRepository $newsRepo */
      $newsRepo = $this->_em->getRepository(News::class);
      $this->news = $newsRepo->find($id);

      $this['addEditNewsForm']->setNews($this->news);
      $this->breadcrumb->add("Editovat: " . $this->news->getTitle(), "edit");

      $this['addEditNewsForm']->onSuccess[] = function (News $news){
         $this->flashMessage("Aktualita <strong>" . $news->getTitle() . "</strong> byla uložena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }


   /**
    * @return DataGrid
    */
   public function createComponentGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("title", "Titulek")
         ->setWidth(40)
         ->setFilterText();

      $grid->addColumnDateTime("date", "Datum")
         ->setWidth(10)
         ->setFormat(ColumnDateTime::FORMAT_DATE)
         ->setFilterDate();

      $grid->addColumnStatus("published", "Publikováno")
         ->setWidth(10)
         ->addOption(1, "ano", "btn-success")
         ->addOption(0, "ne", "btn-danger")
         ->onChange[] = [$this, 'changePublishedStatus'];

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");
      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat firmu %s?", "title")
         ->onClick[] = [$this, "deleteNews"];

      /** @var NewsRepository $newsRepo */
      $newsRepo = $this->_em->getRepository(News::class);

      $grid->setDefaultSorting(["date" => Sorting::SORT_DESC]);

      $grid->setDataSource($newsRepo->getAll());

      return $grid;
   }

   public function changePublishedStatus(News $news, $value)
   {
      $news->setPublished((bool) $value);
      $this->_em->flush($news);
   }

   /**
    * @param News $news
    */
   public function deleteNews(News $news)
   {
      $this->_em->remove($news);
      $this->newsImageStorage->deleteFile($news);
      $this->_em->flush();

      $this->flashMessage("Aktualita <strong>" . $news->getTitle(Language::CS) . "</strong> byla smazána", FlashMessage::TYPE_SUCCESS);
   }

   /**
    * @return AddEditNewsForm
    */
   public function createComponentAddEditNewsForm() : AddEditNewsForm
   {
      $component = $this->addEditNewsFormFactory->create();

      return $component;
   }
}