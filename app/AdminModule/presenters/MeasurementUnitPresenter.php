<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;
use App\AdminModule\Components\AddEditMeasurementUnitForm\AddEditMeasurementUnitForm;
use App\AdminModule\Components\AddEditMeasurementUnitForm\AddEditMeasurementUnitFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\MeasurementUnit;
use App\Model\Enums\UnitFormat;
use App\Model\Repositories\MeasurementUnitRepository;
use Nette\Utils\Html;

/**
 * Class MeasurementUnitPresenter
 * @package App\AdminModule\Presenters
 *
 * @description Managing measurements units
 */
class MeasurementUnitPresenter extends BasePresenter
{
   /** @var MeasurementUnitRepository */
   private $measurementUnitRepo;

   /** @var AddEditMeasurementUnitFormFactory @inject */
   public $addEditMeasurementUnitFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->measurementUnitRepo = $this->_em->getRepository(MeasurementUnit::class);

      $this->breadcrumb->add("Měrné jednotky", "MeasurementUnit:");
   }

   /*********************************************************************************************** List of suppliers */
   public function createComponentMeasurementUnitGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Název")
         ->setWidth(20)
         ->setFilterText();
      $grid->addColumnText("saleUnit", "Prodejní jednotka")
         ->setWidth(20)
         ->setRenderer(function(MeasurementUnit $measurementUnit) {
            if($measurementUnit->getSaleUnitFormat() === UnitFormat::get(UnitFormat::SQUARE)) {
               return Html::el()->setHtml($measurementUnit->getSaleUnit() . "&sup2;");
            }
            elseif($measurementUnit->getSaleUnitFormat() === UnitFormat::get(UnitFormat::CUBIC)) {
               return Html::el()->setHtml($measurementUnit->getSaleUnit() . "&sup3;");
            }
            else {
               return $measurementUnit->getSaleUnit();
            }
         })
         ->setFilterText();
      $grid->addColumnText("measurementUnit", "Měrná jednotka")
         ->setWidth(20)
         ->setRenderer(function(MeasurementUnit $measurementUnit) {
            if($measurementUnit->getMeasurementUnitFormat() === UnitFormat::get(UnitFormat::SQUARE)) {
               return Html::el()->setHtml($measurementUnit->getMeasurementUnit() . "&sup2;");
            }
            elseif($measurementUnit->getMeasurementUnitFormat() === UnitFormat::get(UnitFormat::CUBIC)) {
               return Html::el()->setHtml($measurementUnit->getMeasurementUnit() . "&sup3;");
            }
            else {
               return $measurementUnit->getMeasurementUnit();
            }
         })
         ->setFilterText();

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");

      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat měrnou jednotku %s?", "name")
         ->onClick[] = [$this, "delete"];

      $grid->setDataSource($this->measurementUnitRepo->getAll());

      return $grid;
   }

   /*********************************************************************************************** Add warehouse */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat sazbu", "TaxRate:add");

      $this['addEditMeasurementUnitForm']->onSuccess[] = function (MeasurementUnit $measurementUnit) {
         $this->flashMessage("Měrná jednotka *" . $measurementUnit->getName() . "* vytvořena", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function createComponentAddEditMeasurementUnitForm() : AddEditMeasurementUnitForm
   {
      $comp = $this->addEditMeasurementUnitFormFactory->create();

      return $comp;
   }

   /*********************************************************************************************** Remove warehouse */
   public function delete(MeasurementUnit $measurementUnit)
   {
      //firstly mark as archived
      $measurementUnit->setArchived();
      $this->_em->flush($measurementUnit);

      try {
         $this->_em->remove($measurementUnit);
         $this->_em->flush();
         $this->flashMessage(
            "Měrná jednotka *" . $measurementUnit->getName() . "* smazána",
            FlashMessage::TYPE_SUCCESS
         );

      } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
         $this->flashMessage(
            "Měrná jednotka *" . $measurementUnit->getName() . "* archivována",
            FlashMessage::TYPE_WARNING
         );
      }
   }

   /*********************************************************************************************** Add warehouse */
   public function actionEdit(int $id)
   {
      /** @var MeasurementUnit $unit */
      $unit = $this->measurementUnitRepo->find($id);

      //entity does not exist
      if(!$unit) {
         $this->flashMessage("Měrná jednotka neexistuje");
         $this->redirect("default");
      }

      $this->breadcrumb->add($unit->getName(), "MeasurementUnit:add", null, "editace měrné jednotky");

      $this['addEditMeasurementUnitForm']->setMeasurementUnit($unit);
      $this['addEditMeasurementUnitForm']->onSuccess[] = function (MeasurementUnit $measurementUnit) {
         $this->flashMessage("Měrná jednotka *" . $measurementUnit->getName() . "* aktualizována", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }
}