<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;
use App\AdminModule\Components\AddEditSupplierForm\AddEditSupplierForm;
use App\AdminModule\Components\AddEditSupplierForm\AddEditSupplierFormFactory;
use App\Components\DataGrid\DataGrid;
use App\Components\FlashMessage\FlashMessage;
use App\Model\Entities\Supplier;
use App\Model\Repositories\SupplierRepository;

/**
 * Class SupplierPresenter
 * @package App\AdminModule\Presenters
 *
 * @description Managing suppliers
 */
class SupplierPresenter extends BasePresenter
{
   /** @var SupplierRepository */
   private $supplierRepo;

   /** @var AddEditSupplierFormFactory @inject */
   public $addEditSupplierFormFactory;

   protected function startup()
   {
      parent::startup();

      $this->supplierRepo = $this->_em->getRepository(Supplier::class);

      $this->breadcrumb->add("Dodavatelé", "Supplier:");
   }

   /*********************************************************************************************** List of suppliers */
   public function createComponentSuppliersGrid() : DataGrid
   {
      $grid =  $this->dataGridFactory->create();

      $grid->addColumnText("name", "Název")
         ->setWidth(20)
         ->setFilterText();

      $grid->addColumnText("vatNo", "IČ")
         ->setWidth(10)
         ->setFilterText();

      $grid->addColumnText("phone", "Telefon")
         ->setWidth(15)
         ->setFilterText();

      $grid->addColumnText("email", "Email")
         ->setWidth(15)
         ->setFilterText();

      $grid->addAction("edit", "Editovat", "edit")
         ->setClass("btn btn-primary");

      $grid->addAction("delete", "Smazat")
         ->setClass("btn btn-danger")
         ->setAjax(true)
         ->setConfirm("Opravdu chcete smazat dodavatele %s?", "name")
         ->onClick[] = [$this, "delete"];

      $grid->setDataSource($this->supplierRepo->getAll());

      return $grid;
   }

   /*********************************************************************************************** Add supplier */
   public function actionAdd()
   {
      $this->breadcrumb->add("Přidat dodavatele", "Supplier:add");

      $this['addEditSupplierForm']->onSuccess[] = function (Supplier $supplier) {
         $this->flashMessage("Dodavatel *" . $supplier->getName() . "* přidán", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }

   public function createComponentAddEditSupplierForm() : AddEditSupplierForm
   {
      $comp = $this->addEditSupplierFormFactory->create();

      return $comp;
   }

   /*********************************************************************************************** Remove supplier */
   public function delete(Supplier $supplier)
   {
      //firstly mark as archived
      $supplier->setArchived();
      $this->_em->flush($supplier);

      try {
         $this->_em->remove($supplier);
         $this->_em->flush();
         $this->flashMessage(
            "Dodavatel *" . $supplier->getName() . "* smazán",
            FlashMessage::TYPE_SUCCESS
         );

      } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e) {
         $this->flashMessage(
            "Dodavatel *" . $supplier->getName() . "* archivován",
            FlashMessage::TYPE_WARNING
         );
      }
   }

   /*********************************************************************************************** Add supplier */
   public function actionEdit(int $id)
   {
      /** @var Supplier $supplier */
      $supplier = $this->supplierRepo->find($id);

      //entity does not exist
      if(!$supplier) {
         $this->flashMessage("Dodavatel neexistuje");
         $this->redirect("default");
      }
      elseif($supplier->isArchived()) {
         $this->flashMessage("Dodavatel je již archivován");
         $this->redirect("default");
      }

      $this->breadcrumb->add($supplier->getName(), "Supplier:add", null, "editace dodavatele");

      $this['addEditSupplierForm']->setSupplier($supplier);
      $this['addEditSupplierForm']->onSuccess[] = function (Supplier $supplier) {
         $this->flashMessage("Dodavatel *" . $supplier->getName() . "* uložen", FlashMessage::TYPE_SUCCESS);
         $this->redirect("default");
      };
   }
}