<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\Statistics\Products\Products;
use App\AdminModule\Components\Statistics\Products\ProductsFactory;
use App\AdminModule\Components\Statistics\Revenues\Revenues;
use App\AdminModule\Components\Statistics\Revenues\RevenuesFactory;
use App\AdminModule\Components\Statistics\Stock\Stock;
use App\AdminModule\Components\Statistics\Stock\StockFactory;
use App\AdminModule\Components\Statistics\Tickets\Tickets;
use App\AdminModule\Components\Statistics\Tickets\TicketsFactory;

class StatisticsPresenter extends BasePresenter
{
   /** @var RevenuesFactory @inject */
   public $revenuesFactory;

   /** @var TicketsFactory @inject */
   public $ticketsFactory;

   /** @var StockFactory @inject */
   public $stockFactory;

   /** @var ProductsFactory @inject */
   public $productsFactory;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Statistiky");
   }

   public function actionRevenues()
   {
      $this->breadcrumb->add("Tržby");
   }

   protected function createComponentRevenues() : Revenues
   {
      $comp = $this->revenuesFactory->create();

      return $comp;
   }

   /////////////////////////////////////////////////////////////////////////////////////////////// stock
   public function actionStock()
   {
      $this->breadcrumb->add("Sklad");
   }

   protected function createComponentStock() : Stock
   {
      $comp = $this->stockFactory->create();

      return $comp;
   }

   /////////////////////////////////////////////////////////////////////////////////////////////// products
   public function actionProducts()
   {
      $this->breadcrumb->add("Zboží");
   }

   protected function createComponentProducts() : Products
   {
      $comp = $this->productsFactory->create();

      return $comp;
   }

   /////////////////////////////////////////////////////////////////////////////////////////////// tickets
   public function actionTickets()
   {
      $this->breadcrumb->add("Tikety");
   }

   protected function createComponentTickets() : Tickets
   {
      $comp = $this->ticketsFactory->create();

      return $comp;
   }
}