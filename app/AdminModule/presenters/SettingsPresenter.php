<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;


use App\AdminModule\Components\EditEetSettings\EditEetSettings;
use App\AdminModule\Components\EditEetSettings\EditEetSettingsFactory;
use App\AdminModule\Components\EditGlobalSettings\EditGlobalSettings;
use App\AdminModule\Components\EditGlobalSettings\EditGlobalSettingsFactory;
use App\AdminModule\Components\EditSellerData\EditSellerData;
use App\AdminModule\Components\EditSellerData\EditSellerDataFactory;
use App\Model\Entities\Settings;

class SettingsPresenter extends BasePresenter
{
   /** @var EditSellerDataFactory @inject */
   public $editSellerDataFactory;

   /** @var EditEetSettingsFactory @inject */
   public $editEetSettingsFactory;

   /** @var EditGlobalSettingsFactory @inject */
   public $editGlobalSettingsFactory;

   /** @var int */
   public $activeTab = 1;

   protected function startup()
   {
      parent::startup();

      $this->breadcrumb->add("Nastavení", "Settings:");
   }

   public function actionDefault($activeTab = 1)
   {
      if(!isset($this->template->activeTab)) {
         $this->template->activeTab = $activeTab;
      }
   }

   /**
    * @return EditGlobalSettings
    */
   protected function createComponentEditGlobalSettings() : EditGlobalSettings
   {
      $comp = $this->editGlobalSettingsFactory->create();

      $comp->onSubmit[] = function () {
         $this->template->activeTab = 1;
      };
      $comp->onSuccess[] = function () {
         $this->redirect("this", 1);
      };

      return $comp;
   }

   /**
    * @return EditSellerData
    */
   protected function createComponentEditSellerData() : EditSellerData
   {
      $comp = $this->editSellerDataFactory->create();

      $comp->onSubmit[] = function () {
         $this->template->activeTab = 2;
      };
      $comp->onSuccess[] = function () {
         $this->redirect("this", 2);
      };

      return $comp;
   }

   /**
    * @return EditEetSettings
    */
   protected function createComponentEditEetSettings() : EditEetSettings
   {
      $comp = $this->editEetSettingsFactory->create();

      $comp->onSubmit[] = function () {
         $this->template->activeTab = 3;
      };
      $comp->onSuccess[] = function () {
         $this->redirect("this", 3);
      };

      return $comp;
   }
}