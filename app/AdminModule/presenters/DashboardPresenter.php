<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\AdminModule\Presenters;



use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Entities\StockMovement;
use App\Model\Entities\Ticket;
use App\Model\Enums\TicketStatus;
use App\Model\Helpers\DateHelper;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Repositories\StockMovementRepository;
use App\Model\Repositories\TicketRepository;
use App\TicketsModule\Components\DashboardTicketsTimeline\DashboardTicketsTimeline;
use App\TicketsModule\Components\DashboardTicketsTimeline\DashboardTicketsTimelineFactory;

class DashboardPresenter extends BasePresenter
{
   /**
    * @var DashboardTicketsTimelineFactory
    * @inject
    */
   public $ticketsTimelineFactory;

   /** @var TicketRepository */
   private $ticketRepo;

   protected function startup()
   {
      parent::startup();

      /** @var TicketRepository $ticketRepo */
      $this->ticketRepo = $this->_em->getRepository(Ticket::class);
   }


   ////////////////////////////////////////////////////////////////////////////////////////////// revenues
   public function getTodayRevenuesSum() : float
   {
      $sum = $this->_em->createQueryBuilder()
         ->select("SUM(i.unitPriceWithTax)")
         ->from(ReceiptItem::class, "i")
         ->leftJoin("i.receipt", "r")
         ->where("DATE(r.creationDate) = DATE(NOW())")
         ->getQuery()
         ->getSingleScalarResult();

      if(is_null($sum)) {
         $sum = 0;
      }

      return $sum;
   }

   public function getTodayRevenuesSumPercentageChange() : float
   {
      $sumToday = $this->getTodayRevenuesSum();

      $sumYesterday = $this->_em->createQueryBuilder()
         ->select("SUM(i.unitPriceWithTax)")
         ->from(ReceiptItem::class, "i")
         ->leftJoin("i.receipt", "r")
         ->where("DATE(r.creationDate) = DATE(DATE_SUB(NOW(), 1, 'day'))")
         ->getQuery()
         ->getSingleScalarResult();

      if($sumYesterday == 0) {
         return 0;
      }

      return ($sumToday - $sumYesterday) / $sumYesterday * 100;
   }

   public function getMonthRevenuesSum() : float
   {
      $sum = $this->_em->createQueryBuilder()
         ->select("SUM(i.unitPriceWithTax)")
         ->from(ReceiptItem::class, "i")
         ->leftJoin("i.receipt", "r")
         ->where("MONTH(r.creationDate) = MONTH(NOW()) AND YEAR(r.creationDate) = YEAR(NOW())")
         ->getQuery()
         ->getSingleScalarResult();

      if(is_null($sum)) {
         $sum = 0.0;
      }

      return $sum;
   }

   public function getMonthRevenuesSumPercentageChange() : float
   {
      $sumThisMonth = $this->getMonthRevenuesSum();

      $sumLastMonth = $this->_em->createQueryBuilder()
         ->select("SUM(i.unitPriceWithTax)")
         ->from(ReceiptItem::class, "i")
         ->leftJoin("i.receipt", "r")
         ->where("MONTH(r.creationDate) = MONTH(DATE_SUB(NOW(), 1, 'month')) AND YEAR(r.creationDate) = YEAR(DATE_SUB(NOW(), 1, 'month'))")
         ->getQuery()
         ->getSingleScalarResult();

      if($sumLastMonth == 0) {
         return 0;
      }

      return ($sumThisMonth - $sumLastMonth) / $sumLastMonth * 100;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////// receipts
   public function getTodayReceiptsCount() : float
   {
      $sum = $this->_em->createQueryBuilder()
         ->select("COUNT(r.id)")
         ->from(Receipt::class, "r")
         ->where("DATE(r.creationDate) = DATE(NOW())")
         ->getQuery()
         ->getSingleScalarResult();

      return $sum;
   }

   public function getTodayReceiptsCountPercentageChange() : float
   {
      $sumToday = $this->getTodayReceiptsCount();

      $sumYesterday = $this->_em->createQueryBuilder()
         ->select("COUNT(r.id)")
         ->from(Receipt::class, "r")
         ->where("DATE(r.creationDate) = DATE(DATE_SUB(NOW(), 1, 'day'))")
         ->getQuery()
         ->getSingleScalarResult();

      if($sumYesterday == 0) {
         return 0;
      }

      return ($sumToday - $sumYesterday) / $sumYesterday * 100;
   }

   public function getMonthReceiptsCount() : float
   {
      $sum = $this->_em->createQueryBuilder()
         ->select("COUNT(r.id)")
         ->from(Receipt::class, "r")
         ->where("MONTH(r.creationDate) = MONTH(NOW()) AND YEAR(r.creationDate) = YEAR(NOW())")
         ->getQuery()
         ->getSingleScalarResult();

      return $sum;
   }

   public function getMonthReceiptsCountPercentageChange() : float
   {
      $sumThisMonth = $this->getMonthReceiptsCount();

      $sumLastMonth = $this->_em->createQueryBuilder()
         ->select("COUNT(r.id)")
         ->from(Receipt::class, "r")
         ->where("MONTH(r.creationDate) = MONTH(DATE_SUB(NOW(), 1, 'month')) AND YEAR(r.creationDate) = YEAR(DATE_SUB(NOW(), 1, 'month'))")
         ->getQuery()
         ->getSingleScalarResult();

      if($sumLastMonth == 0) {
         return 100;
      }

      return ($sumThisMonth - $sumLastMonth) / $sumLastMonth * 100;
   }

   public function getNameOfLastMonth() : string
   {
      return DateHelper::czechMonth(date("m", strtotime("last month")));
   }

   public function getNameOfThisMonth() : string
   {
      return DateHelper::czechMonth(date("m"));
   }

   public function getNameOfLastDay() : string
   {
      return DateHelper::czechDay(date("w", strtotime("last day")));
   }

   public function getNameOfThisDay() : string
   {
      return DateHelper::czechDay(date("w"));
   }

   ////////////////////////////////////////////////////////////////////////////////////////////// last receipts
   public function getLastReceipts() : array
   {
      /** @var ReceiptRepository $receiptRepo */
      $receiptRepo = $this->_em->getRepository(Receipt::class);

      $qb = $receiptRepo->getReceiptsQb();
      $qb->orderBy("r.creationDate", "desc");
      $qb->setMaxResults(5);

      return $qb->getQuery()->getResult();
   }

   public function getLastStockMovements() : array
   {
      /** @var StockMovementRepository $receiptRepo */
      $receiptRepo = $this->_em->getRepository(StockMovement::class);

      $qb = $receiptRepo->getMovementsQB();
      $qb->orderBy("m.created", "desc");
      $qb->setMaxResults(5);

      return $qb->getQuery()->getResult();
   }
   ////////////////////////////////////////////////////////////////////////////////////////////// tickets

   public function getNewTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::NEW);

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getClosedTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::CLOSED)
         ->andWhere("t.author = :author")
         ->setParameter("author", $this->presenter->getLoggedUser());

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getOpenedTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status != :status")
         ->setParameter("status", TicketStatus::CLOSED);

      return $qb->getQuery()->getSingleScalarResult();
   }

   public function getNewMessageTicketsCount()
   {
      $qb = $this->ticketRepo->createQueryBuilder("t")
         ->select("COUNT(t.id)")
         ->where("t.status = :status")
         ->setParameter("status", TicketStatus::NEW_MESSAGE);

      return $qb->getQuery()->getSingleScalarResult();
   }

   /**
    * @return DashboardTicketsTimeline
    */
   public function createComponentTimeline() : DashboardTicketsTimeline
   {
      $component = $this->ticketsTimelineFactory->create();

      $component->onTicketOpen[] = function (int $ticketId) {
        $this->redirect(":Tickets:Ticket:", $ticketId);
      };

      return $component;
   }
}