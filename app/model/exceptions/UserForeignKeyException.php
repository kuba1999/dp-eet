<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Exceptions;


class UserForeignKeyException extends BaseException{};