<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use App\Model\Entities\Faq;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;

class FaqFileStorage extends BaseStorage
{
   /** @var String */
   private $path;

   function __construct(string $path)
   {
      $this->path = $path;
   }

   /**
    * @param Faq $faq
    * @return string
    */
   public function getFilePath(Faq $faq)
   {
      return $this->path . DIRECTORY_SEPARATOR . $faq->getId();
   }

   /**
    * @param Faq $faq
    */
   public function deleteFile(Faq $faq)
   {
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . $faq->getId());
   }

   /**
    * @param FileUpload $file
    * @param Faq $faq
    */
   public function saveFile(FileUpload $file, Faq $faq)
   {
      $file->move($this->path . DIRECTORY_SEPARATOR . $faq->getId());
   }

   /**
    * @param Faq $faq
    * @return string
    */
   public function getFileStream(Faq $faq) : string
   {
      return FileSystem::read($this->getFilePath($faq));
   }

   /**
    * @param Faq $faq
    * @return bool
    */
   public function hasFile(Faq $faq) : bool
   {
      return file_exists($this->getFilePath($faq));
   }
}