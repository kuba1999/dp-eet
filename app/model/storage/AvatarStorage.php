<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use App\Model\Entities\User;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;

class AvatarStorage extends BaseStorage
{
   /** @var String */
   private $path;

   /** @var String */
   private $webPath;

   function __construct(string $wwwDir, string $path)
   {
      $this->path = $wwwDir.$path;
      $this->webPath = $path;
   }

   public function getFilePath(User $user, $web = true)
   {
      if($web) {
         return $this->webPath . "/" . $user->getId() . ".jpg";
      } else {
         return $this->path . DIRECTORY_SEPARATOR . $user->getId() . ".jpg";
      }
   }

   public function deleteFile(User $user)
   {
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . $user->getId() . ".jpg");
   }

   public function saveFile(FileUpload $avatar, User $user)
   {
      $image = $avatar->toImage();

      return $image->save($this->path . DIRECTORY_SEPARATOR . $user->getId() . ".jpg", 100, Image::JPEG);
   }

   public function isExists(User $user) : bool
   {
      return file_exists($this->getFilePath($user, false));
   }
}