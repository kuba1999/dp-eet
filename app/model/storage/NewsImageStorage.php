<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use App\Model\Entities\News;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;

class NewsImageStorage extends BaseStorage
{
   /** @var String */
   private $path;

   /** @var String */
   private $webPath;

   function __construct(string $wwwDir, string $path)
   {
      $this->path = $wwwDir.$path;
      $this->webPath = $path;
   }

   public function getFilePath(News $news, $web = true, $thumb = false)
   {
      if($web) {
         return $this->webPath . "/" . ($thumb ? "thumb_":"") . $news->getId() . ".jpg";
      } else {
         return $this->path . DIRECTORY_SEPARATOR . ($thumb ? "thumb_":"") .  $news->getId() . ".jpg";
      }
   }
   public function deleteFile(News $news)
   {
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . $news->getId() . ".jpg");
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . "thumb_" . $news->getId() . ".jpg");
   }

   public function saveFile(FileUpload $file, News $news)
   {
      $image = $file->toImage();

      //original
      $image->resize(1920, 1080);
      $image->save($this->path . DIRECTORY_SEPARATOR . $news->getId() . ".jpg", 80, Image::JPEG);

      //thumb
      $image->resize(150, 150);
      $image->save($this->path . DIRECTORY_SEPARATOR . "thumb_" . $news->getId() . ".jpg", 80, Image::JPEG);
   }

   public function isExists(News $news) : bool
   {
      return file_exists($this->getFilePath($news, false));
   }
}