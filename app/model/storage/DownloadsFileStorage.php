<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use App\Model\Entities\DownloadsFile;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;

class DownloadsFileStorage extends BaseStorage
{
   /** @var String */
   private $path;

   function __construct(string $path)
   {
      $this->path = $path;
   }

   /**
    * @param DownloadsFile $file
    * @return string
    */
   public function getFilePath(DownloadsFile $file)
   {
      return $this->path . DIRECTORY_SEPARATOR . $file->getId();
   }

   /**
    * @param DownloadsFile $file
    */
   public function deleteFile(DownloadsFile $file)
   {
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . $file->getId());
   }

   /**
    * @param FileUpload $file
    * @param DownloadsFile $file
    */
   public function saveFile(FileUpload $uploadedFile, DownloadsFile $file)
   {
      $uploadedFile->move($this->path . DIRECTORY_SEPARATOR . $file->getId());
   }

   /**
    * @param DownloadsFile $file
    * @return string
    */
   public function getFileStream(DownloadsFile $file) : string
   {
      return FileSystem::read($this->getFilePath($file));
   }

   /**
    * @param DownloadsFile $file
    * @return bool
    */
   public function hasFile(DownloadsFile $file) : bool
   {
      return file_exists($this->getFilePath($file));
   }
}