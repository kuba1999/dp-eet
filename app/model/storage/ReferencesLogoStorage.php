<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use Nette\Utils\Finder;

class ReferencesLogoStorage extends BaseStorage
{
   /** @var String */
   private $path;

   /** @var String */
   private $webPath;

   function __construct(string $wwwDir, string $path)
   {
      $this->path = $wwwDir.$path;
      $this->webPath = $path;
   }

   public function getFiles()
   {
      $logos = [];

      foreach (Finder::findFiles('*')->in($this->path) as $key => $file) {
         $logos[] = $this->webPath . "/" . basename($file);
      }

      return $logos;

   }
}