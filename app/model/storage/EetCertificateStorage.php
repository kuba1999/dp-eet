<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use Nette\Utils\FileSystem;

class EetCertificateStorage extends BaseStorage
{
   private const CERT_FILE           =  "cert";
   private const PRIVATE_KEY_FILE    =  "privateKey";

   /** @var String */
   private $path;

   function __construct(string $path)
   {
      $this->path = $path;
   }

   /**
    * Save certificate and private key
    * @param string $cert
    * @param string $privateKey
    */
   public function save(string $cert, string $privateKey)
   {
      FileSystem::write($this->getCertPath(), $cert);
      FileSystem::write($this->getPrivateKeyPath(), $privateKey);
   }

   public function isExists() : bool
   {
      return file_exists($this->getCertPath()) && file_exists($this->getPrivateKeyPath());
   }

   public function getCertPath() : string
   {
      return $this->path . DIRECTORY_SEPARATOR . $this::CERT_FILE;
   }

   public function getPrivateKeyPath() : string
   {
      return $this->path . DIRECTORY_SEPARATOR . $this::PRIVATE_KEY_FILE;
   }
}