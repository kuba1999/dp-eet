<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Storage;


use App\Model\Entities\TicketAttachment;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;
use Nette\Utils\Image;

class TicketAttachmentStorage extends BaseStorage
{
   public static $allowedTypes = [
      'image/jpg',
      'image/jpeg',
      'image/png',
      'application/pdf',
   ];

   public static $allowedFiles = [
      'jpg',
      'png',
      'txt',
      'pdf'
   ];

   /** @var String */
   private $path;

   function __construct(string $path)
   {
      $this->path = $path;
   }

   /**
    * @param TicketAttachment $attachment
    * @return string
    */
   public function getFilePath(TicketAttachment $attachment)
   {
      return $this->path . DIRECTORY_SEPARATOR . $attachment->getMessage()->getId() . DIRECTORY_SEPARATOR . $attachment->getId();
   }

   /**
    * @param TicketAttachment $attachment
    */
   public function deleteFile(TicketAttachment $attachment)
   {
      FileSystem::delete($this->path . DIRECTORY_SEPARATOR . $attachment->getMessage()->getId() . DIRECTORY_SEPARATOR . $attachment->getId());
   }

   /**
    * @param FileUpload $file
    * @param TicketAttachment $attachment
    * @return static
    */
   public function saveFile(FileUpload $file, TicketAttachment $attachment)
   {
      $dir = $this->path . DIRECTORY_SEPARATOR . $attachment->getMessage()->getId();

      //create dir for ticket
      FileSystem::createDir($dir);

      return $file->move($dir . DIRECTORY_SEPARATOR . $attachment->getId());
   }

   /**
    * @param TicketAttachment $attachment
    * @return bool
    */
   public function isImage(TicketAttachment $attachment)
   {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);

      $type = finfo_file($finfo, $this->getFilePath($attachment));

      finfo_close($finfo);

      return in_array($type, ['image/jpg', 'image/jpeg', 'image/png']);
   }

   /**
    * @param FileUpload $file
    * @return bool
    */
   public function isAllowedFile(FileUpload $file)
   {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);

      $type = finfo_file($finfo, $file->getTemporaryFile());

      finfo_close($finfo);

      return in_array($type, self::$allowedTypes);
   }

   /**
    * @param TicketAttachment $attachment
    * @return string
    */
   public function getFileStream(TicketAttachment $attachment) : string
   {
      return FileSystem::read($this->getFilePath($attachment));
   }

   /**
    * @param TicketAttachment $attachment
    * @param bool $thumbnail
    * @return Image
    */
   public function getImage(TicketAttachment $attachment, $thumbnail = false) : Image
   {
      $image = Image::fromString($this->getFileStream($attachment));

      if($thumbnail) {
         $image->resize(150, 100);
      }

      return $image;
   }
}