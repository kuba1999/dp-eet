<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Cash;
use App\Model\Entities\Company;
use App\Model\Entities\User;
use App\Model\Exceptions\CashierWithoutAnyCash;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class CashRepository extends EntityRepository
{
   /**
    * @param bool $notArchived
    * @return QueryBuilder
    */
   public function getAll($notArchived = true) : QueryBuilder
   {
      $qb = $this->createQueryBuilder("c");

      if($notArchived) {
         $qb->where("c.archived = 0");
      }

      return $qb;
   }

   public function getDefaultCashId(User $cashier) : int
   {
      $cash = $this->getAll()
         ->select("PARTIAL c.{id}")
         ->leftJoin("c.privileges", "p")
         ->andWhere(":cashier MEMBER OF c.privileges")
         ->setParameter("cashier", $cashier)
         ->setMaxResults(1)
         ->getQuery()
         ->getOneOrNullResult();

      if($cash) {
         $this->_em->detach($cash);
         return $cash->getId();
      }

      throw new CashierWithoutAnyCash();
   }

   public function getAvailableCash(User $cashier) : array
   {
      $qb = $this->getAll()
         ->leftJoin("c.privileges", "p")
         ->andWhere(":cashier MEMBER OF c.privileges")
         ->setParameter("cashier", $cashier);

      return $qb->getQuery()->getResult();
   }

   public function canUserAccessToCash(User $cashier, Cash $cash) : bool
   {
      $qb = $this->getAll()
         ->select("COUNT(c.id) AS count")
         ->andWhere(":cashier MEMBER OF c.privileges")
         ->setParameter("cashier", $cashier)
         ->andWhere("c = :cash")
         ->setParameter("cash", $cash);

      $result = $qb->getQuery()->getSingleScalarResult();

      return $result == 1;
   }
}