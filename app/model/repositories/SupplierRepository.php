<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Supplier;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class SupplierRepository extends EntityRepository
{
   /**
    * @param bool $notArchived
    * @return QueryBuilder
    */
   public function getAll($notArchived = true) : QueryBuilder
   {
      $qb = $this->createQueryBuilder("s");

      if($notArchived) {
         $qb->where("s.archived = false");
      }

      return $qb;
   }

   /**
    * @return array
    */
   public function getAllForSelectBox() : array
   {
      $suppliers = $this->getAll()
         ->andWhere("s.visible = true")
         ->orderBy("s.name", "asc")
         ->getQuery()
         ->getResult();

      $ret = [];

      /** @var Supplier $supplier */
      foreach ($suppliers AS $supplier) {
         $ret[$supplier->getId()] = $supplier->getName() . " (" . $supplier->getVatNo() . ")";
      }

      return $ret;
   }
}