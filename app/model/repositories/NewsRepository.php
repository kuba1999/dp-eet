<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Company;
use App\Model\Entities\News;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class NewsRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("n");
   }

   /**
    * @return News[]
    */
   public function getNewsForHomepage()
   {
      return $this->getAll()
         ->where("n.published = 1")
         //->andWhere("DATE(n.date) BETWEEN DATESUB(NOW(), 1, 'MONTH') AND DATE(NOW())")
         ->orderBy("n.date", "desc")
         ->setMaxResults(3)
         ->getQuery()
         ->getResult();
   }
}