<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Product;
use App\Model\Entities\StockMovement;
use App\Model\Entities\StockMovementItem;
use App\Model\Enums\StockMovementType;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class StockMovementRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getMovementsQB() : QueryBuilder
   {
      return $this->createQueryBuilder("m")
         ->addSelect("SUM(i.quantity * i.unitPrice) AS priceSum")
         ->addSelect("SUM(i.quantity) AS quantitySum")
         ->leftJoin("m.items", "i")
         ->groupBy("m.id");
   }

   /**
    * @return array
    */
   public function getMovement(int $id) : array
   {
      return $this->getMovementsQB()
         ->where("m.id = :id")
         ->setParameter("id", $id)
         ->getQuery()
         ->getOneOrNullResult();
   }

   /**
    * @param Product $product
    * @return StockMovementItem|null
    */
   public function getLastBuyOfProduct(Product $product) : ?StockMovementItem
   {
      $qb = $this->_em->createQueryBuilder()
         ->select("i")
         ->from(StockMovementItem::class, "i")
         ->leftJoin("i.stockMovement", "m")
         ->setMaxResults(1)
         ->orderBy("m.created", "desc")
         ->where("i.product = :product")
         ->setParameter("product", $product)
         ->andWhere("m.type = :type")
         ->setParameter("type", StockMovementType::BUY);

      return $qb->getQuery()
               ->getOneOrNullResult();
   }

   /**
    * @param StockMovement $stockMovement
    * @return int
    */
   public function getQuantitySum(StockMovement $stockMovement) : float
   {
      $qb = $this->getMovementsQB()
         ->leftJoin("m.items", "i")
         ->select("SUM(i.quantity)")
         ->where("m = :stockMovement")
         ->setParameter("stockMovement", $stockMovement);

      return $qb->getQuery()->getSingleScalarResult();
   }

   /**
    * @param StockMovement $stockMovement
    * @return float
    */
   public function getPurchasePriceSum(StockMovement $stockMovement) : float
   {
      $qb = $this->getMovementsQB()
         ->leftJoin("m.items", "i")
         ->select("SUM(i.quantity * i.unitPrice)")
         ->where("m = :stockMovement")
         ->setParameter("stockMovement", $stockMovement);

      return $qb->getQuery()->getSingleScalarResult();
   }

   /**
    * @param StockMovement $stockMovement
    * @return QueryBuilder
    */
   public function getStockMovementItems(StockMovement $stockMovement) : QueryBuilder
   {
      $qb = $this->_em->createQueryBuilder()
         ->select("i")
         ->addSelect("SUM(i.quantity * i.unitPrice) AS priceSum")
         ->from(StockMovementItem::class, "i")
         ->where("i.stockMovement = :stockMovement")
         ->setParameter("stockMovement", $stockMovement)
         ->groupBy("i.id");

      return $qb;
   }
}