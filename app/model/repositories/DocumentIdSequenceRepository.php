<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Company;
use App\Model\Entities\DocumentIdSequence;
use App\Model\Enums\TypeOfDocument;
use Kdyby\Doctrine\EntityRepository;

class DocumentIdSequenceRepository extends EntityRepository
{
   /**
    * @return DocumentIdSequence|null
    */
   public function getSequence(TypeOfDocument $typeOfDocument) : ?DocumentIdSequence
   {
      $sequence = $this->createQueryBuilder("s")
         ->where("s.typeOfDocument = :type")
         ->setParameter("type", $typeOfDocument->getValue())
         ->getQuery()
         ->getOneOrNullResult();

      return $sequence;
   }
}