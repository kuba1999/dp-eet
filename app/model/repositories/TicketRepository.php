<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\TicketStatus;
use App\Model\Enums\UserRole;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class TicketRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAllQB() : QueryBuilder
   {
      $qb =  $this->createQueryBuilder("t")
         ->addSelect("MAX(m.created) AS lastMessage")
         ->leftJoin("t.messages", "m")
         ->groupBy("t.id");

      return $qb;
   }

   /**
    * @return QueryBuilder
    */
   public function getAllByAuthorQB(User $author) : QueryBuilder
   {
      return $this->getAllQB()
         ->where("t.author = :author")
         ->setParameter("author", $author);
   }

   /**
    * @param Ticket $ticket
    * @param TicketMessage $message
    */
   public static function changeStatusOnReply(Ticket $ticket, TicketMessage $message)
   {
      if(!UserRole::isUserOfBackend($message->getAuthor()) && $ticket->getStatus()->equalsValue(TicketStatus::ANSWERED)) {
         $ticket->setStatus(TicketStatus::get(TicketStatus::NEW_MESSAGE));
      }
      elseif($ticket->getAuthor() === $message->getAuthor()) {
      //message from same author => do not change status
      }
      elseif(UserRole::isUserOfBackend($message->getAuthor())) {
         $ticket->setStatus(TicketStatus::get(TicketStatus::ANSWERED));
      }/*
      else {
         $ticket->setStatus(TicketStatus::NEW_MESSAGE);
      }*/
   }

   /**
    * @param Ticket $ticket
    * @return array
    */
   public function getTicketParticipants(Ticket $ticket)
   {
      return $this->_em->createQueryBuilder()
         ->from(User::class, "u")
         ->select("u")
         ->leftJoin(TicketMessage::class, "m", \Doctrine\ORM\Query\Expr\Join::WITH, 'm.author = u')
         ->where("m.ticket = :ticket")
         ->setParameter("ticket", $ticket)
         ->getQuery()
         ->getResult();
   }

   /**
    * @param User $user
    * @return QueryBuilder
    */
   public function getAllTicketsByParticipant(User $user)
   {
      $ticketIDs =  $this->_em->createQueryBuilder()
                     ->from(Ticket::class, "t")
                     ->distinct()
                     ->select("t.id")
                     ->leftJoin(TicketMessage::class, "m", \Doctrine\ORM\Query\Expr\Join::WITH, 'm.ticket = t')
                     ->where("m.author = :user")
                     ->setParameter("user", $user)
                     ->groupBy("t.id")
                     ->getQuery()
                     ->getResult();

      $IDs = [null]; //workaround due to empty IDs
      foreach ($ticketIDs AS $ticketID) {
         $IDs[] = $ticketID['id'];
      }


      $qb = $this->getAllQB();
      $qb->add('where', $qb->expr()->in('t.id', $IDs));

      return $qb;
   }
}