<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\SellerData;
use Kdyby\Doctrine\EntityRepository;

class SellerDataRepository extends EntityRepository
{
   /**
    * @return SellerData|null
    */
   public function getSellerData() : ?SellerData
   {
      $sellerData = $this->createQueryBuilder("d")
         ->setMaxResults(1)
         ->orderBy("d.validFrom", "desc")
         ->getQuery()->getOneOrNullResult();

      if(is_null($sellerData)) {
         $sellerData = new SellerData();
         $this->_em->persist($sellerData);
      }

      return $sellerData;
   }

   public function isSellerTaxablePerson() : bool
   {
      return $this->getSellerData()->isTaxablePerson();
   }
}