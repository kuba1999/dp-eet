<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Cash;
use App\Model\Entities\DocumentIdSequence;
use App\Model\Entities\Receipt;
use App\Model\Entities\StockMovement;
use App\Model\Entities\User;
use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\StockMovementType;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class ReceiptRepository extends EntityRepository
{
   /**
    * Creates new receipt with generated document ID
    * @param Receipt $receipt
    */
   public function createReceipt(Receipt $receipt)
   {
      $this->_em->beginTransaction();

      foreach ($receipt->getItems() AS $item) {
         $this->_em->persist($item);
      }
      $this->_em->persist($receipt);

      $this->generateDocumentId($receipt);
      $this->makeStockMovement($receipt);

      $this->_em->flush();
      $this->_em->commit();
   }

   public function generateDocumentId(Receipt $receipt)
   {
      /** @var DocumentIdSequenceRepository $seqRepo */
      $seqRepo = $this->_em->getRepository(DocumentIdSequence::class);

      $sequence = $seqRepo->getSequence($receipt->getTypeOfDocument());

      if(is_null($sequence)) {
         $sequence = new DocumentIdSequence($receipt->getTypeOfDocument(), 1);
         $this->_em->persist($sequence);
      }

      $receipt->setDocumentId($sequence->getValue());
      $sequence->incrementValue();
   }

   private function makeStockMovement(Receipt $receipt)
   {
      $stockMovement = new StockMovement(
         StockMovementType::get(StockMovementType::SALE),
         $receipt->getCashier()
      );

      $this->_em->persist($stockMovement);

      //set receipt
      $stockMovement->setReceipt($receipt);

      foreach ($receipt->getItems() AS $item) {
         if(!$item->getProduct()) {
            continue;
         }

         //item
         $stockItem = $stockMovement->addItem(
            $item->getProduct(),
            $item->getQuantity()
         );

         //price from product
         $stockItem->setUnitPrice($item->getUnitPriceWithTax());

         $this->_em->persist($stockItem);
      }

      if(count($stockMovement->getItems()) === 0) {
         $this->_em->detach($stockMovement);
      }
   }

   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("r");
   }

   /**
    * @param Cash $cash
    * @return QueryBuilder
    */
   public function getReceiptsByCash(Cash $cash)
   {
      $qb = $this->getReceiptsQb();

      $qb->where("r.cash = :cash")
         ->setParameter("cash", $cash);

      return $qb;
   }

   /**
    * @param User $customer
    * @return QueryBuilder
    */
   public function getReceiptsByCustomer(User $customer)
   {
      $qb = $this->getReceiptsQb();

      $qb->where("r.customer = :customer")
         ->setParameter("customer", $customer);

      return $qb;
   }

   /**
    * Check if cashier can access to receipt (cashier must have privileges to access to cash)
    * @param User $cashier
    * @param Receipt $receipt
    * @return bool
    * @throws \Doctrine\ORM\NonUniqueResultException
    */
   public function isCashierPrivilegedAccessToReceipt(User $cashier, Receipt $receipt) : bool
   {
      $qb = $this->createQueryBuilder("r")
         ->select("r.id AS allowed")
         ->distinct()
         ->innerJoin("r.cash", "c")
         ->innerJoin("c.privileges", "p")
         ->where(":cashier MEMBER OF c.privileges", "r = :receipt")
         ->setParameter("cashier", $cashier)
         ->setParameter("receipt", $receipt);

      try {
         $allowed = $qb->getQuery()->getSingleScalarResult();
      } catch (NoResultException $e) {
         return false;
      }

      return (bool) $allowed;
   }

   /**
    * @return QueryBuilder
    */
   public function getReceiptsQb() : QueryBuilder
   {
      $qb = $this->createQueryBuilder("r")
         ->addSelect("COUNT(items.id) AS itemsCount")
         ->addSelect("SUM(items.unitPriceWithTax * items.quantity) AS itemsSum")
         ->leftJoin("r.customer", "customer")
         ->leftJoin("r.customerData", "customerData")
         ->leftJoin("r.eetData", "eetData")
         ->leftJoin("r.cashier", "cashier")
         ->leftJoin("r.cash", "cash")
         ->join("r.items", "items")
         ->groupBy("r.id");

      self::addSelectForNiceId($qb, "r");

      return $qb;
   }

   /**
    * @return QueryBuilder
    */
   public function getReceiptsWithAccessFlagQb(User $cashier) : QueryBuilder
   {
      $subQuery = $this->createQueryBuilder("r2")
         ->select("CASE WHEN(r2.id IS NULL) THEN 0 ELSE 1 END")
         ->distinct()
         ->innerJoin("r2.cash", "c2")
         ->innerJoin("c2.privileges", "p2")
         ->where(":cashier MEMBER OF c2.privileges", "r2 = r")
         ->getQuery()
         ->getDQL();

      $qb = $this->getReceiptsQb()
         ->addSelect("(".$subQuery.") AS accessAllowed")
         ->setParameter("cashier", $cashier);

      return $qb;
   }

   /**
    * @return QueryBuilder
    */
   public function getReceiptsWaitingForSendingToEetQB() : QueryBuilder
   {
      $qb = $this->getAll()
         ->join("r.eetData", "eet")
         ->where("eet.state = :state")
         ->setParameter("state", ReceiptEETState::WAITING_NEXT_TRY);

      return $qb;
   }

   public static function addSelectForNiceId(QueryBuilder $qb, string $receiptAlias)
   {
      $qb->addSelect("CONCAT($receiptAlias.typeOfDocument, '/', DATEFORMAT($receiptAlias.creationDate,'%Y/%m'), '/', $receiptAlias.documentId) AS niceID");
   }
}