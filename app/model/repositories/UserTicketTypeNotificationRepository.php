<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\User;
use App\Model\Entities\UserTicketTypeNotification;
use Doctrine\ORM\EntityRepository;

class UserTicketTypeNotificationRepository extends EntityRepository
{
   public function getUsersByNotificationType(int $type)
   {
      return $this->_em->createQueryBuilder()
         ->from(User::class, "u")
         ->select("u")
         ->leftJoin(UserTicketTypeNotification::class, "n", \Doctrine\ORM\Query\Expr\Join::WITH, 'u = n.user')
         ->where("n.type = :type")
         ->setParameter("type", $type)
         ->getQuery()
         ->getResult();
   }
}