<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\User;
use App\Model\Enums\UserRole;
use App\Model\Exceptions\MissingAdminException;
use App\Model\Exceptions\NotUniqueEmailException;
use App\Model\Exceptions\NotUniqueUsernameException;
use App\Model\Exceptions\UserForeignKeyException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\DateTime;
use Nette\Security\Passwords;
use Nette\Utils\Random;

class UserRepository extends EntityRepository
{
   public function __construct(EntityManager $em, Mapping\ClassMetadata $class)
   {
      parent::__construct($em, $class);
   }

   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("u");
   }

   /**
    * @return QueryBuilder
    */
   public function getAllUsersOfSystem() : QueryBuilder
   {
      return $this->createQueryBuilder("u")->where("u.role != :role")->setParameter("role", UserRole::ROLE_CUSTOMER);
   }

   /**
    * @return QueryBuilder
    */
   public function getAllCustomers() : QueryBuilder
   {
      return $this->createQueryBuilder("u")->where("u.role = :role")->setParameter("role", UserRole::ROLE_CUSTOMER);
   }

   /**
    * @return QueryBuilder
    */
   public function getAllEmployees() : QueryBuilder
   {
      return $this->createQueryBuilder("u")->where("u.role = :role")->setParameter("role", UserRole::ROLE_CASHIER);
   }

   /**
    * @param string|null $term
    * @return array
    */
   public function searchCustomers(string $term = null) : array
   {
      $qb = $this->getAllCustomers();

      if($term) {
         $qb->andWhere("u.name LIKE :name");
         $qb->setParameter("name", "%" . $term . "%");
      }

      return $qb->getQuery()->getResult();
   }


   /**
    * @param int $customerId
    * @return User|null
    * @throws \Doctrine\ORM\NonUniqueResultException
    */
   public function getCustomer(int $customerId) : ?User
   {
      $qb = $this->getAllCustomers()
         ->andWhere("u.id LIKE :id")
         ->setParameter("id", $customerId);

      return $qb->getQuery()->getOneOrNullResult();
   }

   /**
    * @param User $user
    * @throws NotUniqueEmailException
    * @throws NotUniqueUsernameException
    */
   public function create(User $user)
   {
      $this->_em->persist($user);

      try {
         $this->_em->flush($user);
      } catch(UniqueConstraintViolationException $e) {
         if(!$this->isUsernameUnique($user->getUsername())) {
            throw new NotUniqueEmailException();
         }
         elseif(!$this->isEmailUnique($user->getEmail())) {
            throw new NotUniqueUsernameException();
         }
      }
   }

   /**
    * Save user
    * @param User $user
    * @throws MissingAdminException
    * @throws NotUniqueEmailException
    * @throws NotUniqueUsernameException
    */
   public function save(User $user)
   {
      $this->_em->beginTransaction();
      $this->_em->merge($user);

      try {
         $this->_em->flush($user);
      } catch(UniqueConstraintViolationException $e) {
         if(!$this->isUsernameUnique($user->getUsername(), $user->getId())) {
            throw new NotUniqueUsernameException();
         }
         elseif(!$this->isEmailUnique($user->getEmail(), $user->getId())) {
            throw new NotUniqueEmailException();
         }
      }

      //check if we have admins
      if(!$this->isAdminExists()) {
         $this->_em->rollback();
         throw new MissingAdminException();
      }

      $this->_em->commit();
   }

   /**
    * Delete user
    * @param User $user
    * @throws MissingAdminException
    * @throws UserForeignKeyException
    */
   public function delete(User $user)
   {
      $this->_em->beginTransaction();
      $this->_em->remove($user);

      try {
         $this->_em->flush($user);
      } catch (ForeignKeyConstraintViolationException $e) {
         throw new UserForeignKeyException();
      }

      //check if we have admins
      if(!$this->isAdminExists()) {
         $this->_em->rollback();
         throw new MissingAdminException();
      }

      $this->_em->commit();
   }

   /**
    * Check if we have any admins
    * @return bool
    */
   public function isAdminExists() : bool
   {
      $qb = $this->createQueryBuilder("u")
            ->select("COUNT(u.id)")
            ->where("u.role = :role")
            ->setParameter("role", UserRole::ROLE_ADMIN);

      return $qb->getQuery()->getSingleScalarResult() > 0;
   }

   /**
    * @param string $password
    * @return string
    */
   public static function generatePasswordHash(string $password) : string
   {
      return Passwords::hash($password);
   }

   /**
    * @param string $password
    * @param User $user
    * @return bool
    */
   public static function passwordVerify(string $password, User $user) : bool
   {
      return Passwords::verify($password, $user->getPassword());
   }

   /**
    * @param User $user
    * @return String
    */
   private function calculateHashForAccountActivation(User $user) : String
   {
      $date = (new DateTime())->format("mY"); //valid for one month
      $hash = md5($date . $user->getId());

      return $hash;
   }

   /**
    * @param User $user
    */
   public function generateHashForAccountActivation(User $user)
   {
      $user->setHash( $this->calculateHashForAccountActivation($user) );
      $this->_em->flush($user);
   }

   /**
    * @param User $user
    * @return string - new password
    */
   public function resetPassword(User $user) : string
   {
      $password = Random::generate();

      $user->setPassword($password);
      $this->save($user);

      return $password;
   }

   /**
    * Check if username is unique
    * @param string $username
    * @param int|null $notUserId
    * @return bool
    */
   public function isUsernameUnique(string $username, int $notUserId = null) : bool
   {
      $qb = $this->createQueryBuilder("u")
         ->select("COUNT(u.id)")
         ->where("u.username = :username")
         ->setParameter("username", $username);

      if($notUserId) {
         $qb->andWhere("u.id != :id")
            ->setParameter("id", $notUserId);
      }

      return $qb->getQuery()->getSingleScalarResult() == 0;
   }

   /**
    * Check if email is unique
    * @param string $email
    * @param int|null $notUserId
    * @return bool
    */
   public function isEmailUnique(string $email, int $notUserId = null) : bool
   {
      $qb = $this->createQueryBuilder("u")
         ->select("COUNT(u.id)")
         ->where("u.email = :email")
         ->setParameter("email", $email);

      if($notUserId) {
         $qb->andWhere("u.id != :id")
            ->setParameter("id", $notUserId);
      }

      return $qb->getQuery()->getSingleScalarResult() == 0;
   }
}