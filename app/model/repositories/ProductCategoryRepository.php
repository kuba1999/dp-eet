<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\ProductCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ProductCategoryRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("c")
         ->orderBy("c.name", "asc");
   }

   /**
    * @param string $searchedTerm
    * @param $actualCategory $category
    * @return QueryBuilder
    */
   public function searchCategories(string $searchedTerm, ProductCategory $actualCategory = null) : QueryBuilder
   {
      /*$qb = $this->getAll()
         ->addSelect("MATCH (c.name) AGAINST (:searchedTerm 'expand') as HIDDEN score")
         ->where('MATCH (c.name) AGAINST (:searchedTerm) > 0')
         ->setParameter('searchedTerm', $searchedTerm)
         ->orderBy('score', 'desc');

      if($category) {
         $qb->andWhere("c.parent = :category")->setParameter("category", $category);
      } else {
         $qb->andWhere("c.parent IS NULL");
      }*/

      $qb = $this->getAll();
      $qb->andWhere($qb->expr()->like('c.name', ':searchedTerm'));
      $qb->setParameter('searchedTerm', "%" . $searchedTerm . "%");

      if($actualCategory) {
         /** @var ProductCategoryRepository $productCategoryRepo */
         $productCategoryRepo = $this->_em->getRepository(ProductCategory::class);
         $categories = $productCategoryRepo->getIDsOfSubCategories($actualCategory->getId());
         $categories[] = $actualCategory->getId();
         $qb->andWhere("c.id IN (:categories)")->setParameter("categories", $categories);
      }

      return $qb;
   }

   /**
    * @param int $categoryID
    * @return int[]
    */
   public function getIDsOfSubCategories(int $categoryID) : array
   {
      $qb = $this->getAll()
         ->select("c.id")
         ->where("c.parent = :parent")
         ->setParameter("parent", $categoryID);

      $ret = [];
      foreach ($qb->getQuery()->getResult() AS $categoryID)
      {
         $ret[] = $categoryID['id'];

         //check children
         $children = $this->getIDsOfSubCategories($categoryID['id']);
         if(count($children) > 0) {
            $ret[] = $children;
         }
      }

      return $ret;
   }

   /**
    * @param ProductCategory $category
    * @return array
    */
   public function getCategoryLocation(ProductCategory $category = null) : array
   {
      if(is_null($category)) { //root
         return [];
      }

      $ret = [];
      $ret[$category->getId()] = $category; //last category

      while ($category = $category->getParent()) {
         $ret[$category->getId()] = $category;
      }

      return array_reverse($ret, true);
   }

   /**
    * @param ProductCategory $category
    * @return array
    */
   public function getNamedCategoryLocation(ProductCategory $category = null) : array
   {
      $ret = [];

      foreach ($this->getCategoryLocation($category) as $category) {
         $ret[$category->getId()] = $category->getName();
      }

      return $ret;
   }
}