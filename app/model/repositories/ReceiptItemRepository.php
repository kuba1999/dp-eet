<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Company;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class ReceiptItemRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      $qb = $this->createQueryBuilder("i");

      return $qb;
   }
}