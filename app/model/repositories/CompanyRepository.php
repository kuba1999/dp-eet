<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Company;
use App\Model\Exceptions\DeleteException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class CompanyRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("c");
   }

   public function create(Company $company)
   {
      $this->_em->persist($company);
      $this->_em->flush($company);
   }

   public function delete(Company $company)
   {
      $this->_em->remove($company);

      try{
         $this->_em->flush($company);
      } catch(ForeignKeyConstraintViolationException $e) {
         throw new DeleteException("Firmu nelze smazat, jelikož je již přiřazena uživateli či obsahuje projekty");
      }
   }

   public function save(Company $company)
   {
      $this->_em->persist($company);
      $this->_em->flush($company);
   }

   /**
    * Get all companies in array
    * @return array
    */
   public function getAllAsArray() : array
   {
      $ret = [];
      foreach ($this->getAll()->getQuery()->getResult() AS $company) {
         $ret[$company->getId()] = $company->getName();
      }

      return $ret;
   }
}