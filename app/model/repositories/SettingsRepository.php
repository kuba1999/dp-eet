<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Settings;
use App\Model\Entities\SettingsEet;
use App\Model\Entities\SettingsGlobal;
use Kdyby\Doctrine\EntityRepository;

class SettingsRepository extends EntityRepository
{
   /**
    * @return SettingsGlobal
    * @throws \Doctrine\ORM\NonUniqueResultException
    */
   public function getGlobalSettings() : SettingsGlobal
   {
      $settings = $this->_em->createQueryBuilder()
         ->select("s")
         ->from(SettingsGlobal::class, "s")
         ->getQuery()
         ->getOneOrNullResult();

      if(is_null($settings)) {
         $settings = new SettingsGlobal();
         $this->_em->persist($settings);
         $this->_em->flush($settings);
      }

      return $settings;
   }

   public function isWarehouseEnabled() : bool
   {
      return $this->getGlobalSettings()->isWarehouseEnabled();
   }

   ////////////////////////////////////////////////////////////////

   /**
    * @return SettingsEet
    * @throws \Doctrine\ORM\NonUniqueResultException
    */
   public function getEetSettings() : SettingsEet
   {
      $settings = $this->_em->createQueryBuilder()
         ->select("s")
         ->from(SettingsEet::class, "s")
         ->getQuery()
         ->getOneOrNullResult();

      if(is_null($settings)) {
         $settings = new SettingsEet();
         $this->_em->persist($settings);
         $this->_em->flush($settings);
      }

      return $settings;
   }

   public function isEetEnabled() : bool
   {
      return $this->getEetSettings()->isEnabled();
   }
}