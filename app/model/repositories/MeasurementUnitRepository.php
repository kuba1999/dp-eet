<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class MeasurementUnitRepository extends EntityRepository
{
   /**
    * @param bool $notArchived
    * @return QueryBuilder
    */
   public function getAll($notArchived = true) : QueryBuilder
   {
      $qb = $this->createQueryBuilder("u");

      if($notArchived) {
         $qb->where("u.archived = 0");
      }

      return $qb;
   }

   public function getAllForSelectBox() : array
   {
      $units = $this->createQueryBuilder("u")
         ->getQuery()
         ->getResult();

      $ret = [];

      foreach ($units AS $unit) {
         $ret[$unit->getId()] = $unit->getName();
      }

      return $ret;
   }
}