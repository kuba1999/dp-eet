<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\ProductCategory;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class ProductRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("p")
         ->andWhere("p.archived = false")
         ->orderBy("p.name", "asc");
   }

   /**
    * @param string $searchedTerm
    * @param ProductCategory|null $actualCategory
    * @return QueryBuilder
    */
   public function searchProducts(string $searchedTerm, ProductCategory $actualCategory = null) : QueryBuilder
   {
      /*$qb = $this->getAll()
         ->addSelect("MATCH (p.name, p.ean, p.plu, p.note) AGAINST (:searchedTerm 'boolean') as HIDDEN score")
         ->where('MATCH (p.name, p.ean, p.plu, p.note) AGAINST (:searchedTerm) > 0')
         ->setParameter('searchedTerm', $searchedTerm)
         ->orderBy('score', 'desc');*/

      $qb = $this->getAll();

      if(!empty($searchedTerm)) {
         $qb->andWhere($qb->expr()->orX(
               $qb->expr()->like('p.name', ':searchedTerm'),
               $qb->expr()->like('p.ean', ':searchedTerm'),
               $qb->expr()->like('p.plu', ':searchedTerm'),
               $qb->expr()->like('p.note', ':searchedTerm')
            ));
         $qb->setParameter('searchedTerm', "%" . $searchedTerm . "%");
      }

      if($actualCategory) {
         /** @var ProductCategoryRepository $productCategoryRepo */
         $productCategoryRepo = $this->_em->getRepository(ProductCategory::class);
         $categories = $productCategoryRepo->getIDsOfSubCategories($actualCategory->getId());
         $categories[] = $actualCategory->getId();
         $qb->andWhere("p.category IN (:categories)")->setParameter("categories", $categories);
      }

      return $qb;
   }

   /**
    * @param string $searchedTerm
    * @param ProductCategory|null $actualCategory
    * @return QueryBuilder
    */
   public function searchInGivenProductsQB(string $searchedTerm, array $productIDs) : QueryBuilder
   {
      $qb = $this->searchProducts($searchedTerm);
      $qb->andWhere("p.id IN (:productIDs)")->setParameter("productIDs", $productIDs);

      return $qb;
   }
}