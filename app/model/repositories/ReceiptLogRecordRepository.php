<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Receipt;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class ReceiptLogRecordRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getLogRecordsQB() : QueryBuilder
   {
      $qb = $this->createQueryBuilder("r")
         ->leftJoin("r.receipt", "receipt");

      ReceiptRepository::addSelectForNiceId($qb, "receipt");

      return $qb;
   }

   /**
    * @param Receipt $receipt
    * @return QueryBuilder
    */
   public function getLogRecordsByReceiptQB(Receipt $receipt) : QueryBuilder
   {
      $qb = $this->getLogRecordsQB();

      $qb->where("r.receipt = :receipt")
         ->setParameter("receipt", $receipt);

      return $qb;
   }
}