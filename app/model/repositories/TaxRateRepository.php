<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\TaxRate;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Doctrine\EntityRepository;

class TaxRateRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll($notArchived = true) : QueryBuilder
   {
      $qb = $this->createQueryBuilder("r");

      if($notArchived) {
         $qb->where("r.archived = 0");
      }

      return $qb;
   }

   /**
    * @param TaxRate $taxRate
    */
   public function synchronizeValuesByType(TaxRate $taxRate)
   {
      $this->createQueryBuilder("r")
         ->update()
         ->set("r.value", $taxRate->getValue())
         ->where("r.archived = 0", "r.type = :type")
         ->setParameter("type", $taxRate->getType()->getValue())
         ->getQuery()
         ->execute();
   }

   /**
    * @return array
    */
   public function getAllForSelectBox() : array
   {
      /** @var TaxRate[] $rates */
      $rates = $this->getAll()
         ->orderBy("r.value", "desc")
         ->getQuery()
         ->getResult();

      $ret = [];

      foreach ($rates AS $rate) {
         $ret[$rate->getId()] = $rate->getName() . " (" . $rate->getValue() . "%)";
      }

      return $ret;
   }
}