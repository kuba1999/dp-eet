<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Company;
use App\Model\Entities\Project;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class ProjectRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("p");
   }

   public function create(Project $project)
   {
      $this->_em->persist($project);
      $this->_em->flush($project);
   }

   public function delete(Project $project)
   {
      $this->_em->remove($project);
      $this->_em->flush($project);
   }

   public function save(Project $project)
   {
      $this->_em->persist($project);
      $this->_em->flush($project);
   }

   /**
    * @param Company $company
    * @return \Doctrine\ORM\QueryBuilder
    */
   public function getAllByCompany(Company $company)
   {
      return $this->getAll()->where("p.company = :company")
               ->setParameter("company", $company);
   }
}