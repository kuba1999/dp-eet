<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Repositories;


use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\User;
use App\Model\Enums\TicketMessageType;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\QueryBuilder;

class TicketMessageRepository extends EntityRepository
{
   /**
    * @return QueryBuilder
    */
   public function getAll() : QueryBuilder
   {
      return $this->createQueryBuilder("m");
   }

   /**
    * @return QueryBuilder
    */
   public function getAllByTicket(Ticket $ticket) : QueryBuilder
   {
      return $this->getAll()
         ->where("m.ticket = :ticket")
         ->setParameter("ticket", $ticket);
   }

   /**
    * @param Ticket $ticket
    * @return TicketMessage|null
    */
   public function getLastTicketMessage(Ticket $ticket) : ?TicketMessage
   {
      $qb = $this->getAllByTicket($ticket);
      $qb->orderBy("m.created", "desc");
      $qb->orderBy("m.id", "desc");
      $qb->setMaxResults(1);

      return $qb->getQuery()->getOneOrNullResult();
   }

   /**
    * @param Ticket $ticket
    * @return int
    */
   public function countMessages(Ticket $ticket, $onlyUserMessages = false) : int
   {
      $qb = $this->createQueryBuilder("m")
         ->select("COUNT(m.id)")
         ->where("m.ticket = :ticket")
         ->setParameter("ticket", $ticket);

      if($onlyUserMessages) {
         $qb->andWhere('m.type IN (:type)')
            ->setParameter('type', [
               TicketMessageType::FIRST_MESSAGE,
               TicketMessageType::MESSAGE
            ]);
      }

      $count = $qb->getQuery()
                  ->getSingleScalarResult();

      return $count;
   }



   /**
    * @param User $user
    * @return QueryBuilder
    */
   public function getAllTicketsMessageByParticipantInTicket(User $user)
   {
      $ticketIDs =  $this->_em->createQueryBuilder()
         ->from(Ticket::class, "t")
         ->distinct()
         ->select("t.id")
         ->leftJoin(TicketMessage::class, "m", \Doctrine\ORM\Query\Expr\Join::WITH, 'm.ticket = t')
         ->where("m.author = :user")
         ->setParameter("user", $user)
         ->groupBy("t.id")
         ->getQuery()
         ->getResult();

      $IDs = [null]; //workaround due to empty IDs
      foreach ($ticketIDs AS $ticketID) {
         $IDs[] = $ticketID['id'];
      }


      $qb = $this->getAll();
      $qb->add('where', $qb->expr()->in('m.ticket', $IDs));

      return $qb;
   }
}