<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


class ColorHelper
{
   /**
    * @param string $hexcolor
    * @return string
    * @source https://24ways.org/2010/calculating-color-contrast/
    */
   public static function getContrastYIQ(string $hexcolor = null)
   {
      if(is_null($hexcolor)) {
         return "#000000";
      }


      $r = hexdec(substr($hexcolor,0,2));
      $g = hexdec(substr($hexcolor,2,2));
      $b = hexdec(substr($hexcolor,4,2));
      $yiq = (($r*299)+($g*587)+($b*114))/1000;

      return ($yiq >= 128) ? '#000000' : '#ffffff';
   }
}