<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;



class BarcodeHelper
{
   public static function isEanValid(string $code) : bool
   {
      $codeArray = str_split($code);
      $reversedCodeArray = array_reverse($codeArray);

      $oddCharsSum = 0;
      $evenCharsSum = 0;
      $roundedSum = 0;
      $sum = 0;

      foreach ($reversedCodeArray AS $i => $c)
      {
         if($i === 0) { //ignore first char, because it is for checking
            continue;
         }

         if(($i+1) % 2 === 0) {
            $evenCharsSum += (int)$c; //even char
         } else {
            $oddCharsSum += (int)$c; //odd char
         }

         $sum = $evenCharsSum * 3 + $oddCharsSum; //multiple even sum by 3
         $roundedSum = (int) ceil($sum / 10) * 10;
      }

      return ($roundedSum - $sum) === (int) $reversedCodeArray[0];
   }
}