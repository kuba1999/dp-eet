<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


use App\Model\Entities\StockMovement;
use App\Model\Enums\StockMovementType;
use Nette\Utils\Html;

class StockMovementHelper
{
   /**
    * Render type as colored label
    * @param StockMovement $movement
    * @return Html
    */
   public static function getStockMovementTypeLabel(StockMovement $movement) : Html
   {
      switch ($movement->getType()->getValue()) {
         case StockMovementType::BUY:
            return Html::el('span')->setText($movement->getType())->setClass("label label-success");
         case StockMovementType::SALE:
            return Html::el('span')->setText($movement->getType())->setClass("label label-warning");
         case StockMovementType::STOCK_TAKING:
            return Html::el('span')->setText($movement->getType())->setClass("label label-primary");
      }
   }
}