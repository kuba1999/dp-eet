<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

/**
 * Created by PhpStorm.
 * User: jakub
 * Date: 9.3.18
 * Time: 19:47
 */

namespace App\Model\Helpers;
use App\Model\Entities\Receipt;
use Hashids\Hashids;


class ReceiptTokenHelper
{
   const SALT              =  "*4(*@&5&*@!da)a(";
   const ALPHABET          =  "23456789abcdefghkmnopqrstuvwxyz";
   const MIN_HAS_LENGTH    =  5;
   const URL_REGEX         =  "[a-z,2-9]+";

   /**
    * @param Receipt $receipt
    * @return string
    */
   public static function encode(Receipt $receipt) : string
   {
      $hashids = new Hashids(self::SALT, self::MIN_HAS_LENGTH, self::ALPHABET);
      $hash = $hashids->encode($receipt->getId());

      return $hash;
   }

   /**
    * @param string $hash
    * @return int|null
    */
   public static function decode(string $hash) : ?int
   {
      $hashids = new Hashids(self::SALT, self::MIN_HAS_LENGTH, self::ALPHABET);

      $decoded = $hashids->decode($hash);

      if($decoded !== []) {
         return $hashids->decode($hash)[0];
      }

      return null;
   }
}