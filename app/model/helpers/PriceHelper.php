<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


use App\Model\Entities\Product;
use App\Model\Entities\TaxRate;

class PriceHelper
{
   /**
    * @param null $taxRate
    * @param $priceWithoutTax
    * @param bool $round
    * @return float|int
    * @throws \Exception
    */
   public static function calculatePriceWithVat($taxRate, $priceWithoutTax, $round = 4)
   {
      if (is_numeric($taxRate)) {
         $value = $taxRate;
      }
      elseif ($taxRate instanceof TaxRate) {
         $value = $taxRate->getValue();
      }
      else {
         throw new \Exception("Uknown value of tax rate");
      }

      $price = $priceWithoutTax + $priceWithoutTax * $value / 100;

      if($round !== false) {
         $price = round($price, $round);
      }

      return $price;
   }

   /**
    * @param null $taxRate
    * @param $priceWithTax
    * @param bool $round
    * @return float|int
    * @throws \Exception
    */
   public static function calculatePriceWithoutVat($taxRate, $priceWithTax, $round = 4)
   {
      if (is_numeric($taxRate)) {
         $value = $taxRate;
      }
      elseif ($taxRate instanceof TaxRate) {
         $value = $taxRate->getValue();
      }
      else {
         throw new \Exception("Uknown value of tax rate");
      }

      $price = $priceWithTax * 100.0 / ($value + 100.0);

      if($round !== false) {
         $price = round($price, 4);
      }

      return $price;
   }

   /**
    * @param Product $product
    * @param float $sellUnitPrice
    * @param float $purchaseUnitPrice
    * @return bool
    */
   public static function isGrossMarginValid(Product $product, float $sellUnitPrice, float $purchaseUnitPrice) : bool
   {
      if(is_null($product->getGrossMargin())) { //without gross margin
         return true;
      }

      $difference = $sellUnitPrice - $purchaseUnitPrice;

      if(is_numeric($product->getGrossMargin())) { //scalar value
         return $difference >= $product->getGrossMargin();
      }
      else { //value in percentage
         $value = explode("%", $product->getGrossMargin());
         $percents = $value[0];

         return ($difference/$sellUnitPrice * 100) >= $percents;
      }
   }
}