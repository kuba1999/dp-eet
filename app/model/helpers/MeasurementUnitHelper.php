<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


use App\Model\Entities\MeasurementUnit;
use App\Model\Enums\UnitFormat;

class MeasurementUnitHelper
{
   /**
    * @param MeasurementUnit $measurementUnit
    * @return string
    */
   public static function getSellUnitText(MeasurementUnit $measurementUnit) : string
   {
      $suffix = "";

      if($measurementUnit->getSaleUnitFormat() === UnitFormat::get(UnitFormat::SQUARE)) {
         $suffix = "²";
      }
      elseif($measurementUnit->getSaleUnitFormat() === UnitFormat::get(UnitFormat::CUBIC)) {
         $suffix = "³";
      }

      return $measurementUnit->getSaleUnit() . $suffix;
   }

   /**
    * @param MeasurementUnit $measurementUnit
    * @return string
    */
   public static function getMeasurementUnitText(MeasurementUnit $measurementUnit) : string
   {
      if(!$measurementUnit->getMeasurementUnit()) { //without => same as sell unit
         return self::getSellUnitText($measurementUnit);
      }

      $suffix = "";

      if($measurementUnit->getMeasurementUnitFormat() === UnitFormat::get(UnitFormat::SQUARE)) {
         $suffix = "²";
      }
      elseif($measurementUnit->getMeasurementUnitFormat() === UnitFormat::get(UnitFormat::CUBIC)) {
         $suffix = "³";
      }

      return $measurementUnit->getMeasurementUnit() . $suffix;
   }
}