<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


class DateHelper
{
   /**
    * @param int $month
    * @return string
    */
   public static function czechMonth(int $month) : string
   {
      static $months = array(1 => 'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec');

      return $months[$month];
   }

   /**
    * @param int $day
    * @return string
    */
   public static function czechDay(int $day) : string
   {
      static $days = array('neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota');
      return $days[$day];
   }

   public static function quarter(int $month) : int
   {
      return ceil($month/3);
   }
}