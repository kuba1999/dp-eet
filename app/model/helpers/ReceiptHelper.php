<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Helpers;


use App\Model\Entities\Receipt;
use App\Model\Enums\TypeOfDocument;

class ReceiptHelper
{
   public static function isPossibleSendToEET(Receipt $receipt) : bool
   {
      return in_array(
         $receipt->getTypeOfDocument()->getValue(),
         [
            TypeOfDocument::CASH_RECEIPT,
            TypeOfDocument::SIMPLIFIED_TAX_INVOICE,
            TypeOfDocument::TAX_INVOICE,
            TypeOfDocument::INVOICE,
            TypeOfDocument::CANCELLATION,
         ]
      );
   }
}