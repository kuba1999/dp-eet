<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Events;


use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use Kdyby\Events\Subscriber;


class UserListener implements Subscriber
{
   public function getSubscribedEvents()
   {
      return [
         'App\Model\Entities\User::prePersist',
         'App\Model\Entities\User::preUpdate'
      ];
   }

   /**
    * @param User $user
    */
   public function prePersist(User $user)
   {
      if ($user->isPasswordChanged() && !is_null($user->getPassword())) {
         $hash = UserRepository::generatePasswordHash($user->getPassword());

         $user->setPasswordHash($hash);
         $user->setPasswordChanged(false);
      }
   }

   /**
    * @param User $user
    */
   public function preUpdate(User $user)
   {
      if ($user->isPasswordChanged() && !is_null($user->getPassword())) {
         $hash = UserRepository::generatePasswordHash($user->getPassword());

         $user->setPasswordHash($hash);
         $user->setPasswordChanged(false);
      }
   }
}