<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Events;


use App\Model\Entities\StockMovement;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Events\Subscriber;
use Nette\Security\User;

class NewStockMovementListener implements Subscriber
{
   /** @var EntityManager */
   private $_em;

   /** @var User */
   private $user;

   /**
    * NewStockMovementListener constructor.
    * @param EntityManager $_em
    * @param User $user
    */
   public function __construct(EntityManager $_em, User $user)
   {
      $this->_em = $_em;
      $this->user = $user;
   }

   public function getSubscribedEvents()
   {
      return [
         //'App\Model\Entities\StockMovement::prePersist'
      ];
   }

   /**
    * @param StockMovement $stockMovement
    */
   /*public function prePersist(StockMovement $stockMovement)
   {
      $stockMovement->setExecutedBy($this->getLoggedUser());
   }*/

   /*private function getLoggedUser() : ?\App\Model\Entities\User
   {
      if($this->user->getId()) {
         $user = $this->_em->find(\App\Model\Entities\User::class, $this->user->getId());

         return $user;
      }

      return null;
   }*/
}