<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Events;


use App\Model\Entities\TicketMessage;
use App\Model\Enums\TicketMessageType;
use App\Model\Services\Mailing\TicketMailingService;
use Kdyby\Events\Subscriber;


class TicketMessageNotificationListener implements Subscriber
{
   /** @var  TicketMailingService */
   private $ticketMailingService;

   public function __construct(TicketMailingService $ticketMailingService)
   {
      $this->ticketMailingService = $ticketMailingService;
   }

   public function getSubscribedEvents()
   {
      return [
         'App\Model\Entities\TicketMessage::postPersist'
      ];
   }

   /**
    * @param TicketMessage $ticketMessage
    */
   public function postPersist(TicketMessage $ticketMessage)
   {
      //new ticket
      if($ticketMessage->getType() == TicketMessageType::FIRST_MESSAGE) {
         $this->ticketMailingService->createdNewTicket($ticketMessage);
      }
      //new message
      elseif($ticketMessage->getType() == TicketMessageType::MESSAGE) {
         $this->ticketMailingService->createdNewMessage($ticketMessage);
      }
      //ticket closed
      elseif($ticketMessage->getType() == TicketMessageType::CLOSED) {
         $this->ticketMailingService->ticketClosed($ticketMessage);
      }
   }
}