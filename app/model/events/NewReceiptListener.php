<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Events;


use App\Model\Entities\Receipt;
use App\Model\Entities\SettingsEet;
use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\TypeOfDocument;
use App\Model\Repositories\SettingsRepository;
use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Events\Subscriber;

class NewReceiptListener implements Subscriber
{
   /** @var EntityManager */
   private $em;

   /** @var EETService */
   private $EETService;

   /** @var SettingsEet */
   private $settingsEET;

   /**
    * NewReceiptListener constructor.
    * @param EntityManager $em
    * @param EETService $EETService
    */
   public function __construct(EntityManager $em, EETService $EETService)
   {
      $this->em = $em;
      $this->EETService = $EETService;

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsEet::class);
      $this->settingsEET = $settingsRepo->getEetSettings();
   }

   public function getSubscribedEvents()
   {
      return [
         'App\Model\Entities\Receipt::postPersist'
      ];
   }

   /**
    * @param Receipt $receipt
    */
   public function postPersist(Receipt $receipt)
   {
      $this->setEetState($receipt);
   }

   private function setEetState(Receipt $receipt)
   {
      if($receipt->getTypeOfDocument()->getValue() === TypeOfDocument::PRO_FORMA_INVOICE) {
         return;
      }

      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsEet::class);
      /** @var SettingsEet $eetSettings */
      $eetSettings = $settingsRepo->getEetSettings();

      if($eetSettings->isEnabled()) {
         $data = $receipt->createEetData();
         $data->setState(ReceiptEETState::get(ReceiptEETState::WAITING));

         //set premise ID and cash ID
         $data->setPremiseId($this->getPremiseId($receipt));
         $data->setCashRegisterId($this->getCashRegisterId($receipt));

         $this->em->persist($data);
         $this->em->flush($data);
      }
   }

   /**
    * Get premise ID from cash or global settings
    * @param Receipt $receipt
    * @return string
    */
   private function getPremiseId(Receipt $receipt) : string
   {
      if(empty($receipt->getCash()->getPremiseId())) {
         return $this->settingsEET->getDefaultPremiseId();
      }

      return $receipt->getCash()->getPremiseId();
   }


   /**
    * Get cash register ID from cash or global settings
    * @param Receipt $receipt
    * @return string
    */
   private function getCashRegisterId(Receipt $receipt) : string
   {
      if(empty($receipt->getCash()->getCashRegisterId())) {
         return $this->settingsEET->getDefaultCashRegisterId();
      }

      return $receipt->getCash()->getCashRegisterId();
   }


}