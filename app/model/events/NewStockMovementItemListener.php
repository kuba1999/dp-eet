<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Events;


use App\Model\Entities\StockMovementItem;
use App\Model\Enums\StockMovementType;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Events\Subscriber;


class NewStockMovementItemListener implements Subscriber
{
   /** @var EntityManager */
   private $_em;

   /**
    * @param EntityManager $_em
    */
   public function __construct(EntityManager $_em)
   {
      $this->_em = $_em;
   }


   public function getSubscribedEvents()
   {
      return [
         'App\Model\Entities\StockMovementItem::postPersist'
      ];
   }

   /**
    * @param StockMovementItem $stockMovementItem
    * @throws \Exception
    */
   public function postPersist(StockMovementItem $stockMovementItem)
   {
      $product = $stockMovementItem->getProduct();


      if($stockMovementItem->getStockMovement()->getType()->equalsValue(StockMovementType::BUY)) {
         $product->addQuantityInStock($stockMovementItem->getQuantity());

         $this->_em->flush($product);
      }
      elseif($stockMovementItem->getStockMovement()->getType()->equalsValue(StockMovementType::SALE)) {
         $product->removeQuantityInStock($stockMovementItem->getQuantity());

         $this->_em->flush($product);
      }
      elseif($stockMovementItem->getStockMovement()->getType()->equalsValue(StockMovementType::STOCK_TAKING)) {
         $product->addQuantityInStock($stockMovementItem->getQuantity());

         $this->_em->flush($product);
      }
   }
}