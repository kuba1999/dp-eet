<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


class TicketMessageType
{
   const CREATED                 = 0;
   const FIRST_MESSAGE           = 1;
   const MESSAGE                 = 2;
   const CLOSED                  = 3;
   const REOPENED                = 4;

   const AUTOMATICALLY_CLOSED    = 5;
}