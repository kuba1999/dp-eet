<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;

use Consistence\Enum\Enum;

class TypeOfPayment extends Enum
{
   const CASH              = 0;
   const BANK_TRANSFER     = 1;

   public static function getNamedTypes() : array
   {
      return [
         self::CASH            =>    "hotovost",
         self::BANK_TRANSFER   =>    "bankovní převod",
      ];
   }

   public static function getName(self $type) : string
   {
      return self::getNamedTypes()[$type->getValue()];
   }

   public static function getTypes() : array
   {
      return [
         self::CASH,
         self::BANK_TRANSFER
      ];
   }

   public function __toString()
   {
      return self::getName($this->getValue());
   }
}