<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class TicketStatus extends Enum
{
   const NEW           = 1;
   const REOPENED      = 2;
   const NEW_MESSAGE   = 3;
   const ANSWERED      = 4;
   const CLOSED        = 5;

   /**
    * @return array
    */
   public static function getNamedStatuses() : array
   {
      return [
         self::NEW         =>  "nový",
         self::REOPENED    =>  "znovuotevřen",
         self::NEW_MESSAGE =>  "nová zpráva",
         self::ANSWERED    =>  "odpovězeno",
         self::CLOSED      =>  "uzavřen",
      ];
   }

   /**
    * @param string $status
    * @return string
    */
   public static function getName(string $status) : string
   {
      return self::getNamedStatuses()[$status];
   }

   public function __toString()
   {
      return self::getName($this->getValue());
   }
}