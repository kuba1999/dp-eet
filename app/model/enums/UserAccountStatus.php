<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class UserAccountStatus extends Enum
{
   const ACTIVE      = 0;
   const INACTIVE    = 1;
   const ARCHIVED    = 2;

   public static function getStatuses()
   {
      return [
         self::ACTIVE    =>  "aktivní",
         self::INACTIVE  =>  "neaktivní",
         self::ARCHIVED  =>  "archivován"
      ];
   }
}