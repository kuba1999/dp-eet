<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;

use Consistence\Enum\Enum;

class TypeOfDocument extends Enum
{
   const CASH_RECEIPT            = 0; //nontaxable person
   const SIMPLIFIED_TAX_INVOICE  = 1; //taxable person
   const TAX_INVOICE             = 2; //taxable person
   const PRO_FORMA_INVOICE       = 3;
   const INVOICE                 = 4; //nontaxable person
   const CANCELLATION            = 5;

   public static function getNamedTypes() : array
   {
      return [
         self::CASH_RECEIPT               =>    "Pokladní doklad",
         self::SIMPLIFIED_TAX_INVOICE     =>    "Zjednodušený daňový doklad",
         self::TAX_INVOICE                =>    "Faktura - daňový doklad",
         self::PRO_FORMA_INVOICE          =>    "Zálohová faktura",
         self::INVOICE                    =>    "Faktura",
         self::CANCELLATION               =>    "Storno",
      ];
   }

   public static function getName(self $type) : string
   {
      return self::getNamedTypes()[$type->getValue()];
   }

   public static function getTypes() : array
   {
      return [
         self::CASH_RECEIPT,
         self::SIMPLIFIED_TAX_INVOICE,
         self::TAX_INVOICE,
         self::PRO_FORMA_INVOICE,
         self::INVOICE,
         self::CANCELLATION,
      ];
   }
}