<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class CashView extends Enum
{
   const TABLE             = 0;
   const GRID              = 1;
   const IMMEDIATE_SALE    = 2;

   public static function getName(self $view) : string
   {
      $view = $view->getValue();

      return self::getNamedViews()[$view];
   }

   public static function getNamedViews() : array
   {
      return [
         self::TABLE           => "výběr z katalogu - tabulka",
         self::GRID            => "výběr z katalogu - mřížka",
         self::IMMEDIATE_SALE  => "okamžitý prodej",
      ];
   }
}