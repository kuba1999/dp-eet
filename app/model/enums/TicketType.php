<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class TicketType extends Enum
{
   const GOODS                   = 1;
   const ORDER                   = 2;
   const COMPLAINT               = 3;
   const OTHER                   = 6;

   public static function getNamedTypes() : array
   {
      return [
         self::GOODS                      =>    "Zboží",
         self::ORDER                      =>    "Objednávka",
         self::COMPLAINT                  =>    "Reklamace",
         self::OTHER                      =>    "Ostatní",
      ];
   }

   public static function getName(int $type) : string
   {
      return self::getNamedTypes()[$type];
   }

   public static function getTypes() : array
   {
      return [
         self::GOODS,
         self::ORDER,
         self::COMPLAINT,
         self::OTHER
      ];
   }

   public function __toString()
   {
      return self::getName($this->getValue());
   }
}