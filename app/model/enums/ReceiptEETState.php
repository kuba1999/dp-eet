<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class ReceiptEETState extends Enum
{
   const WAITING           = 0;
   const OK                = 1;
   const WAITING_NEXT_TRY  = 2;
   const ERROR             = 3;

   public static function getName(ReceiptEETState $state, $short = true) : string
   {
      $state = $state->getValue();

      return self::getNamedStates($short)[$state];
   }

   public static function getNamedStates($short = true) : array
   {
      if($short) {
         return [
            self::WAITING           => "čeká na zaevidování",
            self::OK                => "OK",
            self::WAITING_NEXT_TRY  => "čekání na další pokus o zaevidování",
            self::ERROR             => "CHYBA"
         ];
      } else {
         return [
            self::WAITING           => "čeká",
            self::OK                => "OK",
            self::WAITING_NEXT_TRY  => "opakování",
            self::ERROR             => "CHYBA"
         ];
      }

   }
}