<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class TaxRateType extends Enum
{
   const _NONE_            = 0;

   const BASE              = 1;
   const REDUCED           = 2;
   const SECOND_REDUCED    = 3;
   const ZERO              = 4;

   public static function getName(TaxRateType $type) : string
   {
      $type = $type->getValue();

      return self::getNamedTypes()[$type];
   }

   public static function getNamedTypes() : array
   {
      return [
         self::_NONE_            => "-",
         self::BASE              => "základní",
         self::REDUCED           => "snížená",
         self::SECOND_REDUCED    => "druhá snížená",
         self::ZERO              => "nulová",
      ];
   }
}