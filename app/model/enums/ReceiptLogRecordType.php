<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;
use Nette\Utils\Html;

class ReceiptLogRecordType extends Enum
{
   const INFO        = 0;
   const SUCCESS     = 1;
   const WARNING     = 2;
   const ERROR       = 3;

   /**
    * @param ReceiptLogRecordType $type
    * @return string
    */
   public static function getName(self $type) : string
   {
      return self::getNamedTypes()[$type->getValue()];
   }

   /**
    * @return array
    */
   public static function getNamedTypes() : array
   {
      return [
         self::INFO     =>  "informace",
         self::SUCCESS  =>  "úspěch",
         self::WARNING  =>  "upozornění",
         self::ERROR    =>  "chyba"
      ];
   }

   /**
    * @return Html
    */
   public function getHtmlLabel() : Html
   {
      switch ($this->getValue()) {
         case self::INFO:
            return Html::el('span')->setText(self::getName($this))->setClass("label label-info");
         case self::SUCCESS:
            return Html::el('span')->setText(self::getName($this))->setClass("label label-success");
         case self::WARNING:
            return Html::el('span')->setText(self::getName($this))->setClass("label label-warning");
         case self::ERROR:
            return Html::el('span')->setText(self::getName($this))->setClass("label label-error");
      }
   }
}