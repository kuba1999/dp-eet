<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;

use Consistence\Enum\Enum;

class StockMovementType extends Enum
{
   const BUY               = "buy";
   const SALE              = "sale";
   const STOCK_TAKING      = "stockTaking";

   public static function getNamedTypes() : array
   {
      return [
         self::BUY            =>    "nákup",
         self::SALE           =>    "prodej",
         self::STOCK_TAKING   =>    "inventura",
      ];
   }

   public static function getName(string $type) : string
   {
      return self::getNamedTypes()[$type];
   }

   public static function getTypes() : array
   {
      return [
         self::BUY,
         self::SALE,
         self::STOCK_TAKING
      ];
   }

   public function __toString()
   {
      return self::getName($this->getValue());
   }
}