<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use Consistence\Enum\Enum;

class UnitFormat extends Enum
{
   const    SQUARE   =     0;
   const    CUBIC    =     1;

   public static function getNamedFormats() : array
   {
      return [
         self::SQUARE   =>    "čtvereční - x²",
         self::CUBIC    =>    "krychlový - x³",
      ];
   }

   public static function getName(int $type) : array
   {
      return self::getNamedFormats()[$type];
   }
}