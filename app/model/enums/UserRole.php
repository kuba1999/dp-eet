<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Enums;


use App\Model\Entities\User;
use Consistence\Enum\Enum;

class UserRole extends Enum
{
   const ROLE_CUSTOMER  = "customer";
   const ROLE_ADMIN     = "admin";
   const ROLE_CASHIER   = "cashier";
   const ROLE_GUEST     = "guest";

   public static function getRoles()
   {
      return [
         self::ROLE_CASHIER,
         self::ROLE_CUSTOMER,
         self::ROLE_ADMIN
      ];
   }

   public static function getNamedRoles() : array
   {
      return [
         self::ROLE_CASHIER   =>  "pokladní",
         self::ROLE_CUSTOMER  =>  "zákazník",
         self::ROLE_ADMIN     =>  "provozovatel",
         self::ROLE_GUEST     =>  "host",
      ];
   }

   public static function getName(UserRole $role) : string
   {
      return self::getNamedRoles()[$role->getValue()];
   }

   public static function isUserOfBackend(User $user) : bool
   {
      return in_array($user->getRole()->getValue(), [UserRole::ROLE_ADMIN, UserRole::ROLE_CASHIER]);
   }
}