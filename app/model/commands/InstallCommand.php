<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Commands;


use App\Model\Entities\Receipt;
use App\Model\Entities\User;
use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\UserAccountStatus;
use App\Model\Enums\UserRole;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\DebugFormatterHelper;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\ProcessHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Tests\Helper\ProcessHelperTest;

class InstallCommand extends Command
{
   const E_DATABASE = 1;

   /** @var EntityManager @inject*/
   public $em;

   protected function configure()
   {
      $this->setName('install')
         ->setDescription('Install application');
   }

   protected function execute(InputInterface $input, OutputInterface $output)
   {
      $io = new SymfonyStyle($input, $output);

      $io->title('Instalace aplikace FER POKLADNA');

      if($errorCode = $this->database($input, $output) !== 0) {
         return $errorCode;
      }

      $this->user($input, $output);

      $this->em->flush();
      $io->success("Instalace dokoncena");
      return 0;
   }

   private function database(InputInterface $input, OutputInterface $output)
   {
      $io = new SymfonyStyle($input, $output);

      $io->section("Vytvoreni DB");

      try {
         $connected = $this->em->getConnection()->connect();
      } catch (\Exception $e) {
         $connected = false;
      }

      if($connected) {
         $io->write(
            [
               ' Test pripojeni k DB             ',
               '<info>OK</info>',
            ]
         );
         $io->newLine();
         $io->write(
            [
               ' Databaze                        ',
               '<comment>' . $this->em->getConnection()->getDatabase() . '</comment>',
            ]
         );
      } else {
         $io->write(
            [
               ' Test pripojeni k DB             ',
               '<error>CHYBA</error>',
            ]
         );
         $io->newLine();

         return $this::E_DATABASE;
      }

      if($io->confirm('Vytvorit schema databaze?'))
      {
         //drop
         $command = new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand();
         $command->setApplication($this->getApplication());

         $arguments = array(
            '-f'  => true,
         );

         $dropInput = new ArrayInput($arguments);
         $returnCode = $command->run($dropInput, new NullOutput());

         if($returnCode === 0) {
            $io->write(
               [
                  ' Smazani DB                      ',
                  '<info>OK</info>',
               ]
            );
            $io->newLine();
         } else {
            $io->write(
               [
                  ' Smazani DB                      ',
                  '<error>CHYBA</error>',
               ]
            );
            $io->newLine();

            return $this::E_DATABASE;
         }



         //create
         $command = new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand();
         $command->setApplication($this->getApplication());

         $returnCode = $command->run(new ArrayInput([]), new NullOutput());

         if($returnCode === 0) {
            $io->write(
               [
                  ' Vytvoreni DB                    ',
                  '<info>OK</info>',
               ]
            );
            $io->newLine();
         } else {
            $io->write(
               [
                  ' Vytvoreni DB                    ',
                  '<error>CHYBA</error>',
               ]
            );
            $io->newLine();

            return $this::E_DATABASE;
         }
      }

      return 0;
   }



   private function user(InputInterface $input, OutputInterface $output)
   {
      $io = new SymfonyStyle($input, $output);

      $io->section("Ucet provozovatele");

      $name = $io->ask('Jmeno', null, function ($name) {
         if (empty($name)) {
            throw new \RuntimeException('Zadejte jmeno');
         }

         return $name;
      });

      $username = $io->ask('Uzivatelske jmeno', null, function ($username) {
         if (empty($username)) {
            throw new \RuntimeException('Zadejte uzivatelske jmeno');
         }

         return $username;
      });

      $password = $io->askHidden('Heslo (skryte)', function ($password) {
         if (empty($password)) {
            throw new \RuntimeException('Zadejte heslo');
         }

         return $password;
      });

      $user = new User($name, UserRole::get(UserRole::ROLE_ADMIN));
      $user->setAccountStatus(UserAccountStatus::ACTIVE);
      $user->setUsername($username);
      $user->setPassword($password);

      $this->em->persist($user);

      return 0;
   }
}