<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Commands;


use App\Model\Entities\Receipt;
use App\Model\Entities\ReceiptItem;
use App\Model\Services\ProductSuggesterService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitProductSuggesterCommand extends Command
{
   /** @var EntityManager @inject*/
   public $em;

   /** @var ProductSuggesterService @inject */
   public $productSuggesterService;

   use LockableTrait;

   protected function configure()
   {
      $this->setName('cash:initProductSuggester')
         ->setDescription('Initialize product suggester')
         ->addOption("force", "f");
   }

   protected function execute(InputInterface $input, OutputInterface $output)
   {
      if (!$this->lock()) {
         $output->writeln('The command is already running in another process.');

         return 0;
      }

      if($input->getOption("force")) {
         $output->writeln("cleaning...");
         $this->productSuggesterService->clean();
      }

      $lastLoadedReceiptId = $this->productSuggesterService->getLastProcessedReceiptId();
      $lastReceiptId = $this->getLastReceiptId();

      $output->writeln("Processing receipt ID from " . $lastLoadedReceiptId . " to " . $lastReceiptId);

      if($lastReceiptId <= $lastLoadedReceiptId)
      {
         return 0;
      }

      /** @var ReceiptItem[] $items */
      $items = $this->getReceiptItems();
      $this->productSuggesterService->trainIncremental($items);
      $output->writeln("Loaded: " . count($items) . " items");


      $this->release();
      return 0;
   }

   private function getLastReceiptId() : int
   {
      return $this->em->createQueryBuilder()
         ->select("MAX(r.id)")
         ->from(Receipt::class, "r")
         ->getQuery()
         ->getSingleScalarResult();
   }

   private function getReceiptItems() : array
   {
      return $this->em->createQueryBuilder()
         ->select("PARTIAL i.{id}, PARTIAL p.{id}, PARTIAL r.{id}")
         ->from(ReceiptItem::class, "i")
         ->join("i.product", "p")
         ->join("i.receipt", "r")
         ->where("r.id > :receiptId", "p.archived = false")
         ->setParameter("receiptId", $this->productSuggesterService->getLastProcessedReceiptId())
         ->getQuery()
         ->getResult();
   }
}