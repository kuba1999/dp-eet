<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Commands;


use App\Model\Entities\Receipt;
use App\Model\Enums\ReceiptEETState;
use App\Model\Repositories\ReceiptRepository;
use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendReceiptsToEETCommand extends Command
{
   /** @var EntityManager @inject*/
   public $em;

   /** @var EETService @inject */
   public $EETService;

   use LockableTrait;


   protected function configure()
   {
      $this->setName('cash:sendReceiptsToEET')
         ->setDescription('Send waiting receipts to EET');
   }

   protected function execute(InputInterface $input, OutputInterface $output)
   {
      if (!$this->lock()) {
         $output->writeln('The command is already running in another process.');

         return 0;
      }

      /** @var ReceiptRepository $sellRepo */
      $receiptRepo = $this->em->getRepository(Receipt::class);

      $receipts =  $receiptRepo->getReceiptsWaitingForSendingToEetQB()->getQuery()->getResult();

      $output->writeln("Count of receipts waiting for sending to EET: " . count($receipts));

      $results = [];

      /** @var Receipt $receipt */
      foreach($receipts AS $receipt)
      {
         $result = $this->EETService->send($receipt);

         if(!$result && $receipt->getEetData()->getNumberOfTry() > 20) {
            $receipt->getEetData()->setState(ReceiptEETState::get(ReceiptEETState::ERROR));
            $results[] = [
               $receipt->getNiceId(),
               "ERROR",
               ""
            ];
         }
         elseif(!$result) {
            $results[] = [
               $receipt->getNiceId(),
               "NEXT TRY",
               $receipt->getEetData()->getFik()
            ];
         }
         else {
            $results[] = [
               $receipt->getNiceId(),
               "OK",
               $receipt->getEetData()->getFik()
            ];
         }

         $this->em->flush();
      }

      if(count($receipts)) {
         $table = new Table($output);
         $table
            ->setHeaders(array('ID', 'Result', 'FIK'))
            ->setRows($results);
         $table->render();
      }

      $this->release();

      return 0;
   }
}