<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Commands;


use App\Model\Entities\Ticket;
use App\Model\Enums\TicketMessageType;
use App\Model\Enums\TicketStatus;
use App\Model\Repositories\TicketRepository;
use App\Model\Services\Mailing\TicketMailingService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CloseOpenedTicketsAfterPeriodCommand extends Command
{
   /** @var EntityManager @inject*/
   public $em;

   /** @var TicketMailingService @inject */
   public $ticketMailingService;

   protected function configure()
   {
      $this->setName('cash:close-tickets')
         ->setDescription('Close opened tickets after 14 days since last message');
   }

   protected function execute(InputInterface $input, OutputInterface $output)
   {
      /** @var TicketRepository $ticketRepo */
      $ticketRepo = $this->em->getRepository(Ticket::class);

      $tickets = $ticketRepo->createQueryBuilder("t")
                     ->where("DATE(DATEADD(t.lastMessage, 14, 'DAY')) < DATE(NOW())")
                     ->andWhere("t.status != :status")
                     ->setParameter("status", TicketStatus::CLOSED)
                     ->getQuery()
                     ->getResult();

      $i = 0;
      /** @var Ticket $ticket */
      foreach ($tickets AS $ticket) {
         $ticket->setStatus(TicketStatus::CLOSED);
         $message = $ticket->addLogMessage(TicketMessageType::AUTOMATICALLY_CLOSED, null);
         $this->em->flush($ticket);

         echo "ticket #" . $ticket->getId() . " was closed\n";

         $this->ticketMailingService->ticketClosedAfterPeriod($message);
         $i++;
      }

      echo "Closed " . $i . " tickets\n";

      return 0;
   }
}