<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\PDFGeneratorService;

use App\Model\Entities\Receipt;
use App\Model\Enums\TypeOfDocument;
use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;
use Latte\Engine;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Nette\Application\LinkGenerator;
use Nette\Application\Responses\FileResponse;

class ReceiptPDFGenerator extends Generator
{
   /** @var Receipt */
   private $receipt;

   /** @var LinkGenerator */
   private $linkGenerator;

   /**
    * ReceiptPDFGenerator constructor.
    * @param Receipt $receipt
    */
   public function __construct(
      string $tempDir,
      EntityManager $em,
      LinkGenerator $linkGenerator,
      EETService $EETService,
      Receipt $receipt
   ) {
      parent::__construct($tempDir, $em, $EETService);

      $this->linkGenerator = $linkGenerator;
      $this->receipt = $receipt;
   }

   private function contentCashReceipt() : string
   {
      $template = new Engine();

      $content = $template->renderToString(__DIR__."/templates/cashReceipt.latte", array(
         "generator" => $this,
         "receipt"   => $this->receipt,
         "seller"    => $this->receipt->getSellerData(),
         "link"      => $this->getLink()
      ));

      return $content;
   }

   private function contentInvoice() : string
   {
      $template = new Engine();

      $content = $template->renderToString(__DIR__."/templates/invoice.latte", array(
         "generator" => $this,
         "receipt"   => $this->receipt,
         "customer"  => $this->receipt->getCustomerData(),
         "seller"    => $this->receipt->getSellerData(),
         "link"      => $this->getLink()
      ));

      return $content;
   }

   private function getMPDF() : Mpdf
   {
      switch ($this->receipt->getTypeOfDocument()->getValue()) {
         case TypeOfDocument::CASH_RECEIPT:
         case TypeOfDocument::SIMPLIFIED_TAX_INVOICE:
         case TypeOfDocument::CANCELLATION:
            $content = $this->contentCashReceipt();
            break;
         case TypeOfDocument::TAX_INVOICE:
         case TypeOfDocument::PRO_FORMA_INVOICE:
         case TypeOfDocument::INVOICE:
            $content = $this->contentInvoice();
      }


      $mpdf = new Mpdf([
         'format' => 'A4',
         'default_font_size' => 9,
         'default_font' => 'dejavusans',
         'tempDir' => $this->tempDir
      ]);

      $mpdf->SetTitle(TypeOfDocument::getName($this->receipt->getTypeOfDocument()) . ' č. ' . $this->receipt->getNiceId());
      $mpdf->WriteHTML($content);

      return $mpdf;
   }

   public function show($print = false): void
   {
      $mpdf = $this->getMPDF();

      if($print) {
         $mpdf->SetJS('this.print();');
      }

      $mpdf->Output($this->receipt->getId().'.pdf',Destination::INLINE);
   }

   public function download(): void
   {
      $mpdf = $this->getMPDF();

      $mpdf->Output($this->receipt->getId().'.pdf',Destination::DOWNLOAD);
   }

   public function getFile(): string
   {
      $mpdf = $this->getMPDF();
      $file = $this->tempDir . DIRECTORY_SEPARATOR . $this->getFileName();

      $mpdf->Output($file,Destination::FILE);

      return $file;
   }

   public function getFileName(): string
   {
      return $this->receipt->getId().'.pdf';
   }

   private function getLink() : string
   {
      $link = $this->linkGenerator->link(
         "Portal:ReceiptToken:default",
         [
            $this->receipt->getToken()
         ]
      );

      return $link;
   }

   public function getPkpCode() : string
   {
      return $this->EETService->generatePKPCode($this->receipt);
   }

   public function getBkpCode() : string
   {
      return $this->EETService->generateBKPCode($this->receipt);
   }
}