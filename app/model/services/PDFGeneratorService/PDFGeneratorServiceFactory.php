<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\PDFGeneratorService;

use App\Model\Entities\Receipt;
use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\LinkGenerator;

class PDFGeneratorServiceFactory
{
   /** @var EntityManager */
   private $em;

   /** @var string */
   private $tempDir;

   /** @var LinkGenerator */
   private $linkGenerator;

   /** @var EETService */
   private $EETService;

   /**
    * PDFGeneratorServiceFactory constructor.
    * @param EntityManager $em
    */
   public function __construct(
      string $tempDir,
      EntityManager $em,
      LinkGenerator $linkGenerator,
      EETService $EETService
   ) {
      $this->em = $em;
      $this->tempDir = $tempDir;
      $this->linkGenerator = $linkGenerator;
      $this->EETService = $EETService;
   }

   public function receipt(Receipt $receipt) : ReceiptPDFGenerator
   {
      return new ReceiptPDFGenerator($this->tempDir, $this->em, $this->linkGenerator, $this->EETService, $receipt);
   }
}