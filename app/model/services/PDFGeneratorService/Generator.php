<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\PDFGeneratorService;


use App\Model\Services\EETService;
use Kdyby\Doctrine\EntityManager;

abstract class Generator
{
   /** @var EntityManager */
   protected $em;

   /** @var string */
   protected $tempDir;

   /** @var EETService */
   protected $EETService;

   /**
    * Generator constructor.
    * @param EntityManager $em
    */
   public function __construct(string $tempDir, EntityManager $em, EETService $EETService)
   {
      $this->em = $em;
      $this->tempDir = $tempDir . DIRECTORY_SEPARATOR . "mpdf";
      $this->EETService = $EETService;
   }

   abstract public function show($print = false) : void;
   abstract public function download() : void;
}