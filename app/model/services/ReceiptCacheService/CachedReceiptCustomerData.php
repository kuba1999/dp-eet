<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\ReceiptCacheService;


class CachedReceiptCustomerData
{
   /**
    * @var string|null
    */
   private $name;

   /**
    * @var string|null
    */
   private $street;

   /**
    * @var string|null
    */
   private $city;

   /**
    * @var string|null
    */
   private $zip;

   /**
    * @var string|null
    */
   private $company;

   /**
    * @var string|null
    */
   private $vatNo;

   /**
    * @var string|null
    */
   private $vatId;

   /**
    * @var string|null
    */
   private $phone;

   /**
    * @var string|null
    */
   private $email;

   /**
    * @return string
    */
   public function getName(): ?string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name = null)
   {
      $this->name = $name;
   }

   /**
    * @return string|null
    */
   public function getStreet(): ?string
   {
      return $this->street;
   }

   /**
    * @param string $street
    */
   public function setStreet(string $street = null)
   {
      $this->street = $street;
   }

   /**
    * @return string|null
    */
   public function getCity(): ?string
   {
      return $this->city;
   }

   /**
    * @param string $city
    */
   public function setCity(string $city = null)
   {
      $this->city = $city;
   }

   /**
    * @return string|null
    */
   public function getZip(): ?string
   {
      return $this->zip;
   }

   /**
    * @param string $zip
    */
   public function setZip(string $zip = null)
   {
      $this->zip = $zip;
   }

   /**
    * @return string|null
    */
   public function getVatNo(): ?string
   {
      return $this->vatNo;
   }

   /**
    * @param string $vatNo
    */
   public function setVatNo(string $vatNo = null)
   {
      $this->vatNo = $vatNo;
   }

   /**
    * @return string|null
    */
   public function getVatId(): ?string
   {
      return $this->vatId;
   }

   /**
    * @param string $vatId
    */
   public function setVatId(string $vatId = null)
   {
      $this->vatId = $vatId;
   }

   /**
    * @return string|null
    */
   public function getCompany(): ?string
   {
      return $this->company;
   }

   /**
    * @param string $company
    */
   public function setCompany(string $company = null)
   {
      $this->company = $company;
   }

   /**
    * @return null|string
    */
   public function getPhone(): ?string
   {
      return $this->phone;
   }

   /**
    * @param null|string $phone
    */
   public function setPhone(?string $phone): void
   {
      $this->phone = $phone;
   }

   /**
    * @return null|string
    */
   public function getEmail(): ?string
   {
      return $this->email;
   }

   /**
    * @param null|string $email
    */
   public function setEmail(?string $email): void
   {
      $this->email = $email;
   }

   /**
    * Check if any property is filled
    * @return bool
    */
   public function isAnyDetailFilled() : bool
   {
      return !empty($this->name) ||
         !empty($this->street) ||
         !empty($this->city) ||
         !empty($this->zip) ||
         !empty($this->vatId) ||
         !empty($this->vatNo) ||
         !empty($this->email) ||
         !empty($this->phone);
   }
}