<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\ReceiptCacheService;


use App\Model\Entities\Product;
use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;

class CachedReceiptItem
{
   /** @var int */
   private $id;

   /** @var int|null */
   private $productId;

   /** @var CachedReceipt */
   private $receipt;

   /** @var string|null */
   private $name;

   /**
    * Price without vat
    * @var float
    */
   private $price;

   /** @var float */
   private $quantity = 1.0;

   /** @var string */
   protected $note;

   /** @var int|null */
   protected $taxRateId = null;

   /** @var float */
   protected $taxRateValue = null;

   /** @var TaxRateType */
   protected $taxRateType = TaxRateType::_NONE_;

   /** @var float|null */
   protected $amountInSellUnit = 1.0;

   /**
    * CachedReceiptItem constructor.
    * @param CachedReceipt $receipt
    * @param int|null $productId
    * @param string $name
    * @param float $quantity
    * @param $price
    * @param float $amountInSellUnit
    */
   public function __construct(
      CachedReceipt $receipt,
      int $productId = null,
      ?string $name,
      float $quantity,
      float $price,
      float $amountInSellUnit = 1.0
   ) {
      $this->id = $receipt->generateId();
      $this->receipt = $receipt;
      $this->productId = $productId;
      $this->name = $name;
      $this->quantity = $quantity;
      $this->price = $price;
      $this->amountInSellUnit = $amountInSellUnit;
   }

   /**
    * Create instance from Product object
    * @param CachedReceipt $receipt
    * @param Product $product
    * @param bool $withTax
    * @return CachedReceiptItem
    */
   public static function fromProduct(CachedReceipt $receipt, Product $product, bool $withTax)
   {
      $item = new CachedReceiptItem(
         $receipt,
         $product->getId(),
         $product->getName(),
         1.0,
         $product->getPrice(),
         $product->getAmountInSellUnit()
      );

      if($product->getTaxRate() && $withTax) {
         $item->setTaxRateId($product->getTaxRate()->getId());
         $item->setTaxRateValue($product->getTaxRate()->getValue());
         $item->setTaxRateType($product->getTaxRate()->getType());
      }

      return $item;
   }

   /**
    * @return int
    */
   public function getId(): int
   {
      return $this->id;
   }

   /**
    * @return CachedReceipt
    */
   public function getReceipt(): CachedReceipt
   {
      return $this->receipt;
   }

   /**
    * @return int|null
    */
   public function getProductId(): ?int
   {
      return $this->productId;
   }

   /**
    * @param int|null $productId
    */
   public function setProductId(?int $productId): void
   {
      $this->productId = $productId;
   }

   /**
    * @return string
    */
   public function getName(): ?string
   {
      return $this->name;
   }

   /**
    * unit price without tax
    * @return float
    */
   public function getPrice(): float
   {
      return $this->price;
   }

   /**
    * @return float
    */
   public function getQuantity(): float
   {
      return $this->quantity;
   }

   public function incrementQuantity()
   {
      $this->quantity++;
   }

   public function decrementQuantity()
   {
      if($this->quantity > 1.0) {
         $this->quantity--;
      }
   }

   /**
    * @return int|null
    */
   public function getTaxRateId(): ?int
   {
      return $this->taxRateId;
   }

   /**
    * @param string|null $name$amountInSellUnit
    */
   public function setName(?string $name)
   {
      $this->name = $name;
   }

   /**
    * @param float $price
    */
   public function setPrice(float $price)
   {
      $this->price = $price;
   }

   /**
    * @param float $quantity
    */
   public function setQuantity(float $quantity)
   {
      $this->quantity = $quantity;
   }

   /**
    * @param int $taxRateId
    */
   public function setTaxRateId(int $taxRateId = null)
   {
      $this->taxRateId = $taxRateId;
   }

   /**
    * @return float|null
    */
   public function getAmountInSellUnit()
   {
      return $this->amountInSellUnit;
   }

   /**
    * @param float|null $amountInSellUnit
    */
   public function setAmountInSellUnit($amountInSellUnit)
   {
      $this->amountInSellUnit = $amountInSellUnit;
   }

   /**
    * @return float|null
    */
   public function getTaxRateValue(): ?float
   {
      return $this->taxRateValue;
   }

   /**
    * @param float|null $taxRateValue
    */
   public function setTaxRateValue(float $taxRateValue = null)
   {
      $this->taxRateValue = $taxRateValue;
   }

   /**
    * @return TaxRateType
    */
   public function getTaxRateType(): TaxRateType
   {
      return TaxRateType::get($this->taxRateType);
   }

   /**
    * @param TaxRateType $taxRateType
    */
   public function setTaxRateType(TaxRateType $taxRateType)
   {
      $this->taxRateType = $taxRateType->getValue();
   }

   /**
    * @return string|null
    */
   public function getNote(): ?string
   {
      return $this->note;
   }

   /**
    * @param string|null $note
    */
   public function setNote(?string $note)
   {
      $this->note = $note;
   }

   /**
    * Calculate partial sum for item
    * @param bool $taxIncluded
    * @param bool $round
    * @return float|int
    */
   public function getPartialSum($taxIncluded = true, $round = true)
   {
      if(is_null($this->getTaxRateValue())) {
         $taxIncluded = false;
      }

      if($taxIncluded) {
         $price = PriceHelper::calculatePriceWithVat($this->getTaxRateValue(), $this->getPrice(), $round);

         return $price * $this->getQuantity();
      } else {
         return $this->getPrice() * $this->getQuantity();
      }
   }
}