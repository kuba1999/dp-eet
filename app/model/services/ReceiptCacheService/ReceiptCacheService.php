<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\ReceiptCacheService;


use App\Model\Entities\Cash;
use App\Model\Entities\User;
use App\Model\Services\CashierCacheService\CashierCacheService;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\Storages\NewMemcachedStorage;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;

/**
 * Class ReceiptCacheService
 * @package App\Model\Services
 * @description Cache storage for receipts
 */
class ReceiptCacheService
{
   use SmartObject;

   const KEY_NAMESPACE = "receipts";
   const KEY_CACHED_RECEIPTS = "receipt";
   const KEY_RECEIPT_COUNTER = "counter";

   /** @var Cache */
   private $_cache;

   /** @var int */
   private $counter;

   /** @var ArrayHash|CachedReceipt[] */
   private $receipts;

   /** @var EntityManager */
   private $_em;

   /** @var CashierCacheService */
   private $cashierCacheService;

   /**
    * ReceiptCacheService constructor.
    * @param NewMemcachedStorage $storage
    * @param EntityManager $em
    * @param CashierCacheService $cashierCacheService
    */
   public function __construct(
      NewMemcachedStorage $storage,
      EntityManager $em,
      CashierCacheService $cashierCacheService
   ) {
      $this->_em = $em;
      $this->_cache = new Cache($storage, self::KEY_NAMESPACE);
      $this->cashierCacheService = $cashierCacheService;

      $this->receipts = $this->_cache->load(self::KEY_CACHED_RECEIPTS, function(){
         return new ArrayHash();
      });
      $this->counter = $this->_cache->load(self::KEY_RECEIPT_COUNTER, function(){
         return 0;
      });
   }

   /**
    * @param User $cashier
    * @param Cash $cash
    * @return CachedReceipt
    */
   public function createNewReceipt(User $cashier, Cash $cash) : CachedReceipt
   {
      $id = ++$this->counter;
      $receipt = new CachedReceipt($id, $cash, $cashier);

      //save receipt
      $this->receipts[$id] = $receipt;
      $this->save();

      $this->setActiveReceipt($receipt, $cashier);

      return $receipt;
   }

   /**
    * @param int $receiptId
    * @return CachedReceipt|null
    */
   public function getReceipt(int $receiptId) : ?CachedReceipt
   {
      if(isset($this->receipts[$receiptId])) {
         return $this->receipts[$receiptId];
      }

      return null;
   }

   /**
    * @param Cash $cash
    * @return array
    */
   public function getReceiptsByCash(Cash $cash) : array
   {
      $ret = [];

      foreach ($this->receipts AS $receipt) {
         if($receipt->getCashId() == $cash->getId()) {
            $ret[] = $receipt;
         }
      }

      return $ret;
   }

   /**
    * @param User $cashier
    * @return CachedReceipt|null
    */
   public function getActiveReceiptByCashier(User $cashier, bool $inActiveCash = true) : ?CachedReceipt
   {
      if($inActiveCash) { //get receipt only from active cash
         $cashId = $this->cashierCacheService->getCashierData()->getActiveCashId();
         /** @var Cash $cash */
         $cash = $this->_em->find(Cash::class, $cashId);

         $receipts = $this->getReceiptsByCash($cash);
      } else { //in all receipts
         $receipts = $this->receipts;
      }

      foreach ($receipts AS $cachedReceipt) {
         if($cachedReceipt->getBlockedByCashierId() == $cashier->getId()) {
            return $cachedReceipt;
         }
      }

      return null;
   }

   /**
    * @param CachedReceipt $cachedReceipt
    * @param User $cashier
    * @return CachedReceipt|mixed|null
    */
   public function setActiveReceipt(CachedReceipt $cachedReceipt, User $cashier)
   {
      foreach ($this->receipts AS $receipt) {
         if($receipt->getBlockedByCashierId() == $cashier->getId()) {
            $receipt->unblock();
         }
      }

      $cachedReceipt->blockByCashier($cashier);
      $this->save();
   }

   /**
    * @param int $receiptId
    */
   public function removeReceipt(int $receiptId)
   {
      unset($this->receipts[$receiptId]);
      $this->save();
   }

   public function save()
   {
      $this->_cache->save(self::KEY_CACHED_RECEIPTS, $this->receipts);
      $this->_cache->save(self::KEY_RECEIPT_COUNTER, $this->counter);
   }
}