<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\ReceiptCacheService;


use App\Model\Entities\Cash;
use App\Model\Entities\Product;
use App\Model\Entities\User;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TypeOfPayment;
use App\Model\Helpers\PriceHelper;
use Nette\Utils\ArrayHash;

/**
 * Class CachedReceipt
 * @package App\Model\Services\ReceiptCacheService
 */
class CachedReceipt
{
   /** @var int*/
   private $id;

   /** @var int */
   private $cashId;

   /**
    * @var int|null
    * ID of user (cashier)
    */
   private $createdByUserId;

   /**
    * @var int|null
    * ID of user (cashier)
    */
   private $blockedByCashierId;

   /** @var ArrayHash|CachedReceiptItem[] */
   private $items;

   /** @var int */
   private $itemsCounter = 1;

   /** @var string */
   private $name;

   /** @var CachedReceiptCustomerData */
   private $customerData;

   /** @var int|null */
   private $customerId;

   /** @var  \DateTime|null */
   protected $dueDate;

   /** @var  \DateTime|null */
   protected $dateOfTaxableSupply;

   /** @var  TypeOfPayment|null */
   protected $typeOfPayment;

   /**
    * CachedReceipt constructor.
    * @param int $id
    * @param Cash $cash
    * @param User $cashier
    */
   public function __construct(int $id, Cash $cash, User $cashier)
   {
      $this->id = $id;
      $this->cashId = $cash->getId();
      $this->createdByUserId = $cashier->getId();

      $this->items = new ArrayHash();
      $this->customerData = new CachedReceiptCustomerData();
   }

   /**
    * @return int
    */
   public function getId(): int
   {
      return $this->id;
   }

   /**
    * @param Product $product
    * @return CachedReceiptItem
    */
   public function addProduct(Product $product, bool $withTax) : CachedReceiptItem
   {
      if($item = $this->getItemByProduct($product)) { //item already exists, increment quantity
         $item->incrementQuantity();
      } else {
         $item = CachedReceiptItem::fromProduct($this, $product, $withTax);
         $this->items[$item->getId()] = $item;
      }

      return $item;
   }

   /**
    * @param CachedReceiptItem $cachedReceiptItem
    */
   public function addItem(CachedReceiptItem $cachedReceiptItem)
   {
      if(isset($this->items[$cachedReceiptItem->getId()])) { //item already exists, increment quantity
         /** @var CachedReceiptItem $item */
         $item = $this->items[$cachedReceiptItem->getId()];

         $item->incrementQuantity();
      } else {
         $this->items[$cachedReceiptItem->getId()] = $cachedReceiptItem;
      }
   }

   /********************************************************************************************************* helpers */

   /**
    * @return CachedReceiptItem[]|ArrayHash
    */
   public function getItems() : ArrayHash
   {
      return $this->items;
   }

   /**
    * @param int $itemId
    * @return CachedReceiptItem|null CachedReceiptItem
    */
   public function getItem(int $itemId) : ?CachedReceiptItem
   {
      if(!isset($this->items[$itemId])) {
         return null;
      }

      return $this->items[$itemId];
   }

   /**
    * @return int
    */
   public function countItems() : int
   {
      return count($this->items);
   }

   /**
    * Calculate items
    * @return float
    */
   public function numberOfItems() : float
   {
      $number = 0.0;

      /** @var CachedReceiptItem $item */
      foreach ($this->getItems() AS $item) {
         $number += $item->getQuantity();
      }

      return $number;
   }

   /**
    * @param int $itemId
    */
   public function incrementItemQuantity(int $itemId)
   {
      /** @var CachedReceiptItem $item */
      $item = $this->items[$itemId];

      $item->incrementQuantity();
   }

   /**
    * @param int $itemId
    */
   public function decrementItemQuantity(int $itemId)
   {
      /** @var CachedReceiptItem $item */
      $item = $this->items[$itemId];

      if($item->getQuantity() === 1.0) {
         unset($this->items[$itemId]);
         return;
      }

      $item->decrementQuantity();
   }

   /**
    * @param int $itemId
    * @return CachedReceiptItem|null
    */
   public function removeItem(int $itemId) : ?CachedReceiptItem
   {
      if(!isset($this->items[$itemId])) {
         return null;
      }

      $item = $this->items[$itemId];

      unset($this->items[$itemId]);

      return $item;
   }

   /**
    * @return int
    */
   public function generateId()
   {
      return $this->itemsCounter++;
   }

   /**
    * @param Product $product
    * @return CachedReceiptItem|null
    */
   public function getItemByProduct(Product $product) : ?CachedReceiptItem
   {
      foreach ($this->items AS $item) {
         if($item->getProductId() === $product->getId()) {
            return $item;
         }
      }

      return null;
   }

   /**
    * @return int|null
    */
   public function getBlockedByCashierId(): ?int
   {
      return $this->blockedByCashierId;
   }

   /**
    * @param User $cashier
    */
   public function blockByCashier(User $cashier)
   {
      $this->blockedByCashierId = $cashier->getId();
   }

   /**
    * Allow to block the receipt by another cashier
    */
   public function unblock()
   {
      $this->blockedByCashierId = null;
   }

   /**
    * @return int
    */
   public function getCashId(): int
   {
      return $this->cashId;
   }

   /**
    * @return string
    */
   public function getName(): ?string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return int|null
    */
   public function getCreatedByUserId()
   {
      return $this->createdByUserId;
   }

   /**
    * @return CachedReceiptCustomerData
    */
   public function getCustomerData(): CachedReceiptCustomerData
   {
      return $this->customerData;
   }

   /**
    * @return int
    */
   public function getCustomerId(): ?int
   {
      return $this->customerId;
   }

   /**
    * @param int $customerId
    */
   public function setCustomerId(int $customerId = null)
   {
      $this->customerId = $customerId;
   }

   public function clearCustomer()
   {
      $this->customerId = null;
      $this->customerData = new CachedReceiptCustomerData();
   }

   /**
    * @return \DateTime|null
    */
   public function getDueDate() : ?\DateTime
   {
      return $this->dueDate;
   }

   /**
    * @param \DateTime|null $dueDate
    */
   public function setDueDate(\DateTime $dueDate = null)
   {
      $this->dueDate = $dueDate;
   }

   /**
    * @return \DateTime|null
    */
   public function getDateOfTaxableSupply() : ?\DateTime
   {
      return $this->dateOfTaxableSupply;
   }

   /**
    * @param \DateTime|null $dateOfTaxableSupply
    */
   public function setDateOfTaxableSupply(\DateTime $dateOfTaxableSupply = null)
   {
      $this->dateOfTaxableSupply = $dateOfTaxableSupply;
   }

   /**
    * @return TypeOfPayment|null
    */
   public function getTypeOfPayment() : ?TypeOfPayment
   {
      return $this->typeOfPayment;
   }

   /**
    * @param TypeOfPayment|null $typeOfPayment
    */
   public function setTypeOfPayment(?TypeOfPayment $typeOfPayment)
   {
      $this->typeOfPayment = $typeOfPayment;
   }
   /////////////////////////////////////////////////////////////////////////////////////////////

   /**
    * @param TaxRateType $type
    * @return CachedReceiptItem[]
    */
   public function getItemsByTaxRateType(TaxRateType $type) : array
   {
      $ret = [];

      foreach ($this->getItems() As $item) {
         if($item->getTaxRateType() === $type) {
            $ret[] = $item;
         }
      }

      return $ret;
   }

   /**
    * Calculate items sum in given tax rate type
    * @param TaxRateType $type
    * @param bool $taxIncluded
    * @return float
    */
   public function getReceiptSumByTaxRateType(TaxRateType $type, $taxIncluded = true) : float
   {
      $sum = 0.0;

      foreach ($this->getItemsByTaxRateType($type) AS $item) {
         $sum += $item->getPartialSum($taxIncluded, false);
      }

      return $sum;
   }

   /**
    * Calculate items sum
    * @param bool $taxIncluded
    * @param bool $roundBase
    * @return float
    */
   public function getReceiptSum($taxIncluded = true, $roundBase = true) : float
   {
      $sum = 0.0;

      foreach ($this->getUsedTaxRateTypes() AS $type) {
         $sum += $this->getReceiptSumByTaxRateType($type, $taxIncluded);
      }

      if($roundBase) {
         $sum = round($sum);
      }

      return $sum;
   }

   /**
    * @return float
    */
   public function getTaxAmountByTaxRateType(TaxRateType $type) : float
   {
      $sumWithTax = $this->getReceiptSumByTaxRateType($type,true);
      $sumWithoutTax = PriceHelper::calculatePriceWithoutVat($this->getTaxRateValueByType($type), $sumWithTax, false);

      return $sumWithTax - $sumWithoutTax;
   }

   /**
    * @return float
    */
   public function getTaxAmount() : float
   {
      $amount = 0.0;

      foreach ($this->getUsedTaxRateTypes() AS $type) {
         $amount += $this->getTaxAmountByTaxRateType($type);
      }

      $differenceAfterRounding = round($this->getReceiptSum() - $this->getReceiptSum(true,false), 4);

      //////////// add difference after rounding to the max tax rate category
      if($this->countItems()) {
         $maxTaxRate = $this->getMaxTaxRateValue();
         $differenceTax = $differenceAfterRounding - PriceHelper::calculatePriceWithoutVat($maxTaxRate, $differenceAfterRounding, false);
         $differenceTax = round($differenceTax, 2);

         $amount += $differenceTax;
      }
      //////////// add difference after rounding to the max tax rate category

      return $amount;
   }

   /**
    * @return float
    */
   public function getBaseOfTaxDifference() : float
   {
      /*$sumWithTax = $this->getReceiptSum(true, true);
      $sumWithoutTax = PriceHelper::calculatePriceWithoutVat(21, $sumWithTax, false);

      return round($sumWithoutTax - $this->getReceiptSum(false, false), 2);*/
   }

   /**
    * Get tax rates which are used in receipt
    * @return array
    */
   public function getUsedTaxRateTypes() : array
   {
      $rates = [];

      foreach ($this->getItems() AS $item) {
         $rates [$item->getTaxRateType()->getValue()] = $item->getTaxRateType();
      }

      return array_values($rates);
   }

   /**
    * Get tax rate value
    * @param TaxRateType $type
    * @return float
    */
   public function getTaxRateValueByType(TaxRateType $type) : float
   {
      foreach ($this->getItems() AS $item) {
         if($item->getTaxRateType() === $type) {
            return $item->getTaxRateValue() ? $item->getTaxRateValue() : 0.0;
         }
      }
   }

   /**
    * Get max tax rate value
    * @return float
    */
   public function getMaxTaxRateValue() : float
   {
      $value = 0.0;

      foreach ($this->getItems() AS $item) {
         if($item->getTaxRateValue() > $value) {
            $value = $item->getTaxRateValue();
         }
      }

      return $value;
   }
   /////////////////////////////////////////////////////////////////////////////////////////////
}