<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services;


use App\Model\Entities\Receipt;
use App\Model\Entities\SellerData;
use App\Model\Entities\SettingsEet;
use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\ReceiptLogRecordType;
use App\Model\Enums\TaxRateType;
use App\Model\Exceptions\EETNotAllowed;
use App\Model\Exceptions\EETNotEnabled;
use App\Model\Helpers\ReceiptHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Repositories\SettingsRepository;
use App\Model\Storage\EetCertificateStorage;
use Kdyby\Doctrine\EntityManager;
use SlevomatEET\Client;
use SlevomatEET\Configuration;
use SlevomatEET\Cryptography\CryptographyService;
use SlevomatEET\Driver\GuzzleSoapClientDriver;
use SlevomatEET\EvidenceEnvironment;
use SlevomatEET\EvidenceRequest;

class EETService
{
   /** @var EntityManager */
   private $em;

   /** @var SettingsEet */
   private $settings;

   /** @var EetCertificateStorage */
   private $certificateStorage;

   /**
    * EETService constructor.
    * @param EntityManager $em
    */
   public function __construct(EntityManager $em, EetCertificateStorage $certificateStorage)
   {
      $this->em = $em;
      $this->certificateStorage = $certificateStorage;

      $this->loadSettings();
   }

   private function loadSettings()
   {
      /** @var SettingsRepository $settingsRepo */
      $settingsRepo = $this->em->getRepository(SettingsEet::class);
      $settings = $settingsRepo->getEetSettings();

      $this->settings = $settings;
   }


   /**
    * @param Receipt $receipt
    * @return bool
    * @throws EETNotAllowed
    * @throws EETNotEnabled
    */
   public function send(Receipt $receipt) : bool
   {
      if(!$this->settings->isEnabled()) {
         throw new EETNotEnabled();
      }

      if(!ReceiptHelper::isPossibleSendToEET($receipt)) {
         throw new EETNotAllowed("Sending to EET is not allowed for given receipt type");
      }

      $eetData = $receipt->getEetData();

      $crypto = new CryptographyService(
         $this->certificateStorage->getPrivateKeyPath(),
         $this->certificateStorage->getCertPath(),
         $this->settings->getPrivateKeyPassword()
      );
      $configuration = new Configuration(
         $receipt->getSellerData()->getVatId(),
         $eetData->getPremiseId(),
         $eetData->getCashRegisterId(),
         $this->settings->isPlaygroundEnvironment() ?
            EvidenceEnvironment::get(EvidenceEnvironment::PLAYGROUND) : EvidenceEnvironment::get(EvidenceEnvironment::PRODUCTION),
         $this->settings->isVerificationMode()
      );

      $client = new Client($crypto, $configuration, new GuzzleSoapClientDriver(new \GuzzleHttp\Client()));
      $EETreceipt = $this->buildEETReceipt($receipt);

      try {
         $response = $client->send($EETreceipt);
         $eetData->setFik($response->getFik());
         $eetData->setState(ReceiptEETState::get(ReceiptEETState::OK));

         //log message
         $receipt->addLogRecord(
            "Úspěšně zaevidováno do EET, pokus: " . $receipt->getEetData()->getNumberOfTry() .
            ", fik: ". $receipt->getEetData()->getFik(),
            ReceiptLogRecordType::SUCCESS
         );

         $this->em->flush();

         return true;
      } catch (\SlevomatEET\FailedRequestException $e) {
         $eetData->setState(ReceiptEETState::get(ReceiptEETState::WAITING_NEXT_TRY));

         //log message
         $receipt->addLogRecord(
            "Neúspěšný pokus č. " . $receipt->getEetData()->getNumberOfTry() . " o evidenci do EET. Pokus bude opakován.",
            ReceiptLogRecordType::WARNING
         );

         $receipt->getEetData()->nextTryOfSendingToEET();
         $this->em->flush();

         return false;
      } catch (\SlevomatEET\InvalidResponseReceivedException $e) {
         if($e->getResponse()->getRawData()->Chyba->kod == 0) { //verification mode
            $eetData->setState(ReceiptEETState::get(ReceiptEETState::OK));

            //log message
            $receipt->addLogRecord(
               "OVĚŘOVACÍ MÓD | Úspěšně zaevidováno do EET, pokus: " . $receipt->getEetData()->getNumberOfTry() .
               ", fik: -",
               ReceiptLogRecordType::SUCCESS
            );
            $this->em->flush();

            return true;
         } else {
            $eetData->setState(ReceiptEETState::get(ReceiptEETState::WAITING_NEXT_TRY));

            //log message
            $receipt->addLogRecord(
               "Neúspěšný pokus č. " . $receipt->getEetData()->getNumberOfTry() . " o evidenci do EET. Pokus bude opakován.",
               ReceiptLogRecordType::WARNING
            );

            $receipt->getEetData()->nextTryOfSendingToEET();
            $this->em->flush();

            return false;
         }
      }
   }

   /**
    * @param Receipt $receipt
    * @return string
    * @throws EETNotEnabled
    */
   public function generatePKPCode(Receipt $receipt) : string
   {
      if(!$this->settings->isEnabled()) {
         throw new EETNotEnabled();
      }

      $eetData = $receipt->getEetData();

      $crypto = new CryptographyService(
         $this->certificateStorage->getPrivateKeyPath(),
         $this->certificateStorage->getCertPath(),
         $this->settings->getPrivateKeyPassword()
      );
      $configuration = new Configuration(
         $receipt->getSellerData()->getVatId(),
         $eetData->getPremiseId(),
         $eetData->getCashRegisterId(),
         $this->settings->isPlaygroundEnvironment() ?
            EvidenceEnvironment::get(EvidenceEnvironment::PLAYGROUND) : EvidenceEnvironment::get(EvidenceEnvironment::PRODUCTION),
         $this->settings->isVerificationMode()
      );

      $EETreceipt = $this->buildEETReceipt($receipt);

      $request = new EvidenceRequest($EETreceipt, $configuration, $crypto);
      $pkpCode = base64_encode($request->getPkpCode());

      return $pkpCode;
   }

   /**
    * @param Receipt $receipt
    * @return string
    * @throws EETNotEnabled
    */
   public function generateBKPCode(Receipt $receipt) : string
   {
      if(!$this->settings->isEnabled()) {
         throw new EETNotEnabled();
      }

      $eetData = $receipt->getEetData();

      $crypto = new CryptographyService(
         $this->certificateStorage->getPrivateKeyPath(),
         $this->certificateStorage->getCertPath(),
         $this->settings->getPrivateKeyPassword()
      );
      $configuration = new Configuration(
         $receipt->getSellerData()->getVatId(),
         $eetData->getPremiseId(),
         $eetData->getCashRegisterId(),
         $this->settings->isPlaygroundEnvironment() ?
            EvidenceEnvironment::get(EvidenceEnvironment::PLAYGROUND) : EvidenceEnvironment::get(EvidenceEnvironment::PRODUCTION),
         $this->settings->isVerificationMode()
      );

      $EETreceipt = $this->buildEETReceipt($receipt);

      $request = new EvidenceRequest($EETreceipt, $configuration, $crypto);
      $bkpCode = $request->getBkpCode();

      return $bkpCode;
   }

   /**
    * @param Receipt $receipt
    * @return \SlevomatEET\Receipt
    */
   private function buildEETReceipt(Receipt $receipt) : \SlevomatEET\Receipt
   {
      if($receipt->getSellerData()->isTaxablePerson()) {
         $eetReceipt = $this->buildEETReceiptTaxablePerson($receipt);
      } else {
         $eetReceipt = $this->buildEETReceiptNonTaxablePerson($receipt);
      }

      return $eetReceipt;
   }

   /**
    * @param Receipt $receipt
    * @return \SlevomatEET\Receipt
    */
   private function buildEETReceiptNonTaxablePerson(Receipt $receipt) : \SlevomatEET\Receipt
   {
      $eetData = $receipt->getEetData();

      $eetReceipt = new \SlevomatEET\Receipt(
         $eetData->isFirstTry(),
         $receipt->getSellerData()->getVatId(),
         $receipt->getNiceId(),
         \DateTimeImmutable::createFromMutable($receipt->getCreationDate()),
         (int) ($receipt->calculateReceiptSum() * 100)
      );

      return $eetReceipt;
   }

   /**
    * @param Receipt $receipt
    * @return \SlevomatEET\Receipt
    */
   private function buildEETReceiptTaxablePerson(Receipt $receipt) : \SlevomatEET\Receipt
   {
      $eetData = $receipt->getEetData();

      $priceZeroVat = $receipt->calculateBaseOfTaxAmountByTaxRateType(TaxRateType::get(TaxRateType::ZERO));

      $priceStandardVat = $receipt->calculateReceiptSumByTaxRateType(TaxRateType::get(TaxRateType::BASE), true);
      $vatStandard = $receipt->calculateBaseOfTaxAmountByTaxRateType(TaxRateType::get(TaxRateType::BASE));

      $priceFirstReducedVat = $receipt->calculateReceiptSumByTaxRateType(TaxRateType::get(TaxRateType::REDUCED), true);
      $vatFirstReduced = $receipt->calculateBaseOfTaxAmountByTaxRateType(TaxRateType::get(TaxRateType::REDUCED));

      $priceSecondReducedVat = $receipt->calculateReceiptSumByTaxRateType(TaxRateType::get(TaxRateType::SECOND_REDUCED), true);
      $vatSecondReduced = $receipt->calculateBaseOfTaxAmountByTaxRateType(TaxRateType::get(TaxRateType::SECOND_REDUCED));

      $eetReceipt = new \SlevomatEET\Receipt(
         $eetData->isFirstTry(),
         $receipt->getSellerData()->getVatId(),
         $receipt->getNiceId(),
         \DateTimeImmutable::createFromMutable($receipt->getCreationDate()),
         (int) ($receipt->calculateReceiptSum() * 100),
         (int) ($priceZeroVat * 100),
         (int) ($priceStandardVat * 100),
         (int) ($vatStandard * 100),
         (int) ($priceFirstReducedVat * 100),
         (int) ($vatFirstReduced * 100),
         (int) ($priceSecondReducedVat * 100),
         (int) ($vatSecondReduced * 100)
      );

      return $eetReceipt;
   }

   /**
    * Test connection
    * @return bool|string
    */
   public function testConnection()
   {
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);
      $vatId = $sellerDataRepo->getSellerData()->getVatId();

      $crypto = new CryptographyService(
         $this->certificateStorage->getPrivateKeyPath(),
         $this->certificateStorage->getCertPath(),
         $this->settings->getPrivateKeyPassword()
      );
      $configuration = new Configuration(
         $vatId,
         $this->settings->getDefaultPremiseId(),
         $this->settings->getDefaultCashRegisterId(),
         $this->settings->isPlaygroundEnvironment() ?
            EvidenceEnvironment::get(EvidenceEnvironment::PLAYGROUND) : EvidenceEnvironment::get(EvidenceEnvironment::PRODUCTION),
         $this->settings->isVerificationMode()
      );

      $client = new Client($crypto, $configuration, new GuzzleSoapClientDriver(new \GuzzleHttp\Client()));
      $EETreceipt = new \SlevomatEET\Receipt(
         true,
         null,
         '0',
         new \DateTimeImmutable(),
         0
      );

      try {
         $response = $client->send($EETreceipt);
         if(!isset($response->getRawData()->Chyba->kod)) {
            return true;
         }
      } catch (\SlevomatEET\FailedRequestException $e) {
         return "Nepodařilo se navázat spojení s EET";
      } catch (\SlevomatEET\InvalidResponseReceivedException $e) {
         $response = $e->getResponse();
      }

      //http://www.etrzby.cz/assets/cs/prilohy/EET_popis_rozhrani_v3.1.1.pdf

      if($response->getRawData()->Chyba->kod === 0) {
         if(isset($response->getRawData()->Varovani)) {
            if($response->getRawData()->Varovani->kod_varov === 1) {
               return "DIC poplatnika se neshoduje s DIC v certifikatu";
            }
            elseif($response->getRawData()->Varovani->kod_varov === 2) {
               return "Chybny format DIC poverujiciho poplatnika";
            }
            elseif($response->getRawData()->Varovani->kod_varov === 3) {
               return "Chybna hodnota PKP";
            }
            elseif($response->getRawData()->Varovani->kod_varov === 4) {
               return "Datum a cas prijeti trzby je novejsi nez datum a cas prijeti zpravy";
            }
            elseif($response->getRawData()->Varovani->kod_varov === 5) {
               return "Datum a cas prijeti trzby je vyrazne v minulosti";
            }
         }
      }
      elseif($response->getRawData()->Chyba->kod === -1) {
         return "Docasna technicka chyba zpracovani – odeslete prosim datovou zpravu pozdeji";
      }
      elseif($response->getRawData()->Chyba->kod === 2) {
         return "Kodovani XML neni platne";
      }
      elseif($response->getRawData()->Chyba->kod === 3) {
         return "XML zprava nevyhovela kontrole XML schematu";
      }
      elseif($response->getRawData()->Chyba->kod === 4) {
         return "Neplatny podpis SOAP zpravy";
      }
      elseif($response->getRawData()->Chyba->kod === 5) {
         return "Neplatny kontrolni bezpecnostni kod poplatnika (BKP)";
      }
      elseif($response->getRawData()->Chyba->kod === 6) {
         return "DIC poplatnika ma chybnou strukturu";
      }
      elseif($response->getRawData()->Chyba->kod === 7) {
         return "Datova zprava je prilis velka";
      }
      elseif($response->getRawData()->Chyba->kod === 8) {
         return "Datova zprava nebyla zpracovana kvuli technicke chybe nebo chybe dat";
      }
      else {
         return "Nepodařilo se navázat spojení s EET";
      }

      return true;
   }
}