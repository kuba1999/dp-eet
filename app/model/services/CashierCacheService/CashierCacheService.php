<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\CashierCacheService;


use App\Model\Entities\Cash;
use App\Model\Entities\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\Storages\NewMemcachedStorage;
use Nette\SmartObject;

/**
 * Class CashierCacheService
 * @package App\Model\Services
 * @description Cache storage for cashier
 */
class CashierCacheService
{
   use SmartObject;

   const KEY_NAMESPACE        = "cashiers";
   const KEY_CASHIERS_DATA    = "cashiersData";

   /** @var Cache */
   private $_cache;

   /** @var EntityManager */
   private $_em;

   /** @var User */
   private $cashier;

   /** @var CachedCashierData */
   private $_cashierData;

   /** @var CachedCashSettings */
   private $_cashierCashSettings;

   public function __construct(
      NewMemcachedStorage $storage,
      EntityManager $em,
      \Nette\Security\User $user
   ) {
      if(!$user->isLoggedIn()) {
         return null;
      }

      $this->_em = $em;
      $this->_cache = new Cache($storage, self::KEY_NAMESPACE);
      $this->cashier = $this->_em->find(User::class, $user->getId());
   }

   /**
    * @return CachedCashierData|null
    */
   public function getCashierData() : ?CachedCashierData
   {
      if($this->_cashierData) {
         return $this->_cashierData;
      }

      /** @var CachedCashierData $cashierData */
      $cashierData = $this->_cache->load($this->cashier->getId(), function() {
         return new CachedCashierData($this->cashier, $this->_em);
      });

      $this->_cashierData = $cashierData;

      return $cashierData;
   }

   public function getCashierCashSettings() : ?CachedCashSettings
   {
      if($this->_cashierCashSettings) {
         return $this->_cashierCashSettings;
      }

      /** @var Cash $cash */
      $cash = $this->_em->find(Cash::class, $this->getCashierData()->getActiveCashId());

      /** @var CachedCashSettings $cashierSettings */
      $cashierSettings = $this->_cache->load( $this->cashier->getId() . "_" . $cash->getId(), function() use ($cash) {
         return new CachedCashSettings($this->cashier, $cash);
      });

      $this->_cashierCashSettings = $cashierSettings;

      return $cashierSettings;
   }

   public function saveCashierData()
   {
      $this->_cache->save($this->cashier->getId(), $this->_cashierData);
   }

   public function saveCashierCashSettings()
   {
      $this->_cache->save($this->cashier->getId() . "_" . $this->_cashierCashSettings->getCashId(), $this->_cashierCashSettings);
   }

   public function clearCashierData(User $cashier)
   {
      $this->_cache->remove($cashier->getId());
   }

   public function clearCashierCashSettings(User $cashier, Cash $cash)
   {
      $this->_cache->remove($cash->getId() . "_" . $this->cashier->getId());
   }
}