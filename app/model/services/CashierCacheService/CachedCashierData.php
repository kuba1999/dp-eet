<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\CashierCacheService;


use App\Model\Entities\Cash;
use App\Model\Entities\User;
use App\Model\Repositories\CashRepository;
use Doctrine\ORM\EntityManager;

class CachedCashierData
{
   /** @var int */
   private $cashierId;

   /** @var int|null */
   private $activeCashId = null;

   /**
    * CachedCashierData constructor.
    * @param User $cashier
    */
   public function __construct(User $cashier, EntityManager $em)
   {
      $this->cashierId = $cashier->getId();
      $this->activeCashId = $cashier->getId();

      /** @var CashRepository $cashrepo */
      $cashrepo = $em->getRepository(Cash::class);
      $this->activeCashId = $cashrepo->getDefaultCashId($cashier);
   }

   /**
    * @return int|null
    */
   public function getActiveCashId(): ?int
   {
      return $this->activeCashId;
   }

   /**
    * @param int $activeCashId
    */
   public function setActiveCashId(int $activeCashId)
   {
      $this->activeCashId = $activeCashId;
   }
}