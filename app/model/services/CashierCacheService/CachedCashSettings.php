<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\CashierCacheService;


use App\Model\Entities\Cash;
use App\Model\Entities\User;
use App\Model\Enums\CashView;

class CachedCashSettings
{
   /** @var int */
   private $cashierId;

   /** @var int */
   private $cashId;

   /** @var int|null */
   private $activeReceiptId = null;

   /** @var CashView */
   protected $selectedView = CashView::TABLE;

   /**
    * CachedCashierData constructor.
    * @param User $cashier
    * @param Cash $cash
    */
   public function __construct(User $cashier, Cash $cash)
   {
      $this->cashierId = $cashier->getId();
      $this->cashId = $cash->getId();
      $this->selectedView = $cash->getDefaultView();
   }

   /**
    * @return int|null
    */
   public function getActiveReceiptId(): ?int
   {
      return $this->activeReceiptId;
   }

   /**
    * @param int $activeReceiptId
    */
   public function setActiveReceiptId(int $activeReceiptId)
   {
      $this->activeReceiptId = $activeReceiptId;
   }

   /**
    * @return int
    */
   public function getCashierId(): int
   {
      return $this->cashierId;
   }

   /**
    * @return int
    */
   public function getCashId(): int
   {
      return $this->cashId;
   }

   /**
    * @return CashView
    */
   public function getSelectedView(): CashView
   {
      return $this->selectedView;
   }

   /**
    * @param CashView $selectedView
    */
   public function setSelectedView(CashView $selectedView)
   {
      $this->selectedView = $selectedView;
   }
}