<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services;


use App\Model\Exceptions\UnknownParameterException;

class ParametersService
{
   /**
    * @var array
    */
   private $parameters;

   /**
    * ParametersRepository constructor.
    */
   public function __construct(array $parameters)
   {
      $this->parameters = $parameters;
   }

   public function getParameter(string $name)
   {
      $ret = $this->parameters;

      foreach(explode(".", $name) AS $value)
      {
         if(!isset($ret[$value])) {
            throw new UnknownParameterException("Undefined parameter: " . $name);
         }

         $ret = $ret[$value];
      }


      return $ret;
   }
}