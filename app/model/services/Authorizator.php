<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services;


use App\Model\Enums\UserRole;
use Nette\Security\Permission;

class Authorizator extends Permission
{
   public function __construct()
   {
      //create roles
      $this->addRole(UserRole::ROLE_GUEST);
      $this->addRole(UserRole::ROLE_CUSTOMER, UserRole::ROLE_GUEST);
      $this->addRole(UserRole::ROLE_CASHIER, UserRole::ROLE_GUEST);
      $this->addRole(UserRole::ROLE_ADMIN, UserRole::ROLE_CASHIER);

      $this->admin();
      $this->cash();
      $this->portal();
   }

   private function admin()
   {
      //backend
      $this->addResource("Admin:Dashboard");
      $this->addResource("Admin:Profile");

      //receipts
      $this->addResource("Admin:Receipts");

      //tickets
      $this->addResource("Tickets:Dashboard");
      $this->addResource("Tickets:Profile");
      $this->addResource("Tickets:TicketList");
      $this->addResource("Tickets:CreateTicket");
      $this->addResource("Tickets:Ticket");

      $this->addResource("Admin:Customers");
      $this->addResource("Admin:Users");
      $this->addResource("Admin:News");
      $this->addResource("Admin:Faq");
      $this->addResource("Tickets:Faq");
      $this->addResource("Admin:Downloads");
      $this->addResource("Admin:PortalPageContent");

      //customer downloads
      $this->addResource("Tickets:Downloads");


      $this->addResource("Admin:Goods");
      $this->addResource("Admin:StockInProducts");
      $this->addResource("Admin:Supplier");
      $this->addResource("Admin:TaxRate");
      $this->addResource("Admin:MeasurementUnit");
      $this->addResource("Admin:StockMovementHistory");
      $this->addResource("Admin:Cash");
      $this->addResource("Admin:Settings");
      $this->addResource("Admin:Statistics");
      $this->addResource("Admin:Logs");
      $this->addResource("Admin:Logs:receipts");


      //admin or employee
      $this->allow(UserRole::ROLE_CASHIER, "Tickets:Profile");
      $this->allow(UserRole::ROLE_CASHIER, "Tickets:TicketList");
      $this->allow(UserRole::ROLE_CASHIER, "Tickets:CreateTicket");
      $this->allow(UserRole::ROLE_CASHIER, "Tickets:Ticket");

      $this->allow(UserRole::ROLE_CASHIER, "Admin:Dashboard");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:Profile");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:Receipts");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:Customers");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:Goods");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:StockInProducts");
      $this->allow(UserRole::ROLE_CASHIER, "Admin:StockMovementHistory");

      //admin
      $this->allow(UserRole::ROLE_ADMIN, "Admin:News");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Faq");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Downloads");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:PortalPageContent");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Users");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Supplier");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:TaxRate");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:MeasurementUnit");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Cash");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Settings");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Statistics");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Logs");
      $this->allow(UserRole::ROLE_ADMIN, "Admin:Logs:receipts");
   }

   private function portal()
   {
      //resources
      $this->addResource("Portal:Welcome");
      $this->addResource("Portal:Sign");
      $this->addResource("Portal:Registration");
      $this->addResource("Portal:News");
      $this->addResource("Portal:Dashboard");
      $this->addResource("Portal:ReceiptToken");
      $this->addResource("Portal:Receipts");
      $this->addResource("Portal:Settings");
      $this->addResource("Portal:Support");
      $this->addResource("Portal:Faq");
      $this->addResource("Portal:Downloads");
      $this->addResource("Portal:Contact");

      $this->allow(UserRole::ROLE_GUEST, "Portal:Welcome");
      $this->allow(UserRole::ROLE_GUEST, "Portal:News");
      $this->allow(UserRole::ROLE_GUEST, "Portal:Contact");
      $this->allow(UserRole::ROLE_GUEST, "Portal:Sign");
      $this->allow(UserRole::ROLE_GUEST, "Portal:ReceiptToken");
      $this->allow(UserRole::ROLE_GUEST, "Portal:Registration");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Dashboard");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Receipts");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Settings");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Support");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Faq");
      $this->allow(UserRole::ROLE_CUSTOMER, "Portal:Downloads");
   }

   private function cash()
   {
      //resources
      $this->addResource("Cash:MakeSell");
      $this->addResource("Cash:FinishSell");
      $this->addResource("Cash:ChangeReceipt");
      $this->addResource("Cash:Customers");
      $this->addResource("Cash:Receipts");

      $this->allow(UserRole::ROLE_CASHIER, "Cash:MakeSell");
      $this->allow(UserRole::ROLE_CASHIER, "Cash:FinishSell");
      $this->allow(UserRole::ROLE_CASHIER, "Cash:ChangeReceipt");
      $this->allow(UserRole::ROLE_CASHIER, "Cash:Customers");
      $this->allow(UserRole::ROLE_CASHIER, "Cash:Receipts");
   }
}