<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\AresSellerDataService;


class UnableToConnect extends \Exception
{
   protected $message = "Nepodařilo se spojit s portálem ARES";
}