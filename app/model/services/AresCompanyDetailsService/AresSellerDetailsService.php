<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\AresSellerDataService;


/**
 * Class AresSellerDataService
 * @package App\Model\Services\AresSellerDataService
 * @description Service provide information from API MFCR
 */
class AresSellerDataService
{
   const DATA_VAT_NO          =     "vatNo";
   const DATA_VAT_ID          =     "vatId";
   const DATA_COMPANY         =     "company";
   const DATA_STREET          =     "street";
   const DATA_STREET_NUMBER   =     "streetNumber";
   const DATA_CITY            =     "city";
   const DATA_ZIP             =     "zip";

   /**
    * @param string $vat Company VAT
    * @return array
    * @throws UnknownVatNoException
    */
   public static function getData(string $vatNo)
   {
      $client = new \GuzzleHttp\Client();

      try {
         $res = $client->request('GET', 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=' . $vatNo);
      }
      catch (\GuzzleHttp\Exception\ConnectException $e) {
         throw new UnableToConnect();
      }

      //load XML from response
      $xml = simplexml_load_string($res->getBody());

      $nameSpaces = $xml->getDocNamespaces();

      //get data
      $data = $xml->children($nameSpaces['are'])->children($nameSpaces['D']);

      //VAT is unknown if is isset node E
      if(isset($data->E)) {
         throw new UnknownVatNoException();
      }

      $vbas = $data->VBAS;

      //cislo orientacni
      if(!empty($vbas->AA->CO)) {
         $streetNumber =   $vbas->AA->CD . "/" . $vbas->AA->CO;
      } else {
         $streetNumber =   $vbas->AA->CD;
      }

      //street
      if(!empty($vbas->AA->NU)) {
         $street =   $vbas->AA->NU; //address has stree
      } else {
         $street =   $vbas->AA->N; //address without street => street == city
      }

      $ret = [
         self::DATA_VAT_NO          =>    (string) $vbas->ICO,
         self::DATA_VAT_ID          =>    (string) $vbas->DIC,
         self::DATA_COMPANY         =>    (string) $vbas->OF,
         self::DATA_STREET          =>    (string) $street,
         self::DATA_STREET_NUMBER   =>    (string) $streetNumber,
         self::DATA_CITY            =>    (string) $vbas->AA->N,
         self::DATA_ZIP             =>    (string) $vbas->AA->PSC,
      ];

      return $ret;
   }
}