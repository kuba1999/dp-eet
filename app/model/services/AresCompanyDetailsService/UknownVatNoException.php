<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\AresSellerDataService;


class UnknownVatNoException extends \Exception
{
   protected $message = "Neznámé DIČ";
}