<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services;


use App\Model\Entities\User;
use App\Model\Enums\UserAccountStatus;
use App\Model\Exceptions\BadCredentialsException;
use App\Model\Exceptions\InactiveAccountException;
use App\Model\Repositories\UserRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\Http\Request;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Utils\DateTime;

class Authenticator implements IAuthenticator
{
   /** @var EntityManager */
   private $em;

   /** @var Request */
   private $request;

   public function __construct(EntityManager $em, Request $request)
   {
      $this->em = $em;
      $this->request = $request;
   }

   /**
    * Performs an authentication against e.g. database.
    * and returns IIdentity on success or throws AuthenticationException
    * @param array $credentials
    * @return IIdentity
    * @throws BadCredentialsException
    * @throws InactiveAccountException
    */
   function authenticate(array $credentials)
   {
      list($username, $password) = $credentials;

      /** @var UserRepository $userRepository */
      $userRepository = $this->em->getRepository(User::class);

      /** @var User $user */
      $user = $userRepository->findOneBy([ 'username' => $username ]);

      //check if user exists after check password
      if(is_null($user) || !UserRepository::passwordVerify($password, $user)) {
         throw new BadCredentialsException();
      }

      if(!$user->getAccountStatus()->equalsValue(UserAccountStatus::ACTIVE)) {
         throw new InactiveAccountException();
      }

      //update last login date
      $user->setLastLoginDate(new DateTime());
      $user->setLastLoginFromIP($this->request->getRemoteAddress());
      $this->em->flush($user);

      return new Identity($user->getId(), [$user->getRole()->getValue()]);
   }
}