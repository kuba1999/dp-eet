<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services;


use App\Model\Entities\AprioriRule;
use App\Model\Entities\Product;
use App\Model\Entities\ReceiptItem;
use App\Model\Exceptions\UnknownParameterException;
use App\Model\Repositories\ProductRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use Doctrine\ORM\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\Storages\NewMemcachedStorage;
use Phpml\Association\Apriori;

class ProductSuggesterService
{
   const KEY_NAMESPACE        = "productSuggester";
   const KEY_LAST_RECEIPT     = "lastReceiptId";
   const KEY_RECEIPTS         = "receipts";

   /** @var Cache */
   private $_cache;

   /** @var EntityManager */
   private $em;

   /** @var float */
   private $minSupport;

   /** @var float */
   private $minConfidence;

   /**
    * ProductSuggesterService constructor.
    * @param NewMemcachedStorage $storage
    * @param EntityManager $em
    */
   public function __construct(NewMemcachedStorage $storage, EntityManager $em, ParametersService $parametersService)
   {
      $this->_cache = new Cache($storage, self::KEY_NAMESPACE);
      $this->em = $em;

      $this->loadConfiguration($parametersService);
   }

   private function loadConfiguration(ParametersService $parametersService)
   {
      try {
         $this->minSupport = $parametersService->getParameter("productSuggester.minSupport");
      } catch (UnknownParameterException $e) {
         $this->minSupport = 0.01;
      }
      try {
         $this->minConfidence = $parametersService->getParameter("productSuggester.minConfidence");
      } catch (UnknownParameterException $e) {
         $this->minConfidence = 0.0;
      }
   }

   /**
    * Generate association rules for given set of receipts
    * @param array $receiptItems
    */
   private function generateRules(array $receipts)
   {
      $apriori = new Apriori($this->calculateMinSupport($receipts), $this->minConfidence);

      //train for given set
      $apriori->train($receipts, []);

      //remove all rules from DB
      $this->removeAllSavedRules();

      foreach ($apriori->getRules() AS $rule) {
         $r = new AprioriRule(
            $rule[Apriori::ARRAY_KEY_ANTECEDENT],
            $rule[Apriori::ARRAY_KEY_CONSEQUENT]
         );
         $this->em->persist($r);
      }

      $this->em->flush();
   }

   /**
    * Generate association rules for given products
    * @param array $receiptItems
    */
   public function train(array $receiptItems)
   {
      $receipts = $this->groupByReceipt($receiptItems);

      //filter receipts (interested are only receipts with two or more items)
      $receipts = $this->filterReceipts($receipts);

      $this->generateRules($receipts);
   }

   /**
    * Generate association rules for given products
    * @param array $receiptItems
    */
   public function trainIncremental(array $receiptItems)
   {
      $receipts = $this->groupByReceipt($receiptItems);
      //save max ID of receipt => last processed receipt
      $this->setLastProcessedReceiptId(max(array_keys($receipts)));

      //filter receipts (interested are only receipts with two or more items)
      $receipts = $this->filterReceipts($receipts);

      //add cached
      $cachedReceipts = $this->_cache->load(self::KEY_RECEIPTS, function () {return [];});
      $receipts = array_merge($receipts, $cachedReceipts);

      $this->generateRules($receipts);
   }

   /**
    * @param CachedReceipt $cachedReceipt
    * @return array
    */
   public function suggest(CachedReceipt $cachedReceipt, $maxProducts = 5, ?string $searchedTerm = null) : array
   {
      $productIDs = [];
      foreach ($cachedReceipt->getItems() AS $item) {
         $productId = $item->getProductId();
         if($productId) {
            $productIDs[] = $productId;
         }
      }

      if($productIDs === []) {
         return [];
      }

      asort($productIDs);

      $suggestedIDs = $this->findProducts($productIDs);

      //nothing suggested
      if($suggestedIDs === []) {
         //try again only with last item
         $suggestedIDs = $this->findProducts([ $productIDs[count($productIDs)-1] ]);
      }

      //count frequency
      $countFrequency = array_count_values($suggestedIDs);

      //sort by frequency from high to lower
      arsort($countFrequency);

      //product IDs sorted by frequency
      $productIDs = array_keys($countFrequency);

      //unset products on receipt
      foreach ($cachedReceipt->getItems() AS $item) {
         if (($key = array_search($item->getProductId(), $productIDs)) !== false) {
            unset($productIDs[$key]);
         }
      }

      /** @var ProductRepository $productRepo */
      $productRepo = $this->em->getRepository(Product::class);

      $products = [];
      $counter = 0;
      foreach ($productIDs AS $id) {
         if($counter > $maxProducts) {
            break;
         }

         if($searchedTerm) {
            $p = $productRepo->searchInGivenProductsQB($searchedTerm, [$id])
               ->getQuery()
               ->getOneOrNullResult();
            if(!$p) {
               continue;
            }
            $products[] = $p;
         } else {
            $products[] = $this->em->find(Product::class, $id);
         }

         $counter++;
      }

      return $products;
   }

   private function findProducts(array $productIDs, ?int $maxProducts = null) : array
   {
      $qb = $this->em->createQueryBuilder()
         ->select("PARTIAL r.{id, consequent}")
         ->from(AprioriRule::class, "r")
         ->where("r.antecedent = :antecedent")
         ->setParameter("antecedent", implode(",", $productIDs));

      if($maxProducts) {
         $qb->setMaxResults($maxProducts);
      }

      $suggestedIDs = [];
      foreach ($qb->getQuery()->getArrayResult() AS $rules) {
         $suggestedIDs = array_merge($suggestedIDs, array_values($rules['consequent']));
      }

      return $suggestedIDs;
   }

   /**
    * @param int $id
    */
   private function setLastProcessedReceiptId(int $id)
   {
      $this->_cache->save(self::KEY_LAST_RECEIPT, $id);
   }

   /**
    * @return int
    */
   public function getLastProcessedReceiptId() : int
   {
      return $this->_cache->load(self::KEY_LAST_RECEIPT, function (){ return 0; });
   }

   public function storeReceiptsInCache(array $receipts)
   {
      $this->_cache->save(self::KEY_RECEIPTS, $receipts);
   }
   //////////////////////////////////////////////////////////////////////////////// helpers

   /**
    * @param array $receiptItems
    * @return array
    */
   private function groupByReceipt(array $receiptItems) : array
   {
      $receipts = [];

      /** @var ReceiptItem $item */
      foreach ($receiptItems AS $item) {
         if(!$item->getProduct()) {
            continue;
         }

         $receipts[$item->getReceipt()->getId()][] = $item->getProduct()->getId();
      }

      return $receipts;
   }

   /**
    * @param array $receipts
    * @return array
    */
   private function filterReceipts(array $receipts) : array
   {
      foreach ($receipts AS $key=>$receipt) {
         if(count($receipt) < 2) {
            unset($receipts[$key]);
         }
      }

      return $receipts;
   }

   /**
    * Calculate minimum support
    * @param array $receipts
    * @return float
    */
   private function calculateMinSupport(array $receipts) : float
   {
      $count = count($receipts);

      $a = 0.4;
      $b = 0.2;
      $c = $this->minSupport; //the lowest support

      $s = exp((-$a*$count) - $b) + $c;

      return $s;
   }
   /////////////////////////////////////////////////////////////////////
   /**
    * Clean all
    */
   public function clean()
   {
      $this->_cache->remove(self::KEY_LAST_RECEIPT);
      $this->_cache->remove(self::KEY_RECEIPTS);
      $this->removeAllSavedRules();
   }

   /**
    * Remove all rules from database
    */
   private function removeAllSavedRules()
   {
      //remove all
      $this->em->createQueryBuilder()->delete()->from(AprioriRule::class, "r")->getQuery()->execute();

      //update auto increment
      $tableName = $this->em->getClassMetadata(AprioriRule::class)->getTableName();
      $this->em->getConnection()->exec("ALTER TABLE " . $tableName . " AUTO_INCREMENT = 1;");
   }
}