<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\Mailing;



use App\Model\Entities\Receipt;
use App\Model\Enums\TypeOfDocument;
use App\Model\Services\ParametersService;
use App\Model\Services\PDFGeneratorService\PDFGeneratorServiceFactory;
use Kdyby\Doctrine\EntityManager;
use Latte\Engine;
use Nette\Application\LinkGenerator;

class ReceiptMailingService extends BaseMailingService
{
   /** @var  LinkGenerator */
   protected $linkGenerator;

   /** @var PDFGeneratorServiceFactory */
   private $PDFGeneratorServiceFactory;

   public function __construct(
      LinkGenerator $linkGenerator,
      EntityManager $em,
      PDFGeneratorServiceFactory $PDFGeneratorServiceFactory,
      ParametersService $parametersService
   ) {
      parent::__construct($parametersService);

      $this->linkGenerator = $linkGenerator;
      $this->PDFGeneratorServiceFactory = $PDFGeneratorServiceFactory;
   }

   public function sendReceiptAsAttachment(Receipt $receipt, string $email)
   {
      $message = $this->getMessage();
      $message->addTo($email);

      $latte = new Engine();
      $content = $latte->renderToString(__DIR__ . "/templates/receiptAsAttachment.latte", [
         "receipt" => $receipt,
         "title"   => $this->getTitle($receipt)
      ]);

      $generator = $this->PDFGeneratorServiceFactory->receipt($receipt);

      $message->addAttachment($generator->getFile());

      $message->setHtmlBody(
         $content,
         __DIR__ . "/templates/assets"
      );

      $this->send($message);
   }

   private function getTitle(Receipt $receipt) : string
   {
      return TypeOfDocument::getName($receipt->getTypeOfDocument()) . " č. " . $receipt->getNiceId();
   }
}