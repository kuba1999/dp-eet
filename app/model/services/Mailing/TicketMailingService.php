<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\Mailing;



use App\Model\Entities\Ticket;
use App\Model\Entities\TicketMessage;
use App\Model\Entities\UserTicketTypeNotification;
use App\Model\Repositories\TicketRepository;
use App\Model\Repositories\UserTicketTypeNotificationRepository;
use App\Model\Services\ParametersService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Latte\Engine;
use Nette\Application\LinkGenerator;

class TicketMailingService extends BaseMailingService
{
   /** @var  LinkGenerator */
   protected $linkGenerator;

   /** @var UserTicketTypeNotificationRepository */
   protected $notificationRepository;

   /** @var TicketRepository */
   protected $ticketRepository;

   public function __construct(
      LinkGenerator $linkGenerator,
      EntityManager $em,
      ParametersService $parametersService
   ) {
      parent::__construct($parametersService);

      $this->linkGenerator = $linkGenerator;
      $this->notificationRepository = $em->getRepository(UserTicketTypeNotification::class);
      $this->ticketRepository = $em->getRepository(Ticket::class);
   }

   public function createdNewTicket(TicketMessage $ticketMessage)
   {
      /** @var \App\Model\Entities\User $user */
      foreach ($this->notificationRepository->getUsersByNotificationType($ticketMessage->getType()) AS $user) {

         //email is not required
         if(!$user->hasEmail()) {
            continue; //do not send email
         }

         $message = $this->getMessage();
         $message->addTo($user->getEmail());

         $latte = new Engine();
         $content = $latte->renderToString(__DIR__ . "/templates/createdNewTicket.latte", [
            "user"         =>    $user,
            "message"      =>    $ticketMessage,
            "ticketLink"   =>    $this->linkGenerator->link("Tickets:Ticket:default", [
               "id" => $ticketMessage->getTicket()->getId()
            ])
         ]);

         $message->setHtmlBody(
            $content,
            __DIR__ . "/templates/assets"
         );

         $this->send($message);
      }

   }

   public function createdNewMessage(TicketMessage $ticketMessage)
   {
      $participants = $this->ticketRepository->getTicketParticipants($ticketMessage->getTicket());
      $subcribedNotifications = $this->notificationRepository->getUsersByNotificationType($ticketMessage->getType());

      $users = array_unique(array_merge($participants, $subcribedNotifications));

      /** @var \App\Model\Entities\User $user */
      foreach ($users AS $user) {

         //do not send message to author of message
         if($user === $ticketMessage->getAuthor()) {
            continue;
         }

         //email is not required
         if(!$user->hasEmail()) {
            continue; //do not send email
         }

         $message = $this->getMessage();
         $message->addTo($user->getEmail());

         $latte = new Engine();
         $content = $latte->renderToString(__DIR__ . "/templates/createdNewMessage.latte", [
            "user"         =>    $user,
            "message"      =>    $ticketMessage,
            "ticketLink"   =>    $this->linkGenerator->link("Tickets:Ticket:default", [
            "id" => $ticketMessage->getTicket()->getId()
            ])
         ]);

         $message->setHtmlBody(
            $content,
            __DIR__ . "/templates/assets"
         );

         $this->send($message);
      }
   }

   public function ticketClosed(TicketMessage $ticketMessage)
   {
      $participants = $this->ticketRepository->getTicketParticipants($ticketMessage->getTicket());

      /** @var \App\Model\Entities\User $user */
      foreach ($participants AS $user) {

         //do not send message to author of message
         if($user === $ticketMessage->getAuthor()) {
            continue;
         }

         //email is not required
         if(!$user->hasEmail()) {
            continue; //do not send email
         }

         $message = $this->getMessage();
         $message->addTo($user->getEmail());

         $latte = new Engine();
         $content = $latte->renderToString(__DIR__ . "/templates/closedTicket.latte", [
            "user"         =>    $user,
            "message"      =>    $ticketMessage,
            "ticketLink"   =>    $this->linkGenerator->link("Tickets:Ticket:default", [
            "id" => $ticketMessage->getTicket()->getId()
            ])
         ]);

         $message->setHtmlBody(
            $content,
            __DIR__ . "/templates/assets"
         );

         $this->send($message);
      }
   }

   public function ticketClosedAfterPeriod(TicketMessage $ticketMessage)
   {
      $participants = $this->ticketRepository->getTicketParticipants($ticketMessage->getTicket());

      /** @var \App\Model\Entities\User $user */
      foreach ($participants AS $user) {

         //email is not required
         if(!$user->hasEmail()) {
            continue; //do not send email
         }

         $message = $this->getMessage();
         $message->addTo($user->getEmail());

         $latte = new Engine();
         $content = $latte->renderToString(__DIR__ . "/templates/closedTicketAfterPeriod.latte", [
            "user"         =>    $user,
            "message"      =>    $ticketMessage,
            "ticketLink"   =>    $this->linkGenerator->link("Tickets:Ticket:default", [
               "id" => $ticketMessage->getTicket()->getId()
            ]),
            "language"     =>    $user->getLanguage()
         ]);

         $message->setHtmlBody(
            $content,
            __DIR__ . "/templates/assets"
         );

         $this->send($message);
      }
   }
}