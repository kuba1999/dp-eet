<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\Mailing;



use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Services\ParametersService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Latte\Engine;
use Nette\Application\LinkGenerator;

class UserAccountMailingService extends BaseMailingService
{
   /** @var  LinkGenerator */
   protected $linkGenerator;

   /** @var UserRepository */
   private $userRepository;

   public function __construct(
      LinkGenerator $linkGenerator,
      EntityManager $em,
      ParametersService $parametersService
   ) {
      parent::__construct($parametersService);

      $this->linkGenerator = $linkGenerator;
      $this->userRepository = $em->getRepository(User::class);
   }

   public function sendActivationEmail(User $user)
   {
      //generate hash
      $this->userRepository->generateHashForAccountActivation($user);

      $message = $this->getMessage();
      $message->addTo($user->getEmail());

      $latte = new Engine();
      $content = $latte->renderToString(__DIR__ . "/templates/registeredUserActivation.latte", [
         "user"         =>    $user,
         "linkToActivation"   =>    $this->linkGenerator->link("Portal:Sign:accountActivation", [
            'userID'   =>    $user->getId(),
            'hash'   =>    $user->getHash(),
         ])
      ]);

      $message->setHtmlBody(
         $content,
         __DIR__ . "/templates/assets"
      );

      $this->send($message);
   }

   public function sendNewPasswordAfterResetting(User $user, string $newPassword)
   {
      $message = $this->getMessage();
      $message->addTo($user->getEmail());

      $latte = new Engine();
      $content = $latte->renderToString(__DIR__ . "/templates/newPasswordAfterResetting.latte", [
         "user"         =>    $user,
         "newPassword"  =>    $newPassword,
         "language"     =>    $user->getLanguage(),
         "linkToTicketSystem"   =>    $this->linkGenerator->link("Admin:Tickets:Sign:in")
      ]);

      $message->setHtmlBody(
         $content,
         __DIR__ . "/templates/assets"
      );

      $this->send($message);
   }
}