<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Services\Mailing;


use App\Model\Services\ParametersService;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;

class BaseMailingService
{
   /** @var string */
   private $emailFrom;

   public function __construct(
      ParametersService $parametersService
   ) {
      $this->emailFrom = $parametersService->getParameter("emailFrom");
   }

   protected function getMessage() : Message
   {
      $message = new Message();
      //$message->setFrom('Fér pokladna <'.$this->emailFrom.'>');
      $message->setFrom('Fér pokladna <jakub.svestka@seznam.cz>');

      return $message;
   }

   protected function send(Message $message)
   {
      //$mailer = new SendmailMailer();
      $mailer = new SmtpMailer([
         'host' => 'smtp.seznam.cz',
         'username' => 'jakub.svestka@seznam.cz',
         'password' => 'flatron',
         'secure' => 'ssl',
      ]);
      $mailer->send($message);
   }
}