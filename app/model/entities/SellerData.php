<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\SellerDataRepository")
 */
class SellerData
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=64, nullable = true)
    * @var string|null
    */
   protected $street;

   /**
    * @ORM\Column(type="string", length=64, nullable = true)
    * @var string|null
    */
   protected $city;

   /**
    * @ORM\Column(type="string", length=32, nullable = true)
    * @var string|null
    */
   protected $zip;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $company;

   /**
    * @ORM\Column(type="string", length=32, nullable = true)
    * @var string|null
    * IČ
    */
   protected $vatNo;

   /**
    * @ORM\Column(type="string", length=32, nullable = true)
    * @var string|null
    * DIČ
    */
   protected $vatId;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $phone;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $email;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $web;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $bankAccountNumber;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $taxablePerson = false;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $documentNote;

   /**
    * @ORM\Column(type="datetime")
    * @var \DateTime
    */
   protected $validFrom;

   public function __construct()
   {
      $this->validFrom = new \DateTime();
   }

   /**
    * @return bool
    */
   public function isTaxablePerson(): bool
   {
      return $this->taxablePerson;
   }

   /**
    * @param bool $taxablePerson
    */
   public function setTaxablePerson(bool $taxablePerson)
   {
      $this->taxablePerson = $taxablePerson;
   }

   /**
    * @return ?string
    */
   public function getStreet(): ?string
   {
      return $this->street;
   }

   /**
    * @param ?string $street
    */
   public function setStreet(?string $street)
   {
      $this->street = $street;
   }

   /**
    * @return ?string
    */
   public function getCity(): ?string
   {
      return $this->city;
   }

   /**
    * @param ?string $city
    */
   public function setCity(?string $city)
   {
      $this->city = $city;
   }

   /**
    * @return ?string
    */
   public function getZip(): ?string
   {
      return $this->zip;
   }

   /**
    * @param ?string $zip
    */
   public function setZip(?string $zip)
   {
      $this->zip = $zip;
   }

   /**
    * @return ?string
    */
   public function getCompany(): ?string
   {
      return $this->company;
   }

   /**
    * @param ?string $company
    */
   public function setCompany(?string $company)
   {
      $this->company = $company;
   }

   /**
    * @return ?string
    */
   public function getVatNo(): ?string
   {
      return $this->vatNo;
   }

   /**
    * @param ?string $vatNo
    */
   public function setVatNo(?string $vatNo)
   {
      $this->vatNo = $vatNo;
   }

   /**
    * @return ?string
    */
   public function getVatId(): ?string
   {
      return $this->vatId;
   }

   /**
    * @param ?string $vatId
    */
   public function setVatId(?string $vatId)
   {
      $this->vatId = $vatId;
   }

   /**
    * @return null|string
    */
   public function getPhone(): ?string
   {
      return $this->phone;
   }

   /**
    * @param null|string $phone
    */
   public function setPhone(?string $phone): void
   {
      $this->phone = $phone;
   }

   /**
    * @return null|string
    */
   public function getEmail(): ?string
   {
      return $this->email;
   }

   /**
    * @param null|string $email
    */
   public function setEmail(?string $email): void
   {
      $this->email = $email;
   }

   /**
    * @return null|string
    */
   public function getWeb(): ?string
   {
      return $this->web;
   }

   /**
    * @param null|string $web
    */
   public function setWeb(?string $web): void
   {
      $this->web = $web;
   }

   /**
    * @return null|string
    */
   public function getBankAccountNumber(): ?string
   {
      return $this->bankAccountNumber;
   }

   /**
    * @param null|string $bankAccountNumber
    */
   public function setBankAccountNumber(?string $bankAccountNumber): void
   {
      $this->bankAccountNumber = $bankAccountNumber;
   }

   /**
    * @return null|string
    */
   public function getDocumentNote()
   {
      return $this->documentNote;
   }

   /**
    * @param null|string $documentNote
    */
   public function setDocumentNote($documentNote)
   {
      $this->documentNote = $documentNote;
   }
}