<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\TicketMessageType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\TicketMessageRepository")
 * @ORM\EntityListeners({"App\Model\Events\TicketMessageListener"})
 * @ORM\EntityListeners({"App\Model\Events\TicketMessageNotificationListener"})
 */
class TicketMessage
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\ManyToOne(targetEntity="Ticket", inversedBy="messages")
    * @ORM\JoinColumn(name="ticket_id", referencedColumnName="id", nullable=false)
    * @var Ticket
    */
   protected $ticket;

   /**
    * @ORM\Column(type="text", nullable=true)
    * @var string
    */
   protected $content;

   /**
    * @ORM\Column(type="integer", nullable = false)
    * @var int
    */
   protected $type;

   /**
    * @ORM\Column(type="datetime")
    * @var \DateTime
    */
   protected $created;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
    * @var User
    */
   protected $author;

   /**
    * @ORM\OneToMany(targetEntity="TicketAttachment", mappedBy="message", cascade={"persist"})
    * @var TicketAttachment[]|ArrayCollection
    */
   protected $attachments;

   /**
    * TicketMessage constructor.
    * @param string $content
    * @param User $author
    */
   public function __construct(string $content = null, User $author = null)
   {
      $this->content = $content;
      $this->author = $author;
      $this->created = new DateTime();
      $this->attachments = new ArrayCollection();
      $this->type = TicketMessageType::MESSAGE;
   }

   /**
    * @return Ticket
    */
   public function getTicket(): Ticket
   {
      return $this->ticket;
   }

   /**
    * @param Ticket $ticket
    */
   public function setTicket(Ticket $ticket)
   {
      $this->ticket = $ticket;
   }

   /**
    * @return string
    */
   public function getContent(): ?string
   {
      return $this->content;
   }

   /**
    * @param string $content
    */
   public function setContent(string $content)
   {
      $this->content = $content;
   }

   /**
    * @return \DateTime
    */
   public function getCreated(): \DateTime
   {
      return $this->created;
   }

   /**
    * @param \DateTime $created
    */
   public function setCreated(\DateTime $created)
   {
      $this->created = $created;
   }

   /**
    * @return User
    */
   public function getAuthor(): ?User
   {
      return $this->author;
   }

   /**
    * @param User $author
    */
   public function setAuthor(User $author = null)
   {
      $this->author = $author;
   }

   /**
    * @return int
    */
   public function getType(): int
   {
      return $this->type;
   }

   /**
    * @param int $type
    */
   public function setType(int $type)
   {
      $this->type = $type;
   }

   /**
    * @return TicketAttachment[]|ArrayCollection
    */
   public function getAttachments()
   {
      return $this->attachments;
   }

   /**
    * @param string $name
    * @return TicketAttachment
    */
   public function addAttachment(string $name) : TicketAttachment
   {
      $attachment = new TicketAttachment($this, $name);
      $this->attachments->add($attachment);

      return $attachment;
   }
}