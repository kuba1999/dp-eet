<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\NewsRepository")
 */
class News
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=255)
    * @var string
    */
   protected $title;

   /**
    * @ORM\Column(type="date")
    * @var \DateTime
    */
   protected $date;

   /**
    * @ORM\Column(type="text")
    * @var string
    */
   protected $content;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $published = true;

   public function __construct(string $title, string $content, \DateTime $date)
   {
      $this->title = $title;
      $this->content = $content;
      $this->date = $date;
   }

   /**
    * @return string
    */
   public function getTitle(): string
   {
      return $this->title;
   }

   /**
    * @param string $title
    */
   public function setTitle(string $title)
   {
      $this->title = $title;
   }

   /**
    * @return \DateTime
    */
   public function getDate(): \DateTime
   {
      return $this->date;
   }

   /**
    * @param \DateTime $date
    */
   public function setDate(\DateTime $date)
   {
      $this->date = $date;
   }

   /**
    * @return string
    */
   public function getContent(): string
   {
      return $this->content;
   }

   /**
    * @param string $content
    */
   public function setContent(string $content)
   {
      $this->content = $content;
   }

   /**
    * @return bool
    */
   public function isPublished(): bool
   {
      return $this->published;
   }

   /**
    * @param bool $published
    */
   public function setPublished(bool $published)
   {
      $this->published = $published;
   }
}