<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\ReceiptLogRecordType;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TypeOfDocument;
use App\Model\Enums\TypeOfPayment;
use App\Model\Helpers\PriceHelper;
use App\Model\Helpers\ReceiptTokenHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(uniqueConstraints={
 *   @ORM\UniqueConstraint(name="documentIdAndTypeUnique",
 *     columns={"document_id", "type_of_document"}
 *   )
 * })
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\ReceiptRepository")
 * @ORM\EntityListeners({"App\Model\Events\NewReceiptListener"})
 */
class Receipt
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="integer", nullable=false)
    * @var int
    */
   protected $documentId;

   /**
    * @ORM\Column(type="integer", length=1,  nullable=false)
    * @var TypeOfDocument
    */
   protected $typeOfDocument;

   /**
    * @ORM\Column(type="datetime", nullable=false)
    * @var \DateTime
    */
   protected $creationDate;

   /**
    * @ORM\Column(type="datetime", nullable=true)
    * @var \DateTime|null
    */
   protected $dueDate;

   /**
    * @ORM\Column(type="datetime", nullable=true)
    * @var \DateTime|null
    */
   protected $dateOfTaxableSupply;

   /**
    * @ORM\Column(type="integer", length=1, nullable=true)
    * @var TypeOfPayment|null
    */
   protected $typeOfPayment;

   /**
    * @ORM\OneToMany(targetEntity="ReceiptItem", mappedBy="receipt",cascade={"persist"})
    * @var ReceiptItem[]
    */
   protected $items;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
    * @var User|null
    */
   protected $customer;

   /**
    * @ORM\ManyToOne(targetEntity="SellerData")
    * @ORM\JoinColumn(name="seller_data_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
    * @var SellerData
    */
   protected $sellerData;

   /**
    * @ORM\ManyToOne(targetEntity="Cash")
    * @ORM\JoinColumn(name="cash_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
    * @var Cash
    */
   protected $cash;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="cashier_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
    * @var User
    */
   protected $cashier;

   /**
    * @ORM\OneToOne(targetEntity="ReceiptCustomerData", mappedBy="receipt", cascade={"persist"})
    * @ORM\JoinColumn(name="receipt_customer_data_id", referencedColumnName="id", nullable=true)
    * @var ReceiptCustomerData|null
    */
   protected $customerData;

   /**
    * @ORM\OneToOne(targetEntity="ReceiptEetData", mappedBy="receipt", cascade={"persist"})
    * @ORM\JoinColumn(name="receipt_eet_data_id", referencedColumnName="id", nullable=true)
    * @var ReceiptEetData|null
    */
   protected $eetData;

   /**
    * @ORM\OneToMany(targetEntity="ReceiptLogRecord", mappedBy="receipt", cascade={"persist"})
    * @var ReceiptLogRecord[]|ArrayCollection
    */
   protected $logRecords;

   /**
    * Receipt constructor.
    */
   public function __construct(
      TypeOfDocument $typeOfDocument,
      Cash $cash,
      User $cashier,
      SellerData $sellerData
   ) {
      $this->items = new ArrayCollection();
      $this->creationDate = new \DateTime();
      $this->typeOfDocument = $typeOfDocument->getValue();
      $this->cash = $cash;
      $this->cashier = $cashier;
      $this->customerData = new ReceiptCustomerData($this);
      $this->sellerData = $sellerData;
      $this->logRecords = new ArrayCollection();
   }

   /**
    * @param string $name
    * @param float $unitPrice
    * @param float $quantity
    * @return ReceiptItem
    */
   public function addItem(
      string $name,
      float $unitPrice,
      float $quantity
   ) : ReceiptItem
   {
      $item = new ReceiptItem($this, $name, $unitPrice, $quantity);

      $this->items->add($item);

      return $item;
   }

   /**
    * @return ReceiptItem[]|ArrayCollection
    */
   public function getItems()
   {
      return $this->items;
   }

   /**
    * Get nice number of document
    * @return null|string
    */
   public function getNiceId() : ?string
   {
      return sprintf("%d/%d/%02d/%d",
         $this->getTypeOfDocument()->getValue(),
         $this->getCreationDate()->format("Y"),
         $this->getCreationDate()->format("m"),
         $this->getDocumentId()
      );
   }

   /**
    * @return string
    */
   public function getToken() : string
   {
      return ReceiptTokenHelper::encode($this);
   }


   /**
    * @return TypeOfDocument
    */
   public function getTypeOfDocument(): TypeOfDocument
   {
      return TypeOfDocument::get($this->typeOfDocument);
   }

   /**
    * @param TypeOfDocument $typeOfDocument
    */
   public function setTypeOfDocument(TypeOfDocument $typeOfDocument)
   {
      $this->typeOfDocument = $typeOfDocument->getValue();
   }

   /**
    * @return \DateTime
    */
   public function getCreationDate(): \DateTime
   {
      return $this->creationDate;
   }

   /**
    * @return \DateTime|null
    */
   public function getDueDate(): ? \DateTime
   {
      return $this->dueDate;
   }

   /**
    * @param \DateTime|null $dueDate
    */
   public function setDueDate(\DateTime $dueDate)
   {
      $this->dueDate = $dueDate;
   }

   /**
    * @return \DateTime|null
    */
   public function getDateOfTaxableSupply() : ? \DateTime
   {
      return $this->dateOfTaxableSupply;
   }

   /**
    * @param \DateTime|null $dateOfTaxableSupply
    */
   public function setDateOfTaxableSupply(\DateTime $dateOfTaxableSupply)
   {
      $this->dateOfTaxableSupply = $dateOfTaxableSupply;
   }

   /**
    * @return User|null
    */
   public function getCustomer() : ?User
   {
      return $this->customer;
   }

   /**
    * @param User|null $customer
    */
   public function setCustomer(User $customer = null)
   {
      $this->customer = $customer;
   }

   /**
    * @return SellerData
    */
   public function getSellerData(): SellerData
   {
      return $this->sellerData;
   }

   /**
    * @param SellerData $sellerData
    */
   public function setSellerData(SellerData $sellerData)
   {
      $this->sellerData = $sellerData;
   }

   /**
    * @return int
    */
   public function getDocumentId() : int
   {
      return $this->documentId;
   }

   /**
    * @param int $documentId
    */
   public function setDocumentId(int $documentId)
   {
      $this->documentId = $documentId;
   }

   /**
    * @return Cash
    */
   public function getCash(): Cash
   {
      return $this->cash;
   }

   /**
    * @return User
    */
   public function getCashier(): User
   {
      return $this->cashier;
   }

   /**
    * @return ReceiptCustomerData|null
    */
   public function getCustomerData(): ?ReceiptCustomerData
   {
      return $this->customerData;
   }

   /**
    * @return ReceiptEetData|null
    */
   public function getEetData(): ?ReceiptEetData
   {
      return $this->eetData;
   }

   /**
    * @return ReceiptEetData
    * @throws \Exception
    */
   public function createEetData(): ReceiptEetData
   {
      if($this->eetData) {
         throw new \Exception("EET data already exists");
      }

      $this->eetData = new ReceiptEetData($this);

      return $this->eetData;
   }

   /**
    * @return TypeOfPayment|null
    */
   public function getTypeOfPayment() : ?TypeOfPayment
   {
      if(is_null($this->typeOfPayment)) {
         return null;
      }

      return TypeOfPayment::get($this->typeOfPayment);
   }

   /**
    * @param TypeOfPayment|null $typeOfPayment
    */
   public function setTypeOfPayment(?TypeOfPayment $typeOfPayment)
   {
      if($typeOfPayment) {
         $typeOfPayment = $typeOfPayment->getValue();
      }

      $this->typeOfPayment = $typeOfPayment;
   }


   public function addLogRecord(
      string $message,
      $type = null
   ) : ReceiptLogRecord
   {
      if(!$type instanceof ReceiptLogRecordType && !in_array($type, ReceiptLogRecordType::getAvailableValues())) {
         throw new \Exception("Undefined type");
      }

      if(!$type instanceof ReceiptLogRecordType) {
         $type = ReceiptLogRecordType::get($type);
      }

      $record = new ReceiptLogRecord($this, $message, $type);

      $this->logRecords->add($record);

      return $record;
   }

   ////////////////////////////////////////////////////////////////////  helpers

   /**
    * @return int
    */
   public function countItems() : int
   {
      return count($this->items);
   }

   /**
    * @return float
    */
   public function calculateOverallQuantitySum() : float
   {
      $sum = 0.0;

      foreach ($this->items AS $item) {
         $sum += $item->getQuantity();
      }

      return $sum;
   }

   /**
    * @param bool $withTax
    * @return float
    */
   public function calculateOverallPriceSum(bool $withTax) : float
   {
      $sum = 0.0;

      foreach ($this->items AS $item) {
         $sum += $item->calculatePartialSum($withTax);
      }

      return $sum;
   }

   /**
    * @param bool $withTax
    * @return float
    */
   public function calculateOverallUnitPriceSum(bool $withTax) : float
   {
      $sum = 0.0;

      foreach ($this->items AS $item) {
         $sum += $withTax ? $item->getUnitPriceWithTax() : $item->getUnitPriceWithoutTax();
      }

      return $sum;
   }

   /**
    * @return float
    */
   public function calculateOverallTaxSum() : float
   {
      $sum = 0.0;

      foreach ($this->items AS $item) {
         $sum += $item->calculatePartialTaxSum();
      }

      return $sum;
   }

   /**
    * @param TaxRateType $type
    * @return ReceiptItem[]
    */
   public function getItemsByTaxRateType(TaxRateType $type) : array
   {
      $ret = [];

      foreach ($this->getItems() As $item) {

         $itemType = $item->getTaxRateType();

         if($itemType === $type) {
            $ret[] = $item;
         }
      }

      return $ret;
   }


   /**
    * Calculate items sum in given tax rate type
    * @param TaxRateType $type
    * @param bool $taxIncluded
    * @return float
    */
   public function calculateReceiptSumByTaxRateType(TaxRateType $type, $taxIncluded = true) : float
   {
      $sum = 0.0;

      foreach ($this->getItemsByTaxRateType($type) AS $item) {
         $sum += $item->calculatePartialSum($taxIncluded);
      }

      return $sum;
   }

   /**
    * Calculate items sum
    * @param bool $taxIncluded
    * @param bool $roundBase
    * @return float
    */
   public function calculateReceiptSum($taxIncluded = true, $roundBase = true) : float
   {
      $sum = 0.0;

      foreach ($this->getUsedTaxRateTypes() AS $type) {
         $sum += $this->calculateReceiptSumByTaxRateType($type, $taxIncluded);
      }

      if($roundBase) {
         $sum = round($sum);
      }

      return $sum;
   }

   /**
    * @return float
    */
   public function calculateTaxAmountByTaxRateType(TaxRateType $type) : float
   {
      $sumWithTax = $this->calculateReceiptSumByTaxRateType($type,true);
      $sumWithoutTax = PriceHelper::calculatePriceWithoutVat($this->getTaxRateValueByType($type), $sumWithTax, false);

      $taxAmount = $sumWithTax - $sumWithoutTax;

      //////////// add difference after rounding to the max tax rate category
      if($this->getMaxTaxRate()->getType() === $type && $this->countItems()) {
         $differenceAfterRounding = round($this->calculateReceiptSum() - $this->calculateReceiptSum(true, false), 2);
         $maxTaxRate = $this->getMaxTaxRate()->getValue();
         $differenceTax = $differenceAfterRounding - PriceHelper::calculatePriceWithoutVat($maxTaxRate, $differenceAfterRounding, false);
         $differenceTax = round($differenceTax, 2);

         $taxAmount += $differenceTax;
      }
      //////////// add difference after rounding to the max tax rate category

      return round($taxAmount, 2);
   }



   /**
    * @return float
    */
   public function calculateBaseOfTaxAmountByTaxRateType(TaxRateType $type) : float
   {
      $sumWithoutTax = round($this->calculateReceiptSumByTaxRateType($type,false), 2);

      if($this->getMaxTaxRate()->getType() === $type) {
         $difference = $this->calculateTaxDifference($type);

         $taxRateValue = $this->getTaxRateValueByType($type);
         $differenceWithoutTax = round(PriceHelper::calculatePriceWithoutVat($taxRateValue, $difference, true), 2);

         $sumWithoutTax += $differenceWithoutTax;
      }

      return round($sumWithoutTax, 2);
   }

   /**
    * @return float
    */
   public function calculateTaxAmount() : float
   {
      $amount = 0.0;

      foreach ($this->getUsedTaxRateTypes() AS $type) {
         $amount += $this->calculateTaxAmountByTaxRateType($type);
      }

      return $amount;
   }

   /**
    * @return float
    */
   public function calculateTaxDifference(TaxRateType $type) : float
   {
      if($this->getMaxTaxRate()->getType() !== $type) {
         return 0.0;
      }

      $sumWithTaxNotRounded = $this->calculateReceiptSum(true, false);
      $sumWithTaxRounded = $this->calculateReceiptSum(true, true);

      $differenceWithTax = round($sumWithTaxRounded - $sumWithTaxNotRounded, 2);

      return $differenceWithTax;
   }

   /**
    * Get tax rates which are used in receipt
    * @return TaxRateType[]
    */
   public function getUsedTaxRateTypes() : array
   {
      $rates = [];

      foreach ($this->getItems() AS $item) {
         if(!$item->getTaxRateType()->equalsValue(TaxRateType::_NONE_)) {
            $rates [$item->getTaxRateValue()] = $item->getTaxRateType();
         } else {
            $rates [null] = TaxRateType::get(TaxRateType::_NONE_);
         }
      }

      return array_values($rates);
   }

   /**
    * Get tax rate value
    * @param TaxRateType $type
    * @return float
    */
   public function getTaxRateValueByType(TaxRateType $type) : float
   {
      foreach ($this->getItems() AS $item) {
         if($item->getTaxRateType() === $type) {
            return $item->getTaxRateValue();
         }
      }
   }

   /**
    * Get max tax rate value
    * @return TaxRate|null
    */
   public function getMaxTaxRate() : ?TaxRate
   {
      $rates = [];

      foreach ($this->getItems() AS $item) {
         $rates[(string)$item->getTaxRateValue()] = $item->getTaxRateType();
      }

      if(!count($rates)) {
         return null;
      }

      $type = $rates[max(array_keys($rates))];
      $value = max(array_keys($rates));

      return new TaxRate("", $type, $value);
   }
}