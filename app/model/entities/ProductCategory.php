<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\ProductCategoryRepository")
 */
class ProductCategory
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=255)
    * @var string
    */
   protected $name;

   /**
    * Category can have parent category
    * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="children")
    * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var ProductCategory|null
    */
   protected $parent;

   /**
    * One Category has Many Categories.
    * @ORM\OneToMany(targetEntity="ProductCategory", mappedBy="parent")
    * @var ProductCategory[]|Collection
    */
   private $children;

   /**
    * @ORM\Column(type="string", length=128, nullable=true)
    * @var string|null
    */
   protected $icon;

   /**
    * @ORM\Column(type="string", length=7, nullable=true)
    * @var string|null
    */
   protected $color;

   /**
    * Tax rate category
    * @ORM\ManyToOne(targetEntity="TaxRate")
    * @ORM\JoinColumn(name="tax_rate_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var TaxRate|null
    */
   protected $defaultTaxRate;

   /**
    * @ORM\ManyToOne(targetEntity="MeasurementUnit")
    * @ORM\JoinColumn(name="measurement_unit_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var MeasurementUnit
    */
   protected $defaultMeasurementUnit;

   /**
    * ProductCategory constructor.
    * @param string $name
    * @param ProductCategory|null $parent
    */
   public function __construct(string $name, ProductCategory $parent = null)
   {
      $this->name = $name;
      $this->parent = $parent;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return ProductCategory|null
    */
   public function getParent(): ?ProductCategory
   {
      return $this->parent;
   }

   /**
    * @param ProductCategory|null $parent
    */
   public function setParent(ProductCategory $parent = null)
   {
      $this->parent = $parent;
   }

   /**
    * @return bool
    */
   public function hasParent(): bool
   {
      return !is_null($this->parent);
   }

   /**
    * @return ProductCategory[]|Collection
    */
   public function getChildren()
   {
      return $this->children;
   }

   /**
    * @param ProductCategory[]|Collection $children
    */
   public function setChildren($children)
   {
      $this->children = $children;
   }

   /**
    * @return string
    */
   public function getIcon(): ?string
   {
      return $this->icon;
   }

   /**
    * @param string $icon
    */
   public function setIcon(string $icon)
   {
      $this->icon = $icon;
   }

   /**
    * @return null|string
    */
   public function getColor()
   {
      return $this->color;
   }

   /**
    * @param null|string $color
    */
   public function setColor($color)
   {
      if(empty($color)) {
         $color = null;
      }

      $this->color = $color;
   }

   /**
    * @return TaxRate|null
    */
   public function getDefaultTaxRate() : ?TaxRate
   {
      return $this->defaultTaxRate;
   }

   /**
    * @param TaxRate|null $defaultTaxRate
    */
   public function setDefaultTaxRate($defaultTaxRate = null)
   {
      $this->defaultTaxRate = $defaultTaxRate;
   }

   /**
    * @return MeasurementUnit|null
    */
   public function getDefaultMeasurementUnit(): ?MeasurementUnit
   {
      return $this->defaultMeasurementUnit;
   }

   /**
    * @param MeasurementUnit|null $defaultMeasurementUnit
    */
   public function setDefaultMeasurementUnit(MeasurementUnit $defaultMeasurementUnit = null)
   {
      $this->defaultMeasurementUnit = $defaultMeasurementUnit;
   }
}