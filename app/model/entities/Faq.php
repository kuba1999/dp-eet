<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\FaqRepository")
 */
class Faq
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=256)
    * @var string
    */
   protected $subject;

   /**
    * @ORM\Column(type="text")
    * @var string
    */
   protected $content;

   /**
    * @ORM\Column(type="text", length=256, nullable = true)
    * @var string|null
    */
   protected $fileName;

   public function __construct(string $subject, string $content)
   {
      $this->subject = $subject;
      $this->content = $content;
   }

   /**
    * @return string
    */
   public function getSubject(): string
   {
      return $this->subject;
   }

   /**
    * @param string $subject
    */
   public function setSubject(string $subject)
   {
      $this->subject = $subject;
   }

   /**
    * @return string
    */
   public function getContent(): string
   {
      return $this->content;
   }

   /**
    * @param string $content
    */
   public function setContent(string $content)
   {
      $this->content = $content;
   }

   /**
    * @return string
    */
   public function getFileName(): ?string
   {
      return $this->fileName;
   }

   /**
    * @param string $fileName
    */
   public function setFileName(string $fileName)
   {
      $this->fileName = $fileName;
   }
}