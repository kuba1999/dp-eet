<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

/**
* Created by PhpStorm.
* User: jakub
* Date: 2.2.17
* Time: 16:14
*/
namespace App\Model\Entities;

use App\Model\Enums\ReceiptLogRecordType;
use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\ReceiptLogRecordRepository")
*/
class ReceiptLogRecord
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
   * @ORM\ManyToOne(targetEntity="Receipt", inversedBy="items")
   * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id", nullable=false)
   * @var Receipt
   */
   protected $receipt;

   /**
    * @ORM\Column(type="integer", length=1, nullable=false)
    * @var ReceiptLogRecordType
    */
   protected $type;

   /**
    * @ORM\Column(type="text", nullable=false)
    * @var string
    */
   protected $message;

   /**
    * @ORM\Column(type="datetime", nullable=false)
    * @var \DateTime
    */
   protected $created;

   public function __construct(Receipt $receipt, string $message, ReceiptLogRecordType $type = null)
   {
      if(is_null($type)) {
         $type = ReceiptLogRecordType::get(ReceiptLogRecordType::INFO);
      }

      $this->receipt  = $receipt;
      $this->message  = $message;
      $this->type     = $type->getValue();
      $this->created  = new \DateTime();
   }

   /**
    * @return Receipt
    */
   public function getReceipt(): Receipt
   {
      return $this->receipt;
   }

   /**
    * @return ReceiptLogRecordType
    */
   public function getType(): ReceiptLogRecordType
   {
      return ReceiptLogRecordType::get($this->type);
   }

   /**
    * @return string
    */
   public function getMessage(): string
   {
      return $this->message;
   }

   /**
    * @return \DateTime
    */
   public function getCreated(): \DateTime
   {
      return $this->created;
   }
}
