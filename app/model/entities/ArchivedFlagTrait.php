<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

trait ArchivedFlagTrait
{
   /**
    * @ORM\Column(type="boolean", nullable=false)
    * @var bool
    */
   protected $archived = false;

   /**
    * @return bool
    */
   public function isArchived(): bool
   {
      return $this->archived;
   }

   /**
    * @param bool $archived
    */
   public function setArchived(bool $archived = true)
   {
      $this->archived = $archived;
   }
}