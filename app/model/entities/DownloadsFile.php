<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\Language;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\DownloadsRepository")
 */
class DownloadsFile
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=256)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="text", length=256, nullable=true)
    * @var string
    */
   protected $fileName;

   /**
    * DownloadsFile constructor.
    * @param string $name
    */
   public function __construct(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return string
    */
   public function getFileName(): ?string
   {
      return $this->fileName;
   }

   /**
    * @param string $fileName
    */
   public function setFileName(string $fileName)
   {
      $this->fileName = $fileName;
   }
}