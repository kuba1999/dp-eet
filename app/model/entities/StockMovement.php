<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Enums\StockMovementType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\StockMovementRepository")
 * @ORM\EntityListeners({"App\Model\Events\NewStockMovementListener"})
 */
class StockMovement
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="enum", columnDefinition="ENUM('buy', 'sale', 'stockTaking') NOT NULL")
    * @var StockMovementType|int
    */
   protected $type;

   /**
    * @ORM\OneToMany(targetEntity="StockMovementItem", mappedBy="stockMovement", cascade={"persist"})
    * @var StockMovementItem|ArrayCollection
    */
   protected $items;

   /**
    * @ORM\Column(type="string", nullable=true)
    * @var string|null
    */
   protected $invoiceNumber;

   /**
    * @ORM\ManyToOne(targetEntity="Supplier")
    * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", onDelete="RESTRICT", nullable=true)
    * @var Supplier|null
    */
   protected $supplier;

   /**
    * @ORM\Column(type="text", nullable = true)
    * @var string|null
    */
   protected $note;

   /**
    * @ORM\Column(type="datetime", nullable = false)
    * @var \DateTime
    */
   protected $created;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="executed_by_user_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
    * @var User|null
    */
   protected $executedBy;

   /**
    * @ORM\ManyToOne(targetEntity="Receipt")
    * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id", nullable=true)
    * @var Receipt|null
    */
   private $receipt;

   /**
    * StockMovement constructor.
    * @param StockMovementType $type
    */
   public function __construct(StockMovementType $type, User $executedBy)
   {
      $this->type = $type->getValue();
      $this->items = new ArrayCollection();
      $this->executedBy = $executedBy;

      $this->created = new \DateTime();
   }

   /**
    * @return StockMovementType
    */
   public function getType(): StockMovementType
   {
      return StockMovementType::get($this->type);
   }

   /**
    * @param StockMovementType $type
    */
   public function setType(StockMovementType $type)
   {
      $this->type = $type->getValue();
   }

   /**
    * @return null|string
    */
   public function getInvoiceNumber()
   {
      return $this->invoiceNumber;
   }

   /**
    * @param null|string $invoiceNumber
    */
   public function setInvoiceNumber($invoiceNumber)
   {
      if(empty($invoiceNumber)) {
         $invoiceNumber = null;
      }

      $this->invoiceNumber = $invoiceNumber;
   }

   /**
    * @return Supplier|null
    */
   public function getSupplier()
   {
      return $this->supplier;
   }

   /**
    * @param Supplier|null $supplier
    */
   public function setSupplier($supplier)
   {
      $this->supplier = $supplier;
   }

   /**
    * @return null|string
    */
   public function getNote()
   {
      return $this->note;
   }

   /**
    * @param null|string $note
    */
   public function setNote($note)
   {
      if(empty($note)) {
         $note = null;
      }

      $this->note = $note;
   }

   /**
    * @return \DateTime
    */
   public function getCreated(): \DateTime
   {
      return $this->created;
   }

   /**
    * @param \DateTime $created
    */
   public function setCreated(\DateTime $created)
   {
      $this->created = $created;
   }

   /**
    * @return mixed
    */
   public function getItems()
   {
      return $this->items;
   }

   /**
    * @param Product $product
    * @param $quantity
    * @return StockMovementItem
    */
   public function addItem(Product $product, $quantity) : StockMovementItem
   {
      $item = new StockMovementItem($this, $product, $quantity);

      $this->items->add($item);

      return $item;
   }

   /**
    * @return User|null
    */
   public function getExecutedBy(): ?User
   {
      return $this->executedBy;
   }

   /**
    * @return Receipt|null
    */
   public function getReceipt() : ?Receipt
   {
      return $this->receipt;
   }

   /**
    * @param Receipt|null $receipt
    */
   public function setReceipt(?Receipt $receipt)
   {
      $this->receipt = $receipt;
   }
}