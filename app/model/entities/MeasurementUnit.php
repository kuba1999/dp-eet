<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Enums\UnitFormat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\MeasurementUnitRepository")
 */
class MeasurementUnit
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;
   use ArchivedFlagTrait;

   /**
    * @ORM\Column(type="string", length=255)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="string", length=16, nullable=true)
    * @var string|null
    */
   protected $measurementUnit;

   /**
    * @ORM\Column(type="smallint", length=1, nullable=true)
    * @var UnitFormat|null
    */
   protected $measurementUnitFormat;

   /**
    * @ORM\Column(type="string", length=16, length=255)
    * @var string
    */
   protected $saleUnit;

   /**
    * @ORM\Column(type="smallint", nullable=true)
    * @var UnitFormat|null
    */
   protected $saleUnitFormat;

   /**
    * @ORM\Column(type="float", nullable=true)
    * @var float|null
    */
   protected $conversionCoefficient;

   /**
    * MeasurementUnit constructor.
    * @param string $name
    * @param string $sellUnit
    */
   public function __construct(string $name, string $sellUnit)
   {
      $this->name = $name;
      $this->saleUnit = $sellUnit;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return null|string
    */
   public function getMeasurementUnit()
   {
      return $this->measurementUnit;
   }

   /**
    * @param null|string $measurementUnit
    */
   public function setMeasurementUnit($measurementUnit)
   {
      if(empty($measurementUnit)) {
         $this->measurementUnit = null;
      } else {
         $this->measurementUnit = $measurementUnit;
      }
   }

   /**
    * @return UnitFormat|null
    */
   public function getMeasurementUnitFormat() : ?UnitFormat
   {
      if(!is_null($this->measurementUnitFormat)) {
         return UnitFormat::get((int)$this->measurementUnitFormat);
      }

      return null;
   }

   /**
    * @param UnitFormat|null $measurementUnitFormat
    */
   public function setMeasurementUnitFormat(UnitFormat $measurementUnitFormat = null)
   {
      if($measurementUnitFormat) {
         $measurementUnitFormat = $measurementUnitFormat->getValue();
      }

      $this->measurementUnitFormat = $measurementUnitFormat;
   }

   /**
    * @return string
    */
   public function getSaleUnit(): string
   {
      return $this->saleUnit;
   }

   /**
    * @param string $saleUnit
    */
   public function setSaleUnit(string $saleUnit)
   {
      if(empty($saleUnit)) {
         $this->saleUnit = null;
      } else {
         $this->saleUnit = $saleUnit;
      }
   }

   /**
    * @return UnitFormat|null
    */
   public function getSaleUnitFormat() : ?UnitFormat
   {
      if(!is_null($this->saleUnitFormat)) {
         return UnitFormat::get((int)$this->saleUnitFormat);
      }

      return null;
   }

   /**
    * @param UnitFormat|null $saleUnitFormat
    */
   public function setSaleUnitFormat(UnitFormat $saleUnitFormat = null)
   {
      if($saleUnitFormat) {
         $saleUnitFormat = $saleUnitFormat->getValue();
      }

      $this->saleUnitFormat = $saleUnitFormat;
   }

   /**
    * @return float|null
    */
   public function getConversionCoefficient() : ?float
   {
      return $this->conversionCoefficient;
   }

   /**
    * @param float|null $conversionCoefficient
    */
   public function setConversionCoefficient($conversionCoefficient)
   {
      if(empty($conversionCoefficient)) {
         $this->conversionCoefficient = null;
      } else {
         $this->conversionCoefficient = $conversionCoefficient;
      }
   }
}