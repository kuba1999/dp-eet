<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

/**
* Created by PhpStorm.
* User: jakub
* Date: 2.2.17
* Time: 16:14
*/
namespace App\Model\Entities;

use App\Model\Enums\TaxRateType;
use App\Model\Helpers\PriceHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\ReceiptItemRepository")
*/
class ReceiptItem
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
   * @ORM\ManyToOne(targetEntity="Receipt", inversedBy="items")
   * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id", nullable=false)
   * @var Receipt
   */
   protected $receipt;

   /**
   * @ORM\Column(type="string", length=256, nullable=false)
   * @var string
   */
   protected $name;

   /**
    * @ORM\ManyToOne(targetEntity="Product")
    * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="RESTRICT", nullable=true)
    * @var Product|null
    */
   protected $product;

   /**
   * @ORM\Column(type="float")
   * @var float
   */
   protected $quantity = 1.0;

   /**
    * Price with tax
    * @ORM\Column(type="decimal", precision=20, scale=2)
    * @var float
    */
   protected $unitPriceWithTax;

   /**
    * @ORM\Column(type="float", nullable=true)
    * @var float|null
    */
   protected $taxRateValue;

   /**
    * @ORM\Column(type="integer", length=1, nullable=false)
    * @var TaxRateType
    */
   protected $taxRateType;

   /**
    * @ORM\Column(type="string", length=256, nullable=false)
    * @var string
    */
   protected $measurementUnit = "ks";

   /**
    * @ORM\Column(type="string", length=256, nullable=true)
    * @var string|null
    */
   protected $note;

   /**
    * ReceiptItem constructor.
    * @param Receipt $receipt
    * @param string $name
    * @param float $price
    * @param float $quantity
    */
   public function __construct(
      Receipt $receipt,
      string $name,
      float $price,
      float $quantity
   ) {
     $this->receipt = $receipt;
     $this->name = $name;
     $this->quantity = $quantity;
     $this->unitPriceWithTax = $price;
   }

   /**
    * @return mixed
    */
   public function getReceipt() : Receipt
   {
      return $this->receipt;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return mixed
    */
   public function getProduct() : ?Product
   {
      return $this->product;
   }

   /**
    * @param mixed $product
    */
   public function setProduct(Product $product = null)
   {
      $this->product = $product;
   }

   /**
    * @return float
    */
   public function getQuantity(): float
   {
      return $this->quantity;
   }

   /**
    * @param float $quantity
    */
   public function setQuantity(float $quantity)
   {
      $this->quantity = $quantity;
   }

   /**
    * @return float
    */
   public function getUnitPriceWithTax(): float
   {
      return $this->unitPriceWithTax;
   }

   /**
    * @param float $unitPriceWithTax
    */
   public function setUnitPriceWithTax(float $unitPriceWithTax)
   {
      $this->unitPriceWithTax = $unitPriceWithTax;
   }

   /**
    * @param string
    */
   public function setMeasurementUnit(string $measurementUnit)
   {
      $this->measurementUnit = $measurementUnit;
   }

   /**
    * @return string
    */
   public function getMeasurementUnit() : string
   {
      return $this->measurementUnit;
   }

   /**
    * @return string|null
    */
   public function getNote(): ?string
   {
      return $this->note;
   }

   /**
    * @param string|null $note
    */
   public function setNote(?string $note): void
   {
      $this->note = $note;
   }

   /**
    * @return float|null
    */
   public function getTaxRateValue(): ?float
   {
      return $this->taxRateValue;
   }

   /**
    * @param float|null $taxRateValue
    */
   public function setTaxRateValue(?float $taxRateValue): void
   {
      $this->taxRateValue = $taxRateValue;
   }

   /**
    * @return TaxRateType
    */
   public function getTaxRateType(): TaxRateType
   {
      return TaxRateType::get($this->taxRateType);
   }

   /**
    * @param TaxRateType $taxRateType
    */
   public function setTaxRateType(TaxRateType $taxRateType): void
   {
      $this->taxRateType = $taxRateType->getValue();
   }

   ////////////////////////////////////////////////////////////////////  HELPERS
   public function calculatePartialSum(bool $withTax) : float
   {
      if($this->getTaxRateType()->equalsValue(TaxRateType::_NONE_)) { //without tax
         $withTax = false;
      }

      if($withTax) {
         return $this->getUnitPriceWithTax() * $this->getQuantity();
      }

      return $this->getUnitPriceWithoutTax() * $this->getQuantity();
   }

   /**
    * @return float
    */
   public function getUnitPriceWithoutTax(): float
   {
      if($this->getTaxRateType()->equalsValue(TaxRateType::_NONE_)) { //without tax
         return $this->unitPriceWithTax;
      }

      return PriceHelper::calculatePriceWithoutVat($this->taxRateValue, $this->unitPriceWithTax, false);
   }

   /**
    * @return float
    */
   public function calculatePartialTaxSum(): float
   {
      if($this->getTaxRateType()->equalsValue(TaxRateType::_NONE_)) { //without tax
         return $this->unitPriceWithTax;
      }

      return $this->calculatePartialSum(true) - $this->calculatePartialSum(false);
   }
}
