<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\ReceiptEETState;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TypeOfDocument;
use App\Model\Helpers\PriceHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ReceiptEetData
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="integer", nullable=false)
    * @var int
    */
   protected $sendingTryCounter = 1;

   /**
    * @ORM\Column(type="integer", length=1, nullable=false)
    * @var ReceiptEETState
    */
   protected $state;

   /**
    * @ORM\Column(type="string", length=39, nullable=true)
    * @var string|null
    */
   protected $fik;

   /**
    * @ORM\Column(type="string", length=6, nullable=false)
    * @var string
    */
   protected $premiseId;

   /**
    * @ORM\Column(type="string", length=20, nullable=false)
    * @var string
    */
   protected $cashRegisterId;

   /**
    * ReceiptCustomerData constructor.
    * @param Receipt $receipt
    */
   public function __construct(Receipt $receipt)
   {
      $this->receipt = $receipt;
      $this->state = ReceiptEETState::WAITING;
   }

   /**
    * @param string $fik
    * @throws \Exception
    */
   public function setFik(string $fik)
   {
      if(!is_null($this->fik)) {
         throw new \Exception("Receipt already has FIK code");
      }
      $this->fik = $fik;
   }

   /**
    * @return string
    */
   public function getFik(): ?string
   {
      return $this->fik;
   }

   /**
    * @return int
    */
   public function getNumberOfTry() : int
   {
      return $this->sendingTryCounter;
   }

   public function nextTryOfSendingToEET()
   {
      $this->sendingTryCounter++;
   }

   /**
    * @return bool
    */
   public function isFirstTry() : bool
   {
      return $this->sendingTryCounter === 1;
   }

   /**
    * @param ReceiptEETState $state
    */
   public function setState(ReceiptEETState $state)
   {
      $this->state = $state->getValue();
   }

   /**
    * @return ReceiptEETState
    */
   public function getState() : ReceiptEETState
   {
      return ReceiptEETState::get($this->state);
   }

   /**
    * @return string
    */
   public function getPremiseId(): string
   {
      return $this->premiseId;
   }

   /**
    * @param string $premiseId
    */
   public function setPremiseId(string $premiseId): void
   {
      $this->premiseId = $premiseId;
   }

   /**
    * @return string
    */
   public function getCashRegisterId(): string
   {
      return $this->cashRegisterId;
   }

   /**
    * @param string $cashRegisterId
    */
   public function setCashRegisterId(string $cashRegisterId): void
   {
      $this->cashRegisterId = $cashRegisterId;
   }
}