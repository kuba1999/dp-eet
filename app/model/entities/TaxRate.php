<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Enums\TaxRateType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\TaxRateRepository")
 */
class TaxRate
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;
   use ArchivedFlagTrait;

   /**
    * @ORM\Column(type="string", length=255, nullable=false)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="float", nullable=false)
    * @var float
    */
   protected $value;

   /**
    * @ORM\Column(type="integer", length=1, nullable=false)
    * @var TaxRateType
    */
   protected $type;

   /**
    * @ORM\Column(type="string", length=7, nullable=true)
    * @var string|null
    */
   protected $color;

   /**
    * TaxRate constructor.
    * @param string $name
    */
   public function __construct(string $name, TaxRateType $type, float $value)
   {
      $this->name = $name;
      $this->type = $type->getValue();
      $this->value = $value;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return string
    */
   public function getColor(): ?string
   {
      return $this->color;
   }

   /**
    * @param string|null $color
    */
   public function setColor(string $color = null)
   {
      $this->color = $color;
   }

   /**
    * @return float
    */
   public function getValue(): float
   {
      return $this->value;
   }

   /**
    * @param float $value
    */
   public function setValue(float $value)
   {
      $this->value = $value;
   }

   /**
    * @return TaxRateType
    */
   public function getType(): TaxRateType
   {
      return TaxRateType::get($this->type);
   }

   /**
    * @param TaxRateType $type
    */
   public function setType(TaxRateType $type)
   {
      $this->type = $type->getValue();
   }
}