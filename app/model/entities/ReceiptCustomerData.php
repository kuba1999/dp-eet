<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ReceiptCustomerData
 * @package App\Model\Entities
 * @ORM\Entity
 */
class ReceiptCustomerData
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $street;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $city;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $zip;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $name;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    * IČ
    */
   protected $vatNo;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    * DIČ
    */
   protected $vatId;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $phone;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string|null
    */
   protected $email;

   /**
    * ReceiptCustomerData constructor.
    * @param Receipt $receipt
    */
   public function __construct(Receipt $receipt)
   {
      $this->receipt = $receipt;
   }

   /**
    * @return string|null
    */
   public function getStreet(): ?string
   {
      return $this->street;
   }

   /**
    * @param string|null $street
    */
   public function setStreet(string $street = null)
   {
      $this->street = $street;
   }

   /**
    * @return string|null
    */
   public function getCity(): ?string
   {
      return $this->city;
   }

   /**
    * @param string|null $city
    */
   public function setCity(string $city = null)
   {
      $this->city = $city;
   }

   /**
    * @return string|null
    */
   public function getZip(): ?string
   {
      return $this->zip;
   }

   /**
    * @param string|null $zip
    */
   public function setZip(string $zip = null)
   {
      $this->zip = $zip;
   }

   /**
    * @return string|null
    */
   public function getVatNo(): ?string
   {
      return $this->vatNo;
   }

   /**
    * @param string $vatNo
    */
   public function setVatNo(string $vatNo = null)
   {
      $this->vatNo = $vatNo;
   }

   /**
    * @return string|null
    */
   public function getVatId(): ?string
   {
      return $this->vatId;
   }

   /**
    * @param string|null $vatId
    */
   public function setVatId(string $vatId = null)
   {
      $this->vatId = $vatId;
   }

   /**
    * @return string|null
    */
   public function getName(): ?string
   {
      return $this->name;
   }

   /**
    * @param string|null $name
    */
   public function setName(string $name = null)
   {
      $this->name = $name;
   }

   /**
    * @return string|null
    */
   public function getPhone(): ?string
   {
      return $this->phone;
   }

   /**
    * @param string|null $phone
    */
   public function setPhone(string $phone = null): void
   {
      $this->phone = $phone;
   }

   /**
    * @return string|null
    */
   public function getEmail(): ?string
   {
      return $this->email;
   }

   /**
    * @param string|null $email
    */
   public function setEmail(string $email = null): void
   {
      $this->email = $email;
   }

   /**
    * Check if any property is filled
    * @return bool
    */
   public function isAnyCustomerDetailFilled() : bool
   {
      return !empty($this->name) ||
         !empty($this->street) ||
         !empty($this->city) ||
         !empty($this->zip) ||
         !empty($this->vatId) ||
         !empty($this->vatNo) ||
         !empty($this->email) ||
         !empty($this->phone);
   }
}