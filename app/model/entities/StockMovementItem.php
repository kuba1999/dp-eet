<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Enums\StockMovementType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\StockMovementRepository")
 * @ORM\EntityListeners({"App\Model\Events\NewStockMovementItemListener"})
 */
class StockMovementItem
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\ManyToOne(targetEntity="StockMovement", inversedBy="items")
    * @ORM\JoinColumn(name="stock_movement_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var StockMovement
    */
   protected $stockMovement;

   /**
    * @ORM\ManyToOne(targetEntity="Product")
    * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="RESTRICT")
    * @var Product
    */
   protected $product;

   /**
    * @ORM\Column(type="float")
    * @var float
    */
   protected $quantity = 0.0;

   /**
    * Purchase or sale price
    * @ORM\Column(type="float", nullable=true)
    * @var float|null
    */
   protected $unitPrice;

   /**
    * StockMovementItem constructor.
    * @param StockMovement $stockMovement
    * @param Product $product
    * @param $quantity
    */
   public function __construct(StockMovement $stockMovement, Product $product, float $quantity)
   {
      $this->stockMovement = $stockMovement;
      $this->product = $product;
      $this->quantity = $quantity;
   }

   /**
    * @return StockMovement
    */
   public function getStockMovement() : StockMovement
   {
      return $this->stockMovement;
   }

   /**
    * @return Product
    */
   public function getProduct(): Product
   {
      return $this->product;
   }

   /**
    * @param Product $product
    */
   public function setProduct(Product $product)
   {
      $this->product = $product;
   }

   /**
    * @return float
    */
   public function getQuantity(): float
   {
      return $this->quantity;
   }

   /**
    * @param float $quantity
    */
   public function setQuantity(float $quantity)
   {
      $this->quantity = $quantity;
   }

   /**
    * @return float|null
    */
   public function getUnitPrice(): ?float
   {
      return $this->unitPrice;
   }

   /**
    * @param float|null $unitPrice
    */
   public function setUnitPrice(?float $unitPrice)
   {
      $this->unitPrice = $unitPrice;
   }
}