<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\TypeOfDocument;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\DocumentIdSequenceRepository")
 */
class DocumentIdSequence
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="integer", nullable=false)
    * @var TypeOfDocument
    */
   protected $typeOfDocument;

   /**
    * @ORM\Column(type="integer", nullable=false)
    * @var int
    */
   protected $value;

   /**
    * DocumentIdSequence constructor.
    * @param TypeOfDocument $typeOfDocument
    * @param int $value
    */
   public function __construct(TypeOfDocument $typeOfDocument, $value)
   {
      $this->typeOfDocument = $typeOfDocument->getValue();
      $this->value = $value;
   }

   /**
    * @return TypeOfDocument
    */
   public function getTypeOfDocument(): TypeOfDocument
   {
      return TypeOfDocument::get($this->typeOfDocument);
   }

   /**
    * @return int
    */
   public function getValue(): int
   {
      return $this->value;
   }

   public function incrementValue()
   {
      $this->value++;
   }
}