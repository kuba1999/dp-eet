<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\UserTicketTypeNotificationRepository")
 */
class UserTicketTypeNotification
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
    * @var User
    */
   protected $user;

   /**
    * @ORM\Column(type="integer", nullable = false)
    * @var int
    */
   protected $type;

   /**
    * UserTicketTypeNotification constructor.
    * @param User $user
    * @param int $type
    */
   public function __construct(User $user, int $type)
   {
      $this->user = $user;
      $this->type = $type;
   }

   /**
    * @return User
    */
   public function getUser(): User
   {
      return $this->user;
   }

   /**
    * @param User $user
    */
   public function setUser(User $user)
   {
      $this->user = $user;
   }

   /**
    * @return int
    */
   public function getType(): int
   {
      return $this->type;
   }

   /**
    * @param int $type
    */
   public function setType(int $type)
   {
      $this->type = $type;
   }
}