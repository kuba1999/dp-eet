<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\TicketItemRepository")
 */
class TicketAttachment
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\ManyToOne(targetEntity="TicketMessage", inversedBy="attachments")
    * @ORM\JoinColumn(name="message_id", referencedColumnName="id", nullable=false)
    * @var TicketMessage
    */
   protected $message;

   /**
    * @ORM\Column(type="text", length=255)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="datetime")
    * @var \DateTime
    */
   protected $created;

   public function __construct(TicketMessage $message, string $name)
   {
      $this->message = $message;
      $this->name = $name;
      $this->created = new DateTime();
   }

   /**
    * @return TicketMessage
    */
   public function getMessage(): TicketMessage
   {
      return $this->message;
   }

   /**
    * @param TicketMessage $message
    */
   public function setMessage(TicketMessage $message)
   {
      $this->message = $message;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return \DateTime
    */
   public function getCreated(): \DateTime
   {
      return $this->created;
   }

   /**
    * @param \DateTime $created
    */
   public function setCreated(\DateTime $created)
   {
      $this->created = $created;
   }
}