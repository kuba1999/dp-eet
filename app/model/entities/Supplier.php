<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\SupplierRepository")
 */
class Supplier
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;
   use ArchivedFlagTrait;

   /**
    * @ORM\Column(type="string", length=255)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="string", length=255, nullable = true)
    * @var string|null
    * IČ
    */
   protected $address;

   /**
    * @ORM\Column(type="string", length=8, nullable = true)
    * @var string|null
    * IČ
    */
   protected $vatNo;

   /**
    * @ORM\Column(type="string", length=12, nullable = true)
    * @var string|null
    * DIČ
    */
   protected $vatId;

   /**
    * @ORM\Column(type="string", length=255, nullable = true)
    * @var string
    */
   protected $email;

   /**
    * @ORM\Column(type="string", length=255, nullable = true)
    * @var string|null
    */
   protected $phone;

   /**
    * @ORM\Column(type="text", nullable = true)
    * @var string|null
    */
   protected $note;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $visible = true;

   /**
    * Supplier constructor.
    * @param string $name Name of supplier
    */
   public function __construct(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return string
    */
   public function getAddress(): ?string
   {
      return $this->address;
   }

   /**
    * @param string $address
    */
   public function setAddress(string $address)
   {
      $this->address = $address;
   }

   /**
    * @return string
    */
   public function getVatNo(): ?string
   {
      return $this->vatNo;
   }

   /**
    * @param string $vatNo
    */
   public function setVatNo(string $vatNo)
   {
      $this->vatNo = $vatNo;
   }

   /**
    * @return string
    */
   public function getVatId(): ?string
   {
      return $this->vatId;
   }

   /**
    * @param string $vatId
    */
   public function setVatId(string $vatId)
   {
      $this->vatId = $vatId;
   }

   /**
    * @return string
    */
   public function getEmail(): ?string
   {
      return $this->email;
   }

   /**
    * @param string $email
    */
   public function setEmail(string $email)
   {
      $this->email = $email;
   }

   /**
    * @return string
    */
   public function getPhone(): ?string
   {
      return $this->phone;
   }

   /**
    * @param string $phone
    */
   public function setPhone(string $phone)
   {
      $this->phone = $phone;
   }

   /**
    * @return string
    */
   public function getNote(): ?string
   {
      return $this->note;
   }

   /**
    * @param string $note
    */
   public function setNote(string $note)
   {
      $this->note = $note;
   }

   /**
    * @return bool
    */
   public function isVisible(): bool
   {
      return $this->visible;
   }

   /**
    * @param bool $visible
    */
   public function setVisible(bool $visible)
   {
      $this->visible = $visible;
   }
}