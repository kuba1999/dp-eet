<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Helpers\PriceHelper;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\ProductRepository")
 */
class Product
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;
   use ArchivedFlagTrait;

   /**
    * @ORM\Column(type="string", length=256)
    * @var string
    */
   protected $name;

   /**
    * Product can be assigned in category
    * @ORM\ManyToOne(targetEntity="ProductCategory")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var ProductCategory|null
    */
   protected $category;

   /**
    * commonly will be used EAN-13
    * @ORM\Column(type="string", length=256, nullable=true)
    * @var string|null
    */
   protected $ean;

   /**
    * @ORM\Column(type="string", length=16, nullable=true)
    * @var string|null
    */
   protected $plu;

   /**
    * Price without tax
    * @ORM\Column(type="decimal", precision=20, scale=4)
    * @var float
    */
   protected $price;


   /**
    * @ORM\Column(type="string", length=255, nullable=true)
    * @var string|float
    */
   protected $grossMargin = null;

   /**
    * Tax rate
    * @ORM\ManyToOne(targetEntity="TaxRate")
    * @ORM\JoinColumn(name="tax_rate_id", referencedColumnName="id", onDelete="RESTRICT")
    * @var TaxRate|null
    */
   protected $taxRate;

   /**
    * @ORM\Column(type="string", length=7, nullable=true)
    * @var string|null
    */
   protected $color;

   /**
    * @ORM\ManyToOne(targetEntity="MeasurementUnit")
    * @ORM\JoinColumn(name="measurement_unit_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
    * @var MeasurementUnit
    */
   protected $measurementUnit;

   /**
    * @ORM\Column(type="float")
    * @var float
    */
   protected $amountInSellUnit = 1;

   /**
    * @ORM\Column(type="text", nullable = true)
    * @var string|null
    */
   protected $note;

   /**
    * @ORM\Column(type="float")
    * @var float
    */
   protected $quantityInStock = 0.0;

   /**
    * Product constructor.
    * @param string $name
    * @param ProductCategory|null $category
    * @param float $price
    * @param MeasurementUnit $measurementUnit
    */
   public function __construct(string $name, ProductCategory $category = null, float $price, MeasurementUnit $measurementUnit)
   {
      $this->name = $name;
      $this->category = $category;
      $this->price = $price;
      $this->measurementUnit = $measurementUnit;
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return ProductCategory|null
    */
   public function getCategory()
   {
      return $this->category;
   }

   /**
    * @param ProductCategory|null $category
    */
   public function setCategory($category)
   {
      $this->category = $category;
   }

   /**
    * @return string
    */
   public function getEan(): ?string
   {
      return $this->ean;
   }

   /**
    * @param string $ean
    */
   public function setEan(string $ean)
   {
      if(empty($ean)) {
         $ean = null;
      }

      $this->ean = $ean;
   }

   /**
    * @return string
    */
   public function getPlu(): ?string
   {
      return $this->plu;
   }

   /**
    * @param string $plu
    */
   public function setPlu(string $plu)
   {
      if(empty($plu)) {
         $plu = null;
      }

      $this->plu = $plu;
   }

   /**
    * @return float
    */
   public function getPrice(): float
   {
      return $this->price;
   }

   /**
    * @return float
    */
   public function getUnitPrice(): float
   {
      $unitPrice = $this->price / $this->amountInSellUnit;

      if($this->measurementUnit->getConversionCoefficient()) {
         $unitPrice /= $this->measurementUnit->getConversionCoefficient();
      }

      return $unitPrice;
   }

   /**
    * @param float $price
    */
   public function setPrice(float $price)
   {
      $this->price = $price;
   }

   /**
    * @return string|null
    */
   public function getGrossMargin() : ?string
   {
      return $this->grossMargin;
   }

   /**
    * @param string|null $grossMargin
    */
   public function setGrossMargin(?string $grossMargin)
   {
      if(empty($grossMargin)) {
         $grossMargin = null;
      }

      $this->grossMargin = $grossMargin;
   }

   /**
    * @return TaxRate|null
    */
   public function getTaxRate() : ?TaxRate
   {
      return $this->taxRate;
   }

   /**
    * @param TaxRate|null $taxRate
    */
   public function setTaxRate($taxRate = null)
   {
      $this->taxRate = $taxRate;
   }

   /**
    * @return null|string
    */
   public function getColor()
   {
      return $this->color;
   }

   /**
    * @param null|string $color
    */
   public function setColor($color)
   {
      if(empty($color)) {
         $color = null;
      }

      $this->color = $color;
   }

   /**
    * @return MeasurementUnit
    */
   public function getMeasurementUnit(): MeasurementUnit
   {
      return $this->measurementUnit;
   }

   /**
    * @param MeasurementUnit $measurementUnit
    */
   public function setMeasurementUnit(MeasurementUnit $measurementUnit)
   {
      $this->measurementUnit = $measurementUnit;
   }

   /**
    * @return float
    */
   public function getAmountInSellUnit(): float
   {
      return $this->amountInSellUnit;
   }

   /**
    * @param float $amountInSellUnit
    */
   public function setAmountInSellUnit(float $amountInSellUnit)
   {
      $this->amountInSellUnit = $amountInSellUnit;
   }

   /**
    * @return null|string
    */
   public function getNote()
   {
      return $this->note;
   }

   /**
    * @param null|string $note
    */
   public function setNote($note)
   {
      $this->note = $note;
   }

   /**
    * @return float
    */
   public function getQuantityInStock(): float
   {
      return $this->quantityInStock;
   }

   /**
    * @param float $quantityInStock
    */
   public function setQuantityInStock(float $quantityInStock)
   {
      $this->quantityInStock = $quantityInStock;
   }

   /**
    * @param float $quantityInStock
    */
   public function addQuantityInStock(float $quantityInStock)
   {
      $this->quantityInStock += $quantityInStock;
   }

   /**
    * @param float $quantityInStock
    */
   public function removeQuantityInStock(float $quantityInStock)
   {
      $this->quantityInStock -= $quantityInStock;
   }

   /**
    * Calculate price with VAT
    * @return float
    */
   public function getPriceWithVat() : float
   {
      if(is_null($this->getTaxRate())) {
         return $this->getPrice();
      }

      return PriceHelper::calculatePriceWithVat($this->getTaxRate(), $this->getPrice(), false);
   }

   /**
    * Calculate price with VAT
    * @return float
    */
   public function getUnitPriceWithVat() : float
   {
      if(is_null($this->getTaxRate())) {
         return $this->getUnitPrice();
      }

      return PriceHelper::calculatePriceWithVat($this->getTaxRate(), $this->getUnitPrice(), false);
   }
}