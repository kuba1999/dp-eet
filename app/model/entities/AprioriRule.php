<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class AprioriRule
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="simple_array")
    * @var array
    */
   protected $antecedent;

   /**
    * @ORM\Column(type="simple_array")
    * @var array
    */
   protected $consequent;

   /**
    * AprioriRules constructor.
    * @param array $antecedent
    * @param array $consequent
    */
   public function __construct(array $antecedent, array $consequent)
   {
      asort($antecedent);
      asort($consequent);

      $this->antecedent = $antecedent;
      $this->consequent = $consequent;
   }


   /**
    * @return array
    */
   public function getAntecedent(): array
   {
      return $this->antecedent;
   }

   /**
    * @param array $antecedent
    */
   public function setAntecedent(array $antecedent)
   {
      asort($antecedent);
      $this->antecedent = $antecedent;
   }

   /**
    * @return array
    */
   public function getConsequent(): array
   {
      return $this->consequent;
   }

   /**
    * @param array $consequent
    */
   public function setConsequent(array $consequent)
   {
      asort($consequent);
      $this->consequent = $consequent;
   }
}