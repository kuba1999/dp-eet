<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;
use App\Model\Enums\CashView;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\CashRepository")
 */
class Cash
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;
   use ArchivedFlagTrait;

   /**
    * @ORM\Column(type="string", length=64, nullable=false)
    * @var string
    */
   protected $name;

   /**
    * @ORM\Column(type="string", length=6, nullable=true)
    * @var string|null
    */
   protected $premiseId;

   /**
    * @ORM\Column(type="string", length=20, nullable=true)
    * @var string|null
    */
   protected $cashRegisterId;

   /**
    * @ORM\ManyToMany(targetEntity="User")
    * @ORM\JoinTable(name="cash_privileges",
    *      joinColumns={@ORM\JoinColumn(name="cash_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
    *      )
    * @var User
    */
   protected $privileges;

   /**
    * @ORM\Column(type="integer", length=1, nullable=false)
    * @var CashView
    */
   protected $defaultView = CashView::TABLE;

   /**
    * Cash constructor.
    * @param string $name
    */
   public function __construct(string $name)
   {
      $this->name = $name;
      $this->privileges = new ArrayCollection();
   }

   /**
    * @return string
    */
   public function getName(): string
   {
      return $this->name;
   }

   /**
    * @param string $name
    */
   public function setName(string $name)
   {
      $this->name = $name;
   }

   /**
    * @return null|string
    */
   public function getPremiseId() : ?string
   {
      return $this->premiseId;
   }

   /**
    * @param null|string $premiseId
    */
   public function setPremiseId(string $premiseId = null)
   {
      $this->premiseId = $premiseId;
   }

   /**
    * @return null|string
    */
   public function getCashRegisterId() : ?string
   {
      return $this->cashRegisterId;
   }

   /**
    * @param null|string $cashRegisterId
    */
   public function setCashRegisterId(string $cashRegisterId = null)
   {
      $this->cashRegisterId = $cashRegisterId;
   }

   /**
    * @return User[]|Collection
    */
   public function getPrivileges() : Collection
   {
      return $this->privileges;
   }

   /**
    * @param User $user
    */
   public function addPrivilege(User $user)
   {
      if(!$this->privileges->contains($user)) {
         $this->privileges->add($user);
      }
   }

   /**
    * @param User $user
    */
   public function removePrivilege(User $user)
   {
      $this->privileges->removeElement($user);
   }

   /**
    * @return CashView
    */
   public function getDefaultView(): CashView
   {
      return CashView::get($this->defaultView);
   }

   /**
    * @param CashView $defaultView
    */
   public function setDefaultView(CashView $defaultView)
   {
      $this->defaultView = $defaultView->getValue();
   }
}