<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\SettingsRepository")
 */
class SettingsGlobal
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="text")
    * @var string
    */
   protected $welcomePageContent = "";

   /**
    * @ORM\Column(type="text")
    * @var string
    */
   protected $contactPageContent = "";

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $warehouseEnabled = true;

   /**
    * @return bool
    */
   public function isWarehouseEnabled(): bool
   {
      return $this->warehouseEnabled;
   }

   /**
    * @param bool $warehouseEnabled
    */
   public function setWarehouseEnabled(bool $warehouseEnabled)
   {
      $this->warehouseEnabled = $warehouseEnabled;
   }

   /**
    * @return string
    */
   public function getWelcomePageContent(): string
   {
      return $this->welcomePageContent;
   }

   /**
    * @param string $welcomePageContent
    */
   public function setWelcomePageContent(string $welcomePageContent): void
   {
      $this->welcomePageContent = $welcomePageContent;
   }

   /**
    * @return string
    */
   public function getContactPageContent(): string
   {
      return $this->contactPageContent;
   }

   /**
    * @param string $contactPageContent
    */
   public function setContactPageContent(string $contactPageContent): void
   {
      $this->contactPageContent = $contactPageContent;
   }
}