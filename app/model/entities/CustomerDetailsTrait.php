<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Entities;


trait CustomerDetailsTrait
{
   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    */
   protected $street;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    */
   protected $city;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    */
   protected $zip;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    */
   protected $company;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    * IČ
    */
   protected $vatNo;

   /**
    * @ORM\Column(type="string", length=256, nullable = true)
    * @var string
    * DIČ
    */
   protected $vatId;

   /**
    * @return string|null
    */
   public function getStreet(): ?string
   {
      return $this->street;
   }

   /**
    * @param string $street
    */
   public function setStreet(string $street = null)
   {
      $this->street = $street;
   }

   /**
    * @return string|null
    */
   public function getCity(): ?string
   {
      return $this->city;
   }

   /**
    * @param string $city
    */
   public function setCity(string $city = null)
   {
      $this->city = $city;
   }

   /**
    * @return string|null
    */
   public function getZip(): ?string
   {
      return $this->zip;
   }

   /**
    * @param string $zip
    */
   public function setZip(string $zip = null)
   {
      $this->zip = $zip;
   }

   /**
    * @return string|null
    */
   public function getVatNo(): ?string
   {
      return $this->vatNo;
   }

   /**
    * @param string $vatNo
    */
   public function setVatNo(string $vatNo = null)
   {
      $this->vatNo = $vatNo;
   }

   /**
    * @return string|null
    */
   public function getVatId(): ?string
   {
      return $this->vatId;
   }

   /**
    * @param string $vatId
    */
   public function setVatId(string $vatId = null)
   {
      $this->vatId = $vatId;
   }

   /**
    * @return string|null
    */
   public function getCompany(): ?string
   {
      return $this->company;
   }

   /**
    * @param string $company
    */
   public function setCompany(string $company = null)
   {
      $this->company = $company;
   }
}