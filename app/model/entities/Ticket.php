<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use App\Model\Enums\TicketMessageType;
use App\Model\Enums\TicketStatus;
use App\Model\Enums\TicketType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\TicketRepository")
 */
class Ticket
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="string", length=255, nullable = false)
    * @var string
    */
   protected $subject;

   /**
    * @ORM\Column(type="integer", nullable = false)
    * @var int
    */
   protected $type;

   /**
    * @ORM\Column(type="datetime", nullable = false)
    * @var \DateTime
    */
   protected $created;

   /**
    * @ORM\Column(type="integer")
    * @var int
    */
   protected $status;

   /**
    * @ORM\ManyToOne(targetEntity="Receipt")
    * @ORM\JoinColumn(name="receipt_id", referencedColumnName="id")
    * @var Receipt|null
    */
   protected $receipt;

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
    * @var User
    */
   protected $author;

   /**
    * @ORM\OneToMany(targetEntity="TicketMessage", mappedBy="ticket", cascade={"persist"})
    * @var TicketMessage[]|ArrayCollection
    */
   protected $messages;

   /**
    * Ticket constructor.
    * @param string $subject
    */
   public function __construct(string $subject, int $type, User $author)
   {
      $this->subject = $subject;
      $this->author = $author;
      $this->created = new DateTime();
      $this->status = TicketStatus::NEW;
      $this->receipt = null;
      $this->messages = new ArrayCollection();
      $this->type = $type;
   }

   /**
    * @param TicketMessage $message
    */
   public function assignMessage(TicketMessage $message)
   {
      $message->setTicket($this);
      $this->messages->add($message);
   }

   /**
    * @return string
    */
   public function getSubject(): string
   {
      return $this->subject;
   }

   /**
    * @param string $subject
    */
   public function setSubject(string $subject)
   {
      $this->subject = $subject;
   }

   /**
    * @return TicketType
    */
   public function getType(): TicketType
   {
      return TicketType::get($this->type);
   }

   /**
    * @param TicketType $type
    */
   public function setType(TicketType $type)
   {
      $this->type = $type->getValue();
   }

   /**
    * @return \DateTime
    */
   public function getCreated(): \DateTime
   {
      return $this->created;
   }

   /**
    * @param \DateTime $created
    */
   public function setCreated(\DateTime $created)
   {
      $this->created = $created;
   }

   /**
    * @return TicketStatus
    */
   public function getStatus(): TicketStatus
   {
      return TicketStatus::get($this->status);
   }

   /**
    * @param TicketStatus $status
    */
   public function setStatus(TicketStatus $status)
   {
      $this->status = $status->getValue();
   }

   /**
    * @param string $status
    * @param User $user
    */
   public function changeStatusWithNotification(string $status, User $user)
   {
      if($this->status == $status) {
         return;
      }

      $this->status = $status;

      if($status == TicketStatus::CLOSED) {
         $this->addLogMessage(TicketMessageType::CLOSED, $user);
      }
      elseif($status == TicketStatus::REOPENED) {
         $this->addLogMessage(TicketMessageType::REOPENED, $user);
      }
   }

   /**
    * @return User
    */
   public function getAuthor() : User
   {
      return $this->author;
   }

   /**
    * @param mixed $author
    */
   public function setAuthor($author)
   {
      $this->author = $author;
   }

   /**
    * @return TicketMessage[]|ArrayCollection
    */
   public function getMessages()
   {
      return $this->messages;
   }

   /**
    * @param TicketMessage[]|ArrayCollection $messages
    */
   public function setMessages($messages)
   {
      $this->messages = $messages;
   }

   /**
    * @param $type
    * @return TicketMessage
    */
   public function addLogMessage($type, User $user = null) : TicketMessage
   {
      $message = new TicketMessage();
      $message->setType($type);
      $message->setAuthor($user);

      $this->assignMessage($message);

      return $message;
   }

   /**
    * @return Receipt|null
    */
   public function getReceipt(): ?Receipt
   {
      return $this->receipt;
   }

   /**
    * @param Receipt $receipt
    */
   public function setReceipt(Receipt $receipt)
   {
      $this->receipt = $receipt;
   }
}