<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repositories\SettingsRepository")
 */
class SettingsEet
{
   use \Kdyby\Doctrine\Entities\Attributes\Identifier;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $enabled = false;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $verificationMode = false;

   /**
    * @ORM\Column(type="boolean")
    * @var bool
    */
   protected $playgroundEnvironment = false;

   /**
    * @ORM\Column(type="string", length=6, nullable=true)
    * @var string|null
    */
   protected $defaultPremiseId;

   /**
    * @ORM\Column(type="string", length=20, nullable=true)
    * @var string|null
    */
   protected $defaultCashRegisterId;

   /**
    * @ORM\Column(type="text", nullable=false)
    * @var string
    */
   protected $privateKeyPassword = "";

   //////////////////////////

   /**
    * @return bool
    */
   public function isEnabled(): bool
   {
      return $this->enabled;
   }

   /**
    * @param bool $enabled
    */
   public function setEnabled(bool $enabled)
   {
      $this->enabled = $enabled;
   }

   /**
    * @return bool
    */
   public function isVerificationMode(): bool
   {
      return $this->verificationMode;
   }

   /**
    * @param bool $verificationMode
    */
   public function setVerificationMode(bool $verificationMode)
   {
      $this->verificationMode = $verificationMode;
   }

   /**
    * @return null|string
    */
   public function getDefaultPremiseId() : ?string
   {
      return $this->defaultPremiseId;
   }

   /**
    * @param null|string $defaultPremiseId
    */
   public function setDefaultPremiseId(string $defaultPremiseId = null)
   {
      $this->defaultPremiseId = $defaultPremiseId;
   }

   /**
    * @return null|string
    */
   public function getDefaultCashRegisterId() : ?string
   {
      return $this->defaultCashRegisterId;
   }

   /**
    * @param null|string $defaultCashRegisterId
    */
   public function setDefaultCashRegisterId(string $defaultCashRegisterId = null)
   {
      $this->defaultCashRegisterId = $defaultCashRegisterId;
   }

   /**
    * @return null|string
    */
   public function getPrivateKeyPassword() : ?string
   {
      return $this->privateKeyPassword;
   }

   /**
    * @param null|string $privateKeyPassword
    */
   public function setPrivateKeyPassword(string $privateKeyPassword = null)
   {
      $this->privateKeyPassword = $privateKeyPassword;
   }

   /**
    * @return bool
    */
   public function isPlaygroundEnvironment(): bool
   {
      return $this->playgroundEnvironment;
   }

   /**
    * @param bool $playgroundEnvironment
    */
   public function setPlaygroundEnvironment(bool $playgroundEnvironment)
   {
      $this->playgroundEnvironment = $playgroundEnvironment;
   }
}