<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Model\Builders;

use App\Model\Entities\Cash;
use App\Model\Entities\Product;
use App\Model\Entities\Receipt;
use App\Model\Entities\SellerData;
use App\Model\Entities\User;
use App\Model\Enums\TaxRateType;
use App\Model\Enums\TypeOfDocument;
use App\Model\Helpers\MeasurementUnitHelper;
use App\Model\Helpers\PriceHelper;
use App\Model\Repositories\SellerDataRepository;
use App\Model\Services\ReceiptCacheService\CachedReceipt;
use Kdyby\Doctrine\EntityManager;

class ReceiptBuilder
{
   /** @var EntityManager */
   private $em;

   /** @var \Nette\Security\User */
   private $user;

   /**
    * ReceiptBuilder constructor.
    * @param EntityManager $em
    */
   public function __construct(EntityManager $em, \Nette\Security\User $user)
   {
      $this->em = $em;
      $this->user = $user;
   }

   public function createFromCachedReceipt(CachedReceipt $cachedReceipt, TypeOfDocument $type) : Receipt
   {
      /** @var Cash $cash */
      $cash = $this->em->find(Cash::class, $cachedReceipt->getCashId());

      //seller data
      /** @var SellerDataRepository $sellerDataRepo */
      $sellerDataRepo = $this->em->getRepository(SellerData::class);
      $sellerData = $sellerDataRepo->getSellerData();

      $receipt = new Receipt($type, $cash, $this->getLoggedUser(), $sellerData);

      //insert items
      foreach ($cachedReceipt->getItems() AS $item) {

         if($item->getTaxRateId()) {
            $taxRateType   = $item->getTaxRateType();
            $taxRateValue  = $item->getTaxRateValue();
            $price = round(PriceHelper::calculatePriceWithVat($taxRateValue, $item->getPrice(), false), 2);
         } else {
            $taxRateType   = TaxRateType::get(TaxRateType::_NONE_);
            $taxRateValue  = null;
            $price = $item->getPrice();
         }

         $insertedItem = $receipt->addItem(
            $item->getName(),
            $price,
            $item->getQuantity()
         );

         $insertedItem->setNote($item->getNote());

         if($item->getProductId()) {
            /** @var Product $product */
            $product = $this->em->find(Product::class, $item->getProductId());

            //reference to the product in catalogue
            $insertedItem->setProduct($product);
            $insertedItem->setMeasurementUnit(MeasurementUnitHelper::getSellUnitText($product->getMeasurementUnit()));
         }


         $insertedItem->setTaxRateType($taxRateType);
         $insertedItem->setTaxRateValue($taxRateValue);
      }

      //set receipt details
      if($type === TypeOfDocument::get(TypeOfDocument::PRO_FORMA_INVOICE) ||
         $type === TypeOfDocument::get(TypeOfDocument::TAX_INVOICE) ||
         $type === TypeOfDocument::get(TypeOfDocument::INVOICE)
      ) {
         //dates
         $receipt->setDueDate($cachedReceipt->getDueDate());

         //type of payment
         $receipt->setTypeOfPayment($cachedReceipt->getTypeOfPayment());

         if($type !== TypeOfDocument::get(TypeOfDocument::PRO_FORMA_INVOICE) &&
            $type !== TypeOfDocument::get(TypeOfDocument::INVOICE)
         ) {
            $receipt->setDateOfTaxableSupply($cachedReceipt->getDateOfTaxableSupply());
         }
      }

      //set customer details
      if ($type === TypeOfDocument::get(TypeOfDocument::PRO_FORMA_INVOICE) ||
         $type === TypeOfDocument::get(TypeOfDocument::TAX_INVOICE) ||
         $type === TypeOfDocument::get(TypeOfDocument::INVOICE) ||
         $cachedReceipt->getCustomerData()->isAnyDetailFilled()
      ) {
         //details of customer
         $cachedCustomerData = $cachedReceipt->getCustomerData();
         $customerData = $receipt->getCustomerData();

         $customerData->setName($cachedCustomerData->getName());
         $customerData->setStreet($cachedCustomerData->getStreet());
         $customerData->setCity($cachedCustomerData->getCity());
         $customerData->setZip($cachedCustomerData->getZip());
         $customerData->setVatId($cachedCustomerData->getVatId());
         $customerData->setVatNo($cachedCustomerData->getVatNo());
         $customerData->setPhone($cachedCustomerData->getPhone());
         $customerData->setEmail($cachedCustomerData->getEmail());
      }

      if($id = $cachedReceipt->getCustomerId()) {
         /** @var User $customer */
         $customer = $this->em->find(User::class, $id);

         $receipt->setCustomer($customer);
      }

      $receipt->setSellerData($sellerData);

      return $receipt;
   }

   private function getLoggedUser() : User
   {
      /** @var User $loggedUser */
      $loggedUser = $this->em->find(User::class, $this->user->getId());

      return $loggedUser;
   }
}