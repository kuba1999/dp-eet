<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

declare(strict_types=1);

namespace App\Router;

use App\Model\Helpers\ReceiptTokenHelper;
use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
    use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
       $router = new RouteList;

       //tickets
       $tickets = new RouteList("Tickets");
       $tickets[] = new Route('/admin/tickets/<presenter>/<action>[/<id>]', 'TicketList:default');

       $router[] = $tickets;

       //admin
       $admin = new RouteList("Admin");
       $admin[] = new Route('/admin/<presenter>/<action>[/<id>]', 'Dashboard:default');

       $router[] = $admin;

       //cash
       $admin = new RouteList("Cash");
       $admin[] = new Route('/cash/<presenter>/<action>[/<id>]', 'MakeSell:default');

       $router[] = $admin;

       //portal
       $portal = new RouteList("Portal");

       $portal[] = new Route('/prihlaseni', [
          'presenter' => 'Sign',
          'action'    => 'in',
       ]);

       $portal[] = new Route('/odhlaseni', [
          'presenter' => 'Sign',
          'action'    => 'out',
       ]);

       $portal[] = new Route('/ztrata-hesla', [
          'presenter' => 'Sign',
          'action'    => 'passwordRecovery',
       ]);

       $portal[] = new Route('/aktivace-uctu', [
          'presenter' => 'Sign',
          'action'    => 'accountActivation',
       ]);

       $portal[] = new Route('/<presenter>/<action>[/<id>]', [
          'action'    => [
             //Route::FILTER_STRICT => true,
             Route::VALUE => 'default',
             Route::FILTER_TABLE => [
                'dotaz'              => 'ticketDetail',
                'podat-dotaz'        => 'createTicket',
                'detail'             => 'detail',
             ],
          ],
          'presenter' => [
             //Route::FILTER_STRICT => true,
             Route::VALUE => 'Welcome',
             Route::FILTER_TABLE => [
                'vitejte'              => 'Welcome',
                'aktuality'            => 'News',
                'kontakt'              => 'Contact',
                'registrace'           => 'Registration',

                'prehled'              => 'Dashboard',
                'doklady'              => 'Receipts',
                'nastaveni'            => 'Settings',
                'casto-kladne-otazky'  => 'Faq',
                'ke-stazeni'           => 'Downloads',
                'podpora'              => 'Support',
             ],
          ],
       ]);

       $portal[] = new Route('/<receiptToken ' . ReceiptTokenHelper::URL_REGEX . '>', [
          'presenter' => 'ReceiptToken',
          'action'    => 'default',
       ]);

       $router[] = $portal;


       return $router;
   }

}
