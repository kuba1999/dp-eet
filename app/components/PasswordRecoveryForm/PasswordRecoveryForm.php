<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\PasswordRecoveryForm;


use App\Forms\BaseForm;
use App\Model\Entities\User;
use App\Model\Enums\UserRole;
use App\Model\Repositories\UserRepository;
use App\Model\Services\Mailing\UserAccountMailingService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class PasswordRecoveryForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var UserAccountMailingService */
   private $userAccountMailingService;

   public $onSuccess = [];

   function __construct(
      EntityManager $em,
      UserAccountMailingService $userAccountMailingService
   ) {
      $this->em = $em;
      $this->userAccountMailingService = $userAccountMailingService;
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->setShowFlashErrorMessage(false);


      $form->addText("email")
         ->setTranslator(null)
         ->setAttribute("placeholder", "Email")
         ->setType("username")
         ->setRequired(true)
         ->addRule(BaseForm::FILLED, "Email je povinný");

      $form->addRecaptcha("captcha");

      $form->addSubmit("reset", "Resetovat heslo")
         ->addClass("btn btn-primary");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      /** @var UserRepository $userRepo */
      $userRepo = $this->em->getRepository(User::class);

      /** @var User $user */
      $user = $userRepo->findOneBy(['email' => $form['email']->getValue()]);

      if(!$user) { //user does not exist - send fake message
         $this->onSuccess();
         return;
      }

      //only allowed for customers
      if(UserRole::isUserOfBackend($user)) {
         return;
      }

      $newPassword = $userRepo->resetPassword($user);
      $this->userAccountMailingService->sendNewPasswordAfterResetting($user, $newPassword);

      $this->onSuccess();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}