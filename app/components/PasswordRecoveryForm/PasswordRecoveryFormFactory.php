<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\RecoveryPasswordForm;


use App\Components\PasswordRecoveryForm\PasswordRecoveryForm;
use App\Model\Services\Mailing\UserAccountMailingService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;

class PasswordRecoveryFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var UserAccountMailingService */
   private $userAccountMailingService;

   function __construct(
      EntityManager $em,
      UserAccountMailingService $userAccountMailingService
   ) {
      $this->em = $em;
      $this->userAccountMailingService = $userAccountMailingService;
   }

   /**
    * @return PasswordRecoveryForm
    */
   public function create() : PasswordRecoveryForm
   {
      return new PasswordRecoveryForm($this->em, $this->userAccountMailingService);
   }
}