<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\Breadcrumb;


use Kdyby\Translation\Translator;

class BreadcrumbFactory
{
   public function create() : Breadcrumb
   {
      $component = new Breadcrumb();

      return $component;
   }
}