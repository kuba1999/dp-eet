<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\Breadcrumb;


use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Utils\ArrayList;
use Nette\Utils\Strings;

class Breadcrumb extends Control
{
   /** @var ArrayList|Item[] */
   private $items;

   /** @var bool */
   private $hidden = false;


   public function __construct()
   {
      $this->items = new ArrayList();
   }

   /**
    * @param $name
    * @param $link
    * @param array $args
    * @param null $subName
    * @return Item
    */
   public function add($name, $link = null, $args = [], $subName = null) : Item
   {
      $item = new Item($this, $name, $link, $args, $subName);

      if(Strings::contains($name, "|")) {
         list($name, $subName) = explode("|", $name);

         $item->setName($name);
         $item->setSubName($subName);
      }

      $this->items[] = $item;

      return $item;
   }

   /**
    * @return mixed|null
    */
   public function getLast()
   {
      if(!$this->items->count()) {
         return null;
      }

      return $this->items[$this->items->count() - 1];
   }

   /**
    * Hide component
    */
   public function hide()
   {
      $this->hidden = true;
   }

   /**
    * @return bool
    */
   public function isHidden() : bool
   {
      return $this->hidden;
   }

   public function render()
   {
      $tpl = $this->template;

      $tpl->setFile(__DIR__ . "/template.latte");
      $tpl->items = $this->items;
      $tpl->hidden = $this->hidden;

      return $tpl->render();
   }
}