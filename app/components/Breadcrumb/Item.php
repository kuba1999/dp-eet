<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\Breadcrumb;


class Item
{
   /** @var String */
   private $name;

   /** @var String|null */
   private $subName;

   /** @var String|callable */
   private $link;

   /** @var String */
   private $icon;

   /**
    * @var Breadcrumb
    */
   private $breadcrumb;

   /**
    * Item constructor.
    * @param Breadcrumb $breadcrumb
    * @param String $name
    * @param null $link
    * @param array $args
    * @param string|null $subName
    */
   public function __construct(Breadcrumb $breadcrumb, String $name, $link = null, $args = [], $subName = null)
   {
      $this->name = $name;
      $this->link = $link;
      $this->breadcrumb = $breadcrumb;
      $this->subName = $subName;
   }

   /**
    * @return String
    */
   public function getName(): String
   {
      return $this->name;
   }

   /**
    * @param String $name
    */
   public function setName(String $name)
   {
      $this->name = $name;
   }

   /**
    * @return String
    */
   public function getSubName(): ?String
   {
      return $this->subName;
   }

   /**
    * @param String $subName
    */
   public function setSubName(String $subName)
   {
      $this->subName = $subName;
   }

   /**
    * @return callable|String
    */
   public function getLink()
   {
      return $this->link;
   }

   /**
    * @param callable|String $link
    */
   public function setLink($link)
   {
      $this->link = $link;
   }

   /**
    * @return String
    */
   public function getIcon(): String
   {
      return $this->icon;
   }

   /**
    * @param String $icon
    */
   public function setIcon(String $icon)
   {
      $this->icon = $icon;
   }
}