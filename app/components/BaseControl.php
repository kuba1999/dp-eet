<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */

namespace App\Components;

use App\Components\FlashMessage\FlashMessage;
use App\Components\FlashMessage\FlashMessageNotify;
use Nette\Application\UI\Control;

class BaseControl extends Control
{
   protected function attached($presenter)
   {
      parent::attached($presenter);

      $this['flashMessage']->redrawControl();
      $this['flashMessageNotify']->redrawControl();
   }

   /**
    * @return FlashMessage
    */
   public function createComponentFlashMessage() : FlashMessage
   {
      return new FlashMessage();
   }

   /**
    * @return FlashMessageNotify
    */
   public function createComponentFlashMessageNotify() : FlashMessageNotify
   {
      return new FlashMessageNotify();
   }

   /**
    * @param $message
    * @param string $type
    * @param null $title
    * @return void
    */
   public function flashMessage($message, $type = FlashMessage::TYPE_INFO, $title = null)
   {
      $this['flashMessage']->addMessage($message, $type, $title);
   }

   /**
    * @param $message
    * @param string $type
    * @param null $title
    */
   public function flashMessageNotify($message, $type = FlashMessage::TYPE_INFO, $title = null)
   {
      $this['flashMessageNotify']->addMessage($message, $type, $title);
   }
}