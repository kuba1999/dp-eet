<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\FlashMessage;


class Message
{
   /** @var String */
   private $title;

   /** @var String */
   private $message;

   /** @var String */
   private $type;

   function __construct(String $title, String $message = null, String $type)
   {
      $this->title = $title;
      $this->message = $message;
      $this->type = $type;
   }

   /**
    * @param bool $formatting
    * @return String
    */
   public function getTitle(bool $formatting = false): String
   {
      $title = $this->title;

      if($formatting) {
         $title = preg_replace("/\*(.+)\*/i", "<span class='text-black'>$1</span>", $title);
      }

      return $title;
   }

   /**
    * @param String $title
    */
   public function setTitle(String $title)
   {
      $this->title = $title;
   }

   /**
    * @return String
    */
   public function getMessage(): ?String
   {
      return $this->message;
   }

   /**
    * @param String $message
    */
   public function setMessage(String $message)
   {
      $this->message = $message;
   }

   /**
    * @return String
    */
   public function getType(): String
   {
      return $this->type;
   }

   /**
    * @param String $type
    */
   public function setType(String $type)
   {
      $this->type = $type;
   }
}