<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\FlashMessage;


use Kdyby\Translation\Translator;

class FlashMessageNotify extends FlashMessage
{
   /** @var string */
   private $templateFile = __DIR__ . "/templateNotify.latte";

   public function render()
   {
      $tpl = $this->template;

      $tpl->setFile($this->templateFile);

      return $tpl->render();
   }
}