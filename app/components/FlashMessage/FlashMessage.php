<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\FlashMessage;


use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class FlashMessage extends Control
{
   const TYPE_SUCCESS   = 'success';
   const TYPE_WARNING   = 'warning';
   const TYPE_DANGER    = 'danger';
   const TYPE_INFO      = 'info';

   public static $predefinedTitles = [
      self::TYPE_SUCCESS   => 'Úspěch',
      self::TYPE_WARNING   => 'Upozornění',
      self::TYPE_DANGER    => 'Chyba',
      self::TYPE_INFO      => 'Informace',
   ];

   public static $icons = [
      self::TYPE_SUCCESS   => 'icon fas fa-check',
      self::TYPE_WARNING   => 'icon fas fa-exclamation-triangle',
      self::TYPE_DANGER    => 'icon fas fa-ban',
      self::TYPE_INFO      => 'icon fas fa-info',
   ];

   /** @var string */
   private $templateFile = __DIR__ . "/template.latte";

   public function addMessage(String $message, String $type, String $title = null)
   {
      if(empty($title)) {
         $title = $message;
         $message = null;
      }

      $id = $this->getParameterId('flash');
      $messages = $this->getPresenter()->getFlashSession()->$id;
      $messages[] = $message = new Message(
         $title,
         $message,
         $type
      );
      $this->getTemplate()->flashes = $messages;
      $this->getPresenter()->getFlashSession()->$id = $messages;

      $this->redrawControl();

      return $message;
   }

   public function render()
   {
      $tpl = $this->template;

      $tpl->setFile($this->templateFile);

      return $tpl->render();
   }

   /**
    * @return string
    */
   public function getTemplateFile(): string
   {
      return $this->templateFile;
   }

   /**
    * @param string $templateFile
    */
   public function setTemplateFile(string $templateFile)
   {
      $this->templateFile = $templateFile;
   }
}