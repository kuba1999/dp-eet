<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Column;


use App\Components\DataGrid\Row;

class ColumnDateTime extends FilterableColumn
{
   const FORMAT_DATE         =  "date";
   const FORMAT_TIME         =  "time";
   const FORMAT_DATETIME     =  "dateTime";

   public static $dateFormat = "j. n. Y";
   public static $timeFormat = "H:i";

   /** @var string */
   private $format = self::FORMAT_DATETIME;

   public function getColumnValue(Row $row)
   {
      /** @var \DateTime $date */
      $date = parent::getColumnValue($row);

      if(!$date instanceof \DateTime) {
         try{
            $date = new \DateTime($date);
         }catch (\Exception $e) {
            return $date;
         }
      }

      switch ($this->format) {
         case self::FORMAT_DATE:
            return $date->format(self::$dateFormat);
         case self::FORMAT_TIME:
            return $date->format(self::$timeFormat);
         case self::FORMAT_DATETIME:
            return $date->format(self::$dateFormat) . " " . $date->format(self::$timeFormat);
      }
   }

   /**
    * @return string
    */
   public function getDateFormat(): string
   {
      return self::$dateFormat;
   }

   /**
    * @param string $dateFormat
    * @return $this
    */
   public function setDateFormat(string $dateFormat)
   {
      self::$dateFormat = $dateFormat;

      return $this;
   }

   /**
    * @return string
    */
   public function getTimeFormat(): string
   {
      return self::$timeFormat;
   }

   /**
    * @param string $timeFormat
    * @return $this
    */
   public function setTimeFormat(string $timeFormat)
   {
      self::$timeFormat = $timeFormat;

      return $this;
   }

   /**
    * @return string
    */
   public function getFormat(): string
   {
      return $this->format;
   }

   /**
    * @param string $format
    * @return $this
    */
   public function setFormat(string $format)
   {
      $this->format = $format;

      return $this;
   }
}