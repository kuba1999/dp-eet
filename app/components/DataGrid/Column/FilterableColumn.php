<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Column;


use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Filter\Filter;
use App\Components\DataGrid\Filter\FilterDate;
use App\Components\DataGrid\Filter\FilterDateTime;
use App\Components\DataGrid\Filter\FilterNumber;
use App\Components\DataGrid\Filter\FilterSelect;
use App\Components\DataGrid\Filter\FilterText;
use App\Components\DataGrid\Row;
use Kdyby\Translation\Translator;
use Nette\SmartObject;
use Nette\Utils\Callback;

class FilterableColumn
{
   use SmartObject;

   /** @var DataGrid */
   protected $dataGrid;

   /** @var String */
   protected $key;

   /** @var String */
   protected $title;

   /** @var bool */
   protected $sortable = true;

   /** @var Callback */
   protected $renderer;

   /** @var mixed|Callback */
   protected $valueProvider;

   /** @var array */
   protected $replacement;

   /** @var string */
   protected $template;

   /** @var int */
   protected $width;

   /** @var array */
   protected $cellStyle;

   /** @var array */
   protected $cellClass;

   /** @var bool */
   private $virtualColumn = false;

   /**
    * Column constructor.
    * @param DataGrid $dataGrid
    * @param String $key
    * @param String|NULL $title Title of the column
    */
   public function __construct(DataGrid $dataGrid, String $key, String $title = NULL)
   {
      $this->dataGrid = $dataGrid;
      $this->key = $key;
      $this->title = $title;
      $this->cellStyle = [];
      $this->cellClass = [];

      $dataGrid->attachColumn($this);
   }

   /**
    * @return String
    */
   public function getKey(): String
   {
      return $this->key;
   }

   /**
    * @param String $key
    * @return $this
    */
   public function setKey(String $key)
   {
      $this->key = $key;

      return $this;
   }

   /**
    * @return String
    */
   public function getTitle(): String
   {
      return $this->title;
   }

   /**
    * @param String $title
    * @return $this
    */
   public function setTitle(String $title)
   {
      $this->title = $title;

      return $this;
   }

   /**
    * @return bool
    */
   public function isSortable(): bool
   {
      return $this->sortable;
   }

   /**
    * @param bool $sortable
    * @return $this
    */
   public function setSortable(bool $sortable)
   {
      $this->sortable = $sortable;

      return $this;
   }

   /**
    * @param callable $renderer
    * @return $this
    */
   public function setRenderer(callable $renderer)
   {
      $this->renderer = $renderer;

      return $this;
   }

   /**
    * Get column value of row
    * @param Row $row
    * @return mixed
    */
   public function getColumnValue(Row $row)
   {
      if($this->valueProvider) {
         if(is_callable($this->valueProvider)) {
            return call_user_func($this->valueProvider, $row->getItem(), $row);
         }

         return $this->valueProvider;
      }

      $value = $row->getValue($this->getKey());

      if(!is_object($value) && isset($this->replacement[$value])) {
         $value = $this->replacement[$value];
      }

      return $value;
   }

   /**
    * Render column
    * @param Row $row actual row
    * @return mixed
    */
   public function render(Row $row)
   {
      if(is_callable($this->renderer)) {
         return call_user_func($this->renderer, $row->getItem(), $row);
      }

      return $this->getColumnValue($row);
   }

   /**
    * Does column have filter
    * @return bool
    */
   public function hasFilter() : bool
   {
      return !is_null($this->getFilter());
   }

   /**
    * Get column filter
    * @return Filter|null
    */
   public function getFilter() : ?Filter
   {
      if($filter = $this->dataGrid->getFilter($this)) {
         return $filter;
      }

      return null;
   }

   /**
    * @return array
    */
   public function getReplacement(): array
   {
      return $this->replacement;
   }

   /**
    * @param array $replacement
    * @return FilterableColumn
    */
   public function setReplacement(array $replacement) : FilterableColumn
   {
      $this->replacement = $replacement;

      return $this;
   }

   /**
    * @return string
    */
   public function getTemplate(): ?string
   {
      return $this->template;
   }

   /**
    * @param string $template
    */
   public function setTemplate(string $template)
   {
      $this->template = $template;
   }

   /**
    * @return int
    */
   public function getWidth(): ?int
   {
      return $this->width;
   }

   /**
    * @param int $width
    * @return $this
    */
   public function setWidth(int $width)
   {
      $this->width = $width;

      return $this;
   }

   /**
    * @return Translator
    */
   public function getTranslator(): Translator
   {
      return $this->translator;
   }

   /**
    * @param Translator $translator
    */
   public function setTranslator(Translator $translator)
   {
      $this->translator = $translator;
   }

   /**
    * @return callable|string
    */
   public function renderCellStyle(Row $row)
   {
      $styles = [];
      foreach ($this->cellStyle AS $style) {
         if(is_callable($style)) {
            $styles[] = call_user_func($style, $row->getItem(), $row);
            continue;
         }

         $styles[] = $style;
      }

      return implode(";", $styles);
   }

   /**
    * @return array
    */
   public function getCellStyle() : array
   {
      return $this->cellStyle;
   }

   /**
    * @param callable|string $cellStyle
    * @return FilterableColumn
    */
   public function addCellStyle($cellStyle) : self
   {
      $this->cellStyle[] = $cellStyle;

      return $this;
   }

   /**
    * @return callable|string
    */
   public function renderCellClass(Row $row)
   {
      $classes = [];
      foreach ($this->cellClass AS $class) {
         if(is_callable($class)) {
            $classes[] = call_user_func($class, $row->getItem(), $row);
            continue;
         }

         $classes[] = $class;
      }

      return implode(" ", $classes);
   }

   /**
    * @return array
    */
   public function getCellClass() : array
   {
      return $this->cellClass;
   }

   /**
    * @param callable|string $cellStyle
    * @return FilterableColumn
    */
   public function addCellClass($cellStyle) : self
   {
      $this->cellClass[] = $cellStyle;

      return $this;
   }

   /**
    * @return callable|mixed
    */
   public function getValueProvider()
   {
      return $this->valueProvider;
   }

   /**
    * @param callable|mixed $valueProvider
    * @return FilterableColumn
    */
   public function setValueProvider($valueProvider) : self
   {
      $this->valueProvider = $valueProvider;

      return $this;
   }

   /**
    * @return bool
    */
   public function isVirtualColumn(): bool
   {
      return $this->virtualColumn;
   }

   /**
    * @param bool $virtualColumn
    * @return FilterableColumn
    */
   public function setVirtualColumn(bool $virtualColumn = true) : self
   {
      $this->virtualColumn = $virtualColumn;

      return $this;
   }

   /*****************************************************
    * Filters
    *****************************************************/

   /**
    * @param array $searchScope
    * @return FilterableColumn
    */
   public function setFilterText(array $searchScope = null) : FilterableColumn
   {
      $filter = new FilterText($this->dataGrid, $this->getKey(), $searchScope);
      $this->dataGrid->attachFilter($filter);

      return $this;
   }

   /**
    * @param array $searchScope
    * @return FilterableColumn
    */
   public function setFilterNumber(array $searchScope = null) : FilterableColumn
   {
      $filter = new FilterNumber($this->dataGrid, $this->getKey(), $searchScope);
      $this->dataGrid->attachFilter($filter);

      return $this;
   }

   /**
    * @param array $items
    * @param array $searchScope
    * @return FilterableColumn
    */
   public function setFilterSelect(array $items, array $searchScope = null) : FilterableColumn
   {
      $filter = new FilterSelect($this->dataGrid, $this->getKey(), $items, $searchScope);
      $this->dataGrid->attachFilter($filter);

      return $this;
   }

   /**
    * @param array $searchScope
    * @return FilterableColumn
    */
   public function setFilterDate(array $searchScope = []) : FilterableColumn
   {
      $filter = new FilterDate($this->dataGrid, $this->getKey(), $searchScope);
      $this->dataGrid->attachFilter($filter);

      return $this;
   }

   /*
    public function setFilterDateTime(array $searchScope = []) : FilterableColumn
   {
      $filter = new FilterDateTime($this->dataGrid, $this->getKey(), $searchScope);
      $this->dataGrid->attachFilter($filter);

      return $this;
   }
    */
}