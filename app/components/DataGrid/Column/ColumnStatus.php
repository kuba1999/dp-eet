<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Column;


use App\Components\DataGrid\Column\Status\Option;
use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Row;
use Consistence\Enum\Enum;

class ColumnStatus extends FilterableColumn
{
   /** @var Option[] */
   private $options;

   /** @var callable */
   public $onChange;

   /**
    * ColumnStatus constructor.
    * @param DataGrid $dataGrid
    * @param String $key
    * @param null $title
    */
   public function __construct(DataGrid $dataGrid, $key, $title = NULL)
   {
      parent::__construct($dataGrid, $key, $title);

      $this->setTemplate(__DIR__ . "/Status/template.latte");
   }

   /**
    * @param string|Enum $value
    * @param string $title
    * @param string|null $class
    * @return ColumnStatus
    */
   public function addOption($value, string $title, string $class = null) : ColumnStatus
   {
      $this->options[$value] = new Option($this, $value, $title, $class);

      return $this;
   }


   public function getActiveOption(Row $row) : Option
   {
      $value = $this->getColumnValue($row);

      if($value instanceof Enum) {
         /** @var Enum $value */
         $value = $value->getValue();
      }

      return $this->options[$value];
   }

   /**
    * @return Option[]
    */
   public function getOptions(): array
   {
      return $this->options;
   }

   /**
    * @param Option[] $options
    * @return $this
    */
   public function setOptions(array $options)
   {
      $this->options = $options;

      return $this;
   }
}