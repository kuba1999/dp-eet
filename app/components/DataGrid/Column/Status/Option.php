<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Column\Status;


use App\Components\DataGrid\Column\ColumnStatus;

class Option
{
   /** @var string */
   private $value;

   /** @var string */
   private $title;

   /** @var string */
   private $class;

   function __construct(ColumnStatus $column, string $value, string $title, string $class = null)
   {
      $this->value = $value;
      $this->title = $title;
      $this->class = $class;
   }

   /**
    * @return string
    */
   public function getValue(): string
   {
      return $this->value;
   }

   /**
    * @param string $value
    */
   public function setValue(string $value)
   {
      $this->value = $value;
   }

   /**
    * @return string
    */
   public function getTitle(): string
   {
      return $this->title;
   }

   /**
    * @param string $title
    */
   public function setTitle(string $title)
   {
      $this->title = $title;
   }

   /**
    * @return string
    */
   public function getClass(): string
   {
      return $this->class;
   }

   /**
    * @param string $class
    */
   public function setClass(string $class)
   {
      $this->class = $class;
   }
}