<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Column;


use App\Components\DataGrid\Row;

class ColumnNumber extends FilterableColumn
{
   /** @var string|callable */
   private $valuePrefix;

   /** @var string|callable */
   private $valueSuffix;

   protected $numberFormat = [
      0, //decimals
      '.', //decimal point
      ' ', //thousands point
   ];

   /**
    * Get column value of row
    * @param Row $row
    * @return mixed
    */
   public function getColumnValue(Row $row)
   {
      $value = parent::getColumnValue($row);

      if(!is_numeric($value)) {
         $ret = $value;
      } else {
         $ret = number_format(
            $value,
            $this->numberFormat[0],
            $this->numberFormat[1],
            $this->numberFormat[2]
         );
      }

      return $this->renderValuePrefix($row) . $ret . $this->renderValueSuffix($row);
   }

   /**
    * Set number format
    * @param int $decimals
    * @param int $decimalPoint
    * @param string $thousandsPoint
    * @return $this
    */
   public function setFormat($decimals = 0, $decimalPoint = '.', $thousandsPoint = ' ') : self
   {
      $this->numberFormat = [$decimals, $decimalPoint, $thousandsPoint];

      return $this;
   }

   /**
    * @param int $decimals
    * @return ColumnNumber
    */
   public function setFormatDecimals(int $decimals) : self
   {
      $this->numberFormat[0] = $decimals;

      return $this;
   }

   /**
    * @return array
    */
   public function getFormat() : array
   {
      return $this->numberFormat;
   }

   /**
    * @return string
    */
   public function getValuePrefix(): string
   {
      return $this->valuePrefix;
   }

   /**
    * @param string|callable $valuePrefix
    * @return ColumnNumber
    */
   public function setValuePrefix($valuePrefix) : self
   {
      $this->valuePrefix = $valuePrefix;

      return $this;
   }

   /**
    * @return string
    */
   public function getValueSuffix(): string
   {
      return $this->valueSuffix;
   }

   /**
    * @param string|callable $valueSuffix
    * @return ColumnNumber
    */
   public function setValueSuffix($valueSuffix) : self
   {
      $this->valueSuffix = $valueSuffix;

      return $this;
   }

   /**
    * @param Row $row
    * @return null|string
    */
   private function renderValuePrefix(Row $row) : ?string
   {
      if(is_callable($this->valuePrefix)) {
         return call_user_func($this->valuePrefix, $row->getItem(), $row);
      }

      return $this->valuePrefix;
   }

   /**
    * @param Row $row
    * @return null|string
    */
   private function renderValueSuffix(Row $row) : ?string
   {
      if(is_callable($this->valueSuffix)) {
         return call_user_func($this->valueSuffix, $row->getItem(), $row);
      }

      return $this->valueSuffix;
   }
}