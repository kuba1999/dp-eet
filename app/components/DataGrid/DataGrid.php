<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid;


use App\Components\DataGrid\Action\RowAction;
use App\Components\DataGrid\Column\ColumnDateTime;
use App\Components\DataGrid\Column\ColumnNumber;
use App\Components\DataGrid\Column\ColumnStatus;
use App\Components\DataGrid\Column\ColumnText;
use App\Components\DataGrid\Column\FilterableColumn;
use App\Components\DataGrid\Filter\Filter;
use App\Components\DataGrid\Filter\FilterDate;
use App\Components\DataGrid\Filter\FilterDateRangeButton;
use App\Components\DataGrid\Filter\FilterDateTime;
use App\Components\DataGrid\Filter\FilterSelect;
use App\Components\DataGrid\Filter\FilterText;
use App\Components\DataGrid\Utils\Sorting;
use App\Forms\BaseForm;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Utils\Paginator;


class DataGrid extends Control
{
   /** @var array @persistent */
   public $sorting = [];

   /** @var DataModel */
   private $dataModel;

   /** @var FilterableColumn[] */
   private $columns = [];

   /** @var RowAction[] */
   private $rowActions = [];

   /** @var Filter[] */
   public $filters = [];
   /** @var String */
   private $primaryKey = "id";

   /** @var bool */
   private $multipleSorting = false;

   /** @var array @persistent */
   public $filterValues = [];

   /** @var int @persistent */
   public $itemsPerPage = 10;

   /** @var int @persistent */
   public $page;

   /** @var array */
   private $defaultValues = [];

   /** @var Sorting */
   private $defaultSorting = [];

   /** @var Translator */
   private $translator;


   public function __construct()
   {

   }

   protected function attached($presenter)
   {
      parent::attached($presenter);

      $this->attachFilters($this['gridForm-filters']);
   }

   /**
    * @param $source
    */
   public function setDataSource($source)
   {
      $this->dataModel = new DataModel($this, $source, $this->primaryKey);
   }

   public function getFilteredData() : array
   {
      $items = $this->dataModel->filterData(new Sorting($this->sorting), $this->filters, $this['paginator']->getPaginator(), $this->itemsPerPage);

      $rows = [];
      foreach ($items as $item) {
         $rows[] = new Row($this, $item);
      }

      return $rows;
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/templates/datagrid.latte");
      $tpl->rows = $this->getFilteredData();
      $tpl->translator = $this->translator;

      return $tpl->render();
   }

   /**
    * @return String
    */
   public function getPrimaryKey(): String
   {
      return $this->primaryKey;
   }

   /**
    * @param String $primaryKey
    */
   public function setPrimaryKey(String $primaryKey)
   {
      $this->primaryKey = $primaryKey;
   }


   public function setTranslator(Translator $translator)
   {
      $this->translator = $translator;
   }


   /***************************************************************************
    *  Grid form
    ***************************************************************************/

   public function createComponentGridForm()
   {
      $form = new BaseForm();
      $form->getElementPrototype()->class[] = "ajax";

      $form->addSelect("itemsPerPage", null, [
         1 => 1,
         10 => 10,
         20 => 20,
         50 => 50,
         null => "-"
      ])
         ->setDefaultValue($this->itemsPerPage)
         ->setAttribute('data-autosubmit-change', TRUE);
      $form->addContainer("filters");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      if(empty($form['itemsPerPage']->getValue())) {
         $this->itemsPerPage = null;
      } else {
         $this->itemsPerPage = $form['itemsPerPage']->getValue();
      }
      $this->getPaginator()->setPage(0);

      $this->presenter->template->url = $this->link("this");

      $this->redrawControl("columnSorting");
      $this->redrawControl("body");
      $this->redrawControl("footer");
      $this->redrawControl("resetButton");
      $this->redrawControl("resetButton2");
   }

   public function isShowFilterResetButton() : bool
   {
      foreach ($this->filters AS $filter) {
         if($filter->isActive(true)) {
            return true;
         }
      }

      return false;
   }

   public function isShowResetButton() : bool
   {
      if($this->isShowFilterResetButton()) {
         return true;
      }

      if($this->sorting != $this->defaultSorting) {
         return true;
      }

      if($this->itemsPerPage != 10) {
         return true;
      }

      return false;
   }

   /***************************************************************************
    *  Columns
    ***************************************************************************/

   /**
    * Add new column text
    * @param String $key Key
    * @param String $title Title
    * @return ColumnText
    */
   public function addColumnText(String $key, String $title) :  ColumnText
   {
      $column = new ColumnText($this, $key, $title);

      return $column;
   }

   /**
    * Add new column number
    * @param String $key Key
    * @param String $title Title
    * @return ColumnNumber
    */
   public function addColumnNumber(String $key, String $title) :  ColumnNumber
   {
      $column = new ColumnNumber($this, $key, $title);

      return $column;
   }

   /**
    * Add new column datetime
    * @param String $key Key
    * @param String $title Title
    * @return ColumnDateTime
    */
   public function addColumnDateTime(String $key, String $title) :  ColumnDateTime
   {
      $column = new ColumnDateTime($this, $key, $title);

      return $column;
   }

   /**
    * Add new column status
    * @param String $key Key
    * @param String $title Title
    * @return ColumnStatus
    */
   public function addColumnStatus(String $key, String $title) :  ColumnStatus
   {
      $column = new ColumnStatus($this, $key, $title);

      return $column;
   }

   /**
    * @param FilterableColumn $column
    */
   public function attachColumn(FilterableColumn $column)
   {
      $this->columns[$column->getKey()] = $column;
   }

   /**
    * @return FilterableColumn[]
    */
   public function getColumns() : array
   {
      return $this->columns;
   }

   public function getColumn(string $key) : ?FilterableColumn
   {
      if(!isset($this->columns[$key])) {
         return null;
      }

      return $this->columns[$key];
   }

   /***************************************************************************
    *  Filters
    ***************************************************************************/

   /**
    * @param $key
    * @param null $searchScope
    * @return FilterText
    */
   public function addFilterText($key, array $searchScope = []) : FilterText
   {
      $filter = new FilterText($this, $key, $searchScope);
      $this->attachFilter($filter);

      return $filter;
   }

   /**
    * @param $key
    * @param array $items
    * @param null $searchScope
    * @return FilterSelect
    */
   public function addFilterSelect($key, array $items, array $searchScope = []) : FilterSelect
   {
      $filter = new FilterSelect($this, $key, $items, $searchScope);
      $this->attachFilter($filter);

      return $filter;
   }

   /**
    * @param $key
    * @param null $searchScope
    * @return FilterDateTime
    */
   /*public function addFilterDateTime($key, array $searchScope = []) : FilterDateTime
   {
      $filter = new FilterDateTime($this, $key, $searchScope);
      $this->attachFilter($filter);

      return $filter;
   }*/

   /**
    * @param $key
    * @param null $searchScope
    * @return FilterDate
    */
   public function addFilterDate($key, array $searchScope = []) : FilterDate
   {
      $filter = new FilterDate($this, $key, $searchScope);
      $this->attachFilter($filter);

      return $filter;
   }

   /**
    * @param $key
    * @param null $searchScope
    * @return FilterDateRangeButton
    */
   /*public function addFilterDateRangeButton($key, $searchScope = NULL) : FilterDateRangeButton
   {
      $filter = new FilterDateRangeButton($this, $key, $searchScope);
      $this->filters[$key] = $filter;

      return $filter;
   }*/

   /**
    * @return Filter[]
    */
   public function getFilters() : array
   {
      return $this->filters;
   }

   /**
    * @return Filter|null
    */
   public function getFilter(FilterableColumn $column) : ?Filter
   {
      return isset($this->filters[$column->getKey()]) ? $this->filters[$column->getKey()] : null;
   }

   private function attachFilters(\Nette\Forms\Container $container)
   {
      /** @var Filter $filter */
      foreach ($this->filters as $filter) {
         $filter->attachToForm($container);
      }
   }

   /**
    * @param Filter $filter
    */
   public function attachFilter(Filter $filter)
   {
      $this->filters[$filter->getKey()] = $filter;
   }


   public function setFilterDefaults(array $defaultValues)
   {
      $this->defaultValues = $defaultValues;

      foreach ($defaultValues AS $key => $defaultValue) {
         $this->filters[$key]->setDefaultValue($defaultValue);
      }
   }

   public function handleReset()
   {
      $this->filterValues = null;
      $this->itemsPerPage = 10;

      /** @var Filter $filter */
      foreach ($this->filters as $filter) {
         $filter->reset();
      }

      //sorting
      $this->sorting = $this->defaultSorting;

      $this->redrawControl();
   }

   /***************************************************************************
    *  SORTING
    ***************************************************************************/
   /**
    * Sorting by column
    * @param $columnName
    */
   public function handleSort($columnName)
   {
      if(isset($this->sorting[$columnName])) { //sorting by existing column
         if($this->sorting[$columnName] == Sorting::SORT_ASC) {
            $this->sorting[$columnName] = Sorting::SORT_DESC;
         } else {
            unset($this->sorting[$columnName]); //disable sorting by this column
         }
      } else { //sorting by new column
         $this->sorting[$columnName] = Sorting::SORT_ASC;
      }

      if(!$this->multipleSorting && count($this->sorting) > 1) {
         if(isset($this->sorting[$columnName])) {
            $tmp = $this->sorting[$columnName];
            $this->sorting = [];
            $this->sorting[$columnName] = $tmp;
         } else {
            $this->sorting = [];
         }
      }

      $this->redrawControl();
   }

   /**
    * @param String $columnKey
    * @return bool
    */
   public function isSortingBy(String $columnKey) : bool
   {
      return isset($this->sorting[$columnKey]);
   }

   /**
    * @param String $columnKey
    * @return String
    */
   public function getSortingType(String $columnKey)
   {
      return $this->sorting[$columnKey];
   }

   /**
    * @return bool
    */
   public function isMultipleSorting(): bool
   {
      return $this->multipleSorting;
   }

   /**
    * @param bool $multipleSorting
    */
   public function setMultipleSorting(bool $multipleSorting)
   {
      $this->multipleSorting = $multipleSorting;
   }

   /**
    * @param string $column
    * @param string $order
    */
   public function addDefaultSorting(string $column, string $order)
   {
      $this->defaultSorting[$column] = $order;
      $this->sorting[$column] = $order;
   }

   /**
    * @param array $default
    */
   public function setDefaultSorting(array $default)
   {
      $this->defaultSorting = $default;
      $this->sorting = $default;
   }

   /***************************************************************************
    *  Action
    ***************************************************************************/

   /**
    * @param string $key
    * @param string $title
    * @param string $href
    * @param array|null $params
    * @return RowAction
    */
   public function addAction(string $key, string $title, string $href = null, array $params = []) : RowAction
   {
      $action = new RowAction($this, $key, $title, $href, $params);
      $this->rowActions[$key] = $action;

      return $action;
   }

   /**
    * @return array
    */
   public function getRowActions() : array
   {
      return $this->rowActions;
   }


   /***************************************************************************
    *  Column signals
    ***************************************************************************/
   public function handleChangeColumnStatus($columnKey, $id, $value)
   {
      /** @var ColumnStatus $column */
      $column = $this->getColumn($columnKey);
      $item = $this->dataModel->getItem($this->getPrimaryKey(), $id);

      $column->onChange($item, $value);

      $this->redrawControl("body");
   }

   public function handleColumnRowAction($rowActionKey, $id)
   {
      /** @var RowAction $rowAction */
      $rowAction = $this->rowActions[$rowActionKey];
      $item = $this->dataModel->getItem($this->getPrimaryKey(), $id);

      $rowAction->onClick($item);

      $this->redrawControl("body");
   }


   /***************************************************************************
    *  Paginating
    ***************************************************************************/

   public function createComponentPaginator() : \IPub\VisualPaginator\Components\Control
   {
      $control = new \IPub\VisualPaginator\Components\Control();
      $control->setTemplateFile(__DIR__ . "/templates/paginator.latte");

      if($this->translator) {
         $control->setTranslator($this->translator);
      }

      $control->onShowPage[] = function ($component, $page) {
         $this->redrawControl("dataGrid");
      };

      return $control;
   }

   public function getPaginator() : Paginator
   {
      /** @var \IPub\VisualPaginator\Components\Control $paginator */
      $paginator = $this['paginator'];

      return $paginator->getPaginator();
   }

   /***************************************************************************
    *  FOOTER - count items
    ***************************************************************************/
   public function getShowingItemsFrom() : int
   {
      if(!$this->getPaginator()->getLength()) {
         return 0;
      }

      return $this->getPaginator()->getOffset() + 1;
   }

   public function getShowingItemsTo() : int
   {
      if(!$this->getPaginator()->getLength()) {
         return 0;
      }

      return $this->getPaginator()->getLength() + $this->getShowingItemsFrom() - 1;
   }

   public function getShowingItemsTotal() : int
   {
      return $this->getPaginator()->getItemCount();
   }
}