<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid;


class DataGridFactory
{
   /**
    * @return DataGrid
    */
   public function create() : DataGrid
   {
      return new DataGrid();
   }
}