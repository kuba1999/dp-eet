<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;


use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;

class FilterNumber extends Filter
{
   protected $template = "filterText";

   /** @var TextInput */
   private $input;

   public function attachToForm(Container $container)
   {
      $container = $container->addContainer($this->getContainerName());

      $this->input = $container->addText("value")
                        ->setAttribute('data-autosubmit', TRUE)
                        ->setDefaultValue($this->getDefaultValueForForm())
                        ->addClass("input-sm");
   }

   public function getValue()
   {
      if(strlen(trim($this->input->getValue())) === 0) {
         return null;
      }

      return $this->input->getValue();
   }

   public function reset()
   {
      $this->input->setValue($this->getDefaultValue());
   }
}