<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;


use Nette\Forms\Container;

class FilterDateRangeButton extends FilterDateTimeBase
{
   protected $template = "filterDateRangeButton";

   public function attachToForm(Container $container)
   {
      $container = $container->addContainer($this->getContainerName());

      $container->addHidden("start");
      $container->addHidden("end");
   }
}