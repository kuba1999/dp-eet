<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;


use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;

class FilterText extends Filter
{
   protected $template = "filterText";

   /** @var TextInput */
   private $input;

   /** @var bool */
   private $exact = true;

   public function attachToForm(Container $container)
   {
      $container = $container->addContainer($this->getContainerName());

      $this->input = $container->addText("value")
                        ->setAttribute('data-autosubmit', TRUE)
                        ->setDefaultValue($this->getDefaultValueForForm())
                        ->addClass("input-sm");


      $this->input->getForm()->onSubmit[] = function () {
         $this->dataGrid->filterValues[$this->getKey()] = $this->input->getValue();
      };
   }

   public function getValue()
   {
      if(strlen(trim($this->input->getValue())) === 0) {
         return null;
      }

      return $this->input->getValue();
   }

   public function reset()
   {
      $this->input->setValue($this->getDefaultValue());
   }

   /**
    * @return bool
    */
   public function isExact(): bool
   {
      return $this->exact;
   }

   /**
    * @param bool $exact
    */
   public function setExact(bool $exact): void
   {
      $this->exact = $exact;
   }
}