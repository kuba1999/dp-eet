<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;


use App\Components\DataGrid\DataGrid;
use Nette\Forms\Container;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;

class FilterSelect extends Filter
{
   protected $template = "filterSelect";

   /** @var array */
   private $items = [];

   /** @var bool */
   private $multiple = false;

   /** @var SelectBox|MultiSelectBox */
   private $selectbox;

   public function __construct(DataGrid $dataGrid, $key, array $items, array $searchScope = null)
   {
      parent::__construct($dataGrid, $key, $searchScope);

      $this->items = $items;
   }

   public function attachToForm(Container $container)
   {
      $container = $container->addContainer($this->getContainerName());

      if($this->isMultiple()) {
         $this->selectbox = $container->addMultiSelect("value", null, $this->items)
            ->setAttribute("title", "vyberte")
            ->setAttribute('data-autosubmit-change', TRUE)
            ->setDefaultValue($this->getDefaultValueForForm())
            ->addClass("input-sm selectpicker");
      } else {
         $this->selectbox = $container->addSelect("value", null, $this->items)
            ->setPrompt("-")
            ->setAttribute('data-autosubmit-change', TRUE)
            ->setDefaultValue($this->getDefaultValueForForm())
            ->addClass("input-sm");
      }

      $this->selectbox->getForm()->onSubmit[] = function () {
         $this->dataGrid->filterValues[$this->getKey()] = $this->selectbox->getValue();
      };

   }

   /**
    * @return bool
    */
   public function isMultiple(): bool
   {
      return $this->multiple;
   }

   /**
    * @param bool $multiple
    * @return FilterSelect
    */
   public function setMultiple(bool $multiple) : FilterSelect
   {
      $this->multiple = $multiple;

      return $this;
   }

   public function getValue()
   {
      if($this->selectbox->getValue() === []) {
         return null;
      }

      return $this->selectbox->getValue();
   }

   public function reset()
   {
      $this->selectbox->setValue($this->getDefaultValue());
   }
}