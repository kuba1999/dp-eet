<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;



use App\Components\DataGrid\DataGrid;
use Kdyby\Translation\Translator;
use Nette\Forms\Container;

abstract class Filter
{
   /** @var DataGrid */
   protected $dataGrid;

   /** @var String */
   protected $key;

   /** @var array */
   protected $searchScope;

   /** @var string|array */
   protected $defaultValue = null;

   /** @var Translator */
   private $translator;

   public function __construct(DataGrid $dataGrid, String $key, array $searchScope = null)
   {
      $this->dataGrid = $dataGrid;
      $this->key = $key;
      $this->searchScope = $searchScope;

      if(is_null($this->searchScope)) {
         $this->searchScope = [$this->key];
      }
   }

   abstract public function attachToForm(Container $container);

   abstract public function getValue();
   abstract public function reset();


   public function isActive($byUser = false) : bool
   {
      if($byUser) {
         return $this->getValue() !== $this->getDefaultValue();
      }

      return !is_null($this->getValue());
   }

   public function getTemplate() : String
   {
      return "filters/" . $this->template . ".latte";
   }

   /**
    * Name of the form container
    * @return String
    */
   public function getContainerName() : String
   {
      return "filter_" . str_replace(".", "_", $this->key);
   }

   /**
    * @return String
    */
   public function getKey(): String
   {
      return $this->key;
   }

   /**
    * @param String $key
    */
   public function setKey(String $key)
   {
      $this->key = $key;
   }

   /**
    * @return array
    */
   public function getSearchScope(): array
   {
      return $this->searchScope;
   }

   /**
    * @param array $searchScope
    */
   public function setSearchScope(array $searchScope)
   {
      $this->searchScope = $searchScope;
   }

   /**
    * @return array|string
    */
   public function getDefaultValueForForm()
   {
      if($this->getValueFromURI()) {
         return $this->getValueFromURI();
      }

      return $this->defaultValue;
   }

   /**
    * @return array|string
    */
   public function getDefaultValue()
   {
      return $this->defaultValue;
   }

   /**
    * @param array|string $defaultValue
    */
   public function setDefaultValue($defaultValue)
   {
      $this->defaultValue = $defaultValue;
   }

   /**
    * @return string|null
    */
   public function getValueFromURI()
   {
      if(isset($this->dataGrid->filterValues[$this->getKey()])) {
         return $this->dataGrid->filterValues[$this->getKey()];
      }

      return null;
   }

   /**
    * @return Translator
    */
   public function getTranslator(): Translator
   {
      return $this->translator;
   }

   /**
    * @param Translator $translator
    */
   public function setTranslator(Translator $translator)
   {
      $this->translator = $translator;
   }

   /**
    * @return bool
    */
   public function isVirtualColumn(): bool
   {
      return $this->dataGrid->getColumn($this->key)->isVirtualColumn();
   }
}