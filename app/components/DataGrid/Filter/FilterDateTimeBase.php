<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Filter;


use Nette\DateTime;
use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;

abstract class FilterDateTimeBase extends Filter
{
   protected $template = "filterDateTime";

   protected $dateFormat = "j.n.Y";
   protected $dateFormatJS = "dd.mm.yyyy";

   protected $timeFormat = "H:i";
   protected $timeFormatJS = "H:i";

   /** @var TextInput */
   private $input;

   public function attachToForm(Container $container)
   {
      $container = $container->addContainer($this->getContainerName());

      $this->input = $container->addText("value")
         ->setAttribute('data-autosubmit', TRUE)
         ->addClass("input-sm");
   }

   /**
    * @return null|DateTime
    */
   public function getValue()
   {
      if(empty($this->input->getValue())) {
         return null;
      }

      return new DateTime($this->input->getValue());
   }

   public function reset()
   {
      $this->input->setValue($this->getDefaultValue());
   }

   /**
    * @return string
    */
   public function getDateFormat(): string
   {
      return $this->dateFormat;
   }

   /**
    * @param string $dateFormat
    */
   public function setDateFormat(string $dateFormat)
   {
      $this->dateFormat = $dateFormat;
   }

   /**
    * @return string
    */
   public function getDateFormatJS(): string
   {
      return $this->dateFormatJS;
   }

   /**
    * @param string $dateFormatJS
    */
   public function setDateFormatJS(string $dateFormatJS)
   {
      $this->dateFormatJS = $dateFormatJS;
   }

   /**
    * @return string
    */
   public function getTimeFormat(): string
   {
      return $this->timeFormat;
   }

   /**
    * @param string $timeFormat
    */
   public function setTimeFormat(string $timeFormat)
   {
      $this->timeFormat = $timeFormat;
   }

   /**
    * @return string
    */
   public function getTimeFormatJS(): string
   {
      return $this->timeFormatJS;
   }

   /**
    * @param string $timeFormatJS
    */
   public function setTimeFormatJS(string $timeFormatJS)
   {
      $this->timeFormatJS = $timeFormatJS;
   }
}