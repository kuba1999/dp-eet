<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Action;


use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Row;
use Nette\SmartObject;
use Nette\Utils\Html;

class RowAction
{
   use SmartObject;

   /** @var DataGrid */
   protected $dataGrid;

   /** @var String */
   protected $key;

   /** @var String */
   protected $title;

   /** @var string */
   protected $link;

   /** @var string|callable */
   protected $class;

   /** @var array */
   protected $params;

   /** @var callable */
   public $onClick = [];

   /** @var bool */
   private $ajax = false;

   /** @var array|null */
   private $confirm;

   /** @var string */
   private $icon;

   function __construct(DataGrid $dataGrid, string $key, string $title, string $link = null, array $params = [])
   {
      $this->dataGrid = $dataGrid;
      $this->key = $key;
      $this->title = $title;
      $this->link = $link;
      $this->setParams($params);

      //if params is null add primary key
      if(!count($this->params)) {
         $this->params[$this->dataGrid->getPrimaryKey()] = null;
      }
   }

   public function render(Row $row)
   {
      $link = Html::el('a');

      if($this->icon) {
         $link->addHtml(
            Html::el("span")
               ->setClass($this->icon)
         );
      }

      $class = is_callable($this->getClass()) ? call_user_func($this->getClass(), $row) : $this->getClass();

      $link->addText(" " . $this->getTitle())
            ->setClass($class . ($this->isAjax() ? " ajax":""));


      if($this->getLink()) {
         $link->setHref($this->dataGrid->getPresenter()->link($this->getLink(), $this->getFilledParameters($row)));
      }
      else if(!is_null($this->onClick)) {
         $link->setHref($this->dataGrid->link("columnRowAction", [$this->getKey()] + $this->getFilledParameters($row)));
      }

      if($this->confirm) {
         $link->data("confirm", sprintf($this->confirm[0], $row->getValue($this->confirm[1])));
      }

      return $link;
   }

   private function getFilledParameters(Row $row)
   {
      $params = [];
      foreach ($this->getParams() as $paramName=>$paramValue) {
         if(is_null($paramValue)) {
            $params[$paramName] = $row->getValue($paramName);
         } else {
            $params[$paramName] = $paramValue;
         }
      }

      return $params;
   }

   /**
    * @return string
    */
   public function getLink(): ?string
   {
      return $this->link;
   }

   /**
    * @param string $link
    */
   public function setHref(string $link)
   {
      $this->link = $link;
   }

   /**
    * @return array
    */
   public function getParams(): array
   {
      return $this->params;
   }

   /**
    * @param array $params
    */
   public function setParams(array $params)
   {
      $newParams = [];
      foreach ($params AS $paramName=>$paramValue) {
         $newParams[is_string($paramName) ? $paramName:$paramValue] = is_string($paramName) ? $paramValue:null;
      }

      $this->params = $newParams;
   }

   /**
    * @return DataGrid
    */
   public function getDataGrid(): DataGrid
   {
      return $this->dataGrid;
   }

   /**
    * @param DataGrid $dataGrid
    */
   public function setDataGrid(DataGrid $dataGrid)
   {
      $this->dataGrid = $dataGrid;
   }

   /**
    * @return String
    */
   public function getKey(): String
   {
      return $this->key;
   }

   /**
    * @param String $key
    * @return $this
    */
   public function setKey(String $key)
   {
      $this->key = $key;

      return $this;
   }

   /**
    * @return String
    */
   public function getTitle(): String
   {
      return $this->title;
   }

   /**
    * @param String $title
    * @return $this
    */
   public function setTitle(String $title)
   {
      $this->title = $title;

      return $this;
   }

   /**
    * @return string|callable
    */
   public function getClass()
   {
      return $this->class;
   }

   /**
    * @param string|callable $class
    * @return $this
    */
   public function setClass($class)
   {
      $this->class = $class;

      return $this;
   }

   /**
    * @return bool
    */
   public function isAjax(): bool
   {
      return $this->ajax;
   }

   /**
    * @param bool $ajax
    * @return $this
    */
   public function setAjax(bool $ajax)
   {
      $this->ajax = $ajax;

      return $this;
   }

   /**
    * @return array
    */
   public function getConfirm(): ?array
   {
      return $this->confirm;
   }

   /**
    * @param string $message
    * @param string|null $columnKey
    * @return $this
    */
   public function setConfirm(string $message, string $columnKey = null)
   {
      $this->confirm = [$message, $columnKey];

      return $this;
   }

   /**
    * @return string
    */
   public function getIcon(): string
   {
      return $this->icon;
   }

   /**
    * @param string $icon
    * @return $this
    */
   public function setIcon(string $icon)
   {
      $this->icon = $icon;

      return $this;
   }
}