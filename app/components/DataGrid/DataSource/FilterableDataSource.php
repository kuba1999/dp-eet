<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\DataSource;


use App\Components\DataGrid\Filter\FilterDate;
use App\Components\DataGrid\Filter\FilterDateTime;
use App\Components\DataGrid\Filter\FilterNumber;
use App\Components\DataGrid\Filter\FilterSelect;
use App\Components\DataGrid\Filter\FilterText;
use App\Components\DataGrid\Utils\Sorting;

abstract class FilterableDataSource
{
   public function filter(array $filters) : FilterableDataSource
   {
      foreach ($filters AS $filter) {
         if($filter instanceof FilterText) {
            $this->filterText($filter);
         }
         else if($filter instanceof FilterSelect) {
            $this->filterSelect($filter);
         }
         else if($filter instanceof FilterDate) {
            $this->filterDate($filter);
         }
         else if($filter instanceof FilterNumber) {
            $this->filterNumber($filter);
         }
      }

      return $this;
   }

   public abstract function sort(Sorting $sorting);
   public abstract function filterText(FilterText $filter);
   public abstract function filterSelect(FilterSelect $filter);
   public abstract function filterDate(FilterDate $filter);
   public abstract function filterNumber(FilterNumber $filter);
}