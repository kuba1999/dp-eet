<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\DataSource;



use App\Components\DataGrid\DataGrid;
use App\Components\DataGrid\Filter\FilterDate;
use App\Components\DataGrid\Filter\FilterDateTime;
use App\Components\DataGrid\Filter\FilterNumber;
use App\Components\DataGrid\Filter\FilterSelect;
use App\Components\DataGrid\Filter\FilterText;
use App\Components\DataGrid\Utils\Sorting;
use Kdyby\Doctrine\QueryBuilder;
use Nette\Utils\Strings;

class DoctrineDataSource extends FilterableDataSource implements IDataSource
{
   /** @var DataGrid */
   private $dataGrid;

   /** @var QueryBuilder */
   private $dataSource;

   /** @var String */
   private $rootAlias;

   /** @var string */
   private $primaryKey;

   function __construct(DataGrid $dataGrid, QueryBuilder $dataSrouce, $primaryKey)
   {
      $this->dataGrid = $dataGrid;
      $this->dataSource = $dataSrouce;
      $this->primaryKey = $primaryKey;
   }

   public function getData() : QueryBuilder
   {
      return $this->dataSource;
   }

   /**
    * @param string $primaryKey
    * @param string $value
    * @return mixed
    */
   public function getItem(string $primaryKey, string $value)
   {
      $dataSource = clone $this->dataSource;

      return $dataSource
         ->andwhere($this->checkAliases($primaryKey) . " = :id")
         ->setParameter("id", $value)
         ->getQuery()
         ->getOneOrNullResult();
   }

   /**
   * @param  string  $column
   * @return string
   */
   private function checkAliases($column)
   {
      if (Strings::contains($column, ".")) {
         return $column;
      }

      if (!isset($this->rootAlias)) {
        $rootAliases = $this->dataSource->getRootAliases();
        $this->rootAlias = current($rootAliases);
      }

      return $this->rootAlias.'.'.$column;
   }

   /**
    * @param Sorting $sorting
    */
   public function sort(Sorting $sorting)
   {
      if(!empty($sorting)) {
         foreach ($sorting->getSort() AS $column=>$order) {
            $gridColumn = $this->dataGrid->getColumn($column);

            if($gridColumn && $gridColumn->isVirtualColumn()) {
               $this->dataSource->orderBy($column, $order);
            } else {
               $this->dataSource->orderBy($this->checkAliases($column), $order);
            }
         }
      }
   }

   /**
    * @param FilterText $filter
    */
   public function filterText(FilterText $filter)
   {
      if(!$filter->isActive()) {
         return;
      }

      if($filter->isVirtualColumn()) {
         $column = $filter->getKey();
      } else {
         $column = $this->checkAliases($filter->getKey());
      }

      if($filter->isExact()) {
         $words = [$filter->getValue()];
      } else {
         $words = explode(' ', $filter->getValue());
      }

      $exprs = [];
      foreach ($words AS $word) {
         $exprs[] = $this->dataSource->expr()->like($column, $this->dataSource->expr()->literal("%$word%"));
      }

      $or = call_user_func_array([$this->dataSource->expr(), 'orX'], $exprs);

      if($filter->isVirtualColumn()) {
         $this->dataSource->andHaving($or);
      } else {
         $this->dataSource->andWhere($or);
      }
   }

   /**
    * @param FilterNumber $filter
    */
   public function filterNumber(FilterNumber $filter)
   {
      if(!$filter->isActive()) {
         return;
      }

      if($filter->isVirtualColumn()) {
         $column = $filter->getKey();
         $this->dataSource->andHaving(
            $this->dataSource->expr()->eq($column, $this->dataSource->expr()->literal($filter->getValue()))
         );
      } else {
         $column = $this->checkAliases($filter->getKey());
         $this->dataSource->andWhere(
            $this->dataSource->expr()->eq($column, $this->dataSource->expr()->literal($filter->getValue()))
         );
      }
   }

   /**
    * @param FilterDate $filter
    */
   public function filterDate(FilterDate $filter)
   {
      if(!$filter->isActive()) {
         return;
      }

      $or = $this->dataSource->expr()->eq(
         "DATE(" . $this->checkAliases($filter->getKey()) . ")",
         "DATE(" . $this->dataSource->expr()->literal($filter->getValue()) . ")"
      );

      $this->dataSource->andWhere($or);
   }


   public function filterSelect(FilterSelect $filter)
   {
      if(!$filter->isActive()) {
         return;
      }

      if(is_null($filter->getSearchScope()) || count($filter->getSearchScope()) === 0) {
         $filter->setSearchScope([$filter->getKey()]);
      }

      if($filter->isMultiple()) {
         $values = $filter->getValue();
      } else {
         $values = [$filter->getValue()];
      }


      $exprs = [];
      foreach ($values AS $value) {
         foreach ($filter->getSearchScope() AS $scope) {
            $exprs[] = $this->dataSource->expr()->eq($this->checkAliases($scope), $this->dataSource->expr()->literal($value));
         }
      }

      $or = call_user_func_array([$this->dataSource->expr(), 'orX'], $exprs);

      $this->dataSource->andWhere($or);
   }

   public function count()
   {
      $qb = $this->dataSource;

      $qb = new \Doctrine\ORM\Tools\Pagination\Paginator(
         $qb->getQuery(),
         false
      );

      return  $qb->count();
   }

   /*public function count()
   {
      $dataSource = clone $this->dataSource;
      //$dataSource->select($dataSource->expr()->count( $this->checkAliases($this->primaryKey) ));
      $dql =  $this->dataSource->getQuery()->getDQL();

      $dataSource->addSelect($dataSource->expr()->count( $this->checkAliases($this->primaryKey) ))
         ->from("(" . $dql . ")", "b");

      echo ($dataSource->getQuery()->getSQL());exit;
      exit;
      $dataSource = new \Doctrine\ORM\Tools\Pagination\Paginator(
         $dataSource->getQuery(),
         false
      );


      return  0;
   }*/

   public function limit(int $itemsPerPage, int $offset)
   {
      $this->dataSource->setFirstResult($offset)->setMaxResults($itemsPerPage);
   }
}