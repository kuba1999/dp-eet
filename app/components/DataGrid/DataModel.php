<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid;


use App\Components\DataGrid\DataSource\DoctrineDataSource;
use App\Components\DataGrid\DataSource\IDataSource;
use App\Components\DataGrid\Utils\Sorting;
use Nette\Utils\Paginator;

class DataModel
{
   /** @var IDataSource */
   private $dataSource;

   public function __construct(DataGrid $dataGrid, $source, $primaryKey)
   {
      if($source instanceof \Kdyby\Doctrine\QueryBuilder) {
         $this->dataSource = new DoctrineDataSource($dataGrid, $source, $primaryKey);
      }
   }

   public function filterData(Sorting $sorting, array $filters, Paginator $paginator, int $itemsPerPage = null)
   {

      $this->dataSource->filter($filters);
      $this->dataSource->sort($sorting);

      $paginator->itemCount = $this->dataSource->count();

      if($itemsPerPage) {
         $paginator->setItemsPerPage($itemsPerPage);
      } else {
         $paginator->setItemsPerPage($paginator->itemCount);
      }

      $this->dataSource->limit($paginator->itemsPerPage, $paginator->getOffset());

      $results = new \Doctrine\ORM\Tools\Pagination\Paginator(
         $this->dataSource->getData()->getQuery(),
         false
      );

      return $results;
   }

   public function getItem(string $primaryKey, string $value)
   {
      return $this->dataSource->getItem($primaryKey, $value);
   }
}