<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid;


use App\Components\DataGrid\Utils\PropertyAccessHelper;

class Row
{
   /** @var DataGrid */
   private $dataGrid;


   /** @var mixed */
   private $item;

   /** @var array */
   private $values = [];


   function __construct(DataGrid $dataGrid, $item)
   {
      $this->dataGrid = $dataGrid;

      if(is_array($item)) {
         $this->item = $item[0];

         $this->values = $item;
         unset($this->values[0]);
      }
      else {
         $this->item = $item;
      }
   }

   /**
    * @param String $key Column key
    * @return mixed
    */
   public function getValue(String $key)
   {
      if(array_key_exists($key, $this->values)) {
         return $this->values[$key];
      }

      //doctrine entity
      return $this->getDoctrineEntityProperty($key);
   }

   protected function getDoctrineEntityProperty(String $key)
   {
      $properties = explode('.', $key);

      $value = $this->item;
      while ($property = array_shift($properties)) {
         if(!is_object($value) && !$value) {
            return $value;
         }

         $value = PropertyAccessHelper::getAccessor()->getValue($value, $property);
      }

      return $value;
   }

   public function getId()
   {
      return $this->getValue($this->dataGrid->getPrimaryKey());
   }

   public function getItem()
   {
      return $this->item;
   }
}