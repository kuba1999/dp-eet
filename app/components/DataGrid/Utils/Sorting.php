<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Utils;


class Sorting
{
   const SORT_ASC = "asc";
   const SORT_DESC = "desc";

   private $sort = []; // column_name => sort_type

   function __construct(array $sorting)
   {
      $this->sort = $sorting;
   }

   public function getSort()
   {
      return $this->sort;
   }
}