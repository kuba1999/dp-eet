<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\DataGrid\Utils;



use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class PropertyAccessHelper
{
   private static $accessor = NULL;

   public static function getAccessor() : PropertyAccessor
   {
      if(!self::$accessor) {
         self::$accessor = PropertyAccess::createPropertyAccessor();
      }

      return self::$accessor;
   }
}