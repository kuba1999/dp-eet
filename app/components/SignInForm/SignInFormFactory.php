<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\SignInForm;


use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Security\User;

class SignInFormFactory
{
   /** @var EntityManager */
   private $em;

   /** @var User */
   private $user;

   function __construct(EntityManager $em, User $user)
   {
      $this->em = $em;
      $this->user = $user;
   }

   /**
    * @return SignInForm
    */
   public function create() : SignInForm
   {
      return new SignInForm($this->em, $this->user);
   }
}