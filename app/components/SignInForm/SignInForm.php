<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Components\SignInForm;


use App\Forms\BaseForm;
use App\Model\Exceptions\BadCredentialsException;
use App\Model\Exceptions\InactiveAccountException;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;

class SignInForm extends Control
{
   /** @var EntityManager */
   private $em;

   /** @var \Nette\Security\User */
   private $user;

   public $onSuccess = [];

   public $onError = [];

   public function __construct(EntityManager $em, \Nette\Security\User $user)
   {
      $this->em = $em;
      $this->user = $user;
   }

   public function createComponentForm() : BaseForm
   {
      $form = new BaseForm();
      $form->setShowFlashErrorMessage(false);


      $form->addText("email")
         ->setAttribute("placeholder", "uživatelské jméno")
         ->setRequired(true, "Uživatelské jméno je povinné");

      $form->addPassword("password")
         ->setAttribute("placeholder", "heslo")
         ->setRequired(true, "Heslo je povinné");

      $form->addCheckbox("rememberMe", "pamatuj si mne");

      $form->addSubmit("signIn", "Přihlásit se")
         ->addClass("btn btn-primary");

      $form->onSuccess[] = [$this, "formSuccess"];

      return $form;
   }

   public function formSuccess(BaseForm $form)
   {
      try{
         $this->user->login(
            $form['email']->getValue(),
            $form['password']->getValue()
         );

         //remember me?
         if($form['rememberMe']->getValue()) {
            $this->user->setExpiration("+1 month");
         }

      } catch (BadCredentialsException $e) {
         $form->addError("Špatné přihlašovací údaje");
         $this->onError();
         return;
      } catch (InactiveAccountException $e) {
         $form->addError("Účet není aktivní");
         $this->onError();
         return;
      }

      $this->onSuccess();
   }

   public function render()
   {
      $tpl = $this->template;
      $tpl->setFile(__DIR__ . "/template.latte");

      return $tpl->render();
   }
}