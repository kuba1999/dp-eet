<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Forms\Controls\ColorPicker;


use App\Forms\BaseForm;
use Nette\Forms\Controls\TextInput;

class ColorPicker extends TextInput
{
   const BASE_COLORS = [
      "#7bd148" => "Green",
      "#5484ed" => "Bold blue",
      "#a4bdfc" => "Blue",
      "#46d6db" => "Turquoise",
      "#7ae7bf" => "Light green",
      "#51b749" => "Bold green",
      "#fbd75b" => "Yellow",
      "#ffb878" => "Orange",
      "#ff887c" => "Red",
      "#dc2127" => "Bold red",
      "#dbadff" => "Purple",
      "#e1e1e1" => "Gray"
   ];

   const EXTENDED_COLORS = [
      null      => "",
      "#ac725e" => "#ac725e",
      "#d06b64" => "#d06b64",
      "#f83a22" => "#f83a22",
      "#fa573c" => "#fa573c",
      "#ff7537" => "#ff7537",
      "#ffad46" => "#ffad46",
      "#42d692" => "#42d692",
      "#16a765" => "#16a765",
      "#7bd148" => "#7bd148",
      "#b3dc6c" => "#b3dc6c",
      "#fbe983" => "#fbe983",
      "#fad165" => "#fad165",
      "#92e1c0" => "#92e1c0",
      "#9fe1e7" => "#9fe1e7",
      "#9fc6e7" => "#9fc6e7",
      "#4986e7" => "#4986e7",
      "#9a9cff" => "#9a9cff",
      "#b99aff" => "#b99aff",
      "#c2c2c2" => "#c2c2c2",
      "#cabdbf" => "#cabdbf",
      "#cca6ac" => "#cca6ac",
      "#f691b2" => "#f691b2",
      "#cd74e6" => "#cd74e6",
      "#a47ae2" => "#a47ae2"
   ];

   private $items;

   public static function register()
   {
      BaseForm::extensionMethod('addColorPicker', [__CLASS__, 'addColorPicker']);
      \Nette\Forms\Container::extensionMethod('addColorPicker', [__CLASS__, 'addColorPicker']);
   }

   public function __construct(string $label, array $colors)
   {
      parent::__construct($label);

      $this->items = $colors;
   }

   /**
    * @param BaseForm $container
    * @param string $name
    * @param string|NULL $label
    * @param array|null $colors
    * @return ColorPicker
    */
   public static function addColorPicker(BaseForm $container, string $name, string $label = NULL, array $colors = self::BASE_COLORS)
   {
      $control = new ColorPicker($label, $colors);

      return $container[$name] = $control;
   }

   public function validate()
   {
      parent::validate();

      if(!empty($this->getValue()) && !preg_match("/#[0-9a-fA-F]{6}/i", $this->getValue())) {
         $this->addError("Zadejte validní kód barvy");
      }
   }

   public function getControl()
   {
      $control = parent::getControl();

      $ret = '<div class="input-group" id="'. $this->getHtmlId() .'_selector">
                  <span class="input-group-addon" id="'. $this->getHtmlId() .'_selectedColor" style="padding-left: 50px;">
                    <span></span>
                  </span>
                  
                  '. $control .'

                  <span class="input-group-btn">
                     <a class="btn btn-primary" id="'. $this->getHtmlId() .'_deleteColor" href="javascript:;">
                       vymazat
                     </a>
                  </span>
             </div>';


      return $ret . $this->getJavaScript();
   }

   public function getJavaScript()
   {
      $javascript = "  <script>
        $(function() {
           $('#".$this->getHtmlId()."').colorpicker({
              format: 'hex'
           });
           
           $('#".$this->getHtmlId()."').on('changeColor', function(){
               $('#".$this->getHtmlId()."_selectedColor').css('background-color', $('#".$this->getHtmlId()."').val());
           });
           
           $('#".$this->getHtmlId()."_selectedColor').css('background-color', $('#".$this->getHtmlId()."').val());
           
           $('#".$this->getHtmlId()."_deleteColor').click(function(){
               $('#".$this->getHtmlId()."').val('');
               $('#".$this->getHtmlId()."_selectedColor').css('background-color', '');
           });
        });
      </script>";

      return $javascript;
   }

}