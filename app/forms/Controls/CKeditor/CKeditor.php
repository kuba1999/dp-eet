<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Forms\Controls\CKeditor;


use App\Forms\BaseForm;
use Nette\Forms\Controls\TextArea;

class CKeditor extends TextArea
{
   const VIEW_BASIC  =  "basic";
   const VIEW_FULL   =  "full";

   private $view = self::VIEW_BASIC;

   public static function register()
   {
      BaseForm::extensionMethod('addEditor', [__CLASS__, 'addEditor']);
      \Nette\Forms\Container::extensionMethod('addEditor', [__CLASS__, 'addEditor']);
   }

   public function __construct($label = NULL)
   {
      parent::__construct($label);
   }

   public static function addEditor(\Nette\Application\UI\Form $container, $name, $label = NULL)
   {
      $control = new CKeditor($label);

      return $container[$name] = $control;
   }

   /**
    * Generates control's HTML element
    *
    * @return \Nette\Utils\Html
    */
   public function getControl()
   {
       $control = parent::getControl();

       $template = new \Latte\Engine;

       $html = $template->renderToString(
          __DIR__."/template.latte",
          [
             "controlRender"  => $control,
             "control"        => $this,
             "view"           => $this->view
          ]
       );

       return \Nette\Utils\Html::el()->setHtml($html);
   }

   /**
    * @return string
    */
   public function getView(): string
   {
      return $this->view;
   }

   /**
    * @param string $view
    * @return CKeditor
    */
   public function setView(string $view) : self
   {
      $this->view = $view;

      return $this;
   }
}