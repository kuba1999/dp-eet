<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Forms\Controls;

use App\Forms\BaseForm;
use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;

/**
 * Class Price
 * @package App\Forms\Controls
 *
 * Text input for price
 */
class PriceInput extends TextInput
{
   public static function register()
   {
      BaseForm::extensionMethod('addPrice', [__CLASS__, 'addPrice']);
      \Nette\Forms\Container::extensionMethod('addPrice', [__CLASS__, 'addPrice']);
   }

   /**
    * @param BaseForm $container
    * @param string $name
    * @param string|NULL $label
    * @return TextInput
    */
   public static function addPrice(Container $container, string $name, string $label = NULL) : TextInput
   {
      $control = new TextInput($label);

      $control->addRule(BaseForm::FLOAT);

      return $container[$name] = $control;
   }
}