<?php
/**
 * Copyright (c) 2018 Jakub Svestka VUT FIT - diplomova prace
 */


namespace App\Forms\Controls\DatePicker;


use App\Forms\BaseForm;
use Nette\Forms\Controls\TextInput;
use Nette\Utils\DateTime;

class DatePicker extends TextInput
{
   protected $dateFormat = "j. n. Y";
   protected $dateFormatJS = "dd.mm.yyyy";

   public static function register()
   {
      BaseForm::extensionMethod('addDatePicker', [__CLASS__, 'addDatePicker']);
      \Nette\Forms\Container::extensionMethod('addDatePicker', [__CLASS__, 'addDatePicker']);
   }

   public function __construct($label = NULL)
   {
      parent::__construct($label);
   }

   public static function addDatePicker(\Nette\Application\UI\Form $container, $name, $label = NULL)
   {
      $control = new DatePicker($label);

      return $container[$name] = $control;
   }

   /**
    * Generates control's HTML element
    *
    * @return \Nette\Utils\Html
    */
   public function getControl()
   {
       $control = parent::getControl();

       $template = new \Latte\Engine;

       $html = $template->renderToString(
          __DIR__."/template.latte",
          [
             "input"       => $control,
             "datePicker"  => $this
          ]
       );

       return \Nette\Utils\Html::el()->setHtml($html);
   }

   /**
    * @return \DateTime|null
    */
   public function getValue() : ?\DateTime
   {
      $value = parent::getValue();
      $value = str_replace(" ", "", $value);

      return (is_null($value) || empty($value)) ? null : new DateTime($value);
   }


   public function setValue($value)
   {
      if ($value instanceof \DateTime) {
         $value = $value->format($this->getDateFormat());
      }

      return parent::setValue($value);
   }

   /**
    * @return string
    */
   public function getDateFormat(): string
   {
      return $this->dateFormat;
   }

   /**
    * @param string $dateFormat
    */
   public function setDateFormat(string $dateFormat)
   {
      $this->dateFormat = $dateFormat;
   }

   /**
    * @return string
    */
   public function getDateFormatJS(): string
   {
      return $this->dateFormatJS;
   }

   /**
    * @param string $dateFormatJS
    */
   public function setDateFormatJS(string $dateFormatJS)
   {
      $this->dateFormatJS = $dateFormatJS;
   }
}